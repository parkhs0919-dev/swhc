## 헬스케어
# README #

# 소스폴더
- /routes/ 라우팅
- /server/api api함수
- /server/queries/ 쿼리
- /server/config 설정파일
- /view/ html

# api 문서 
- http://re.hpitech.com:5000/

# 문서 파일
- /doc/db/ db
- /doc/aws/ 서버
- /doc/docker/ docker문서

# 서버(aws)
- 운영서버1
- 운영서버2
- 테스트서버
- elb(로드밸런서) 
- Aurora MySQL
# 사용 언어
- MySQL
- nodejs  v14.16.0
- react ^17.0.1


# test 실행
- NODE_ENV=development node app.js
- NODE_ENV=development  PORT=3000 pm2 start app.js --log-date-format="YYYY-MM-DD HH:mm:ss" --name app --watch --max-memory-restart 600M    --node-args="--max_old_space_size=512"