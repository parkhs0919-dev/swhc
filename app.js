const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const app = express();
const http = require('http').createServer(app);

global.RedisClientWrite = null;
global.RedisClientRead = null;
global.SERVER_ENV = app.get('env') || 'development'; //'production'
global.CONFIG = require('./server/config');
global.Mysql = require('./server/utils/MysqlWrapper');
global.Utils = require('./server/utils');
global.Queries = require('./server/queries');
global.Api = require('./server/api');
global.Metadata = require('./server/metadata');

const cron = require('./server/cron');
const initialize = require('./server/utils/initialize');
const clientIp = require('./server/middleware/client-ip');
const accessControl = require('./server/middleware/access-control');
const apiPath = require('./server/middleware/api');
const metaData = require('./server/middleware/metadata');
const timeZone = require('./server/middleware/time-zone');
const response = require('./server/middleware/response');

const LISTEN_PORT = (process.env.PORT) ? process.env.PORT : CONFIG.common.PORT;

app.set('views', path.join(__dirname, './server/views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, './')));

clientIp(app); 
accessControl(app);
metaData(app);  
timeZone(app);
response(app);
apiPath(app);

// catch 404 and forward to error handler
app.use( (req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send({ msg: err.message })
  //console.error(err.message)
});

cron();
initialize.initDirectories();

http.listen(LISTEN_PORT, () => {
  console.log('server listenling on port ' + http.address().port + ' start');
});

