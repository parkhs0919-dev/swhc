
module.exports= {
    ko:{
        codes : require('./codes/ko.json'),
        langs :require('./langs/ko.json')
    },
    en:{
        codes : require('./codes/en.json'),
        langs :require('./langs/en.json')
    }
};                                         