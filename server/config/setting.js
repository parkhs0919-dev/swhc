module.exports =
{
    temperature : 
    [
          {
            "temperature_cd": "T1",
            "temperature_cd_nm": "정상",
            "min": "34",
            "max": "37.0",
          },
          {
            "temperature_cd": "T2",
            "temperature_cd_nm": "주의",
            "min": "37.0",
            "max": "37.2",
          },
          {
            "temperature_cd": "T3",
            "temperature_cd_nm": "경계",
            "min": "37.3",
            "max": "37.4",
          },
          {
            "temperature_cd": "T4",
            "temperature_cd_nm": "심각",
            "min": "37.5",
            "max": "100",
          },
          {
            "temperature_cd": "T5",
            "temperature_cd_nm": "미착용",
            "min": "1",
            "max": "34.0",
          },  
          {
            "temperature_cd": "T0",
            "temperature_cd_nm": "미착용",
            "min": "0",
            "max": "0",
          }                                 
    ],
    device_connect_st : [
      {code : "ALL" , value : "전체"},
      {code : "1" , value : "정상"},
      {code : "0" , value : "비활성"}, 
    ]  
}
