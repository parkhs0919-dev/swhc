module.exports =
{
  "development": {
    "allowOrigin": [
    "http://localhost"

    ]
    , "allowMethods": "GET, PUT, POST, DELETE, HEAD"
    , "allowHeaders": "Content-Type, Content-Range, Content-Disposition"
  },
  "local": {
    "allowOrigin": [
        "http://localhost"
        ]
        , "allowMethods": "GET, PUT, POST, DELETE, HEAD"
        , "allowHeaders": "Content-Type, Content-Range, Content-Disposition"
  },
  "production": {
    "allowOrigin": [
        "http://localhost"
        ]
        , "allowMethods": "GET, PUT, POST, DELETE, HEAD"
        , "allowHeaders": "Content-Type, Content-Range, Content-Disposition"
  }
}



