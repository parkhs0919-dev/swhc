module.exports = {
  "development": {
    "write": 
      { "host": "smarthealthcare.emzpox.ng.0001.apn2.cache.amazonaws.com", port: 6379 }
    ,
    "read": 
      { "host": "smarthealthcare-ro.emzpox.ng.0001.apn2.cache.amazonaws.com", port: 6379 }
    ,
  },
  "local": {
    "host": "127.0.0.1",
    "port": 6379,
  },
  "production":{
    "write": 
      { "host": "folifelabs-smarthealthcare.emzpox.ng.0001.apn2.cache.amazonaws.com", port: 6379 }
    ,
    "read": 
      { "host": "folifelabs-smarthealthcare-ro.emzpox.ng.0001.apn2.cache.amazonaws.com", port: 6379 }
    ,
  }
}
