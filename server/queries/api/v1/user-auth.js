const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserAuth: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_auth` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`birth` ');        
        query.push(' ,`name` ');        
        query.push(' ,`phone` ');        
        query.push(' ,`imp_uid` ');        
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');                                    
        query.push(' )');   
        
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_id));
        query.push(util.format(' ,"%s" ', params.birth));
        query.push(util.format(' ,"%s" ', params.name));
        query.push(util.format(' ,"%s" ', params.phone));
        query.push(util.format(' ,"%s" ', params.imp_uid));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');        
        query.push(' )');       
        
        return query.join('');          
    }                                                  
};           


