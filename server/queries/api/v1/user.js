const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUser: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user` ');
        query.push(' ( ');
        query.push(' `user_auth_id` ');
        query.push(' ,`user_auth_pw` ');        
        if(params.user_email) query.push(' ,`user_email` ');
        if(params.user_nm) query.push(' ,`user_nm` ');
        if(params.user_tel) query.push(' ,`user_tel` ');
        if(params.user_birth) query.push(' ,`user_birth` ');
        if(params.gender) query.push(' ,`gender` ');
        if(params.foreigner_yn) query.push(' ,`foreigner_yn` ');
        if(params.child_yn) query.push(' ,`child_yn` ');
        if(params.user_st) query.push(' ,`user_st` ');
        query.push(' ,`created_dt` ');            
        query.push(' ,`updated_dt` ');                                    
        query.push(' )');   
        
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_auth_id));
        query.push(util.format(' ,HEX(AES_ENCRYPT("%s", "%s")) ', params.user_auth_pw, auth.crypto_key));  
        if(params.user_email) query.push(util.format(' ,"%s" ', params.user_email));
        if(params.user_nm) query.push(util.format(' ,"%s" ', params.user_nm));
        if(params.user_tel) query.push(util.format(' ,"%s" ', params.user_tel));
        if(params.user_birth) query.push(util.format(' ,"%s" ', params.user_birth));
        if(params.gender) query.push(util.format(' ,"%s" ', params.gender));
        if(params.foreigner_yn) query.push(util.format(' ,"%s" ', params.foreigner_yn));
        if(params.child_yn) query.push(util.format(' ,"%s" ', params.child_yn));
        if(params.user_st) query.push(util.format(' ,"%s" ', params.user_st));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');        
        query.push(' )');       
        
        return query.join('');          
    },  
    getUserDuplicateCheck: (params) =>  {
        let query = [];   

        query.push('  SELECT count(*) as cnt ');
        query.push('  from tb_user as AA ');
        query.push('  where 1=1 ');
        query.push(util.format(' AND AA.user_auth_id="%s" ', params.user_auth_id));

        return query.join('');          
    },
    getUserDuplicateCheckEmail: (params) =>  {
        let query = [];   

        query.push('  SELECT count(*) as cnt ');
        query.push('  from tb_user as AA ');
        query.push('  where 1=1 ');
        query.push(util.format(' AND AA.user_email="%s" ', params.user_email));

        return query.join('');          
    },
    getUserLoginCheck: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.user_id,AA.user_auth_id,AA.user_nm,AA.login_first_yn  ');
        query.push('  FROM `tb_user` AS AA ');
        query.push('  where 1=1  AND AA.del_yn="N"  ');
        query.push(util.format(' AND AA.user_auth_id="%s" ', params.user_auth_id));
        query.push(util.format(' AND AA.user_auth_pw=HEX(AES_ENCRYPT("%s", "%s")) ', params.user_auth_pw, auth.crypto_key));  
        query.push('  AND AA.user_st="1" ');
        return query.join('');          
    }, 
    getUserLoginCheckToken: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.user_id,AA.user_auth_id,AA.user_nm,AA.login_first_yn  ');
        query.push('  FROM `tb_user` AS AA ');
        query.push('  where 1=1  AND AA.del_yn="N"  ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id)); 
        query.push('  AND AA.user_st="1" ');
        return query.join('');          
    }, 
    getUserFindId: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_user` AS AA ');
        query.push('  where 1=1  AND AA.del_yn="N" AND AA.user_st="1" ');
        query.push(util.format(' AND AA.user_email="%s" ', params.user_email));
        return query.join('');          
    }, 
    getUserFindIdV2: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_user` AS AA ');
        query.push('  where 1=1  AND AA.del_yn="N" AND AA.user_st="1" ');
       
        query.push(util.format(' AND AA.user_nm="%s" ', params.user_nm));  
        query.push(util.format(' AND AA.user_tel="%s" ', params.user_tel));  
        return query.join('');          
    }, 
    getUserFindPw: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_user` AS AA ');
        query.push('  where 1=1 ');
        query.push(util.format(' AND AA.user_auth_id="%s" ', params.user_auth_id));
        query.push(util.format(' AND AA.user_email="%s" ', params.user_email));
        return query.join('');          
    }, 
    getUserFindPwV2: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_user` AS AA ');
        query.push('  where 1=1 ');
        query.push(util.format(' AND AA.user_tel="%s" ', params.user_tel));
        query.push(util.format(' AND AA.user_auth_id="%s" ', params.user_auth_id));
        return query.join('');          
    }, 

    getUser: (params) =>  {
        let query = [];   
        let ymdhhmi='%Y-%m-%d %H:%i';
        query.push(' SELECT AA.user_id,AA.user_nm,AA.user_email,AA.user_tel,ifnull(BB.user_device_sn,"") as user_device_sn  ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push('  FROM `tb_user` AS AA   ');
        query.push('  left outer join tb_user_device as BB on AA.user_id=BB.user_id  ');
        query.push('  where 1=1  AND AA.del_yn="N" AND AA.user_st="1" ');
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));
        return query.join('');          
    },  
    getUserGroupRels: (params) =>  {
        let query = [];   
        query.push(' SELECT BB.*,CC.group_nm ,DD.organization_nm,DD.organization_id  FROM tb_user as AA  ');
        query.push(' left join tb_user_group_rels as BB  on AA.user_id=BB.user_id  ');
        query.push(' left join tb_group  as CC  on BB.group_id=CC.group_id  ');
        query.push(' left join tb_organization  as DD  on CC.organization_id=DD.organization_id  ');
        query.push(' where 1=1  and BB.apply_cd="A1" and BB.expire_dt>now() AND CC.del_yn="N"  AND AA.del_yn="N" AND AA.user_st="1"  ');
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));
        return query.join('');          
    },   
    getUserGroupRelsList: (params) =>  {
        let query = [];   
        let ymdhh='%y-%m-%d';
        query.push('  SELECT BB.user_group_rels_id,CC.group_nm ,DD.organization_nm,DD.organization_id  ');
        query.push('  ,DD.organization_tel,DD.company_num,DD.addr1,DD.addr2 ,DD.grade  ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(BB.updated_dt, "+00:00", "%s"), "%s") as start_dt_str ',params.z_value , ymdhh ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(BB.expire_dt, "+00:00", "%s"), "%s") as expire_dt_str ',params.z_value , ymdhh ));

        query.push(' ,CASE WHEN DD.type="ST" and DD.grade="G1" and ( EE.grade_cd="G1") THEN "A1" ');
        query.push(' WHEN DD.type="ST" and DD.grade="G2" and ( EE.grade_cd="G1" OR EE.grade_cd="G2") THEN "A1"   ');
        query.push(' WHEN DD.type="ST" and DD.grade="G3" and ( EE.grade_cd="G1" OR EE.grade_cd="G2" OR EE.grade_cd="G3") THEN "A1" ');
        query.push(' WHEN DD.type="LT" THEN BB.apply_cd   ELSE "A3" END as apply_cd ');

        query.push(' FROM tb_user as AA ');
        query.push(' left join tb_user_group_rels as BB  on AA.user_id=BB.user_id  ');
        query.push(' left join tb_group  as CC  on BB.group_id=CC.group_id ');
        query.push(' left join tb_organization  as DD  on CC.organization_id=DD.organization_id  ');
        query.push(' left join tb_health  as EE  on AA.user_id=EE.user_id  ');
        query.push(' where 1=1  AND CC.del_yn="N"  AND AA.del_yn="N" AND AA.user_st="1" ');
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));
        query.push(' and BB.apply_cd="A1" and BB.expire_dt>now() ');
        if(params.type) query.push(util.format(' and ( "ALL"="%s"  OR DD.type="%s" ) ', params.type, params.type));
        if(params.user_group_rels_id) query.push(util.format(' and  BB.user_group_rels_id="%s" ) ',  params.user_group_rels_id));
        query.push(' order by BB.created_dt desc ');
        return query.join('');          
    },       
    getUserPasswordCheck: (params) =>  {
        let query = [];   
        query.push('  SELECT count(*) as cnt FROM `tb_user` AS AA ');
        query.push('  where 1=1 AND AA.del_yn="N" AND AA.user_st="1" ');
        query.push(util.format(' AND AA.user_auth_pw=HEX(AES_ENCRYPT("%s", "%s")) ', params.old_user_auth_pw, auth.crypto_key));  
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        return query.join('');          
    },  
    getUserGroupSearch: (params) =>  {
        let query = [];   
        query.push('  SELECT  AA.group_id ,BB.organization_nm  ,AA.group_nm   ');
        query.push('  ,BB.organization_tel,BB.company_num,BB.addr1,BB.addr2 ,BB.grade   ');
        query.push('  FROM tb_group as AA ');
        query.push('  left join tb_organization as BB on AA.organization_id=BB.organization_id ');
        query.push('  where AA.group_id not in( ');
        query.push('  select group_id from tb_user_group_rels ');
        query.push('  where 1=1 and apply_cd in("A1") ');
        query.push(util.format(' AND user_id="%s" ', params.user_id));
        query.push('  and expire_dt>now()   ');
        query.push('  ) ');
        query.push('  and AA.group_st = "1" and AA.del_yn="N" and BB.del_yn="N" ');
        if(params.type) query.push(util.format(' and ( "ALL"="%s"  OR BB.type="%s" ) ', params.type, params.type));
        if(params.search_name) query.push(util.format(' and (BB.organization_nm  like "%s" OR AA.group_nm like "%s" ) ', '%'+params.search_name+'%', '%'+params.search_name+'%'));  
        query.push('  order by BB.organization_nm asc ,AA.group_nm asc ');
        return query.join('');          
    },
    getUserHealth: (params) =>  {
        let query = [];   
        query.push('  SELECT BB.* FROM `tb_user` AS AA ');
        query.push('  left join tb_health as BB on AA.user_id=BB.user_id ');
        query.push('  where 1=1  '); 
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        return query.join('');          
    },     
    getUserPhone: (params) =>  {
        let query = [];   
        query.push('  SELECT count(*) as cnt FROM `tb_user` AS AA ');
        query.push('  where 1=1  and AA.del_yn="N"  and AA.child_yn="N" '); 
        query.push(util.format(' AND AA.user_tel="%s" ', params.user_tel));
        return query.join('');          
    },     
    updateUserPassword:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,user_auth_pw = HEX(AES_ENCRYPT("%s", "%s")) ', params.new_user_auth_pw ,auth.crypto_key)); 
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },              
    updateUserFirstPassword:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,user_auth_pw = HEX(AES_ENCRYPT("%s", "%s")) ', params.user_auth_pw ,auth.crypto_key)); 
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },  
    updateUserFirstInfo:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,login_first_yn="N" ');
        if(params.user_auth_id) query.push(util.format(' ,user_auth_id="%s" ', params.user_auth_id));
        query.push(util.format(' ,user_auth_pw = HEX(AES_ENCRYPT("%s", "%s")) ', params.user_auth_pw ,auth.crypto_key)); 
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },    
    updateUser:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        if(params.user_nm) query.push(util.format(' ,user_nm="%s" ', params.user_nm));
        if(params.user_tel) query.push(util.format(' ,user_tel="%s" ', params.user_tel));
        //if(params.user_auth_pw) query.push(util.format(' ,user_auth_pw = HEX(AES_ENCRYPT("%s", "%s")) ', params.user_auth_pw ,auth.crypto_key)); 
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },
    updateUserLoginTime:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' login_updated_dt=utc_timestamp() ');
        //query.push(' ,login_first_yn="N" ');
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },     
                        
};         