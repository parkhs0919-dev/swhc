const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {

    getScheduleList: (params) =>  {
        let query = [];   
        let ymd='%Y%m%d';
        query.push(' select ZZ.organization_id , ZZ.* from (  ');
        query.push(' SELECT AA.group_id,BB.* ,CAST(BB.visit_time_str AS unsigned) AS visit_no  ');
        query.push(' FROM tb_schedule_rels as AA   ');
        query.push(' left join  tb_schedule as BB on AA.schedule_id=BB.schedule_id  ');
        query.push(' WHERE 1=1 AND BB.del_yn="N" AND BB.schedule_st="1"   ');

        query.push(' AND AA.group_id in(   ');
        query.push(' SELECT AA.group_id from tb_user_group_rels as AA   ');
        query.push(' left join tb_group  as BB  on AA.group_id=BB.group_id   ');
        query.push(' where 1=1  AND BB.del_yn="N"  AND AA.user_group_rels_st="1"   ');
        query.push(' and AA.apply_cd="A1"  and AA.expire_dt>now()   ');
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));
        query.push(' group by BB.group_id   ');
        query.push(' )   ');

        query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.start_date, "+00:00", "%s"), "%s")<="%s"  ',params.z_value ,ymd ,params.today.replace(/\-/g,'')));  
        query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.end_date, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.today.replace(/\-/g,'')));  
        query.push(util.format(' and BB.week like %s ', '"%'+params.week+'%"' ));
        query.push('  ) as ZZ  ');
        query.push('  order by ZZ.visit_no asc ,ZZ.organization_id asc   ');
    
        
        return query.join('');          
    },
             
};         
