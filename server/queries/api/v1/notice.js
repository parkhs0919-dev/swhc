const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    getNotice: (params) =>  {
        let query = [];   
        let ymdhhmi='%Y-%m-%d %H:%i:%s';

        query.push(' select * from ( ');
        query.push(' select BB.notice_id,BB.title,BB.contents,BB.notice_gb ,BB.created_dt,AA.group_nm ,AA.organization_nm ');
        query.push(util.format(',DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' from   (  ');

        query.push(' SELECT BB.organization_id,CC.organization_nm  ,BB.group_nm  FROM tb_user_group_rels as AA    ');
        query.push(' left join tb_group as BB on AA.group_id=BB.group_id    ');
        query.push(' left join tb_organization as CC on BB.organization_id=CC.organization_id  ');
        query.push(util.format(' where AA.user_id="%s" ', params.user_id));
        query.push(' and AA.apply_cd="A1"   ');
        query.push(' and AA.expire_dt>now()   ');
        query.push(' ) as AA  ');
        query.push(' left join tb_notice as BB  on AA.organization_id = BB.organization_id  ');
        query.push(' where BB.notice_st="1"   ')
        query.push(' and BB.notice_gb="admin"   ')

        query.push(' union   ')
        query.push(' select BB.notice_id,BB.title,BB.contents,BB.notice_gb ,BB.created_dt,"" as group_nm ,"" as organization_nm   ')
        query.push(util.format(',DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' from tb_notice as BB   ')
        query.push(' where BB.notice_st="1"    ')
        query.push(' and BB.notice_gb="sys"   ')
        query.push('  ) as ZZ   ')
        query.push('  order by ZZ.created_dt DESC   ')

        return query.join('');          
    },  
    getNoticeLast: (params) =>  {
        let query = [];   
        query.push(' select * from ( ');
        query.push(' select BB.notice_id,BB.title,BB.contents,BB.notice_gb ,AA.group_nm ,AA.organization_nm ');
        query.push(util.format(',DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' from   (  ');

        query.push(' SELECT BB.organization_id,CC.organization_nm  ,BB.group_nm  FROM tb_user_group_rels as AA    ');
        query.push(' left join tb_group as BB on AA.group_id=BB.group_id    ');
        query.push(' left join tb_organization as CC on BB.organization_id=CC.organization_id  ');
        query.push(util.format(' where AA.user_id="%s" ', params.user_id));
        query.push(' and AA.apply_cd="A1"   ');
        query.push(' and AA.expire_dt>now()   ');
        query.push(' ) as AA  ');
        query.push(' left join tb_notice as BB  on AA.organization_id = BB.organization_id  ');
        query.push(' where BB.notice_st="1"   ')
        query.push(' and BB.notice_gb="admin"   ')

        query.push(' union   ')
        query.push(' select BB.notice_id,BB.title,BB.contents,BB.notice_gb ,"" as group_nm ,"" as organization_nm   ')
        query.push(util.format(',DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' from tb_notice as BB   ')
        query.push(' where BB.notice_st="1"    ')
        query.push(' and BB.notice_gb="sys"   ')
        query.push('  ) as ZZ   ')
        query.push('  order by ZZ.created_dt DESC   ')
        query.push(' limit 1 ');
        return query.join('');          
    },                                      
};         
