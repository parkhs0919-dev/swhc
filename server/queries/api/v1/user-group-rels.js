const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {   
    createUserGroupRels: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_group_rels` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`group_id` ');
        query.push(' ,`apply_cd` ');
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');                                    
        query.push(' ,`expire_dt` ');                                    
        query.push(' )');   
           
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_id));
        query.push(util.format(' ,"%s" ', params.group_id));
        query.push(util.format(' ,"%s" ', params.apply_cd));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');   
        query.push(util.format('  ,DATE_ADD(utc_timestamp(), INTERVAL %s DAY) ', params.expire_dt));
        query.push(' )');       

        return query.join('');          
    }, 
    updateUserGroupRels : (params,id) => {
        let query = [];
        query.push(' UPDATE `tb_user_group_rels` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,apply_cd="%s" ', params.apply_cd));
        if(params.expire_dt) query.push(util.format('  ,expire_dt=DATE_ADD(utc_timestamp(), INTERVAL %s DAY) ', params.expire_dt));
        
        query.push('  where 1=1 ');
        query.push(util.format(' and user_group_rels_id="%s" ', id));
        return query.join('');    
    },
    getUserGroupRels: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_user_group_rels` AS AA ');
        query.push('  where 1=1 ');
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));
        query.push(util.format(' and AA.group_id="%s" ', params.group_id));
        return query.join('');          
    },   
    getUserGroupRelsCheck: (params) =>  {
        let query = [];  
        query.push(' SELECT BB.*,CC.group_nm ,DD.type,DD.grade   FROM tb_user as AA  ');
        query.push(' left join tb_user_group_rels as BB  on AA.user_id=BB.user_id  ');
        query.push(' left join tb_group  as CC  on BB.group_id=CC.group_id  ');
        query.push(' left join tb_organization  as DD  on CC.organization_id=DD.organization_id  ');
        query.push(' where 1=1  ');
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));  
        query.push(util.format(' and CC.group_id="%s" ', params.group_id));

        return query.join('');          
    },
    getUserGroupRelsOrganizationCheck: (params) =>  {
        let query = [];  
        query.push(' SELECT count(*) as cnt   FROM tb_user as AA  ');
        query.push(' left join tb_user_group_rels as BB  on AA.user_id=BB.user_id  ');
        query.push(' left join tb_group  as CC  on BB.group_id=CC.group_id  ');
        query.push(' left join tb_organization  as DD  on CC.organization_id=DD.organization_id  ');
        query.push(' where 1=1  ');
        query.push(' and BB.apply_cd="A1" and BB.expire_dt>now() and CC.del_yn="N" and DD.del_yn="N" ');
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));  
        query.push('and DD.organization_id=(  ');
        query.push(' select BB.organization_id from tb_group as AA ');
        query.push(' left join tb_organization as BB  on AA.organization_id =BB.organization_id ');
        query.push(util.format(' where AA.group_id="%s" ', params.group_id)); 
        query.push(' limit 1 ');
        query.push(' ) ');

        return query.join('');          
    },      
    getUserGroupRelsMessage: (params) =>  {
        let query = [];  
        query.push(' SELECT BB.*,CC.group_nm ,DD.organization_nm,DD.organization_id  FROM tb_user as AA  ');
        query.push(' left join tb_user_group_rels as BB  on AA.user_id=BB.user_id  ');
        query.push(' left join tb_group  as CC  on BB.group_id=CC.group_id  ');
        query.push(' left join tb_organization  as DD  on CC.organization_id=DD.organization_id  ');
        query.push(' where 1=1  AND CC.del_yn="N"  AND AA.del_yn="N" AND AA.user_st="1"  ');
        query.push(' and BB.apply_cd="A1" and BB.expire_dt>now() and DD.type="LT"  ');
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));
        return query.join('');              
    },                                   
};         
