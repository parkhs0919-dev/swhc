const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createTableHealthLog: (params) =>  {
        let query = [];
        query.push(util.format(' CREATE TABLE IF NOT EXISTS  `tb_health_log_%s` ( ', params.yyyymm));
        query.push(' `health_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT "헬스정보 테이블 pk id", ');
        query.push(' `user_id` int(11) NOT NULL COMMENT "사용자 fk id", ');
        query.push(' `lat` decimal(16,14) DEFAULT NULL COMMENT "위도값", ');
        query.push(' `lng` decimal(17,14) DEFAULT NULL COMMENT "경도값", ');
        query.push(' `temperature` DECIMAL(5,2) NULL COMMENT "경도값", ');
        query.push(' `heart_rat` decimal(4,0) DEFAULT NULL COMMENT "심장박동수", ');  
        query.push(' `device_num` VARCHAR(50) NULL, ');
        query.push(' `model_num` VARCHAR(30) NULL, ');
        query.push(' `grade` ENUM("G0", "G1", "G2", "G3", "G4","G5", "G6") NULL DEFAULT "G0", ');
        query.push(' `device_connect_st` ENUM("1", "0") NULL DEFAULT "1", '); 
        query.push(' `created_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT "등록시간",');   
        query.push(' PRIMARY KEY (`health_log_id`),');   
        query.push(util.format(' KEY `fk_tb_health_user_id_idx_%s` (`user_id`),  ', params.yyyymm));  
        query.push(util.format(' CONSTRAINT `fk_tb_health_user_id_%s` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`user_id`)  ', params.yyyymm));   
        query.push(' ON DELETE NO ACTION ON UPDATE NO ACTION ');   
        query.push(' ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ');     
        return query.join('');          
    },      
    createHealthLog: (params) =>  {
        let query = [];
        query.push(util.format(' INSERT INTO `tb_health_log_%s` ', params.currentMonth));
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`temperature` ');
        query.push(' ,`grade` ');
        if(params.lat) query.push(' ,`lat` ');
        if(params.lng) query.push(' ,`lng` ');
        if(params.heart_rat) query.push(' ,`heart_rat` ');   
        if(params.device_num) query.push(' ,`device_num` '); 
        if(params.model_num) query.push(' ,`model_num` '); 
        if(params.device_connect_st) query.push(' ,`device_connect_st` ');   
        query.push(' ,`created_dt` ');        
        query.push(' )');   
           
        query.push(' select  ');
        query.push(' ');
        query.push(util.format(' "%s" ', params.user_id));
        query.push(util.format(' ,"%s" ', params.temperature));
        query.push(' ,grade_cd ');
        if(params.lat) query.push(util.format(' ,"%s" ', params.lat));
        if(params.lng) query.push(util.format(' ,"%s" ', params.lng));
        if(params.heart_rat) query.push(util.format(' ,"%s" ', params.heart_rat));    
        if(params.device_num) query.push(util.format(' ,"%s" ', params.device_num));   
        if(params.model_num) query.push(util.format(' ,"%s" ', params.model_num));   
        if(params.device_connect_st) query.push(util.format(' ,"%s" ', params.device_connect_st));   
        query.push(' ,utc_timestamp() ');
        query.push(' from tb_health '); 
        query.push(util.format(' where user_id="%s" ', params.user_id));      
         
        return query.join('');          
    },                               
};         
