const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {   
    updateUserNotificationDuplicate : (params,id) => {
        let query = [];

        query.push(' INSERT INTO `tb_user_notification` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        if(params.grade) query.push(' ,`grade` ');
        query.push(' ,`updated_dt` ');
        query.push(' )');   
  
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_id));
        if(params.grade) query.push(util.format(' ,"%s" ', params.grade));
        query.push(' ,utc_timestamp() ');
        query.push(' )');         

        query.push(' ON DUPLICATE KEY UPDATE ');
        query.push(' updated_dt=utc_timestamp() ');     
        query.push(util.format(' ,user_id="%s" ', params.user_id));
        if(params.grade) query.push(util.format(' ,grade="%s" ', params.grade));

        return query.join('');    
    }                        
};         
