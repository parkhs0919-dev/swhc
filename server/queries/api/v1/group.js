const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {   
    getGroup: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.*,BB.type,BB.grade FROM `tb_group` AS AA ');
        query.push('  left join tb_organization as BB on AA.organization_id=BB.organization_id ');
        query.push('  where 1=1 ');
        query.push(util.format(' and AA.group_id="%s" ', params.group_id));
        return query.join('');          
    },
                              
};         
