const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserVaccineFile: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_vaccine_file` ');
        query.push(' ( ');
        query.push(' `user_vaccine_id` ');
        query.push(' ,`file_url` ');        
        query.push(' ,`file_name` ');        
        query.push(' ,`bucket_name` '); 
        query.push(' ,`created_dt` ');                                              
        query.push(' )');   
        
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_vaccine_id));
        query.push(util.format(' ,"%s" ', params.file_url));
        query.push(util.format(' ,"%s" ', params.file_name));
        query.push(util.format(' ,"%s" ', params.bucket_name));
        query.push(' ,utc_timestamp() ');       
        query.push(' )');       
        
        return query.join('');          
    },  
    getUserVaccineFile: (params) =>  {
        let query = [];  

        query.push(' SELECT *  ');
        query.push(' FROM tb_user_vaccine_file as AA ');
        query.push(util.format(' where AA.user_vaccine_file_id="%s" ', params.user_vaccine_file_id));
        return query.join('');          
    },
    deleteUserVaccineFile: (id) =>  {
        let query = [];
        query.push(' DELETE FROM `tb_user_vaccine_file` '); 
        query.push(util.format(' where user_vaccine_file_id="%s" ', id));
                    
        return query.join('');      
    },                                              
};           
