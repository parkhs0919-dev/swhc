const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createMessage: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_message` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`organization_id` ');
        query.push(' ,`group_id` ');
        query.push(' ,`contents` ');
        query.push(' ,`message_gb` ');
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');         
        query.push(' )');   
           
        query.push(' SELECT ');
        query.push(util.format(' "%s" ', params.user_id));
        query.push(' ,BB.organization_id   ');  
        query.push(' ,AA.group_id ');  
        query.push(util.format(' ,"%s" ', params.contents));
        query.push(util.format(' ,"%s" ', params.message_gb));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');        
        query.push(' FROM tb_group as AA  ');       
        query.push(' left join tb_organization  as BB  on AA.organization_id=BB.organization_id');  
        query.push(util.format(' where AA.group_id="%s" ', params.group_id));     
      
        return query.join('');          
    },   
    createMessageReply: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_message` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`reply_id` ');
        query.push(' ,`organization_id` ');
        query.push(' ,`group_id` ');
        query.push(' ,`contents` ');
        query.push(' ,`message_gb` ');
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');         
        query.push(' )');   
           
        query.push(' SELECT ');
        query.push(util.format(' "%s" ', params.user_id));
        query.push(' ,AA.message_id   ');  
        query.push(' ,AA.organization_id   ');  
        query.push(' ,AA.group_id ');  
        query.push(util.format(' ,"%s" ', params.contents));
        query.push(util.format(' ,"%s" ', params.message_gb));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');        
        query.push(' FROM tb_message as AA  ');       
        query.push(util.format(' where AA.message_id="%s" ', params.message_id));     
      
        return query.join('');          
    },       
    getMessageSend: (params) =>  {
        let query = [];   
        let ymdhhmi='%Y-%m-%d %H:%i:%s';

        query.push(' SELECT  AA.message_id,AA.title,AA.contents,AA.read_st ,AA.message_gb ');
        query.push(' ,ifnull(BB.group_nm,"") as group_nm ,ifnull(CC.organization_nm,"") as organization_nm ');
        query.push(' ,ifnull(DD.admin_nm,"") as admin_nm ');
        query.push(util.format(',DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' from tb_message  as AA    ');
        query.push(' left outer join tb_group  as BB  on AA.group_id=BB.group_id   ');
        query.push(' left outer join tb_organization  as CC  on AA.organization_id=CC.organization_id   ');
        query.push(' left outer join tb_organization_admin  as DD  on AA.organization_admin_id=DD.organization_admin_id   ');
        query.push(' where message_gb="user"  ');
        query.push(util.format(' and user_id="%s" ', params.user_id));
        query.push(' order by AA.created_dt desc  ');
        return query.join('');          
    },  

    getMessageReceive: (params) =>  {
        let query = [];   
        let ymdhhmi='%Y-%m-%d %H:%i:%s';
        query.push(' SELECT  AA.message_id,AA.title,AA.contents,AA.read_st ,AA.message_gb ');
        query.push(' ,ifnull(BB.group_nm,"") as group_nm ,ifnull(CC.organization_nm,"") as organization_nm ');
        query.push(' ,ifnull(DD.admin_nm,"") as admin_nm ');
        query.push(util.format(',DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' from tb_message  as AA    ');
        query.push(' left outer join tb_group  as BB  on AA.group_id=BB.group_id   ');
        query.push(' left outer join tb_organization  as CC  on AA.organization_id=CC.organization_id   ');
        query.push(' left outer join tb_organization_admin  as DD  on AA.organization_admin_id=DD.organization_admin_id   ');
        query.push(' where message_gb in("sys","admin")  ');
        query.push(util.format(' and user_id="%s" ', params.user_id));
        query.push(' order by AA.created_dt desc  ') 
        return query.join('');          
    }, 
    updateMessageReadSt:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_message` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,read_st ="%s" ', params.read_st)); 
        query.push(util.format(' where user_id="%s" ', params.user_id));
        query.push(util.format(' and message_id="%s" ', params.message_id));
        return query.join('');
    },                              
};         
