const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserDevice: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_device` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        if(params.user_device_sn)query.push(' ,`user_device_sn` ');
        if(params.fcm_token)query.push(' ,`fcm_token` ');
        query.push(' ,`updated_dt` ');     
        query.push(' )');   
           
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_id));
        if(params.user_device_sn)query.push(util.format(' ,"%s" ', params.user_device_sn));
        if(params.fcm_token)query.push(util.format(' ,"%s" ', params.fcm_token));
        query.push(' ,utc_timestamp() ');
        query.push(' )');       
                
        return query.join('');          
    },        
    updateUserDevice:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user_device` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        if(params.device_sn) query.push(util.format(' ,user_device_sn="%s" ', params.device_sn));
        if(params.fcm_token) query.push(util.format(' ,fcm_token="%s" ', params.fcm_token));
        if(params.uuid) query.push(util.format(' ,uuid="%s" ', params.uuid));
        if(params.platform) query.push(util.format(' ,platform="%s" ', params.platform));        
        if(params.model_num) query.push(util.format(' ,model_num="%s" ', params.model_num));        
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');   
    },    
    getUserDeviceCheck: (params) =>  {
        let query = [];   
        query.push(' SELECT AA.* ,ifnull(BB.grade,"N") as grade FROM `tb_user_device` AS AA  ');
        query.push(' left outer join tb_user_notification as BB ON  AA.user_id=BB.user_id ');
        query.push(util.format(' where AA.user_id="%s" ', params.user_id));
        return query.join('');          
    },  
    getUserDevice: (params) =>  {
        let query = [];   
        query.push(' SELECT AA.* FROM `tb_user_device` AS AA  ');
        query.push(util.format(' where AA.user_id="%s" ', params.user_id));
        return query.join('');          
    },  
    getUserDeviceSnListCheck: (params) =>  {
        let query = []; 
        let deviceList=params.device_list.replace(/"/g,'');
        let deviceSnArray =deviceList.split(',');
        let deviceSnCnt =deviceSnArray.length;

        query.push('  select ZZ.user_device as device_sn from ( ');
        query.push('  select  * ,  ');
        query.push('  CASE WHEN AA.user_device = BB.user_device_sn THEN "0"  ');
        query.push('  WHEN AA.user_device != BB.user_device_sn THEN "1"  ');
        query.push('  ELSE "1" END AS is_device ');
        query.push('  from (  ');

        deviceSnArray.forEach( (val ,index) => {
            query.push(SubQueries.selectUserdeviceSnSelectUnion(val));
            if(index == (deviceSnCnt-1)) query.push(' ');
            else query.push(' union ');
        });   

        query.push('  ) as AA  ');
        query.push('  left outer join (  ');
        query.push('  select BB.user_device_sn from tb_user as AA ');
        query.push('  left join tb_user_device as BB on AA.user_id=BB.user_id  ');
        query.push('  where AA.user_st="1"  ');
        //query.push(util.format(' and AA.organization_id="%s" ', params.organization_id));
        query.push('  ) as BB  ');
        query.push('  on AA.user_device=BB.user_device_sn ');
        query.push('  ) as ZZ  ');
        query.push('  where ZZ.is_device=1  ');

        return query.join('');           
    },
    updateUserDeviceInit:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user_device` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,user_device_sn="" ,platform="",model_num="",fcm_token="",uuid="" ');
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },     
    updateUserDeviceSnInit : (params,id) => {
        let query = [];
        query.push(' UPDATE `tb_user_device` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,user_device_sn=null ');
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');    
    },  
    updateUserDeviceSnInitMac : (params) => {
        let query = [];
        query.push(' UPDATE `tb_user_device` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,user_device_sn=null ');
        query.push(util.format(' where user_device_sn="%s" ', params.mac));
        return query.join('');    
    },                                           
};           

let SubQueries = {
    selectUserdeviceSnSelectUnion:  (val) => {
        let query = [];
        query.push(util.format(' select "%s" as user_device ', val));
        return query.join('');
    }
};
