const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserVaccine: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_vaccine` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`check_dt` ');        
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');                                    
        query.push(' )');   
        
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_id));
        query.push(util.format(' ,"%s" ', params.check_dt));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');        
        query.push(' )');       
        
        return query.join('');          
    },   
    getUserVaccine: (params) =>  {
        let query = [];  
        let ymdhhmi='%Y-%m-%d %H:%i';
        let ymd='%Y-%m-%d';

        query.push(' SELECT AA.user_vaccine_id as  paper_id , "vaccine" as type , AA.check_cd  ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.check_dt, "+00:00", "%s"), "%s") as check_dt_str ',params.z_value , ymd ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(DATE_ADD(AA.check_dt, INTERVAL 12 MONTH), "+00:00", "%s"), "%s") as expiry_dt_str ',params.z_value , ymd ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' ,CASE  ');
        query.push(util.format(' WHEN TIMESTAMPDIFF(MINUTE,convert_tz(DATE_ADD(AA.check_dt, INTERVAL 12 MONTH), "+00:00", "%s"),convert_tz(utc_timestamp(), "+00:00", "%s"))>0 ', params.z_value, params.z_value));
        query.push(' THEN "03" ELSE AA.approval_cd ');
        query.push(' END AS approval_cd  ');
        query.push(' FROM tb_user_vaccine as AA ');
        query.push(util.format(' where AA.user_id="%s" ', params.user_id));
        query.push(' order by created_dt desc  ');
        return query.join('');               
    }, 
    getUserVaccineId: (id) =>  {
        let query = [];  

        query.push(' SELECT * FROM tb_user_vaccine AS AA  ');
        query.push(' left join tb_user_vaccine_file AS BB on AA.user_vaccine_id=BB.user_vaccine_id ');
        query.push(' where AA.approval_cd="00" ');
        query.push(util.format(' and AA.user_Vaccine_id="%s" ',id));
        return query.join('');   
    },       
    deleteUserVaccine: (id) =>  {
        let query = [];
        query.push(' DELETE FROM `tb_user_vaccine` '); 
        query.push(util.format(' where user_vaccine_id="%s" ', id));
                    
        return query.join('');      
    },                                                   
};           


