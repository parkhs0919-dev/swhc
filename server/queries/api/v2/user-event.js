const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {       
    getUserEventList: (params) =>  {
        let query = [];  
        query.push('  select BB.* from  tb_user_event_rels as AA ');
        query.push('  left join tb_user_event as BB on AA.user_event_id =BB.user_event_id ');
        query.push('  WHERE 1=1 AND BB.del_yn="N" ');
        // query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id ));
        // query.push(util.format(' AND AA.group_id="%s" ', params.group_id ));
        query.push(' AND AA.group_id in( ');
        query.push('  SELECT group_id FROM tb_user_group_rels ');
        query.push(util.format(' where user_id="%s" ', params.user_id ));
        query.push('  and apply_cd="A1"   ');
        query.push('  and expire_dt>now() ');
        query.push(' ) ');

        query.push('  order by BB.start_time ASC ');

        return query.join('');          
    },
    getUserEvent: (params) =>  {
        let query = [];  
        query.push('  select AA.* from  tb_user_event as AA ');
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.user_event_id="%s" ', params.user_event_id ));

        return query.join('');          
    }, 
    getUserEventToday: (params) =>  {
        let query = []; 
        let ymd='%Y%m%d'; 
        query.push('  select * from ( ');
        query.push('  SELECT BB.* ,CAST(BB.visit_time_str AS unsigned) AS visit_no  FROM tb_user_event_rels as AA ');
        query.push('  left join  tb_user_event as BB on AA.user_event_id=BB.user_event_id ');
        query.push('  WHERE 1=1 AND BB.del_yn="N" AND BB.user_event_st="1" ');
        // query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id ));
        // query.push(util.format(' AND AA.group_id="%s" ', params.group_id ));

        query.push(' AND AA.group_id in( ');
        query.push('  SELECT group_id FROM tb_user_group_rels ');
        query.push(util.format(' where user_id="%s" ', params.user_id ));
        query.push('  and apply_cd="A1"   ');
        query.push('  and expire_dt>now() ');
        query.push(' ) ');
            
        query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.start_date, "+00:00", "%s"), "%s")<="%s"  ',params.z_value ,ymd ,params.today.replace(/\-/g,'')));  
        query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.end_date, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.today.replace(/\-/g,'')));  
        query.push(util.format(' and BB.week like %s ', '"%'+params.week+'%"' ));
        query.push('  ) as ZZ  ');
        query.push('  order by ZZ.visit_no asc  ');

        return query.join('');          
    }, 
    getUserEventHealthLog: (params) =>  {
        let query = [];
        let date_ymd='%Y%m%d';
        let date_hi='%H%i';
        let start_time=(params.s_time.length==3)? "0"+params.s_time : params.s_time ;
        let end_time=(params.e_time.length==3)? "0"+params.e_time : params.e_time ;

        query.push(' select ZZ.*  ');
        query.push(' from ( ');

        query.push(' select ');
        query.push(util.format(' "%s" as title ', params.title ));
        query.push(util.format(' ,"%s" as start_date  ', params.start_date ));
        query.push(util.format(' ,"%s" as end_date  ', params.end_date ));
        query.push(util.format(' ,"%s" as week  ', params.week ));
        query.push(util.format(' ,"%s" as start_time  ', params.start_time ));
        query.push(util.format(' ,"%s" as end_time  ', params.end_time ));
        query.push(util.format(' ,"%s" as visit_time  ', params.visit_time ));
        query.push(util.format(' ,"%s" as diff  ', params.diff ));
        query.push(util.format(' ,"%s%s" as s_time  ', params.today.replace(/\-/g,''),start_time ));
        query.push(util.format(' ,"%s%s" as e_time  ', params.today.replace(/\-/g,''),end_time ));
        query.push(' ,count(CASE WHEN temperature>34 and temperature<=37 THEN 1  END) AS temp_cnt ' );
        query.push(util.format(' ,"%s" as user_event_id ,count(*) as cnt from tb_health_log_%s  ', params.user_event_id, params.yyyymm));
        query.push(' where 1=1 ');
        query.push(' and device_use_yn="Y"');
        query.push(util.format(' AND user_id="%s" ', params.user_id ));
        query.push(util.format(' AND DATE_FORMAT(convert_tz(created_dt, "+00:00", "%s"), "%s")="%s"  ',params.z_value ,date_ymd ,params.today.replace(/\-/g,'')));  
        query.push(util.format(' AND DATE_FORMAT(convert_tz(created_dt, "+00:00", "%s"), "%s")>="%s" ',params.z_value, date_hi,start_time ));
        query.push(util.format(' AND DATE_FORMAT(convert_tz(created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value, date_hi,end_time ));

        query.push(' ) as ZZ ');

         
        return query.join('');          
    },
    getUserEventHealthLogCheck: (params) =>  {
        let query = [];
        let date_ymd='%Y%m%d';
        let date_hi='%H%i';
        let start_time=(params.start_time.length==3)? "0"+params.start_time : params.start_time ;
        let end_time=(params.end_time.length==3)? "0"+params.end_time : params.end_time ;

        query.push(' select * ');
        query.push(' ,AVG(AA.temperature) as avg ,count(*) as cnt from ( ');
        query.push(util.format(' select *  from tb_health_log_%s  ', params.yyyymm));
        query.push(' where 1=1 ');
        query.push(' and device_use_yn="Y"');
        query.push(util.format(' AND user_id="%s" ', params.user_id ));
        query.push(util.format(' AND DATE_FORMAT(convert_tz(created_dt, "+00:00", "%s"), "%s")="%s"  ',params.z_value ,date_ymd ,params.today.replace(/\-/g,'')));  
        query.push(util.format(' AND DATE_FORMAT(convert_tz(created_dt, "+00:00", "%s"), "%s")>="%s" ',params.z_value, date_hi,start_time ));
        query.push(util.format(' AND DATE_FORMAT(convert_tz(created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value, date_hi,end_time ));

        query.push(' order by health_log_id desc  limit 20 ) as AA ');

        return query.join('');          
    },                       
};       
