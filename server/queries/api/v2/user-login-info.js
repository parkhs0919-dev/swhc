const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {   
    createUserLoginInfo: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_login_info` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`uuid` ');
        query.push(' ,`platform` ');
        query.push(' ,`fcm_token` ');
        query.push(' )');   
           
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_id));
        query.push(util.format(' ,"%s" ', params.uuid));
        query.push(util.format(' ,"%s" ', params.platform));
        query.push(util.format(' ,"%s" ', params.fcm_token));
        query.push(' )');       
      
        return query.join('');          
    }, 
    updateUserLoginInfo : (params,id) => {
        let query = [];
        query.push(' UPDATE `tb_user_login_info` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,login_yn="%s" ', params.login_yn));
        
        query.push('  where 1=1 ');
        query.push(util.format(' and user_id="%s" ', params.user_id));
        if(params.uuid) query.push(util.format(' and uuid="%s" ', params.uuid));
        if(params.platform) query.push(util.format(' and platform="%s" ', params.platform));
        return query.join('');    
    }, 
    updateUserLoginInfoDuplicate : (params,id) => {
        let query = [];

        query.push(' INSERT INTO `tb_user_login_info` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`uuid` ');
        if(params.platform)query.push(' ,`platform` ');
        if(params.fcm_token)query.push(' ,`fcm_token` ');
        if(params.login_yn) query.push(' ,`login_yn` ');
        query.push(' )');   
  
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_id));
        query.push(util.format(' ,"%s" ', params.uuid));
        if(params.platform) query.push(util.format(' ,"%s" ', params.platform));
        if(params.fcm_token) query.push(util.format(' ,"%s" ', params.fcm_token));
        if(params.login_yn) query.push(util.format(' ,"%s" ', params.login_yn));
        query.push(' )');         

        query.push(' ON DUPLICATE KEY UPDATE ');
        query.push(' updated_dt=utc_timestamp() ');     
        query.push(util.format(' ,user_id="%s" ', params.user_id));
        query.push(util.format(' ,uuid="%s" ', params.uuid));
        if(params.platform) query.push(util.format(' ,platform="%s" ', params.platform));
        if(params.fcm_token) query.push(util.format(' ,fcm_token="%s" ', params.fcm_token));
        if(params.login_yn) query.push(util.format(' ,login_yn="%s" ', params.login_yn));
        return query.join('');    
    },                       
    getUserLoginInfoCheck: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_user_login_info` AS AA ');
        query.push('  where 1=1 ');
        query.push('  and AA.login_yn="Y" ');   
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));
        return query.join('');          
    },
    getUserLoginInfoDeviceCheck: (params) =>  {
        let query = [];   
        query.push('  SELECT count(*) as cnt FROM `tb_user_login_info` AS AA ');
        query.push('  where 1=1 ');
        query.push('  and AA.login_yn="Y" ');   
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));
        query.push(util.format(' and AA.platform="%s" ', params.platform));
        query.push(util.format(' and AA.uuid="%s" ', params.uuid));
        return query.join('');          
    },
    deleteUserLoginInfo : (id) => {
		let query = [];
        query.push(' DELETE FROM `tb_user_login_info` '); 
        query.push(util.format(' where user_id="%s" ', id));
                    
		return query.join('');      
    },                   
};         
