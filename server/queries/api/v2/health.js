const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {   
    createHealth: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_health` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        if( params.temperature) query.push(' ,`temperature` ');
        if( params.heart_rat) query.push(' ,`heart_rat` ');
        if( params.temperature_cd) query.push(' ,`temperature_cd` ');      
        query.push(' ,`updated_dt` ');                
        query.push(' )');   
           
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_id));
        if( params.temperature) query.push(util.format(' ,"%s" ', params.temperature));
        if( params.heart_rat) query.push(util.format(' ,"%s" ', params.heart_rat));
        if( params.temperature_cd) query.push(util.format(' ,"%s" ', params.temperature_cd));     
        query.push(' ,utc_timestamp() ');               
        query.push(' )');           
      
        return query.join('');          
    },     
    updateHealth:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_health` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,temperature="%s" ', params.temperature));
        if(params.temperature_cd) query.push(util.format(' ,temperature_cd="%s" ', params.temperature_cd));
        if(params.heart_rat) query.push(util.format(' ,heart_rat="%s" ', params.heart_rat));
        else  query.push(' ,heart_rat="0" ');
        if(params.device_connect_st) query.push(util.format(' ,device_connect_st="%s" ', params.device_connect_st));
        if(params.network_connect_st) query.push(util.format(' ,network_connect_st="%s" ', params.network_connect_st));
        if(params.battery) query.push(util.format(' ,battery="%s" ', params.battery));
        if(params.device_sn) query.push(util.format(' ,device_num="%s" ', params.device_sn));
        if(params.sbp) query.push(util.format(' ,sbp="%s" ', params.sbp));
        if(params.dbp) query.push(util.format(' ,dbp="%s" ', params.dbp));
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },  
    updateHealthStatus:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_health` ');
        query.push(' SET ');
        query.push(' status_dt=utc_timestamp() ');
        query.push(util.format(' ,status_cd="%s" ', params.status_cd));
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    }, 
    updateHealthGrade:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_health` ');
        query.push(' SET ');
        query.push(' grade_dt=utc_timestamp() ');
        query.push(util.format(' ,grade_cd="%s" ', params.grade_cd));
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    }, 
    getHealthCheck: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_health` AS AA ');
        query.push('  where 1=1 ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        return query.join('');          
    },  
    getHealthGradeStatus: (params) =>  {
        let query = [];   
        query.push('  select BB.self_quarantine_yn,BB.grade_cd,BB.status_cd, ifnull(CC.check_cd,"C0") as check_status ,ifnull(DD.check_cd,"C0") as vaccine_status  ');
        query.push('  ,ifnull(EE.grade,"N") as grade from tb_user as AA ');
        query.push('  left join tb_health as BB on AA.user_id=BB.user_id ');

        query.push('  left outer join ( ');
        query.push('  select * from ( ');
        query.push('  SELECT AA.* FROM tb_user_check as AA ');
        query.push('  left join tb_user_check_file as BB on AA.user_check_id=BB.user_check_id ');
        query.push('  where AA.approval_cd = "01" ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        query.push(util.format('and TIMESTAMPDIFF(MINUTE,convert_tz(utc_timestamp(), "+00:00", "%s"),convert_tz(DATE_ADD(AA.check_dt, INTERVAL 14 DAY), "+00:00", "%s"))>0 ', params.z_value, params.z_value));        
        query.push('  order by check_dt desc ');
        query.push('  ) as ZZ limit 1 ');
        query.push('  ) as CC on AA.user_id=CC.user_id');

        query.push('  left outer join ( ');
        query.push('  select * from ( ');
        query.push('  SELECT AA.* FROM tb_user_vaccine as AA ');
        query.push('  left join tb_user_vaccine_file as BB on AA.user_vaccine_id=BB.user_vaccine_id ');
        query.push('  where AA.approval_cd = "01" ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        query.push(util.format('and TIMESTAMPDIFF(MINUTE,convert_tz(utc_timestamp(), "+00:00", "%s"),convert_tz(DATE_ADD(AA.check_dt, INTERVAL 365 DAY), "+00:00", "%s"))>0 ', params.z_value, params.z_value));        
        query.push('  order by check_dt desc ');
        query.push('  ) as ZZ limit 1 ');
        query.push('  ) as DD on AA.user_id=DD.user_id ');
        query.push('  left outer join tb_user_notification as EE ON  AA.user_id=EE.user_id ');
        query.push(util.format(' where AA.user_id="%s" ', params.user_id));
        return query.join('');          
    },       
};         

