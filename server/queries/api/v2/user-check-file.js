const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserCheckFile: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_check_file` ');
        query.push(' ( ');
        query.push(' `user_check_id` ');
        query.push(' ,`file_url` ');        
        query.push(' ,`file_name` ');        
        query.push(' ,`bucket_name` ');        
        query.push(' ,`created_dt` ');                                              
        query.push(' )');   
        
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_check_id));
        query.push(util.format(' ,"%s" ', params.file_url));
        query.push(util.format(' ,"%s" ', params.file_name));
        query.push(util.format(' ,"%s" ', params.bucket_name));
        query.push(' ,utc_timestamp() ');       
        query.push(' )');       
        
        return query.join('');          
    }, 
    getUserCheckFile: (params) =>  {
        let query = [];  

        query.push(' SELECT *  ');
        query.push(' FROM tb_user_check_file as AA ');
        query.push(util.format(' where AA.user_check_file_id="%s" ', params.user_check_file_id));
        return query.join('');          
    },  
    deleteUserCheckFile: (id) =>  {
        let query = [];
        query.push(' DELETE FROM `tb_user_check_file` '); 
        query.push(util.format(' where user_check_file_id="%s" ', id));
                    
        return query.join('');      
    },                                           
};           
