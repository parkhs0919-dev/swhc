const util = require('util');
const auth = CONFIG.common.auth;
const moment = require('moment');

module.exports = {
    createUserInfo: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user` ');
        query.push(' ( ');
        query.push(' `organization_id` ');        
        query.push(' ,`group_id` ');
        query.push(' ,`user_auth_id` ');
        query.push(' ,`user_auth_pw` ');
        query.push(' ,`user_nm` ');
        query.push(' ,`user_tel` ');
        query.push(' ,`user_st` ');
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');                                    
        query.push(' )');   
        
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_id));
        query.push(util.format(' ,"%s" ', params.group_id));
        query.push(util.format(' ,"%s" ', params.user_auth_id));
        query.push(util.format(' ,HEX(AES_ENCRYPT("%s", "%s")) ', params.admin_auth_pw, auth.crypto_key));  
        query.push(util.format(' ,"%s" ', params.user_nm));
        query.push(util.format(' ,"%s" ', params.user_tel));
        query.push(util.format(' ,"%s" ', params.user_st));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');        
        query.push(' )');       
        
        return query.join('');          
    },  
    getUserList: (params) =>  {
        let query = [];  
        let ymdhhmi='%Y-%m-%d %H:%i:%s';
        
        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push('  SELECT AA.user_id,BB.user_auth_id ,BB.user_nm, BB.user_tel ,CC.user_device_sn,AA.organization_nm ,DD.temperature,DD.device_connect_st ');
        query.push('  ,DD.heart_rat,DD.temperature_cd,DD.grade_cd  ');
        query.push(util.format(',DATE_FORMAT(convert_tz(DD.updated_dt, "+00:00", "%s"), "%s") as updated_dt_str ',params.z_value , ymdhhmi ));  
        
        query.push('  FROM (')
        query.push(SubQueries.setUserListAll(params));
        query.push('  ) as AA');
        query.push('  LEFT JOIN tb_user as BB ON AA.user_id=BB.user_id');
        query.push('  LEFT JOIN tb_user_device as CC ON AA.user_id=CC.user_id ');
        query.push('  LEFT JOIN tb_health as DD ON AA.user_id=DD.user_id ');

        query.push('  WHERE 1=1 AND BB.del_yn="N" ');
        if(params.grade_cd) query.push(util.format(' AND ( "ALL" ="%s" OR DD.grade_cd="%s") ',  params.grade_cd , params.grade_cd));
        //query.push(util.format(' AND ( "ALL" ="%s" OR CC.device_connect_st="%s") ',  params.device_connect_st , params.device_connect_st));
        if(params.search_name)  query.push(util.format(' AND (BB.user_auth_id  like "%s" OR BB.user_nm  like "%s" OR BB.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by BB.user_nm ASC ');

        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    }, 
    getUserListTotalCnt: (params) =>  {
        let query = [];  

        query.push('  SELECT count(*) as total_cnt ');
        query.push('  FROM (')
        query.push(SubQueries.setUserListAll(params));
        query.push('  ) as AA');
        query.push('  LEFT JOIN tb_user as BB ON AA.user_id=BB.user_id');
        query.push('  LEFT JOIN tb_user_device as CC ON AA.user_id=CC.user_id ');
        query.push('  LEFT JOIN tb_health as DD ON AA.user_id=DD.user_id ');

        query.push('  WHERE 1=1 AND BB.del_yn="N" ');
        if(params.grade_cd) query.push(util.format(' AND ( "ALL" ="%s" OR DD.grade_cd="%s") ',  params.grade_cd , params.grade_cd));
        //query.push(util.format(' AND ( "ALL" ="%s" OR CC.device_connect_st="%s") ',  params.device_connect_st , params.device_connect_st));
        if(params.search_name)  query.push(util.format(' AND (BB.user_auth_id  like "%s" OR BB.user_nm  like "%s" OR BB.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by BB.user_nm ASC ');

        return query.join('');          
    },     
    getUserDetail: (params) =>  {
        let query = []; 
        let ymdhhmi='%Y-%m-%d %H:%i:%s';

        query.push('  SELECT AA.user_id,AA.user_auth_id,AA.user_nm, AA.user_tel, DD.group_nm, AA.user_st ,BB.user_device_sn,DD.organization_nm ,CC.temperature,CC.device_connect_st ');
        query.push('  ,CC.heart_rat,CC.temperature_cd,CC.grade_cd,ifnull(EE.check_cd,"C5") as check_status,ifnull(FF.check_cd,"C5") as vaccine_status ');
        query.push('  ,ifnull(EE.approval_cd,"05") as check_approval,ifnull(FF.approval_cd,"05") as vaccine_approval ');
        query.push('  ,BB.fcm_token,BB.uuid,BB.model_num,BB.platform,CC.sbp,CC.dbp ');
        query.push(util.format(',DATE_FORMAT(convert_tz(CC.updated_dt, "+00:00", "%s"), "%s") as updated_dt_str ',params.z_value , ymdhhmi )); 
        query.push('  FROM tb_user as AA');
        query.push('  LEFT JOIN tb_user_device as BB ON AA.user_id=BB.user_id ');
        query.push('  LEFT JOIN tb_health as CC ON AA.user_id=CC.user_id ');

        query.push('  LEFT JOIN ( ');
        query.push(SubQueries.setUserGroupRels(params));
        query.push('  ) as DD ON AA.user_id=DD.user_id  ');

        query.push('  LEFT JOIN ( ');
        query.push(SubQueries.setUserCheckLimit(params));
        query.push('  ) as EE on AA.user_id=EE.user_id  ');

        query.push('  LEFT JOIN ( ');
        query.push(SubQueries.setUserVaccineLimit(params));
        query.push('  ) as FF on AA.user_id=FF.user_id  ');

        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));

        return query.join('');          
    },     
    getUserDetailTempLog: (params) =>  {
        let query = [];   
        //let currentMonth =moment().add(0, 'M').format("YYYYMM");
        let ymdhhmi='%Y-%m-%d %H:%i';
        let date_ym = getDates_yyyymm(params.start_date,params.end_date);
        let date_ym_cnt=(date_ym.length)? date_ym.length : 0;

        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        date_ym.forEach( (date ,index) => {
            params.currentMonth=date;
            query.push(SubQueries.getUserDetailTempLogUnion(params));
            if(index == (date_ym_cnt-1)) query.push(' ');
            else query.push(' union all  ');
        });
        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        //query.push('  limit 10 ');
        return query.join('');          
    }, 
    getUserDetailTempLogTotalCnt: (params) =>  {
        let query = [];   
        //let currentMonth =moment().add(0, 'M').format("YYYYMM");
        let ymdhhmi='%Y-%m-%d %H:%i';
        let date_ym = getDates_yyyymm(params.start_date,params.end_date);
        let date_ym_cnt=(date_ym.length)? date_ym.length : 0;

        query.push('  SELECT count(*) as total_cnt from   ( ');

        date_ym.forEach( (date ,index) => {
            params.currentMonth=date;
            query.push(SubQueries.getUserDetailTempLogUnion(params));
            if(index == (date_ym_cnt-1)) query.push(' ');
            else query.push(' union all  ');
        });

        query.push('  )as AA order by AA.created_dt DESC');
      
        return query.join('');       
    },     
    getUserSearchSelectBox: (params) =>  {
        let query = [];  
        query.push('  SELECT user_id as id ,AA.user_nm as name, AA.user_tel as tel  FROM `tb_user` AS AA ');
        query.push('  where  1=1 AND AA.del_yn="N"  ');
        query.push('  and user_st="1" ');      
        query.push('  and user_id in( ');   
        
        query.push(SubQueries.setUserGroupRelsAll(params));
        // if(params.admin_grade=="M"){
        //     query.push('  SELECT group_id FROM `tb_group` AS AA  ');            
        //     query.push(util.format(' where organization_id="%s" ', params.organization_id));
        // }else{
        //     query.push('  select AA.group_id from tb_group_admin_rels as AA ');
        //     query.push('  left join tb_organization_admin as BB ');
        //     query.push('  on AA.organization_admin_id=BB.organization_admin_id ');
        //     query.push(util.format(' where BB.organization_admin_id="%s" ', params.organization_admin_id));

        // } 
        query.push(' ) ');
        if(params.search_name)query.push(util.format(' AND  AA.user_nm like "%s" ' ,'%'+params.search_name+'%' ));   
        query.push(' order by AA.user_nm asc ');    
        return query.join('');          
    },       
    updateUserLoginTime:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' login_updated_dt=utc_timestamp() ');
        //query.push(' ,login_first_yn="N" ');
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },  
    updateUserInfo:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        //query.push(util.format(' ,group_id="%s" ', params.group_id));
        query.push(util.format(' ,user_nm="%s" ', params.user_nm));
        query.push(util.format(' ,user_tel="%s" ', params.user_tel));
        query.push(util.format(' ,user_st="%s" ', params.user_st));
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },   
    updateUserPasswordInit:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,user_auth_pw=HEX(AES_ENCRYPT("%s", "%s")) ', params.user_auth_pw, auth.crypto_key)); 
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },
    updateUserDeviceInit:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user_device` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,user_device_sn="" ,platform="",model_num="",fcm_token="",uuid="" ');
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },             
    getUserDuplicateCheck: (params) =>  {
        let query = [];   

        query.push('  SELECT count(*) as cnt ');
        query.push('  from tb_user as AA ');
        query.push('  where 1=1 ');
        query.push(util.format(' AND AA.user_auth_id="%s" ', params.user_auth_id));

        return query.join('');          
    },
    getUserAuthIdDuplicateCheck: (params) =>  {
        let query = [];   

        query.push('  select AA.* ,BB.user_id  from ( ');
        query.push(util.format(' select "%s" as user_auth_id , 1 as id union', randomString()));
        query.push(util.format(' select "%s" as user_auth_id , 2 as id union',randomString()));
        query.push(util.format(' select "%s" as user_auth_id , 3 as id ', randomString()));
        query.push('  ) as AA ');
        query.push('  left outer join tb_user as BB   on AA.user_auth_id=BB.user_auth_id ');

        return query.join('');          
    },     
    getUserInfoList: (params) =>  {
        let query = [];  
        let ymdhhmi='%Y-%m-%d %H:%i%s';
        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push('  SELECT BB.*,DD.temperature,DD.temperature_cd,AA.organization_nm');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));

        query.push('  FROM (')
        query.push(SubQueries.setUserListAll(params));
        query.push('  ) as AA');
        query.push('  LEFT JOIN tb_user as BB ON AA.user_id=BB.user_id');
        query.push('  LEFT JOIN tb_user_device as CC ON AA.user_id=CC.user_id ');
        query.push('  LEFT JOIN tb_health as DD ON AA.user_id=DD.user_id ');

        query.push('  WHERE 1=1 AND BB.del_yn="N" ');
        query.push(util.format(' AND ( "ALL" ="%s" OR BB.user_st="%s") ',  params.user_st , params.user_st));
        if(params.search_name)  query.push(util.format(' AND (BB.user_auth_id  like "%s" OR BB.user_nm  like "%s" OR BB.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by BB.user_nm ASC ');

        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    }, 
    getUserInfoListTotalCnt: (params) =>  {
        let query = [];  

        query.push('  SELECT count(*) as total_cnt ');
        query.push('  FROM (')
        query.push(SubQueries.setUserListAll(params)); 
        query.push('  ) as AA');
        query.push('  LEFT JOIN tb_user as BB ON AA.user_id=BB.user_id');
        query.push('  LEFT JOIN tb_user_device as CC ON AA.user_id=CC.user_id ');
        query.push('  LEFT JOIN tb_health as DD ON AA.user_id=DD.user_id ');

        query.push('  WHERE 1=1 AND BB.del_yn="N" ');
        query.push(util.format(' AND ( "ALL" ="%s" OR BB.user_st="%s") ',  params.user_st , params.user_st));
        if(params.search_name)  query.push(util.format(' AND (BB.user_auth_id  like "%s" OR BB.user_nm  like "%s" OR BB.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by BB.user_nm ASC ');

        return query.join('');          
    }, 
    getUserTempLogExcelData: (params) =>  {
        let query = [];   
        let currentMonth =moment().add(0, 'M').format("YYYYMM");
        let ymdhhmi='%Y-%m-%d %H:%i';

        query.push('  SELECT BB.* ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push('  FROM tb_user as AA');
        query.push(util.format(' LEFT JOIN tb_health_log_%s as BB ON AA.user_id=BB.user_id ', currentMonth));
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        query.push('  order by BB.created_dt desc ');
        query.push('  limit 10 ');
        return query.join('');          
    },  
    getUserMainFcmToken: (params) =>  {
        let query = [];   

        query.push('  SELECT BB.* FROM tb_user as AA ');
        query.push('  left join tb_user_device as BB on AA.user_id=BB.user_id ');
        query.push('  where 1=1 ');
        query.push(util.format(' AND  AA.user_id in( %s ) ', params.user_id));

        return query.join('');          
    },  
    deleteUser : (id) => {
		let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,del_yn="Y" ');
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },  
    getUserDetailTempLogExcel: (params) =>  {
        let query = [];   
        //let currentMonth =moment().add(0, 'M').format("YYYYMM");
        let ymdhhmi='%Y-%m-%d %H:%i';
        let date_ym = getDates_yyyymm(params.start_date,params.end_date);
        let date_ym_cnt=(date_ym.length)? date_ym.length : 0;

        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        date_ym.forEach( (date ,index) => {
            params.currentMonth=date;
            query.push(SubQueries.getUserDetailTempLogUnion(params));
            if(index == (date_ym_cnt-1)) query.push(' ');
            else query.push(' union all  ');
        });
        query.push('  ) A  ');
        query.push('  ) Z ');
        //query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        //query.push('  limit 10 ');
        return query.join('');          
    },                                                   
};           


let SubQueries = {
    getUserDetailTempLogUnion:  (params) => {
        let query = [];
        let ymdhhmi='%Y-%m-%d %H:%i';
        let ymd='%Y%m%d';

        query.push('  ( SELECT BB.* ');
        query.push(util.format(',DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push('  FROM tb_user as AA');
        query.push(util.format(' LEFT JOIN tb_health_log_%s as BB ON AA.user_id=BB.user_id ', params.currentMonth));
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));  
        if(params.start_date) query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        if(params.end_date) query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd ,params.end_date.replace(/\-/g,'')));       
        query.push('  order by BB.created_dt desc ) ');

        return query.join('');
    },    
    setUserGroupRels:  (params) => {
        let query = [];
        if(!params.group_id) params.group_id = "ALL";
        if(params.group_id=="ALL"){
            query.push('  select AA.user_id, CC.organization_id ,group_concat(CC.organization_nm) as organization_nm ,group_concat(BB.group_nm) as group_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N" ');
            // query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
            query.push(util.format(' AND ( "ALL" ="%s" OR BB.group_id="%s") ', params.group_id , params.group_id));
            query.push('  group by AA.user_id ');

        }else{
            query.push('  select AA.user_id,BB.group_id , CC.organization_id , CC.organization_nm,BB.group_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N" ');
            // query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
            query.push(util.format(' AND ( "ALL" ="%s" OR BB.group_id="%s") ', params.group_id , params.group_id));
        }

        return query.join('');
    },  
    setUserGroupRelsAll:  (params) => {
        let query = [];
            query.push('  select AA.user_id from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N" ');
            query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
            query.push('  group by AA.user_id ');

        return query.join('');
    }, 
    setUserListAll:  (params) => {
        let query = [];
        if(params.organization_id=="ALL"){
            query.push('  select AA.user_id ,BB.organization_nm from tb_user as AA ');
            query.push('  left outer join ( ');
            query.push('  select ZZ.user_id , GROUP_CONCAT(ZZ.organization_nm) AS organization_nm  from ( ');
            query.push('  select AA.user_id,CC.organization_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  1=1 ');
            query.push('  and  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N" ');
            query.push('  GROUP BY AA.user_id,CC.organization_nm ASC  ');
            query.push('  ) AS ZZ GROUP BY ZZ.user_id  ');
            query.push('  ) AS BB on AA.user_id=BB.user_id  ');
        }else if(params.organization_id=="NONE"){
            query.push('  select * from ( ');
            query.push('  select AA.user_id ,BB.organization_nm from tb_user as AA ');
            query.push('  left outer join ( ');
            query.push('  select ZZ.user_id , GROUP_CONCAT(ZZ.organization_nm) AS organization_nm  from ( ');
            query.push('  select AA.user_id,CC.organization_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  1=1 ');
            query.push('  and  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N" ');
            query.push('  GROUP BY AA.user_id,CC.organization_nm ASC  ');
            query.push('  ) AS ZZ GROUP BY ZZ.user_id  ');
            query.push('  ) AS BB on AA.user_id=BB.user_id  ');
            query.push('  ) AS MM  where MM.organization_nm is null  ');
        }else{
            query.push('  select ZZ.user_id , GROUP_CONCAT(ZZ.organization_nm) AS organization_nm  from ( ');
            query.push('  select AA.user_id,CC.organization_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  1=1 ');
            query.push('  and  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N" ');
            query.push(util.format(' AND ( "ALL"="%s" OR CC.organization_id="%s") ', params.organization_id ,params.organization_id));
            query.push('  GROUP BY AA.user_id,CC.organization_nm ASC  ');
            query.push('  ) AS ZZ GROUP BY ZZ.user_id  ');
        }


        return query.join('');
    },     
    setUserCheckLimit:  (params) => {
        let query = [];

        query.push('  SELECT AA.user_id,AA.check_cd,AA.approval_cd FROM tb_user_check as AA ');
        query.push('  where AA.approval_cd = "01" ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        query.push(util.format('and TIMESTAMPDIFF(MINUTE,convert_tz(utc_timestamp(), "+00:00", "%s"),convert_tz(DATE_ADD(AA.check_dt, INTERVAL 14 DAY), "+00:00", "%s"))>0 ', params.z_value, params.z_value));        
        query.push('  order by check_dt desc  limit 1');
 
        return query.join('');
    },    
    setUserVaccineLimit:  (params) => {
        let query = [];

        query.push('  SELECT AA.user_id,AA.check_cd,AA.approval_cd FROM tb_user_vaccine as AA ');
        query.push('  where AA.approval_cd = "01" ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        query.push(util.format('and TIMESTAMPDIFF(MINUTE,convert_tz(utc_timestamp(), "+00:00", "%s"),convert_tz(DATE_ADD(AA.check_dt, INTERVAL 365 DAY), "+00:00", "%s"))>0 ', params.z_value, params.z_value));        
        query.push('  order by check_dt desc  limit 1');
 
        return query.join('');
    } 
};


function randomString() {
    let chars = "abcdefghiklmnopqrstuvwxyz";
    let ints = "0123456789";
    let string_length = 4;
    let int_length = 3;
    let randomstring = '';
    for (let i = 0; i < string_length; i++) {
        let rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    for (let i = 0; i < int_length; i++) {
        let rnum2 = Math.floor(Math.random() * ints.length);
        randomstring += ints.substring(rnum2, rnum2 + 1);
    }
    return randomstring;
}

function getDates_yyyymm(start, stop) {
    let startDate =new Date(start)
    let stopDate =new Date(stop)
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(currentDate.toISOString().substring(0, 7).replace(/\-/,''));
        currentDate.setDate(currentDate.getDate() + 1);  
    }
    return (dateArray)? Array.from(new Set(dateArray)) : [];
  }