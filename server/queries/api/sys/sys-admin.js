const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    getSysAdminLoginCheck: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.admin_auth_id,AA.admin_nm from tb_sys_admin as AA ');
        query.push(util.format(' where AA.admin_auth_id="%s" ', params.admin_auth_id));
        query.push(util.format(' AND AA.admin_auth_pw=HEX(AES_ENCRYPT("%s", "%s")) ', params.admin_auth_pw, auth.crypto_key));  
        return query.join('');          
    },   
    getSysAdminLoginNewWindow: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.organization_admin_id ,AA.admin_auth_id,BB.type ');
        query.push(util.format(' ,CAST( AES_DECRYPT(UNHEX(AA.admin_auth_pw), "%s") AS CHAR (10000) CHARACTER SET UTF8) AS admin_auth_pw',  auth.crypto_key)); 
        query.push('  FROM tb_organization_admin as AA ');
        query.push('  left join tb_organization as BB on AA.organization_id=BB.organization_id  ');
        query.push(util.format(' where AA.admin_auth_id="%s" ', params.admin_auth_id));
        query.push(util.format(' AND AA.admin_auth_pw=HEX(AES_ENCRYPT("%s", "%s")) ', params.admin_auth_pw, auth.crypto_key));  
        query.push(util.format(' AND BB.organization_auth_id="%s" ', params.organization_auth_id));
        query.push('  and AA.admin_st="1" ');
        return query.join('');          
    },   
    updateSysOrganizationAdmin:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_organization_admin` ');
        query.push(' SET ');
        query.push(util.format(' link_login_key="%s" ', params.link_login_key));
        query.push(util.format(' where organization_admin_id="%s" ', id));
        return query.join('');
    },         
}; 
