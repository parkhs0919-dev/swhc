const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createNotice: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_notice` ');
        query.push(' ( ');
        query.push(' `organization_id` ');
        query.push(' ,`title` ');
        query.push(' ,`contents` ');
        query.push(' ,`notice_gb` ');
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');               
        query.push(' )');   
           
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_id));
        query.push(util.format(' ,"%s" ', params.title));
        query.push(util.format(' ,"%s" ', params.contents));
        query.push(util.format(' ,"%s" ', params.notice_gb));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');        
        query.push(' )');           
      
        return query.join('');          
    },    
    getNoticeList: (params) =>  {
        let query = [];  
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';

        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push('  SELECT AA.* ');
        // query.push('  ,date_format(AA.created_dt, "%Y-%m-%d %H:%i") as created_dt_str');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.updated_dt, "+00:00", "%s"), "%s") as updated_dt_str ',params.z_value , ymdhhmi ));
        query.push('  FROM tb_notice as AA ');
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.organization_id="%s"  ', params.organization_id ));
        if(params.notice_gb) query.push(util.format(' AND ( "ALL"="%s" OR AA.notice_gb="%s")  ', params.notice_gb , params.notice_gb ));
        if(params.start_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        if(params.end_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,'')));             
        if(params.search_name)  query.push(util.format(' AND (AA.contents  like "%s" OR title like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by AA.created_dt DESC ');

        query.push('  ) A  ');
        query.push('  ) Z ');
        if( params.count_num) query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    }, 
    getNoticeListTotalCnt: (params) =>  {
        let query = [];  
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';

        query.push('  SELECT  ');
        query.push('  count(*) as total_cnt');
        query.push('  FROM tb_notice as AA ');
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.organization_id="%s"  ', params.organization_id ));
        if(params.notice_gb) query.push(util.format(' AND ( "ALL"="%s" OR AA.notice_gb="%s")  ', params.notice_gb , params.notice_gb ));
        if(params.start_date) query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        if(params.end_date) query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,'')));  
        if(params.search_name)  query.push(util.format(' AND (AA.contents  like "%s" OR title like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by AA.created_dt DESC ');

        return query.join('');          
    },
    getNoticeDetail: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.* FROM tb_notice as AA ');
        query.push('  where 1=1');
        query.push(util.format(' AND AA.organization_id="%s"  ', params.organization_id ))
        query.push(util.format(' AND AA.notice_id="%s"  ', params.notice_id ))
        return query.join('');          
    },  
    updateNotice:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_notice` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,title="%s" ', params.title));
        query.push(util.format(' ,contents="%s" ', params.contents));
        query.push(util.format(' where notice_id="%s" ', id));
        return query.join('');
    },                                         
};         
