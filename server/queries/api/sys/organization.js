const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    getOrganizationAll: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.organization_id,AA.organization_nm from tb_organization as AA ');
        query.push('  where 1=1 ');
        if(params.del_yn)query.push(util.format(' and ( "ALL"="%s" OR AA.del_yn="%s")  ', params.del_yn ,params.del_yn));  
        query.push('  order by AA.organization_nm asc ');
        return query.join('');          
    },   
    getOrganizationList: (params) =>  {
        let query = [];  
        let ymdhhmi='%Y-%m-%d %H:%i%s';
        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push('  select AA.* ,BB.admin_tel,BB.admin_nm,BB.admin_auth_id,BB.admin_auth_pw from tb_organization as AA ');
        query.push('  left join ( ');
        query.push('  select organization_id , admin_tel ,admin_nm ,admin_auth_id  ');
        query.push(util.format(' ,CAST( AES_DECRYPT(UNHEX(admin_auth_pw), "%s") AS CHAR (10000) CHARACTER SET UTF8) AS admin_auth_pw',  auth.crypto_key));          
        query.push('  from tb_organization_admin  ');
        query.push('  where admin_st="1"  ');
        query.push('  group by organization_id ');
        query.push('  ) as BB on AA.organization_id=BB.organization_id ');

        query.push('  WHERE 1=1 AND AA.del_yn="N" ');
        if(params.organization_id) query.push(util.format(' AND ( "ALL" ="%s" OR AA.organization_id="%s") ',  params.organization_id , params.organization_id));
        if(params.search_name)  query.push(util.format(' AND (AA.organization_nm  like "%s" OR BB.admin_tel like "%s" OR BB.admin_nm like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%','%'+params.search_name+'%'));
        query.push('  order by AA.organization_nm asc ');

        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    }, 
    getOrganizationListTotalCnt: (params) =>  {
        let query = [];  

        query.push('  SELECT count(*) as total_cnt  ');
        query.push('  from tb_organization as AA ');
        query.push('  left join ( ');
        query.push('  select organization_id , admin_tel ,admin_nm from tb_organization_admin  ');
        query.push('  where admin_st="1"  ');
        query.push('  group by organization_id ');
        query.push('  ) as BB on AA.organization_id=BB.organization_id ');

        query.push('  WHERE 1=1 AND AA.del_yn="N" ');
        if(params.organization_id) query.push(util.format(' AND ( "ALL" ="%s" OR AA.organization_id="%s") ',  params.organization_id , params.organization_id));
        if(params.search_name)  query.push(util.format(' AND (AA.organization_nm  like "%s" OR BB.admin_tel like "%s" "%s" OR BB.admin_nm like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%','%'+params.search_name+'%'));
        query.push('  order by AA.organization_nm asc ');

        return query.join('');          
    }, 
    deleteOrganization : (id) => {
		let query = [];
        query.push(' UPDATE `tb_organization` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,del_yn="Y" ');
        query.push(util.format(' where organization_id="%s" ', id));
        return query.join('');
    },      
}; 
