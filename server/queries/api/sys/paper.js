const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {   
    getUserCheckUnionAll: (params) =>  {
        let query = [];  
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';
        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push(' SELECT * from ( ');
        query.push(' SELECT AA.user_check_id as  paper_id, "check" as type ,AA.check_cd,AA.created_dt ,BB.organization_nm,CC.user_nm,CC.user_tel ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.check_dt, "+00:00", "%s"), "%s") as check_dt_str ',params.z_value , ymd ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(DATE_ADD(AA.check_dt, INTERVAL 14 DAY), "+00:00", "%s"), "%s") as expiry_dt_str ',params.z_value , ymd ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' ,CASE  ');
        query.push(util.format(' WHEN TIMESTAMPDIFF(MINUTE,convert_tz(DATE_ADD(AA.check_dt, INTERVAL 14 DAY), "+00:00", "%s"),convert_tz(utc_timestamp(), "+00:00", "%s"))>0 ', params.z_value, params.z_value));
        query.push(' THEN "03" ELSE AA.approval_cd ');
        query.push(' END AS approval_cd ,CC.user_auth_id ');
   
        query.push(' from ( ');
        query.push(SubQueries.setUserGroupRels(params));
        query.push(' ) as BB   ');
        query.push(' left join  tb_user_check as AA  on AA.user_id=BB.user_id ');
        query.push(' left join tb_user as CC  on AA.user_id=CC.user_id ');
        query.push(' where  1=1 ');
        if(params.approval_cd) query.push(util.format(' AND ( "ALL"="%s" OR AA.approval_cd="%s") ', params.approval_cd, params.approval_cd));
        if(params.start_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        if(params.end_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,'')));   
        if(params.search_name)query.push(util.format(' and (CC.user_nm like "%s" OR  CC.user_tel like "%s") ', '%'+params.search_name+'%' , '%'+params.search_name+'%')); 
   
        query.push(' union all  ');

        query.push(' SELECT AA.user_vaccine_id as  paper_id , "vaccine" as type , AA.check_cd,AA.created_dt ,BB.organization_nm ,CC.user_nm,CC.user_tel ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.check_dt, "+00:00", "%s"), "%s") as check_dt_str ',params.z_value , ymd ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(DATE_ADD(AA.check_dt, INTERVAL 12 MONTH), "+00:00", "%s"), "%s") as expiry_dt_str ',params.z_value , ymd ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' ,CASE  ');
        query.push(util.format(' WHEN TIMESTAMPDIFF(MINUTE,convert_tz(DATE_ADD(AA.check_dt, INTERVAL 12 MONTH), "+00:00", "%s"),convert_tz(utc_timestamp(), "+00:00", "%s"))>0 ', params.z_value, params.z_value));
        query.push(' THEN "03" ELSE AA.approval_cd ');
        query.push(' END AS approval_cd ,CC.user_auth_id ');
        query.push(' from ( ');
        query.push(SubQueries.setUserGroupRels(params));
        query.push(' ) as BB   ');
        query.push(' left join  tb_user_vaccine as AA  on AA.user_id=BB.user_id ');
        query.push(' left join tb_user as CC  on AA.user_id=CC.user_id ');        
        query.push(' where  1=1 ');
        if(params.approval_cd) query.push(util.format(' AND ( "ALL"="%s" OR AA.approval_cd="%s") ', params.approval_cd, params.approval_cd));
        if(params.start_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        if(params.end_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,''))); 
        if(params.search_name)query.push(util.format(' and (CC.user_nm like "%s" OR  CC.user_tel like "%s") ', '%'+params.search_name+'%' , '%'+params.search_name+'%'));        

        query.push(' ) as ZZ ');
        if(params.type) query.push(util.format(' where ( "ALL"="%s" OR ZZ.type="%s") ', params.type, params.type));
        query.push(' order by ZZ.created_dt desc  ');

        query.push('  ) A  ');
        query.push('  ) Z ');
        if(params.count_num) query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    },    
    getUserCheckUnionAllTotalCnt: (params) =>  {
        let query = [];  
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';       

        query.push(' SELECT count(*) as total_cnt from ( ');
        query.push(' SELECT AA.user_check_id as  paper_id, "check" as type ,AA.check_cd,AA.created_dt ,BB.organization_nm,CC.user_nm,CC.user_tel ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.check_dt, "+00:00", "%s"), "%s") as check_dt_str ',params.z_value , ymd ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(DATE_ADD(AA.check_dt, INTERVAL 14 DAY), "+00:00", "%s"), "%s") as expiry_dt_str ',params.z_value , ymd ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' ,CASE  ');
        query.push(util.format(' WHEN TIMESTAMPDIFF(MINUTE,convert_tz(DATE_ADD(AA.check_dt, INTERVAL 14 DAY), "+00:00", "%s"),convert_tz(utc_timestamp(), "+00:00", "%s"))>0 ', params.z_value, params.z_value));
        query.push(' THEN "03" ELSE AA.approval_cd ');
        query.push(' END AS approval_cd  ');
   
        query.push(' from ( ');
        query.push(SubQueries.setUserGroupRels(params));
        query.push(' ) as BB   ');
        query.push(' left join  tb_user_check as AA  on AA.user_id=BB.user_id ');
        query.push(' left join tb_user as CC  on AA.user_id=CC.user_id ');
        query.push(' where  1=1 ');
        if(params.approval_cd) query.push(util.format(' AND ( "ALL"="%s" OR AA.approval_cd="%s") ', params.approval_cd, params.approval_cd));
        if(params.start_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        if(params.end_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,''))); 
        if(params.search_name)query.push(util.format(' and (CC.user_nm like "%s" OR  CC.user_tel like "%s") ', '%'+params.search_name+'%' , '%'+params.search_name+'%')); 
   
        query.push(' union all  ');
 
        query.push(' SELECT AA.user_vaccine_id as  paper_id , "vaccine" as type , AA.check_cd,AA.created_dt ,BB.organization_nm ,CC.user_nm,CC.user_tel ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.check_dt, "+00:00", "%s"), "%s") as check_dt_str ',params.z_value , ymd ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(DATE_ADD(AA.check_dt, INTERVAL 12 MONTH), "+00:00", "%s"), "%s") as expiry_dt_str ',params.z_value , ymd ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(' ,CASE  ');
        query.push(util.format(' WHEN TIMESTAMPDIFF(MINUTE,convert_tz(DATE_ADD(AA.check_dt, INTERVAL 12 MONTH), "+00:00", "%s"),convert_tz(utc_timestamp(), "+00:00", "%s"))>0 ', params.z_value, params.z_value));
        query.push(' THEN "03" ELSE AA.approval_cd ');
        query.push(' END AS approval_cd  ');
        query.push(' from ( ');
        query.push(SubQueries.setUserGroupRels(params));
        query.push(' ) as BB   ');
        query.push(' left join  tb_user_vaccine as AA  on AA.user_id=BB.user_id ');
        query.push(' left join tb_user as CC  on AA.user_id=CC.user_id ');        
        query.push(' where  1=1 ');
        if(params.approval_cd) query.push(util.format(' AND ( "ALL"="%s" OR AA.approval_cd="%s") ', params.approval_cd, params.approval_cd));
        if(params.start_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        if(params.end_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,''))); 
        if(params.search_name)query.push(util.format(' and (CC.user_nm like "%s" OR  CC.user_tel like "%s") ', '%'+params.search_name+'%' , '%'+params.search_name+'%'));        

        query.push(' ) as ZZ ');
        if(params.type) query.push(util.format(' where ( "ALL"="%s" OR ZZ.type="%s") ', params.type, params.type));
        //     query.push(' order by ZZ.created_dt desc  ');
        return query.join('');          
    },     
    getPaperDetail: (params) =>  {
        let query = [];   
        let ymd='%Y-%m-%d';
        let ymdhhmi='%Y-%m-%d %H:%i';

        if(params.type=="check"){
            query.push(' SELECT AA.user_check_id as  paper_id, "check" as type ,AA.check_cd,AA.created_dt,CC.user_nm,CC.user_tel,CC.user_id,AA.reason ');
            query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.check_dt, "+00:00", "%s"), "%s") as check_dt_str ',params.z_value , ymd ));
            query.push(util.format(' ,DATE_FORMAT(convert_tz(DATE_ADD(AA.check_dt, INTERVAL 14 DAY), "+00:00", "%s"), "%s") as expiry_dt_str ',params.z_value , ymd ));
            query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
            query.push(' ,BB.* ');
            query.push(' ,AA.approval_cd  ');
            query.push(' from tb_user_check as AA   ');
            query.push(' left join tb_user_check_file as BB  on AA.user_check_id=BB.user_check_id ');
            query.push(' left join tb_user as CC  on AA.user_id=CC.user_id ');
            query.push(' where  1=1 ');
            if(params.paper_id) query.push(util.format(' AND AA.user_check_id="%s" ', params.paper_id)); 
        
        }else if(params.type=="vaccine"){

            query.push(' SELECT AA.user_vaccine_id as  paper_id , "vaccine" as type , AA.check_cd,AA.created_dt,CC.user_nm,CC.user_tel,CC.user_id,AA.reason ');
            query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.check_dt, "+00:00", "%s"), "%s") as check_dt_str ',params.z_value , ymd ));
            query.push(util.format(' ,DATE_FORMAT(convert_tz(DATE_ADD(AA.check_dt, INTERVAL 12 MONTH), "+00:00", "%s"), "%s") as expiry_dt_str ',params.z_value , ymd ));
            query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
            query.push(' ,BB.* ');
            query.push(' ,AA.approval_cd  ');
            query.push(' from tb_user_vaccine as AA   ');
            query.push(' left join tb_user_vaccine_file as BB  on AA.user_vaccine_id=BB.user_vaccine_id ');
            query.push(' left join tb_user as CC  on AA.user_id=CC.user_id ');        
            query.push(' where  1=1 ');
            if(params.paper_id) query.push(util.format(' AND AA.user_vaccine_id="%s" ', params.paper_id));      

        }

        return query.join('');          
    }, 
    updatePaper:  (params, id) => {
        let query = [];
        if(params.type=="check"){
            query.push(' UPDATE `tb_user_check` ');
            query.push(' SET ');
            query.push(' updated_dt=utc_timestamp() ');
            query.push(util.format(' ,approval_cd="%s" ', params.approval_cd));
            if( params.check_cd)query.push(util.format(' ,check_cd="%s" ', params.check_cd));
            if( params.check_dt)query.push(util.format(' ,check_dt="%s" ', params.check_dt));
            if( params.reason)query.push(util.format(' ,reason="%s" ', params.reason));

            query.push(util.format(' where user_check_id="%s" ', id));
        }else if(params.type=="vaccine"){
            query.push(' UPDATE `tb_user_vaccine` ');
            query.push(' SET ');
            query.push(' updated_dt=utc_timestamp() ');
            query.push(util.format(' ,approval_cd="%s" ', params.approval_cd));
            if( params.check_cd)query.push(util.format(' ,check_cd="%s" ', params.check_cd));
            if( params.check_dt)query.push(util.format(' ,check_dt="%s" ', params.check_dt));
            if( params.reason)query.push(util.format(' ,reason="%s" ', params.reason));
            
            query.push(util.format(' where user_vaccine_id="%s" ', id));
        }    

        return query.join('');
    },      
};          


let SubQueries = {
    setUserGroupRels:  (params) => {
        let query = [];
            query.push('  select AA.user_id,group_concat(CC.organization_nm) as organization_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N" ');
            query.push(util.format(' AND ( "ALL"="%s" OR CC.organization_id="%s")  ', params.organization_id, params.organization_id));
            if( params.group_id) query.push(util.format(' AND ( "ALL"="%s" OR BB.group_id="%s")  ', params.group_id, params.group_id));
            query.push('  group by AA.user_id ');

        return query.join('');
    },  

};