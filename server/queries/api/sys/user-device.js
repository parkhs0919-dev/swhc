const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {   
    getUserDeviceSearch: (params) =>  {
        let query = [];  
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';

        // query.push('  SELECT * FROM  ( ');
        // query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        // query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push(' select AA.user_id,AA.user_auth_id,AA.user_nm,user_tel,BB.user_device_sn,BB.model_num  ');
        query.push(' from tb_user as AA ');
        query.push(' left join tb_user_device as BB on AA.user_id=BB.user_id ');
        query.push('  WHERE 1=1 ');       
        if(params.search_name)  query.push(util.format(' AND (BB.user_device_sn  like "%s" OR BB.model_num like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));

        // query.push('  ) A  ');
        // query.push('  ) Z ');
        // if( params.count_num) query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    },  
    deleteUserDevice:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user_device` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,user_device_sn="%s" ', params.user_device_sn));
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },                                         
};         
