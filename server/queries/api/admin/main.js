const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    getMainUserCount: (params) =>  {
        let query = [];   
        query.push('  SELECT  ');
        query.push('  COUNT(*) as total, ');
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T1" THEN 1 ELSE 0 END) as t1_cnt, ');
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T2" THEN 1 ELSE 0 END) as t2_cnt, ');
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T3" THEN 1 ELSE 0 END) as t3_cnt, ');
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T4" THEN 1 ELSE 0 END) as t4_cnt, '); 
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T5" THEN 1 ELSE 0 END) as t5_cnt, ');
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T0" THEN 1 ELSE 0 END) as t0_cnt, ');        
        query.push('  SUM(CASE WHEN BB.device_connect_st = "0" THEN 1 ELSE 0 END) as device_connect_cnt ');
        query.push('  from (  ');
        query.push('  select AA.user_id from tb_user_group_rels as AA ');
        query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id   ');
        query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
        query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N"  ');
        query.push(util.format(' AND CC.organization_id="%s"  ' , params.organization_id ));
        if(params.group_id) query.push(util.format(' AND ( "ALL"="%s" OR BB.group_id="%s" )  ' , params.group_id ));
        query.push('  group by AA.user_id  ');
        query.push('  ) as AA  ');
        query.push('  left join tb_health as BB on AA.user_id=BB.user_id  ');
        
        return query.join('');          
    }, 
    getMainUserCountGroup: (params) =>  {
        let query = [];   
        query.push(' select AA.group_id,AA.group_nm  ');
        query.push(' ,ifnull(BB.total,0) as total  ');
        query.push(' ,ifnull(BB.t1_cnt,0) as t1_cnt  ');
        query.push(' ,ifnull(BB.t2_cnt,0) as t2_cnt  ');
        query.push(' ,ifnull(BB.t3_cnt,0) as t3_cnt  ');
        query.push(' ,ifnull(BB.t4_cnt,0) as t4_cnt  ');
        query.push(' ,ifnull(BB.t5_cnt,0) as t5_cnt  ');
        query.push(' ,ifnull(BB.t0_cnt,0) as t0_cnt ');
        query.push(' ,ifnull(BB.device_connect_cnt,0) as device_connect_cnt ');
        query.push(' from ( ');
        query.push(' select * from tb_group ');
        query.push(' where 1=1 ');
        query.push(util.format(' AND organization_id="%s"  ' , params.organization_id ));
        query.push(' ) as AA ');
        query.push(' left outer join (  ');
        query.push('  SELECT  ');
        query.push('   AA.group_id,  ');
        query.push('  COUNT(*) as total, ');
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T1" THEN 1 ELSE 0 END) as t1_cnt, ');
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T2" THEN 1 ELSE 0 END) as t2_cnt, ');
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T3" THEN 1 ELSE 0 END) as t3_cnt, ');
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T4" THEN 1 ELSE 0 END) as t4_cnt, '); 
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T5" THEN 1 ELSE 0 END) as t5_cnt, ');
        query.push('  SUM(CASE WHEN BB.device_connect_st = "1" and BB.temperature_cd = "T0" THEN 1 ELSE 0 END) as t0_cnt, ');        
        query.push('  SUM(CASE WHEN BB.device_connect_st = "0" THEN 1 ELSE 0 END) as device_connect_cnt ');
        query.push('  from (  ');
        query.push('  select AA.user_id,AA.group_id  from tb_user_group_rels as AA ');
        query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id   ');
        query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
        query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N"   ');
        query.push(util.format(' AND CC.organization_id="%s"  ' , params.organization_id ));
        if(params.group_id) query.push(util.format(' AND ( "ALL"="%s" OR BB.group_id="%s" )  ' , params.group_id ));
        query.push('  group by AA.user_id,AA.group_id  ');
        query.push('  ) as AA  ');
        query.push('  left join tb_health as BB on AA.user_id=BB.user_id  ');
        query.push('  group by AA.group_id  ');
        query.push('  ) as BB  on AA.group_id=BB.group_id ');
        query.push(' where 1=1 AND AA.del_yn="N"  ');

        return query.join('');          
    }, 
    getMainUserApplyCdCount: (params) =>  {
        let query = [];   
        query.push(' select ZZ.organization_id,COUNT(*) as total,  ');
        query.push(' SUM(CASE WHEN ZZ.apply_cd = "A1"  THEN 1 ELSE 0 END) as a1_cnt,   ');
        query.push(' SUM(CASE WHEN ZZ.apply_cd = "A3"  THEN 1 ELSE 0 END) as a2_cnt  ');
        query.push(' from (  ');
        query.push(' SELECT AA.organization_id,AA.user_id,AA.grade,BB.user_nm,BB.user_tel,CC.grade_cd     ');
        query.push(' ,CASE WHEN  AA.grade="G1" and ( CC.grade_cd="G1") THEN "A1"  ');
        query.push(' WHEN AA.grade="G2" and ( CC.grade_cd="G1" OR CC.grade_cd="G2") THEN "A1"   ');
        query.push(' WHEN AA.grade="G3" and ( CC.grade_cd="G1" OR CC.grade_cd="G2" OR CC.grade_cd="G3") THEN "A1"  ELSE "A3" END as apply_cd  ');
        query.push(' FROM ( ');
        query.push(' select AA.user_id,CC.grade,CC.organization_id from tb_user_group_rels as AA ');
        query.push(' LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
        query.push(' LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id   ');
        query.push(' where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N"   ');
        query.push(util.format(' AND CC.organization_id="%s"  ' , params.organization_id ));
        query.push(' group by AA.user_id   ');
        query.push(' ) as AA  ');
        query.push(' left join  tb_user AS BB on AA.user_id=BB.user_id   ');
        query.push(' left join  tb_health AS CC ON AA.user_id = CC.user_id  ');
        query.push(' ) as ZZ  ');
        query.push(' group by ZZ.organization_id ');

        return query.join('');          
    }, 
    // getMainUser: (params) =>  {
    //     let query = [];   
    //     query.push(' select  * from tb_group as AA  ');
    //     query.push('  left join tb_user as BB on AA.group_id=BB.group_id ');
    //     query.push('  left join tb_health as CC on BB.user_id=CC.user_id ');
    //     query.push('  WHERE 1=1  AND AA.del_yn="N"  AND BB.del_yn="N" AND BB.user_st="1" ');
    //     query.push(util.format(' AND AA.organization_id="%s"  ' , params.organization_id ));
    //     if(params.group_id) query.push(util.format(' and ( "ALL"="%s" OR AA.group_id="%s")   ' , params.group_id, params.group_id ));
    //     if(params.temperature_cd) query.push(util.format(' and ( "ALL"="%s" OR CC.temperature_cd="%s")  ' , params.temperature_cd , params.temperature_cd ));
    //     if(params.device_connect_st) query.push(util.format(' and ( "ALL"="%s" OR CC.device_connect_st="%s")  ' , params.device_connect_st , params.device_connect_st ));
    //     query.push(' and BB.user_id is not null ');
    //     return query.join('');          
    // }, 
    // getMainUser2: (params) =>  {
    //     let query = [];   
    //     query.push(' select  * from tb_group as AA  ');
    //     query.push('  left join tb_user as BB on AA.group_id=BB.group_id ');
    //     query.push('  left join tb_health as CC on BB.user_id=CC.user_id ');
    //     query.push('  WHERE 1=1  AND AA.del_yn="N"  AND BB.del_yn="N" AND BB.user_st="1" ');
    //     query.push(util.format(' AND AA.organization_id="%s"  ' , params.organization_id ));
    //     if(params.group_id) query.push(util.format(' and ( "ALL"="%s" OR AA.group_id="%s")   ' , params.group_id, params.group_id ));
    //     if(params.type!="ALL"){
    //         if(params.type=="T1"||params.type=="T2"||params.type=="T3") {
    //             query.push('  and ( CC.temperature_cd="T3" OR CC.temperature_cd="T2" OR CC.temperature_cd="T1")  and CC.device_connect_st="1" ');
    //         }else if(params.type=="T4"){
    //             query.push('  and ( CC.temperature_cd="T4" )  and CC.device_connect_st="1" ');
    //         }else if(params.type=="T0"){
    //             query.push('  and ( CC.temperature_cd="T5" OR CC.temperature_cd="T0" )  and CC.device_connect_st="1" ');
    //         }else if(params.type=="device"){
    //             query.push('  and CC.device_connect_st="0" ');
    //         }
    //     }
    //     query.push(' and BB.user_id is not null ');
    //     return query.join('');          
    // },                                 
};           



let SubQueries = {  
    setUserGroupRelsAll:  (params) => {
        let query = [];
            query.push('  select AA.user_id from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N" ');
            query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
            query.push('  group by AA.user_id ');

        return query.join('');
    },  
    setUserGroupRels:  (params) => {
        let query = [];
            query.push('  select AA.user_id from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N" ');
            query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
            query.push('  group by AA.user_id ');

        return query.join('');
    }, 
};
