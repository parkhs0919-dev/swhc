const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserEvent: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_event` ');
        query.push(' ( ');
        query.push(' `organization_id` ');
        query.push(' ,`start_date` ');
        query.push(' ,`end_date` ');
        query.push(' ,`week` ');   
        query.push(' ,`start_time` ');   
        query.push(' ,`end_time` ');    
        query.push(' ,`visit_time` ');    
        query.push(' ,`start_time_str` ');   
        query.push(' ,`end_time_str` ');              
        query.push(' ,`visit_time_str` ');  
        query.push(' ,`user_event_st` '); 
        query.push(' ,`title` '); 
        query.push(' ,`created_dt` ');    
        query.push(' ,`updated_dt` ');           
        query.push(' )');   

        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_id));
        query.push(util.format(' ,"%s" ', params.start_date));
        query.push(util.format(' ,"%s" ', params.end_date));
        query.push(util.format(' ,"%s" ', params.week));
        query.push(util.format(' ,"%s" ', params.start_time));
        query.push(util.format(' ,"%s" ', params.end_time));
        query.push(util.format(' ,"%s" ', params.visit_time));  
        query.push(util.format(' ,"%s" ', params.start_time_str));
        query.push(util.format(' ,"%s" ', params.end_time_str));        
        query.push(util.format(' ,"%s" ', params.visit_time_str));  
        query.push(util.format(' ,"%s" ', params.user_event_st));   
        query.push(util.format(' ,"%s" ', params.title));  
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');
        query.push(' )');       
             
        return query.join('');          
    }, 
    updateUserEvent:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user_event` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,title="%s" ', params.title));  
        query.push(util.format(' ,start_date="%s" ', params.start_date));  
        query.push(util.format(' ,end_date="%s" ', params.end_date));  
        query.push(util.format(' ,week="%s" ', params.week));  
        query.push(util.format(' ,start_time="%s" ', params.start_time));   
        query.push(util.format(' ,end_time="%s" ', params.end_time));
        query.push(util.format(' ,visit_time="%s" ', params.visit_time)); 
        query.push(util.format(' ,start_time_str="%s" ', params.start_time_str));   
        query.push(util.format(' ,end_time_str="%s" ', params.end_time_str)); 
        query.push(util.format(' ,visit_time_str="%s" ', params.visit_time_str)); 
        query.push(util.format(' ,user_event_st="%s" ', params.user_event_st));  
        query.push(util.format(' where user_event_id="%s" ', id));
        return query.join('');
    },       
    getUserEventList: (params) =>  {
        let query = [];  
        query.push('  SELECT AA.*,BB.group_nm FROM tb_user_event as AA ');
        query.push('  left outer join ( ');
        query.push('  select  AA.user_event_id,GROUP_CONCAT(BB.group_nm SEPARATOR ",") as group_nm  from tb_user_event_rels as AA ');
        query.push('  left join tb_group as BB on AA.group_id=BB.group_id ');
        query.push('  group by  AA.user_event_id ');
        query.push('  )as BB on AA.user_event_id=BB.user_event_id ');
        query.push('  WHERE 1=1 AND AA.del_yn="N" ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id ));
        query.push(util.format(' AND ( "ALL" ="%s" OR AA.user_event_st="%s") ',  params.user_event_st , params.user_event_st));
        if(params.search_name)  query.push(util.format(' AND (AA.title  like "%s" OR BB.group_nm like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));

        if(params.group_id!="ALL"){
            query.push('  AND AA.user_event_id in ( ');
            query.push('  select DISTINCT  user_event_id as user_event_id from tb_user_event_rels ');
            query.push('  where 1=1 ');
            query.push(util.format(' AND organization_id="%s" ', params.organization_id ));
            query.push(util.format(' AND ( "ALL"="%s" OR group_id ="%s") ', params.group_id , params.group_id ));
            query.push('  ) ');
        } 
        query.push('  order by AA.user_event_id DESC ');

        return query.join('');          
    },    
    deleteUserEvent : (id) => {
		let query = [];
        query.push(' UPDATE `tb_user_event` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,del_yn="Y" ');
        query.push(util.format(' where user_event_id="%s" ', id));
        return query.join('');
    },              
};         