const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createHealth: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_health` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        if( params.temperature) query.push(' ,`temperature` ');
        if( params.heart_rat) query.push(' ,`heart_rat` ');
        if( params.temperature_cd) query.push(' ,`temperature_cd` ');      
        query.push(' ,`updated_dt` ');                
        query.push(' )');   
           
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_id));
        if( params.temperature) query.push(util.format(' ,"%s" ', params.temperature));
        if( params.heart_rat) query.push(util.format(' ,"%s" ', params.heart_rat));
        if( params.temperature_cd) query.push(util.format(' ,"%s" ', params.temperature_cd));     
        query.push(' ,utc_timestamp() ');               
        query.push(' )');           
      
        return query.join('');          
    }, 
    cronInitHealthCheck:  () => {
        let query = [];
        query.push(' UPDATE `tb_health` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,temperature="0" ');
        query.push(' ,heart_rat="0" ');
        query.push(' ,device_connect_st="0" ');
        query.push(' ,network_connect_st="0" ');
        query.push(' ,temperature_cd="T0" ');
        query.push(' ,battery="0" ');
        query.push(' where 1=1 ');
        query.push(' AND  TIMESTAMPDIFF(MINUTE,convert_tz(updated_dt, "+00:00", "+09:00"),convert_tz(utc_timestamp(), "+00:00", "+09:00"))>30 ');
        return query.join('');
    }, 
    cronInitHealthGradeCheck:  () => {
        let query = [];
        query.push(' UPDATE `tb_health` ');
        query.push(' SET ');
        query.push(' grade_dt=utc_timestamp() ');
        query.push(' ,status_cd="S0" ');
        query.push(' ,grade_cd="G0" ');

        query.push(' where 1=1 ');
        query.push(' AND  TIMESTAMPDIFF(MINUTE,convert_tz(grade_dt, "+00:00", "+09:00"),convert_tz(utc_timestamp(), "+00:00", "+09:00"))>300 ');
        return query.join('');
    },                                                 
};         
