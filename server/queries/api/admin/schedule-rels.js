const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createScheduleRels: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_schedule_rels` ');
        query.push(' ( ');
        query.push(' `organization_id` ');
        query.push(' ,`schedule_id` ');
        query.push(' ,`group_id` ');
        query.push(' )');   
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_id));
        query.push(util.format(' ,"%s" ', params.schedule_id));
        query.push(util.format(' ,"%s" ', params.group_id));
        query.push(' )');       
        return query.join('');          
    }, 
    getScheduleRelsLeft: (params) =>  {
        let query = [];   
        query.push('  select AA.group_id,AA.group_nm ');
        query.push('  from tb_group as AA  ');
        query.push('  where 1=1 AND del_yn="N" ');
        query.push(util.format(' and AA.organization_id="%s" ', params.organization_id));
        if(params.search_name)  query.push(util.format(' AND  AA.group_nm like "%s" ' ,'%'+params.search_name+'%' ));
        query.push('  and group_id not in ( ');
        query.push('  select group_id from tb_schedule_rels   ');
        query.push(util.format(' where organization_id="%s" ', params.organization_id));
        if(params.schedule_id) query.push(util.format(' and schedule_id="%s" ', params.schedule_id));
        query.push('  ) ');
        
        return query.join('');          
    },
    getScheduleRelsRight: (params) =>  {
        let query = [];   
        query.push('  select AA.group_id,AA.group_nm ');
        query.push('  from tb_group as AA  ');
        query.push('  where 1=1 AND del_yn="N" ');
        query.push(util.format(' and AA.organization_id="%s" ', params.organization_id));
        query.push('  and group_id in ( ');
        query.push('  select group_id from tb_schedule_rels   ');
        query.push(util.format(' where organization_id="%s" ', params.organization_id));
        if(params.schedule_id) query.push(util.format(' and schedule_id="%s" ', params.schedule_id));
        query.push('  ) ');
        
        return query.join('');          
    },    
    getScheduleListMessage: (params) =>  {
        let query = [];   
        query.push('  select AA.group_id,AA.group_nm  from tb_group as AA ');
        query.push('  where 1=1 AND del_yn="N" ');
        query.push(util.format(' and AA.organization_id="%s" ', params.organization_id));     
        return query.join('');          
    },      
    getScheduleRelsList: (params) =>  {
        let query = [];   
        query.push('  select AA.group_id,AA.group_nm,BB.schedule_id  from tb_group as AA ');
        query.push('  left outer join ( ');
        query.push('  select * from tb_schedule_rels ');
        query.push(util.format(' where  schedule_id="%s" ', params.schedule_id));
        query.push('  ) as BB on AA.group_id =BB.group_id and  AA.organization_id =BB.organization_id ');
        query.push('  where 1=1 AND del_yn="N" ');
        query.push(util.format(' and AA.organization_id="%s" ', params.organization_id));
        
        return query.join('');          
    },    
    deleteScheduleRels: (params,id) =>  {
        let query = [];   
        query.push('  delete from  tb_schedule_rels ');
        query.push('  where 1=1 ');
        query.push(util.format(' and organization_id="%s" ', params.organization_id));
        query.push(util.format(' and schedule_id="%s" ', id));        
        return query.join('');          
    },                
};         