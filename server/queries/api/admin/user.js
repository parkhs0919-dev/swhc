const util = require('util');
const auth = CONFIG.common.auth;
const moment = require('moment');

module.exports = {
    createUserInfo: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user` ');
        query.push(' ( ');
        query.push(' `organization_id` ');        
        query.push(' ,`group_id` ');
        query.push(' ,`user_auth_id` ');
        query.push(' ,`user_auth_pw` ');
        query.push(' ,`user_nm` ');
        query.push(' ,`user_tel` ');
        query.push(' ,`user_st` ');
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');                                    
        query.push(' )');   
        
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_id));
        query.push(util.format(' ,"%s" ', params.group_id));
        query.push(util.format(' ,"%s" ', params.user_auth_id));
        query.push(util.format(' ,HEX(AES_ENCRYPT("%s", "%s")) ', params.admin_auth_pw, auth.crypto_key));  
        query.push(util.format(' ,"%s" ', params.user_nm));
        query.push(util.format(' ,"%s" ', params.user_tel));
        query.push(util.format(' ,"%s" ', params.user_st));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');        
        query.push(' )');       
        
        return query.join('');          
    },  
    getUserListAll: (params) =>  {
        let query = [];  
        let ymdhhmi='%Y-%m-%d %H:%i:%s';

        query.push('  SELECT AA.user_id ,BB.user_nm, BB.user_tel ,CC.user_device_sn,AA.group_nm ,DD.temperature,DD.device_connect_st ');
        query.push('  ,DD.heart_rat,DD.temperature_cd,DD.grade_cd  ');
        query.push(util.format(',DATE_FORMAT(convert_tz(DD.updated_dt, "+00:00", "%s"), "%s") as updated_dt_str ',params.z_value , ymdhhmi ));  
        
        query.push('  FROM (')
        query.push(SubQueries.setUserListAll(params));
        query.push('  ) as AA');
        query.push('  LEFT JOIN tb_user as BB ON AA.user_id=BB.user_id');
        query.push('  LEFT JOIN tb_user_device as CC ON AA.user_id=CC.user_id ');
        query.push('  LEFT JOIN tb_health as DD ON AA.user_id=DD.user_id ');

        query.push('  WHERE 1=1 AND BB.del_yn="N" ');
        if(params.grade_cd) query.push(util.format(' AND ( "ALL" ="%s" OR DD.grade_cd="%s") ',  params.grade_cd , params.grade_cd));
        //query.push(util.format(' AND ( "ALL" ="%s" OR CC.device_connect_st="%s") ',  params.device_connect_st , params.device_connect_st));
        if(params.search_name)  query.push(util.format(' AND (BB.user_nm  like "%s" OR BB.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by BB.user_nm ASC ');


        return query.join('');          
    },    
    getUserList: (params) =>  {
        let query = [];  
        let ymdhhmi='%Y-%m-%d %H:%i:%s';
        
        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push('  SELECT AA.user_id ,BB.user_auth_id,BB.user_nm, BB.user_tel ,CC.user_device_sn,AA.group_nm ,DD.temperature,DD.device_connect_st ');
        query.push('  ,DD.heart_rat,DD.temperature_cd,DD.grade_cd  ');
        query.push(util.format(',DATE_FORMAT(convert_tz(DD.updated_dt, "+00:00", "%s"), "%s") as updated_dt_str ',params.z_value , ymdhhmi ));  
        
        query.push('  FROM (')
        query.push(SubQueries.setUserListAll(params));
        query.push('  ) as AA'); 
        query.push('  LEFT JOIN tb_user as BB ON AA.user_id=BB.user_id');
        query.push('  LEFT JOIN tb_user_device as CC ON AA.user_id=CC.user_id ');
        query.push('  LEFT JOIN tb_health as DD ON AA.user_id=DD.user_id ');

        query.push('  WHERE 1=1 AND BB.del_yn="N" ');
        if(params.status && params.status!="ALL"){
            if(params.status=="1") query.push(' AND  DD.device_connect_st="1" and (DD.temperature_cd="T1" OR DD.temperature_cd="T2" OR DD.temperature_cd="T3" OR DD.temperature_cd="T4") '); 
            else if(params.status=="2") query.push(' AND  DD.device_connect_st="1" and (DD.temperature_cd="T1" OR DD.temperature_cd="T2" OR DD.temperature_cd="T3") '); 
            else if(params.status=="3") query.push(' AND  DD.device_connect_st="1" and DD.temperature_cd="T4" '); 
            else if(params.status=="4") query.push(' AND  DD.device_connect_st="1" and (DD.temperature_cd="T0" OR DD.temperature_cd="T5" ) '); 
            else if(params.status=="5") query.push(' AND  DD.device_connect_st="0" ');         
        }
        
        if(params.grade_cd) query.push(util.format(' AND ( "ALL" ="%s" OR DD.grade_cd="%s") ',  params.grade_cd , params.grade_cd));
        if(params.search_name)  query.push(util.format(' AND (BB.user_auth_id like "%s" OR BB.user_nm  like "%s" OR BB.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by BB.user_nm ASC ');

        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    }, 
    getUserListTotalCnt: (params) =>  {
        let query = [];  

        query.push('  SELECT count(*) as total_cnt ');
        query.push('  FROM (')
        query.push(SubQueries.setUserListAll(params));
        query.push('  ) as AA');
        query.push('  LEFT JOIN tb_user as BB ON AA.user_id=BB.user_id');
        query.push('  LEFT JOIN tb_user_device as CC ON AA.user_id=CC.user_id ');
        query.push('  LEFT JOIN tb_health as DD ON AA.user_id=DD.user_id ');

        query.push('  WHERE 1=1 AND BB.del_yn="N" ');
        if(params.status && params.status!="ALL"){
            if(params.status=="1") query.push(' AND  DD.device_connect_st="1" and (DD.temperature_cd="T1" OR DD.temperature_cd="T2" OR DD.temperature_cd="T3" OR DD.temperature_cd="T4") '); 
            else if(params.status=="2") query.push(' AND  DD.device_connect_st="1" and (DD.temperature_cd="T1" OR DD.temperature_cd="T2" OR DD.temperature_cd="T3") '); 
            else if(params.status=="3") query.push(' AND  DD.device_connect_st="1" and DD.temperature_cd="T4" '); 
            else if(params.status=="4") query.push(' AND  DD.device_connect_st="1" and (DD.temperature_cd="T0" OR DD.temperature_cd="T5" ) '); 
            else if(params.status=="5") query.push(' AND  DD.device_connect_st="0" ');         
        }
        
        if(params.grade_cd) query.push(util.format(' AND ( "ALL" ="%s" OR DD.grade_cd="%s") ',  params.grade_cd , params.grade_cd));
        //query.push(util.format(' AND ( "ALL" ="%s" OR CC.device_connect_st="%s") ',  params.device_connect_st , params.device_connect_st));
        if(params.search_name)  query.push(util.format(' AND (BB.user_auth_id like "%s" OR BB.user_nm  like "%s" OR BB.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by BB.user_nm ASC ');

        return query.join('');          
    },  
    getUserDetail: (params) =>  {
        let query = []; 
        let ymdhhmi='%Y-%m-%d %H:%i:%s';

        query.push('  SELECT AA.user_id,AA.user_auth_id,AA.user_nm, AA.user_tel, DD.group_nm, AA.user_st ,BB.user_device_sn,DD.organization_nm ,CC.temperature,CC.device_connect_st ');
        query.push('  ,CC.heart_rat,CC.temperature_cd,CC.grade_cd,ifnull(EE.check_cd,"C5") as check_status,ifnull(FF.check_cd,"C5") as vaccine_status ');
        query.push('  ,ifnull(EE.approval_cd,"05") as check_approval,ifnull(FF.approval_cd,"05") as vaccine_approval ');
        query.push('  ,BB.fcm_token,BB.uuid,BB.model_num,BB.platform,CC.sbp,CC.dbp ');
        query.push(util.format(',DATE_FORMAT(convert_tz(CC.updated_dt, "+00:00", "%s"), "%s") as updated_dt_str ',params.z_value , ymdhhmi )); 
        query.push('  FROM tb_user as AA');
        query.push('  LEFT JOIN tb_user_device as BB ON AA.user_id=BB.user_id ');
        query.push('  LEFT JOIN tb_health as CC ON AA.user_id=CC.user_id ');

        query.push('  LEFT JOIN ( ');
        query.push(SubQueries.setUserGroupRels(params));
        query.push('  ) as DD ON AA.user_id=DD.user_id  ');

        query.push('  LEFT JOIN ( ');
        query.push(SubQueries.setUserCheckLimit(params));
        query.push('  ) as EE on AA.user_id=EE.user_id  ');

        query.push('  LEFT JOIN ( ');
        query.push(SubQueries.setUserVaccineLimit(params));
        query.push('  ) as FF on AA.user_id=FF.user_id  ');

        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));

        return query.join('');          
    }, 
    getUserConfirmedDetail: (params) =>  {
        let query = [];   
        let ymdhhmi='%Y-%m-%d %H:%i:%s';

        query.push('  SELECT AA.user_id,AA.user_confirmed_yn ,AA.user_nm, AA.user_tel ,BB.user_device_sn,DD.group_nm ,CC.temperature,CC.device_connect_st ');
        query.push('  ,CC.heart_rat,CC.temperature_cd ');
        query.push(util.format(',DATE_FORMAT(convert_tz(CC.updated_dt, "+00:00", "%s"), "%s") as updated_dt_str ',params.z_value , ymdhhmi ));
        query.push('  FROM tb_user as AA');
        query.push('  LEFT JOIN tb_user_device as BB ON AA.user_id=BB.user_id ');
        query.push('  LEFT JOIN tb_health as CC ON AA.user_id=BB.user_id ');
        query.push('  LEFT JOIN tb_group as DD ON AA.group_id=DD.group_id ');
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));

        return query.join('');          
    },     
    getUserDetailTempLog: (params) =>  {
        let query = [];   
        //let currentMonth =moment().add(0, 'M').format("YYYYMM");
        let ymdhhmi='%Y-%m-%d %H:%i';
        let date_ym = getDates_yyyymm(params.start_date,params.end_date);
        let date_ym_cnt=(date_ym.length)? date_ym.length : 0;

        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        date_ym.forEach( (date ,index) => {
            params.currentMonth=date;
            query.push(SubQueries.getUserDetailTempLogUnion(params));
            if(index == (date_ym_cnt-1)) query.push(' ');
            else query.push(' union all  ');
        });
        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        //query.push('  limit 10 ');
        return query.join('');          
    }, 
    getUserDetailTempLogTotalCnt: (params) =>  {
        let query = [];   
        //let currentMonth =moment().add(0, 'M').format("YYYYMM");
        let ymdhhmi='%Y-%m-%d %H:%i';
        let date_ym = getDates_yyyymm(params.start_date,params.end_date);
        let date_ym_cnt=(date_ym.length)? date_ym.length : 0;

        query.push('  SELECT count(*) as total_cnt from   ( ');

        date_ym.forEach( (date ,index) => {
            params.currentMonth=date;
            query.push(SubQueries.getUserDetailTempLogUnion(params));
            if(index == (date_ym_cnt-1)) query.push(' ');
            else query.push(' union all  ');
        });

        query.push('  )as AA order by AA.created_dt DESC');
      
        return query.join('');       
    },     
    getUserConfirmedDetailTempLog: (params ,dateArray ) =>  {
        let query = [];   
        let dateCnt =dateArray.length;
        let ymdhhmi='%Y-%m-%d %H:%i';
        let ymd_1='%Y%m';
        let ymd_2='%d';

        query.push('  select AA.* ');
        query.push(util.format(',DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push(util.format(',DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_ym_str ',params.z_value , ymd_1 ));
        query.push(util.format(',DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_d_str ',params.z_value , ymd_2 ));
        
        query.push('  from  (  ');
        dateArray.forEach( (date ,index) => {
            let table_name ="tb_health_log_"+date;
            query.push(SubQueries.selectUserConfirmedHealthLog(params,table_name));
            if(index == (dateCnt-1)) query.push(' ');
            else query.push(' union all  ');
        });               

        query.push('  ) as AA  order by AA.created_dt desc  ');

        return query.join('');          
    },    
    getUserConfirmedList: (params) =>  {
        let query = [];
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';

        query.push('  SELECT AA.user_id ,AA.user_nm, AA.user_tel ,BB.user_device_sn,DD.group_nm ,CC.temperature,CC.device_connect_st ');
        query.push('  ,CC.heart_rat,CC.temperature_cd ');
        query.push(util.format(',DATE_FORMAT(convert_tz(CC.updated_dt, "+00:00", "%s"), "%s") as updated_dt_str ',params.z_value , ymdhhmi ));
        //query.push('  ,date_format(AA.user_confirmed_dt, "%Y-%m-%d %H:%i:%s") as user_confirmed_dt_str ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.user_confirmed_dt, "+00:00", "%s"), "%s") as user_confirmed_dt_str ',params.z_value , ymd ));
        query.push('  ,AA.user_confirmed_yn  FROM tb_user as AA');
        query.push('  LEFT JOIN tb_user_device as BB ON AA.user_id=BB.user_id ');
        query.push('  LEFT JOIN tb_health as CC ON AA.user_id=CC.user_id ');
        query.push('  LEFT JOIN tb_group as DD ON AA.group_id=DD.group_id ');
        query.push('  WHERE 1=1 AND AA.del_yn="N" ');
        query.push('  AND user_confirmed_yn="Y" ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push(util.format(' AND ( "ALL" ="%s" OR DD.group_id="%s") ', params.group_id , params.group_id)); 
        if(params.start_date && params.end_date){
            //query.push(util.format(' AND DATE_FORMAT(AA.user_confirmed_dt, "%s")>="%s" AND DATE_FORMAT(AA.user_confirmed_dt, "%s")<="%s" ',ymd ,params.start_date.replace(/\-/g,''), ymd,params.end_date.replace(/\-/g,'')));  
            query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.user_confirmed_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
            query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.user_confirmed_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,'')));             
        }    
        if(params.search_name)  query.push(util.format(' AND (AA.user_nm  like "%s" OR user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        return query.join('');          
    },
    getUserSearchSelectBox: (params) =>  {
        let query = [];  
        query.push('  SELECT AA.user_id as id ,BB.user_nm as name, BB.user_tel as tel,AA.group_nm ');
        query.push('  from (   ');
        query.push(SubQueries.setUserGroupRelsAll(params));
        query.push(' ) as AA ');
        query.push(' left join tb_user as BB on AA.user_id=BB.user_id ');
        query.push(' where  1=1 AND BB.del_yn="N" and BB.user_st="1"  ');
        if(params.search_name)query.push(util.format(' AND  BB.user_nm like "%s" ' ,'%'+params.search_name+'%' ));   
        query.push(' order by BB.user_nm asc ');    
        return query.join('');          
    },       
    updateUserLoginTime:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' login_updated_dt=utc_timestamp() ');
        //query.push(' ,login_first_yn="N" ');
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },  
    updateUserConfirmed:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' login_updated_dt=utc_timestamp() ');
        query.push(' ,user_confirmed_dt=utc_timestamp() ');
        query.push(util.format(' ,user_confirmed_yn="%s" ', params.user_confirmed_yn));
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },
    updateUserInfo:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        //query.push(util.format(' ,group_id="%s" ', params.group_id));
        query.push(util.format(' ,user_nm="%s" ', params.user_nm));
        query.push(util.format(' ,user_tel="%s" ', params.user_tel));
        query.push(util.format(' ,user_st="%s" ', params.user_st));
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },   
    updateUserPasswordInit:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,user_auth_pw=HEX(AES_ENCRYPT("%s", "%s")) ', params.user_auth_pw, auth.crypto_key)); 
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },      
    getUserDuplicateCheck: (params) =>  {
        let query = [];   

        query.push('  SELECT count(*) as cnt ');
        query.push('  from tb_user as AA ');
        query.push('  where 1=1 ');
        query.push(util.format(' AND AA.user_auth_id="%s" ', params.user_auth_id));

        return query.join('');          
    },
    getUserAuthIdDuplicateCheck: (params) =>  {
        let query = [];   

        query.push('  select AA.* ,BB.user_id  from ( ');
        query.push(util.format(' select "%s" as user_auth_id , 1 as id union', randomString()));
        query.push(util.format(' select "%s" as user_auth_id , 2 as id union',randomString()));
        query.push(util.format(' select "%s" as user_auth_id , 3 as id ', randomString()));
        query.push('  ) as AA ');
        query.push('  left outer join tb_user as BB   on AA.user_auth_id=BB.user_auth_id ');

        return query.join('');          
    },     
    getUserInfoList: (params) =>  {
        let query = [];  
        let ymdhhmi='%Y-%m-%d %H:%i%s';
        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push('  SELECT AA.*,CC.temperature,CC.temperature_cd,DD.group_nm');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push('   FROM tb_user as AA ');
        query.push('  left join tb_user_device as BB on AA.user_id=BB.user_id ');
        query.push('  left join tb_health as CC on AA.user_id=CC.user_id ');
        query.push('  left join tb_group as DD on AA.group_id=DD.group_id ');
        query.push('  WHERE 1=1 AND AA.del_yn="N" ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push(util.format(' AND ( "ALL" ="%s" OR AA.group_id="%s") ', params.group_id , params.group_id));
        query.push(util.format(' AND ( "ALL" ="%s" OR AA.user_st="%s") ',  params.user_st , params.user_st));
        if(params.search_name)  query.push(util.format(' AND (AA.user_nm  like "%s" OR user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by AA.user_nm ASC ');

        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    }, 
    getUserInfoListTotalCnt: (params) =>  {
        let query = [];  

        query.push('  SELECT count(*) as total_cnt FROM tb_user as AA');
        query.push('  left join tb_user_device as BB on AA.user_id=BB.user_id ');
        query.push('  left join tb_health as CC on AA.user_id=CC.user_id ');
        query.push('  WHERE 1=1 AND AA.del_yn="N" ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push(util.format(' AND ( "ALL" ="%s" OR AA.group_id="%s") ', params.group_id , params.group_id));
        query.push(util.format(' AND ( "ALL" ="%s" OR AA.user_st="%s") ',  params.user_st , params.user_st));
        if(params.search_name)  query.push(util.format(' AND (AA.user_nm  like "%s" OR user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by AA.user_nm ASC ');

        return query.join('');          
    }, 
    getUserTempLogExcelData: (params) =>  {
        let query = [];   
        let currentMonth =moment().add(0, 'M').format("YYYYMM");
        let ymdhhmi='%Y-%m-%d %H:%i';

        query.push('  SELECT BB.* ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push('  FROM tb_user as AA');
        query.push(util.format(' LEFT JOIN tb_health_log_%s as BB ON AA.user_id=BB.user_id ', currentMonth));
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        query.push('  order by BB.created_dt desc ');
        query.push('  limit 10 ');
        return query.join('');          
    },  
    getUserMainFcmToken: (params) =>  {
        let query = [];   

        query.push('  SELECT BB.* FROM tb_user as AA ');
        query.push('  left join tb_user_device as BB on AA.user_id=BB.user_id ');
        query.push('  where 1=1 ');
        query.push(util.format(' AND  AA.user_id in( %s ) ', params.user_id));

        return query.join('');          
    },  
    getUserDetailTempLogExcel: (params) =>  {
        let query = [];   
        //let currentMonth =moment().add(0, 'M').format("YYYYMM");
        let ymdhhmi='%Y-%m-%d %H:%i';
        let date_ym = getDates_yyyymm(params.start_date,params.end_date);
        let date_ym_cnt=(date_ym.length)? date_ym.length : 0;

        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        date_ym.forEach( (date ,index) => {
            params.currentMonth=date;
            query.push(SubQueries.getUserDetailTempLogUnion(params));
            if(index == (date_ym_cnt-1)) query.push(' ');
            else query.push(' union all  ');
        });
        query.push('  ) A  ');
        query.push('  ) Z ');
        //query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        //query.push('  limit 10 ');
        return query.join('');          
    }, 
    getUserAdminList: (params) =>  {
        let query = [];   
        query.push('  select AA.* ,BB.*,CC.user_nm from ( ');
        query.push('  select AA.user_id,BB.group_id,CC.organization_id from tb_user_group_rels as AA  ');
        query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id ');
        query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id  ');
        query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now() and CC.type="LT"  and BB.del_yn="N" and CC.del_yn="N"  ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));

        query.push(' AND CC.organization_id not in( ');
        query.push(' select organization_id from tb_user_sms  ');
        query.push(' where 1=1 ');
        query.push(util.format(' AND user_id="%s" ', params.user_id));
        query.push(' AND sms_stop_yn="Y" ');
        query.push(' AND end_dt>utc_timestamp()  ');
        query.push(' ) ');

        query.push('  group by AA.user_id ,CC.organization_id ');
        query.push('  ) as AA ');

        query.push('  left join tb_organization_admin as BB on AA.organization_id=BB.organization_id ');
        query.push('  left join tb_user as CC on AA.user_id=CC.user_id ');
        query.push('  where 1=1 ');
        query.push('  and BB.admin_st="1" ');
        query.push('  and CC.del_yn="N" ');

        return query.join('');          
    }, 
    getUserGroupRelsList: (params) =>  {
        let query = [];   
        let ymdhh='%y-%m-%d';
        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push('  SELECT * from (  ');
        query.push('  SELECT AA.user_id,AA.grade,BB.user_nm,BB.user_tel,CC.grade_cd,CC.temperature  ');

        query.push(' ,CASE WHEN  AA.grade="G1" and ( CC.grade_cd="G1") THEN "A1" ');
        query.push(' WHEN AA.grade="G2" and ( CC.grade_cd="G1" OR CC.grade_cd="G2") THEN "A1"   ');
        query.push(' WHEN AA.grade="G3" and ( CC.grade_cd="G1" OR CC.grade_cd="G2" OR CC.grade_cd="G3") THEN "A1" ');
        query.push(' ELSE "A3" END as apply_cd ');

        query.push(' FROM ( ');
        query.push(' select AA.user_id,CC.grade from tb_user_group_rels as AA  ');
        query.push(' LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
        query.push(' LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id  ');
        query.push(' where  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N"  ');
        query.push(util.format(' and CC.organization_id="%s" ', params.organization_id));
        query.push(' and CC.type="ST" ');
        query.push(' group by AA.user_id ');
        query.push(' ) as AA ');

        query.push(' left join  tb_user AS BB on AA.user_id=BB.user_id  ');
        query.push(' left join  tb_health AS CC ON AA.user_id = CC.user_id');

        query.push(' and 1=1 ');
        if(params.search_name)  query.push(util.format(' AND (BB.user_nm  like "%s" OR BB.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push(' order by BB.created_dt desc ');
        query.push(' ) as ZZ  ');
        query.push(util.format(' where  ( "ALL"="%s" OR ZZ.apply_cd="%s") ', params.apply_cd, params.apply_cd));
        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    },     
    getUserGroupRelsListTotalCnt: (params) =>  {
        let query = [];  

        query.push('  SELECT count(*) as total_cnt from (  ');
        query.push('  SELECT AA.user_id,AA.grade,BB.user_nm,BB.user_tel,CC.grade_cd  ');

        query.push(' ,CASE WHEN  AA.grade="G1" and ( CC.grade_cd="G1") THEN "A1" ');
        query.push(' WHEN AA.grade="G2" and ( CC.grade_cd="G1" OR CC.grade_cd="G2") THEN "A1"   ');
        query.push(' WHEN AA.grade="G3" and ( CC.grade_cd="G1" OR CC.grade_cd="G2" OR CC.grade_cd="G3") THEN "A1" ');
        query.push(' ELSE "A3" END as apply_cd ');

        query.push(' FROM ( ');
        query.push(' select AA.user_id,CC.grade from tb_user_group_rels as AA  ');
        query.push(' LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
        query.push(' LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id  ');
        query.push(' where  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N"  ');
        query.push(util.format(' and CC.organization_id="%s" ', params.organization_id));
        query.push(' and CC.type="ST" ');
        query.push(' group by AA.user_id ');
        query.push(' ) as AA ');

        query.push(' left join  tb_user AS BB on AA.user_id=BB.user_id  ');
        query.push(' left join  tb_health AS CC ON AA.user_id = CC.user_id');

        query.push(' where 1=1 ');
        if(params.search_name)  query.push(util.format(' AND (BB.user_nm  like "%s" OR BB.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push(' ) as ZZ  ');
        query.push(util.format(' where  ( "ALL"="%s" OR ZZ.apply_cd="%s") ', params.apply_cd, params.apply_cd));

        return query.join('');          
    },  
    deleteUser : (id) => {
		let query = [];
        query.push(' UPDATE `tb_user` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,del_yn="Y" ');
        //query.push(util.format(' ,user_auth_id="%s" ', 'del_'+randomIdString(8)));
        query.push(util.format(' where user_id="%s" ', id));
        return query.join('');
    },       
    deleteUserOut : (params,id) => {
		let query = [];
        query.push(' UPDATE `tb_user_group_rels` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,apply_cd="A4" ');
        query.push(' where user_group_rels_id in ( ');
        query.push(' select AA.user_group_rels_id from ( ');
        query.push(' SELECT AA.user_group_rels_id FROM sw_healthcare_dev.tb_user_group_rels as AA ');
        query.push(' left join sw_healthcare_dev.tb_group as BB on AA.group_id=BB.group_id ');
        query.push(' left join sw_healthcare_dev.tb_organization as CC on BB.organization_id=CC.organization_id ');

        query.push(' where 1=1 ');
        query.push(util.format(' and AA.user_id="%s" ', id));
        query.push(util.format(' and CC.organization_id="%s" ', params.organization_id));
        query.push(' ) as AA ');
        query.push(' ) ');
        return query.join('');
    },         
};           


let SubQueries = {
    selectUserConfirmedHealthLog:  (params,table_name) => {
        let query = [];
        let ymd='%Y%m%d%H%i%S';

        query.push('  SELECT AA.user_id,BB.health_log_id,BB.created_dt,BB.lat,BB.lng,BB.temperature,BB.heart_rat,BB.device_sn,BB.device_use_yn ');
        query.push('  FROM tb_user as AA ');
        query.push(util.format('  LEFT JOIN %s as BB ON AA.user_id=BB.user_id  ', table_name));
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        query.push(util.format(' AND MOD(SUBSTR(DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s"),11,2),10) = 0 ',params.z_value ,ymd));
  
        
        return query.join('');
    },
    getUserDetailTempLogUnion:  (params) => {
        let query = [];
        let ymdhhmi='%Y-%m-%d %H:%i';
        let ymd='%Y%m%d';

        query.push('  ( SELECT BB.* ');
        query.push(util.format(',DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push('  FROM tb_user as AA');
        query.push(util.format(' LEFT JOIN tb_health_log_%s as BB ON AA.user_id=BB.user_id ', params.currentMonth));
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));  
        if(params.start_date) query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        if(params.end_date) query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd ,params.end_date.replace(/\-/g,'')));       
        query.push('  order by BB.created_dt desc ) ');

        return query.join('');
    },    
    setUserGroupRels:  (params) => { 
        let query = [];
        if(!params.group_id) params.group_id = "ALL";
        if(params.group_id=="ALL"){
            query.push('  select AA.user_id, CC.organization_id ,group_concat(CC.organization_nm) as organization_nm ,group_concat(BB.group_nm) as group_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N" ');
            query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
            query.push(util.format(' AND ( "ALL" ="%s" OR BB.group_id="%s") ', params.group_id , params.group_id));
            query.push('  group by AA.user_id ');

        }else{
            query.push('  select AA.user_id,BB.group_id , CC.organization_id , CC.organization_nm,BB.group_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N"   ');
            query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
            query.push(util.format(' AND ( "ALL" ="%s" OR BB.group_id="%s") ', params.group_id , params.group_id));
        }

        return query.join('');
    },  
    setUserGroupRelsAll:  (params) => {
        let query = [];
            query.push('  select AA.user_id,group_concat(BB.group_nm) as group_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N" ');
            query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
            query.push('  group by AA.user_id ');

        return query.join('');
    },  
    setUserCheckLimit:  (params) => {
        let query = [];

        query.push('  SELECT AA.user_id,AA.check_cd,AA.approval_cd FROM tb_user_check as AA ');
        query.push('  where AA.approval_cd = "01" ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        query.push(util.format('and TIMESTAMPDIFF(MINUTE,convert_tz(utc_timestamp(), "+00:00", "%s"),convert_tz(DATE_ADD(AA.check_dt, INTERVAL 14 DAY), "+00:00", "%s"))>0 ', params.z_value, params.z_value));        
        query.push('  order by check_dt desc  limit 1');
 
        return query.join('');
    },    
    setUserVaccineLimit:  (params) => {
        let query = [];

        query.push('  SELECT AA.user_id,AA.check_cd,AA.approval_cd FROM tb_user_vaccine as AA ');
        query.push('  where AA.approval_cd = "01" ');
        query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        query.push(util.format('and TIMESTAMPDIFF(MINUTE,convert_tz(utc_timestamp(), "+00:00", "%s"),convert_tz(DATE_ADD(AA.check_dt, INTERVAL 365 DAY), "+00:00", "%s"))>0 ', params.z_value, params.z_value));        
        query.push('  order by check_dt desc  limit 1');
 
        return query.join('');
    },
    setUserListAll:  (params) => {
        let query = [];
            query.push('  select ZZ.user_id , GROUP_CONCAT(ZZ.group_nm) AS group_nm  from ( ');
            query.push('  select AA.user_id,BB.group_id,BB.group_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
            query.push('  where  1=1 ');
            query.push('  and  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N" ');
            query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id ));
            query.push(util.format(' AND ( "ALL"="%s" OR BB.group_id="%s") ', params.group_id ,params.group_id));
            query.push('  GROUP BY AA.user_id,BB.group_id,BB.group_nm   ');
            query.push('  ) AS ZZ GROUP BY ZZ.user_id  ');

        return query.join('');
    }, 
     
};

function randomString() {
    let chars = "abcdefghiklmnopqrstuvwxyz";
    let ints = "0123456789";
    let string_length = 4;
    let int_length = 3;
    let randomstring = '';
    for (let i = 0; i < string_length; i++) {
        let rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    for (let i = 0; i < int_length; i++) {
        let rnum2 = Math.floor(Math.random() * ints.length);
        randomstring += ints.substring(rnum2, rnum2 + 1);
    }
    return randomstring;
}

function getDates_yyyymm(start, stop) {
    let startDate =new Date(start)
    let stopDate =new Date(stop)
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(currentDate.toISOString().substring(0, 7).replace(/\-/,''));
        currentDate.setDate(currentDate.getDate() + 1);  
    }
    return (dateArray)? Array.from(new Set(dateArray)) : [];
  }


  function randomIdString(num) {
    let chars = "abcdefghiklmnopqrstuvwxyz0123456789";
    let string_length = num;
    let randomstring = '';
    for (let i = 0; i < string_length; i++) {
        let rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
  }