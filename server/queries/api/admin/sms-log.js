const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createSmsLog: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_sms_log` ');
        query.push(' ( ');
        query.push(' `ip` ');   
        query.push(' ,`rcv_num` ');     
        query.push(' ,`send_num` '); 
        query.push(' ,`contents` '); 
        query.push(' ,`status` '); 
        query.push(' ,`created_dt` ');      
        query.push(' )');   
           
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' inet_aton("%s") ', params.ip));  
        query.push(util.format(' ,"%s" ', params.rcv_num)); 
        query.push(util.format(' ,"%s" ', params.send_num)); 
        query.push(util.format(' ,"%s" ', params.contents)); 
        query.push(util.format(' ,"%s" ', params.status)); 
        query.push(' ,utc_timestamp() ');
        query.push(' )');           
      
        return query.join('');          
    },
    getSmsLogCheckCnt: (params) =>  {
        let query = [];   
        query.push(' select count(*) as cnt from tb_sms_log  ');
        query.push(' where 1=1  ');
        query.push(util.format(' and ip=inet_aton("%s") ', params.clientIp)); 
        query.push(util.format(' and rcv_num="%s" ', params.rcvNum)); 
        query.push(util.format(' and TIMESTAMPDIFF(MINUTE,convert_tz(created_dt, "+00:00", "%s"),convert_tz(utc_timestamp(), "+00:00", "%s"))<10 ', params.z_value, params.z_value));
        return query.join('');          
    },   
                                                 
};         
