const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createMessageArray: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_message` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`group_id` ');        
        query.push(' ,`organization_id` ');
        query.push(' ,`organization_admin_id` ');
        if(params.title) query.push(' ,`title` ');
        query.push(' ,`contents` ');
        query.push(' ,`message_gb` ');           
        query.push(' ,`message_cd` ');      
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');                             
        query.push(' )');   
        
        let userArray = params.user_id.split(',');
        let dateCnt =userArray.length;

        query.push(' select ZZ.user_id ');
        query.push(' , ZZ.group_id ');
        query.push(util.format(' ,"%s" ', params.organization_id));
        query.push(util.format(' ,"%s" ', params.organization_admin_id));
        if(params.title) query.push(util.format(' ,"%s" ', params.title));
        query.push(util.format(' ,"%s" ', params.contents));
        query.push(util.format(' ,"%s" ', params.message_gb));
        query.push(util.format(' ,"%s" ', params.message_cd));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');
        query.push(' from (');  

        query.push(' select  AA.user_id ,BB.group_id   from (');  

        userArray.forEach( (id ,index) => {                     
            if (id) query.push(util.format('select %s as user_id ', id));
            if(index == (dateCnt-1)) query.push(' ');
            else query.push(' union  ');  
        });            
        query.push(' ) as AA');  
        query.push(' left join ( ');  
        query.push(' select AA.user_id,BB.group_id   from tb_user_group_rels as AA  ');  
        query.push(' LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id');  
        query.push(' LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');  
        query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N" ');  
        query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id)); 
        query.push(' group by AA.user_id ,BB.group_id ');   
        query.push(' ) as BB on AA.user_id=BB.user_id ');  
        query.push(' ) as ZZ ');  
    
        return query.join('');          
    },  
    createMessageArrayGroup: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_message` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`organization_id` ');
        query.push(' ,`group_id` ');
        query.push(' ,`organization_admin_id` ');
        if(params.title) query.push(' ,`title` ');
        query.push(' ,`contents` ');
        query.push(' ,`message_gb` ');           
        query.push(' ,`message_cd` ');      
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');                             
        query.push(' )');   
        
        let groupIdArray = params.group_id.split(',');
        let dateCnt =groupIdArray.length;

        query.push(' select AA.user_id ');
        query.push(' ,AA.organization_id ');
        query.push(' ,AA.group_id ');
        query.push(util.format(' ,"%s" ', params.organization_admin_id));
        if(params.title) query.push(util.format(' ,"%s" ', params.title));
        query.push(util.format(' ,"%s" ', params.contents));
        query.push(util.format(' ,"%s" ', params.message_gb));
        query.push(util.format(' ,"%s" ', params.message_cd));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');
        query.push(' from (');  
        query.push('  select AA.user_id,BB.group_id , CC.organization_id  from tb_user_group_rels as AA ');
        query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
        query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
        query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N" ');
        query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
        query.push(util.format(' AND AA.group_id in (%s) ', params.group_id));
            
        query.push(' ) as AA');  
    
        return query.join('');          
    },     
    createMessageArrayAll: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_message` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`organization_id` ');
        query.push(' ,`group_id` ');
        query.push(' ,`organization_admin_id` ');
        if(params.title) query.push(' ,`title` ');
        query.push(' ,`contents` ');
        query.push(' ,`message_gb` ');           
        query.push(' ,`message_cd` ');      
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');                             
        query.push(' )');   
        
        query.push(' select AA.user_id ');
        query.push(' ,AA.organization_id ');
        query.push(' ,AA.group_id ');
        query.push(util.format(' ,"%s" ', params.organization_admin_id));
        if(params.title) query.push(util.format(' ,"%s" ', params.title));
        query.push(util.format(' ,"%s" ', params.contents));
        query.push(util.format(' ,"%s" ', params.message_gb));
        query.push(util.format(' ,"%s" ', params.message_cd));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');
        query.push(' from (');  
        query.push('  select AA.user_id,BB.group_id , CC.organization_id  from tb_user_group_rels as AA ');
        query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
        query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');
        query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now()  and BB.del_yn="N" and CC.del_yn="N" ');
        query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
            
        query.push(' ) as AA');  
    
        return query.join('');          
    },     
    createMessageMainArray: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_message` ');
        query.push(' ( ');
        query.push(' `user_id` ');
        query.push(' ,`organization_admin_id` ');
        if(params.title) query.push(' ,`title` ');
        query.push(' ,`contents` ');
        query.push(' ,`message_gb` ');           
        query.push(' ,`message_cd` ');      
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');                             
        query.push(' )');   
        
        let userArray = params.user_id.split(',');
        let dateCnt =userArray.length;

        query.push(' select user_id ');
        query.push(util.format(' ,"%s" ', params.organization_admin_id));
        if(params.title) query.push(util.format(' ,"%s" ', params.title));
        query.push(util.format(' ,"%s" ', params.contents));
        query.push(util.format(' ,"%s" ', params.message_gb));
        query.push(util.format(' ,"%s" ', params.message_cd));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');
        query.push(' from (');  

        userArray.forEach( (id ,index) => {    
            console.log(id)        
            if(index == (dateCnt-1)) query.push(util.format('select %s as user_id ', id));
            else  query.push(util.format('select %s as user_id union ', id));
        });            
        query.push(' ) as AA');  
    
        return query.join('');          
    },        
    getMessageList: (params) =>  {
        let query = [];  
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';
        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push('  select BB.* ,CC.user_nm ,AA.group_nm  ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
        query.push('  FROM (    ');
        query.push(SubQueries.setUserGroupRels(params));
        query.push('  ) as AA  ');
        query.push('  left join tb_message as BB ON AA.user_id=BB.user_id    ');
        query.push('  left join tb_user as CC on AA.user_id=CC.user_id ')
        query.push('  WHERE 1=1 AND CC.del_yn="N"');
        if(params.message_gb=="send"){
            query.push('  and BB.message_gb in("sys","admin") ');
        }else if(params.message_gb=="receive"){
            query.push('  and BB.message_gb="user" ');
        }else{
            query.push('  AND ( "ALL" ="ALL" OR BB.message_gb="ALL") ');
        }
        query.push(util.format(' AND ( "ALL" ="%s" OR BB.group_id="%s") ', params.group_id , params.group_id));
        query.push(util.format(' AND BB.organization_id="%s" ', params.organization_id));
        if(params.read_st)query.push(util.format(' AND ( "ALL" ="%s" OR BB.read_st="%s") ',  params.read_st , params.read_st));
        if(params.start_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        if(params.end_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,'')));            
        if(params.search_name)  query.push(util.format(' AND (BB.title  like "%s" OR CC.user_nm like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by BB.created_dt DESC ');

        query.push('  ) A  ');
        query.push('  ) Z ');
        if(params.count_num) query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    }, 
    getMessageListTotalCnt: (params) =>  {
        let query = [];  
        let ymd='%Y%m%d';

        query.push('  SELECT  ');
        query.push('  count(*) as total_cnt');
        query.push('  FROM (    ');
        query.push(SubQueries.setUserGroupRels(params));
        query.push('  ) as AA  ');
        query.push('  left join tb_message as BB ON AA.user_id=BB.user_id    ');
        query.push('  left join tb_user as CC on AA.user_id=CC.user_id ')
        query.push('  WHERE 1=1 AND CC.del_yn="N"');
        if(params.message_gb=="send"){
            query.push('  and BB.message_gb in("sys","admin") ');
        }else if(params.message_gb=="receive"){
            query.push('  and BB.message_gb="user" ');
        }else{
            query.push('  AND ( "ALL" ="ALL" OR BB.message_gb="ALL") ');
        }
        query.push(util.format(' AND ( "ALL" ="%s" OR BB.group_id="%s") ', params.group_id , params.group_id));
        query.push(util.format(' AND BB.organization_id="%s" ', params.organization_id));
        if(params.read_st)query.push(util.format(' AND ( "ALL" ="%s" OR BB.read_st="%s") ',  params.read_st , params.read_st));
        if(params.start_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        if(params.end_date)query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,'')));            
        if(params.search_name)  query.push(util.format(' AND (BB.title  like "%s" OR CC.user_nm like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by BB.created_dt DESC ');

        return query.join('');          
    },
    updateMessageRead:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_message` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,read_st ="%s" ', params.read_st)); 
        query.push(util.format(' where message_id="%s" ', params.message_id));
        return query.join('');
    },                                     
    // getUserMessageList: (params) =>  {
    //     let query = [];  
    //     let ymd='%Y%m%d';
    //     let ymdhhmi='%Y-%m-%d %H:%i';

    //     query.push('  SELECT AA.* ,BB.user_nm ');
    //     // query.push('  ,CC.group_nm ,date_format(AA.created_dt, "%Y-%m-%d %H:%i") as created_dt_str');
    //     query.push(util.format(',CC.group_nm ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));
    //     query.push('  FROM tb_message as AA ');
    //     query.push('  left join tb_user as BB on AA.user_id=BB.user_id ');
    //     query.push('  left join tb_group as CC on BB.group_id=CC.group_id    ');
    //     query.push('  WHERE 1=1 AND BB.del_yn="N" ');
    //     query.push(util.format(' AND CC.organization_id="%s" ',  params.organization_id ));
    //     query.push(util.format(' AND AA.user_id="%s" ',  params.user_id ));
    //     //query.push(util.format(' AND ( "ALL" ="%s" OR AA.message_gb="%s") ', params.message_gb , params.message_gb));
    //     // query.push(util.format(' AND ( "ALL" ="%s" OR AA.read_st="%s") ',  params.read_st , params.read_st));
    //     // query.push(util.format(' AND ( "ALL" ="%s" OR BB.group_id="%s") ',  params.group_id , params.group_id));         
    //     query.push('  order by AA.created_dt DESC  limit 20 ');

    //     return query.join('');          
    // },                
};    


let SubQueries = { 
    setUserGroupRels:  (params) => {
        let query = [];
            query.push('  select AA.user_id ,group_concat(CC.organization_nm) as organization_nm ,group_concat(BB.group_nm) as group_nm from tb_user_group_rels as AA ');
            query.push('  LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id  ');
            query.push('  LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id ');    
            query.push('  where  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N"  ');
            query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
            //     query.push(util.format(' AND ( "ALL" ="%s" OR BB.group_id="%s") ', params.group_id , params.group_id));
            query.push('  group by AA.user_id  ');
        return query.join('');
    }
};
