const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserEventRels: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_event_rels` ');
        query.push(' ( ');
        query.push(' `organization_id` ');
        query.push(' ,`user_event_id` ');
        query.push(' ,`group_id` ');
        query.push(' )');   
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_id));
        query.push(util.format(' ,"%s" ', params.user_event_id));
        query.push(util.format(' ,"%s" ', params.group_id));
        query.push(' )');       
        return query.join('');          
    }, 
    getUserEventRelsListLeft: (params) =>  {
        let query = [];   
        query.push('  select AA.group_id,AA.group_nm,BB.user_event_id  from tb_group as AA ');
        query.push('  left outer join ( ');
        query.push('  select * from tb_user_event_rels ');
        query.push(util.format(' where  user_event_id="%s" ', params.user_event_id));
        query.push('  ) as BB on AA.group_id =BB.group_id and  AA.organization_id =BB.organization_id ');
        query.push('  where 1=1 AND del_yn="N" ');
        query.push(util.format(' and AA.organization_id="%s" ', params.organization_id));
        if(params.search_name)  query.push(util.format(' AND  AA.group_nm like "%s" ' ,'%'+params.search_name+'%' ));
        
        return query.join('');          
    },
    getUserEventListMessage: (params) =>  {
        let query = [];   
        query.push('  select AA.group_id,AA.group_nm  from tb_group as AA ');
        query.push('  where 1=1 AND del_yn="N" ');
        query.push(util.format(' and AA.organization_id="%s" ', params.organization_id));     
        return query.join('');          
    },      
    getUserEventRelsList: (params) =>  {
        let query = [];   
        query.push('  select AA.group_id,AA.group_nm,BB.user_event_id  from tb_group as AA ');
        query.push('  left outer join ( ');
        query.push('  select * from tb_user_event_rels ');
        query.push(util.format(' where  user_event_id="%s" ', params.user_event_id));
        query.push('  ) as BB on AA.group_id =BB.group_id and  AA.organization_id =BB.organization_id ');
        query.push('  where 1=1 AND del_yn="N" ');
        query.push(util.format(' and AA.organization_id="%s" ', params.organization_id));
        
        return query.join('');          
    },    
    deleteUserEventRels: (params,id) =>  {
        let query = [];   
        query.push('  delete from  tb_user_event_rels ');
        query.push('  where 1=1 ');
        query.push(util.format(' and organization_id="%s" ', params.organization_id));
        query.push(util.format(' and user_event_id="%s" ', id));        
        return query.join('');          
    },                
};         