const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserDevice: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_device` ');
        query.push(' ( ');
        query.push(' `user_id` ');   
        query.push(' ,`updated_dt` ');          
        query.push(' )');   
           
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.user_id));  
        query.push(' ,utc_timestamp() ');
        query.push(' )');           
      
        return query.join('');          
    },
    getUserDevice: (params) =>  {
        let query = [];   
        let arrData = (params.user_id)? params.user_id.split(',') : [];
        let arrData_cnt =arrData.length;
        query.push(' select * from (  ');
        arrData.forEach(function (row,index) {
            query.push(util.format('select "%s" as user_id ', row));
            if(index!=(arrData_cnt-1)) query.push(' union ');
        })   
        query.push(' )  as AA  ');
        query.push(' left join  tb_user_device as BB on AA.user_id=BB.user_id  ');

        return query.join('');          
    },   
    getUserLogDevice: (params) =>  {
        let query = [];   
        query.push(' SELECT * FROM `tb_user_device` AS AA  ');
        query.push(util.format(' where AA.user_id ="%s" ', params.user_id));
        return query.join('');          
    },                                                   
};         