const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createOrganization: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_organization` ');
        query.push(' ( ');
        query.push(' `organization_auth_id` ');
        query.push(' ,`organization_nm` ');
        if(params.type)query.push(' ,`type` ');  
        if(params.organization_tel)query.push(' ,`organization_tel` ');  
        if(params.company_num)query.push(' ,`company_num` ');  
        if(params.addr1)query.push(' ,`addr1` ');  
        if(params.addr2)query.push(' ,`addr2` ');  
        if(params.grade)query.push(' ,`grade` ');  
        query.push(' ,`auth_code` ');  
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');        
        query.push(' )');   
           
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_auth_id));
        query.push(util.format(' ,"%s" ', params.organization_nm));
        if(params.type)query.push(util.format(' ,"%s" ', params.type));     
        if(params.organization_tel)query.push(util.format(' ,"%s" ', params.organization_tel));     
        if(params.company_num)query.push(util.format(' ,"%s" ', params.company_num));     
        if(params.addr1)query.push(util.format(' ,"%s" ', params.addr1));     
        if(params.addr2)query.push(util.format(' ,"%s" ', params.addr2));     
        if(params.grade)query.push(util.format(' ,"%s" ', params.grade));     
        query.push(',"'+randomString(4)+'"');
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');
        query.push(' )');       
      
        return query.join('');          
    },  
    updateOrganization:  (params,id) => {
        let query = [];
        query.push(' UPDATE `tb_organization` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        if(params.organization_nm)query.push(util.format(' ,organization_nm="%s" ', params.organization_nm));
        if(params.organization_tel)query.push(util.format(' ,organization_tel="%s" ', params.organization_tel));     
        if(params.company_num)query.push(util.format(' ,company_num="%s" ', params.company_num));     
        if(params.addr1)query.push(util.format(' ,addr1="%s" ', params.addr1));     
        if(params.addr2)query.push(util.format(' ,addr2="%s" '));     
        if(params.grade)query.push(util.format(' ,grade="%s" ', params.grade));   
        //if(params.auth_code)query.push(util.format(' ,auth_code="%s" ', params.auth_code));   
        query.push(util.format(' where organization_id="%s" ', id));
          
        return query.join('');   
    }, 
    updateOrganizationAdmin:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_organization_admin` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        if( params.admin_auth_pw) query.push(util.format(' ,admin_auth_pw=HEX(AES_ENCRYPT("%s", "%s")) ', params.admin_auth_pw, auth.crypto_key)); 
        query.push(util.format(' ,admin_nm="%s" ', params.admin_nm));
        query.push(util.format(' ,admin_tel="%s" ', params.admin_tel));
        if(params.admin_grade)query.push(util.format(' ,admin_grade="%s" ', params.admin_grade));
        if(params.admin_st) query.push(util.format(' ,admin_st="%s" ', params.admin_st));        
        query.push(' where organization_admin_id=( '); 
        query.push(' SELECT BB.organization_admin_id FROM tb_organization as AA '); 
        query.push(' left join (select * from tb_organization_admin) AS BB on AA.organization_id=BB.organization_id '); 
        query.push(' where 1=1 '); 
        query.push(util.format(' AND AA.organization_id="%s" ', id));
        query.push(' limit 1  ');
        query.push(' ) '); 
        return query.join('');
    },
    getOrganizationCheck: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_organization` AS AA ');
        query.push(util.format(' where AA.organization_auth_id="%s" ', params.organization_auth_id));
        return query.join('');          
    },
    getOrganizationInfo: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM tb_organization as AA ');
        query.push('  left join tb_organization_admin as BB on AA.organization_id=BB.organization_id ');
        query.push('  where 1=1 ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push('  limit 1  ');
        return query.join('');          
    },
    deleteOrganization : (id) => {
		let query = [];
        query.push(' DELETE FROM `tb_organization` '); 
        query.push(util.format(' where organization_id="%s" ', id));
                    
		return query.join('');      
    },
    getOrganizationOptionList: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_organization` AS AA ');
        if(params.type)query.push(util.format(' and ( "ALL"="%s" OR AA.type="%s" ) ', params.type));
        return query.join('');          
    },
                   
};         

function randomString(num) {
    let chars = "abcdefghiklmnopqrstuvwxyz0123456789";
    let string_length = num;
    let randomstring = '';
    for (let i = 0; i < string_length; i++) {
        let rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}