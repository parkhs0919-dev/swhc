const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createGroup: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_group` ');
        query.push(' ( ');
        query.push(' `organization_id` ');        
        query.push(' ,`group_nm` ');
        if(params.group_st)query.push(' ,`group_st` ');
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');              
        query.push(' )');   
        
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_id));
        query.push(util.format(' ,"%s" ', params.group_nm));
        if(params.group_st)query.push(util.format(' ,"%s" ', params.group_st));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');        
        query.push(' )');       
        
        return query.join('');          
    },  
    updateGroup:  (params,id) => {
        let query = [];
        query.push(' UPDATE `tb_group` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp()  ');
        query.push(util.format(' ,group_nm="%s" ', params.group_nm));
        if(params.gps_save_yn) query.push(util.format(' ,gps_save_yn="%s" ', params.gps_save_yn));
        if(params.group_st) query.push(util.format(' ,group_st="%s" ', params.group_st));
        query.push(util.format(' where group_id="%s" ', id));
            
        return query.join('');   
    }, 
    getGroupDetail: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.*,BB.organization_admin_id ,CC.admin_nm ,BB.group_admin_rels_id  FROM tb_group as AA ');
        query.push('  left outer join tb_group_admin_rels as BB  on AA.group_id=BB.group_id ');
        query.push('  left outer join tb_organization_admin as CC on BB.organization_admin_id = CC.organization_admin_id ');
        query.push(util.format(' where AA.group_id="%s" ', params.group_id));
        return query.join('');          
    },
    getGroupOptionList: (params) =>  {
        let query = [];   
        if(params.admin_grade=="M"){
            query.push('  SELECT AA.group_id,AA.group_nm FROM `tb_group` AS AA ');
            query.push(util.format(' where organization_id="%s" ', params.organization_id));
            query.push('  AND AA.del_yn="N"   ');
        }else{
            query.push('  SELECT AA.group_id,AA.group_nm FROM `tb_group` AS AA ');
            query.push('  where group_id in( ');
            query.push('  select AA.group_id from tb_group_admin_rels as AA ');
            query.push('  left join tb_organization_admin as BB ');
            query.push('  on AA.organization_admin_id=BB.organization_admin_id ');
            query.push(util.format(' where BB.organization_admin_id="%s" ', params.organization_admin_id));
            query.push('  ) ');
            query.push('  where 1=1 AND AA.del_yn="N"  ');
        }  
        query.push('  order by AA.group_nm desc ');            
        return query.join('');          
    },
    getGroupList: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push('  SELECT AA.*,BB.admin_nm,BB.organization_admin_id from  tb_group as AA');
        query.push('  left outer join ( ');

        query.push('  select AA.group_id,group_concat(BB.admin_nm) as admin_nm ,group_concat(BB.organization_admin_id) as organization_admin_id from tb_group_admin_rels  as AA ');
        query.push('  left join tb_organization_admin as BB on AA.organization_admin_id=BB.organization_admin_id ');
        query.push(util.format(' where BB.organization_id="%s" ', params.organization_id));
        query.push('  group by group_id ');
        query.push('  ) as BB on AA.group_id=BB.group_id ');

        query.push('  where 1=1 AND AA.del_yn="N" ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push(util.format(' AND ( "ALL" ="%s" OR AA.group_st="%s") ', params.group_st , params.group_st)); 
        if(params.search_name)  query.push(util.format(' AND AA.group_nm  like "%s"  ' ,'%'+params.search_name+'%'));
        query.push('  order by AA.group_id DESC ');

        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));
        return query.join('');          
    },
    getGroupListTotalCnt: (params) =>  {
        let query = [];  

        query.push('  SELECT count(*) as total_cnt  from  tb_group as AA');
        query.push('  left outer join ( ');
        query.push('  select AA.group_id,group_concat(BB.admin_nm) as admin_nm from tb_group_admin_rels  as AA ');
        query.push('  left join tb_organization_admin as BB on AA.organization_admin_id=BB.organization_admin_id ');
        query.push(util.format(' where BB.organization_id="%s" ', params.organization_id));
        query.push('  group by group_id ');
        query.push('  ) as BB on AA.group_id=BB.group_id ');

        query.push('  where 1=1 AND AA.del_yn="N"  ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push(util.format(' AND ( "ALL" ="%s" OR AA.group_st="%s") ', params.group_st , params.group_st)); 
        if(params.search_name)  query.push(util.format(' AND AA.group_nm  like "%s"  ' ,'%'+params.search_name+'%'));
        query.push('  order by AA.group_id DESC ');

        return query.join('');          
    }, 
    getGroupSearchSelectBox: (params) =>  {
        let query = [];  
        query.push('  SELECT group_id as id ,group_nm as text FROM `tb_group` AS AA ');
        query.push('  where  1=1 AND AA.del_yn="N"   ');
        query.push(util.format(' AND organization_id="%s" ', params.organization_id));
        query.push('  AND group_st="1" ');  
        query.push('  and group_id in( ');    
        if(params.admin_grade=="M"){
            query.push('  SELECT group_id FROM `tb_group` AS AA  ');            
            query.push(util.format(' where organization_id="%s" ', params.organization_id));
        }else{
            query.push('  select AA.group_id from tb_group_admin_rels as AA ');
            query.push('  left join tb_organization_admin as BB ');
            query.push('  on AA.organization_admin_id=BB.organization_admin_id ');
            query.push(util.format(' where BB.organization_admin_id="%s" ', params.organization_admin_id));

        } 
        query.push(util.format(' AND  AA.group_nm like "%s" ' ,'%'+params.search_name+'%' ));   
        query.push(' order by AA.group_nm asc ');    
        return query.join('');          
    }, 
    getGroupSearchLeft: (params) =>  {
        let query = [];  
        query.push('  SELECT group_id as id ,group_nm as text FROM `tb_group` AS AA ');
        query.push('  where  1=1 AND AA.del_yn="N"   ');
        query.push(util.format(' AND organization_id="%s" ', params.organization_id));
        query.push('  AND group_st="1" ');  
        query.push('  AND group_id not in( ');  
        query.push(SubQueries.selectGroupAdminRels(params));
        query.push('  ) '); 
        if(params.search_name)query.push(util.format(' AND  AA.group_nm like "%s" ' ,'%'+params.search_name+'%' ));   
        query.push(' order by AA.group_nm asc ');    
        return query.join('');          
    }, 
    getGroupSearchRight: (params) =>  {
        let query = [];  
        query.push('  SELECT group_id as id ,group_nm as text FROM `tb_group` AS AA ');
        query.push('  where  1=1 AND AA.del_yn="N"   ');
        query.push(util.format(' AND organization_id="%s" ', params.organization_id));
        query.push('  AND group_st="1" ');  
        query.push('  AND group_id in( ');  
        query.push(SubQueries.selectGroupAdminRels(params));
        query.push('  ) '); 
        if(params.search_name)query.push(util.format(' AND  AA.group_nm like "%s" ' ,'%'+params.search_name+'%' ));   
        query.push(' order by AA.group_nm asc ');    
        return query.join('');          
    },         
    getGroupListTopic: (params) =>  {
        let query = [];   
        query.push(' SELECT * FROM tb_group ');
        query.push(' where 1=1 AND del_yn="N"  ');
        query.push(util.format(' AND organization_id="%s" ', params.organization_id));
        return query.join('');          
    },  
    getGroupArray: (params) =>  {
        let query = [];   
        let arrData = (params.group_id)? params.group_id.split(',') : [];
        let arrData_cnt =arrData.length;
        query.push(' select * from (  ');
        arrData.forEach(function (row,index) {
            query.push(util.format('select "%s" as group_id ', row));
            if(index!=(arrData_cnt-1)) query.push(' union ');
        })   
        query.push(' )  as AA  ');
        query.push(' left join  tb_group as BB on AA.group_id=BB.group_id  ');
        query.push(' where 1=1 AND BB.del_yn="N" AND group_st="1" ');

        return query.join('');          
    },    
    getGroupArrayAll: (params) =>  {
        let query = [];   
        query.push(' select * from tb_group as AA  ');
        query.push(' where 1=1 AND AA.del_yn="N" AND group_st="1" ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));

        return query.join('');          
    },    
    deleteGroup : (id) => {
		let query = [];
        query.push(' UPDATE `tb_group` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(' ,del_yn="Y" ');
        query.push(util.format(' where group_id="%s" ', id));
        return query.join('');
    },                                   
};         

let SubQueries = {
    selectGroupAdminRels:  (params) => {
        let query = [];

        query.push('  SELECT AA.group_id FROM tb_group_admin_rels as AA ');
        query.push('  left join tb_organization_admin as BB on AA.organization_admin_id=BB.organization_admin_id ');
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND BB.organization_id="%s" ', params.organization_id));
        if(params.organization_admin_id) query.push(util.format(' AND AA.organization_admin_id="%s" ', params.organization_admin_id));

        return query.join('');
    }
};