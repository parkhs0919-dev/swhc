const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createGroupAdminRels: (params) => {
      let query = [];
      query.push(' INSERT INTO `tb_group_admin_rels` ');
      query.push(' ( ');
      query.push(' `organization_admin_id` ');
      query.push(' ,`group_id` ');
      query.push(' )');

      query.push(' VALUES ');
      query.push(' (');
      query.push(util.format(' "%s" ', params.organization_admin_id));
      query.push(util.format(' ,"%s" ', params.group_id));
      query.push(' )');

      return query.join('');
    },
    deleteGroupAdminRelsGroupId : (id) => {
		  let query = [];
      query.push(' DELETE FROM `tb_group_admin_rels` '); 
      query.push(util.format(' where group_id="%s" ', id));
                    
		return query.join('');      
    }, 
    deleteGroupAdminRelsId : (id) => {
		  let query = [];
      query.push(' DELETE FROM `tb_group_admin_rels` '); 
      query.push(util.format(' where group_admin_rels_id="%s" ', id));
                    
		return query.join('');      
    }, 
    deleteGroupAdminRelsOrganizationAdminId: (id) => {
      let query = [];
      query.push(' DELETE FROM `tb_group_admin_rels` ');
      query.push(util.format(' where organization_admin_id="%s" ', id));

      return query.join('');
    },
    getGroupAdminRelsDetail: (params) => {
      let query = [];
      query.push('  SELECT  AA.group_id,CC.group_nm  FROM tb_group_admin_rels as AA ');
      query.push('  left join tb_organization_admin as BB on AA.organization_admin_id =BB.organization_admin_id');
      query.push('  left join tb_group as CC on AA.group_id=CC.group_id ');
      query.push('  where 1=1 ');
      query.push(util.format(' AND AA.organization_admin_id="%s"  ', params.organization_admin_id))
      return query.join('');
    },
};         



