const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createOrganizationAdmin: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_organization_admin` ');
        query.push(' ( ');
        query.push(' `organization_id` ');
        query.push(' ,`admin_auth_id` ');
        query.push(' ,`admin_auth_pw` ');
        query.push(' ,`admin_nm` ');   
        query.push(' ,`admin_tel` ');
        if(params.admin_grade) query.push(' ,`admin_grade` ');   
        query.push(' ,`created_dt` ');           
        query.push(' ,`updated_dt` ');                     
        query.push(' )');   

        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_id));
        query.push(util.format(' ,"%s" ', params.admin_auth_id));
        query.push(util.format(' ,HEX(AES_ENCRYPT("%s", "%s")) ', params.admin_auth_pw, auth.crypto_key));  
        query.push(util.format(' ,"%s" ', params.admin_nm));
        query.push(util.format(' ,"%s" ', params.admin_tel));
        if(params.admin_grade) query.push(util.format(' ,"%s" ', params.admin_grade));
        query.push(' ,utc_timestamp() ');
        query.push(' ,utc_timestamp() ');        
        query.push(' )');       
          
        return query.join('');          
    },  
    getOrganizationAdminLoginCheck: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.admin_grade,AA.organization_admin_id,AA.admin_nm ,AA.admin_auth_id,AA.admin_tel,BB.organization_nm,BB.organization_id,BB.type FROM tb_organization_admin as AA ');
        query.push('  left join tb_organization as BB on AA.organization_id=BB.organization_id  ');
        query.push(util.format(' where AA.admin_auth_id="%s" ', params.admin_auth_id));
        query.push(util.format(' AND AA.admin_auth_pw=HEX(AES_ENCRYPT("%s", "%s")) ', params.admin_auth_pw, auth.crypto_key));  
        query.push(util.format(' AND BB.organization_auth_id="%s" ', params.organization_auth_id));
        query.push('  and AA.admin_st="1" ');
        return query.join('');          
    },   
    getOrganizationAdminCheck: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_organization_admin` AS AA ');
        query.push(util.format(' where AA.admin_auth_id="%s" ', params.admin_auth_id));
        return query.join('');          
    },  
    getOrganizationAdminLoginNewWindow: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.admin_grade,AA.organization_admin_id,AA.admin_nm ,AA.admin_auth_id,AA.admin_tel,BB.organization_nm,BB.organization_id,BB.type FROM `tb_organization_admin` AS AA ');
        query.push('  left join tb_organization as BB on AA.organization_id=BB.organization_id  ');
        query.push(util.format(' where AA.link_login_key="%s" ', params.link_login_key));
        return query.join('');          
    },  
    getOrganizationAdminSearchList: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_organization_admin` AS AA ');      
        query.push('  where 1=1  ');
        if(params.search_name)  query.push(util.format(' AND (AA.admin_nm  like "%s" OR AA.admin_auth_id like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push('  order by AA.admin_nm asc ');
        return query.join('');          
    },
    getOrganizationAdminDuplicateCheck: (params) =>  {
        let query = [];   
        query.push('  SELECT count(*) as cnt FROM `tb_organization_admin` AS AA ');
        query.push(util.format(' where AA.admin_auth_id="%s" ', params.admin_auth_id));
        return query.join('');           
    },    
    getOrganizationAdminInfoList: (params) =>  {
        let query = [];  
        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push('  SELECT * FROM ( ');
        query.push('   SELECT AA.* ,BB.group_nm_concat,BB.group_id_concat FROM tb_organization_admin as AA ');
        query.push('  left outer join ( ');
        query.push('  select AA.organization_admin_id,group_concat(BB.group_nm separator  ",") as group_nm_concat ');
        query.push('  ,group_concat(BB.group_id separator  ",") as group_id_concat ');
        query.push('  from tb_group_admin_rels as AA ');
        query.push('  left join tb_group as BB on AA.group_id=BB.group_id ');
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND ( "ALL" ="%s" OR BB.group_id="%s") ', params.group_id , params.group_id));
        query.push('  group by AA.organization_admin_id ');
        query.push('  ) as BB on AA.organization_admin_id=BB.organization_admin_id ');
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push(util.format(' AND ( "ALL" ="%s" OR AA.admin_st="%s") ',  params.admin_st , params.admin_st));
        if(params.search_name)  query.push(util.format(' AND (AA.admin_nm  like "%s" OR admin_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by AA.organization_admin_id DESC ');
        query.push('  ) as ZZ ');        

        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));

        return query.join('');          
    }, 
    getOrganizationAdminInfoListTotalCnt: (params) =>  {
        let query = [];  

        query.push('  SELECT count(*) as total_cnt FROM ( ');
        query.push('   SELECT AA.* ,BB.group_nm_concat,BB.group_id_concat FROM tb_organization_admin as AA ');
        query.push('  left outer join ( ');
        query.push('  select AA.organization_admin_id,group_concat(BB.group_nm separator  ",") as group_nm_concat ');
        query.push('  ,group_concat(BB.group_id separator  ",") as group_id_concat ');
        query.push('  from tb_group_admin_rels as AA ');
        query.push('  left join tb_group as BB on AA.group_id=BB.group_id ');
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND ( "ALL" ="%s" OR BB.group_id="%s") ', params.group_id , params.group_id));
        query.push('  group by AA.organization_admin_id ');
        query.push('  ) as BB on AA.organization_admin_id=BB.organization_admin_id ');
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push(util.format(' AND ( "ALL" ="%s" OR AA.admin_st="%s") ',  params.admin_st , params.admin_st));
        if(params.search_name)  query.push(util.format(' AND (AA.admin_nm  like "%s" OR admin_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push('  order by AA.organization_admin_id DESC ');
        query.push('  ) as ZZ ');   

        return query.join('');          
    }, 
    getOrganizationAdminDetail: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.*  ');
        query.push(util.format(' ,CAST( AES_DECRYPT(UNHEX(AA.admin_auth_pw), "%s") AS CHAR (10000) CHARACTER SET UTF8) AS admin_auth_pw_str',  auth.crypto_key));  
        query.push('  FROM tb_organization_admin as AA ');
        query.push('  where 1=1');
        query.push(util.format(' AND AA.organization_id="%s"  ', params.organization_id ))
        query.push(util.format(' AND AA.organization_admin_id="%s"  ', params.organization_admin_id ))
        return query.join('');          
    },  
    updateOrganizationAdmin:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_organization_admin` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,admin_auth_pw=HEX(AES_ENCRYPT("%s", "%s")) ', params.admin_auth_pw, auth.crypto_key)); 
        query.push(util.format(' ,admin_nm="%s" ', params.admin_nm));
        query.push(util.format(' ,admin_tel="%s" ', params.admin_tel));
        if(params.admin_grade)query.push(util.format(' ,admin_grade="%s" ', params.admin_grade));
        if(params.admin_st) query.push(util.format(' ,admin_st="%s" ', params.admin_st));        
        query.push(util.format(' where organization_admin_id="%s" ', id));
        return query.join('');
    },
    updateOrganizationAdminNewWindow:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_organization_admin` ');
        query.push(' SET ');
        query.push(util.format(' link_login_key="%s" ', params.link_login_key));
        query.push(util.format(' where organization_admin_id="%s" ', id));
        return query.join('');
    },      
    getOrganizationAdminFindId: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.admin_auth_id FROM `tb_organization_admin` AS AA ');
        query.push('  where 1=1  ');
        query.push(util.format(' and AA.admin_nm="%s" ', params.admin_nm));
        query.push(util.format(' and AA.admin_tel="%s" ', params.admin_tel));
        return query.join('');           
    },            
    getOrganizationAdminFindPw: (params) =>  {
        let query = [];   
        query.push('  SELECT AA.organization_admin_id ,AA.admin_auth_id,AA.admin_tel,AA.admin_nm FROM `tb_organization_admin` AS AA ');
        query.push('  where 1=1  ');
        query.push(util.format(' and AA.admin_nm="%s" ', params.admin_nm));
        query.push(util.format(' and AA.admin_tel="%s" ', params.admin_tel));
        query.push(util.format(' and AA.admin_auth_id="%s" ', params.admin_auth_id));        
        return query.join('');           
    }, 
    updateOrganizationAdminPassword:  (params, id) => {
        let query = [];
        query.push(' UPDATE `tb_organization_admin` ');
        query.push(' SET ');
        query.push(' updated_dt=utc_timestamp() ');
        query.push(util.format(' ,admin_auth_pw=HEX(AES_ENCRYPT("%s", "%s")) ', params.admin_auth_pw, auth.crypto_key));  
        query.push(util.format(' where organization_admin_id="%s" ', id));
        return query.join('');
    },     
    getOrganizationAdminSearchAllList: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_organization_admin` AS AA ');      
        query.push('  where 1=1  ');
        if(params.search_name)  query.push(util.format(' AND (AA.admin_nm  like "%s" OR AA.admin_auth_id like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push('  and AA.admin_st="1" ');
        query.push('  order by AA.admin_nm asc ');
        return query.join('');          
    },
    getOrganizationAdminSearchLeftList: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_organization_admin` AS AA ');      
        query.push('  where 1=1  ');
        if(params.search_name)  query.push(util.format(' AND (AA.admin_nm  like "%s" OR AA.admin_auth_id like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push('  and AA.admin_st="1" ');
        query.push('  AND AA.organization_admin_id not in ');
        query.push('  ( ');
        query.push(SubQueries.selectGroupAdminRels(params));
        query.push('  ) ');
        query.push('  order by AA.admin_nm asc ');
        return query.join('');          
    },
    getOrganizationAdminSearchRightList: (params) =>  {
        let query = [];   
        query.push('  SELECT * FROM `tb_organization_admin` AS AA ');      
        query.push('  where 1=1  ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));
        query.push('  and AA.admin_st="1" ');
        query.push('  AND AA.organization_admin_id in ');
        query.push('  ( ');
        query.push(SubQueries.selectGroupAdminRels(params));
        query.push('  ) ');
        query.push('  order by AA.admin_nm asc ');
        return query.join('');          
    },
}; 

let SubQueries = {
    selectGroupAdminRels:  (params) => {
        let query = [];

        query.push('  SELECT AA.organization_admin_id FROM tb_group_admin_rels as AA ');
        query.push('  left join tb_organization_admin as BB on AA.organization_admin_id=BB.organization_admin_id ');
        query.push('  WHERE 1=1 ');
        query.push(util.format(' AND BB.organization_id="%s" ', params.organization_id));
        if(params.group_id) query.push(util.format(' AND AA.group_id="%s" ', params.group_id));

        return query.join('');
    }
};