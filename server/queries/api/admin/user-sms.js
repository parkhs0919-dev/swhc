const util = require('util');

module.exports = {   
    createUserSmsLog: (params) =>  {
        let query = [];
        query.push(' INSERT INTO `tb_user_sms_log` ');
        query.push(' ( ');
        query.push(' `organization_id` ');   
        query.push(' ,`group_id` ');   
        query.push(' ,`organization_admin_id` ');   
        query.push(' ,`user_id` ');   
        query.push(' ,`ip` ');   
        query.push(' ,`rcv_num` ');     
        query.push(' ,`send_num` '); 
        query.push(' ,`contents` '); 
        query.push(' ,`status` '); 
        query.push(' ,`created_dt` ');      
        query.push(' )');   
           
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_id)); 
        query.push(util.format(' ,"%s" ', params.group_id)); 
        query.push(util.format(' ,"%s" ', params.organization_admin_id));         
        query.push(util.format(' ,"%s" ', params.user_id));         
        query.push(util.format(' ,inet_aton("%s") ', params.ip));  
        query.push(util.format(' ,"%s" ', params.rcv_num)); 
        query.push(util.format(' ,"%s" ', params.send_num)); 
        query.push(util.format(' ,"%s" ', params.contents)); 
        query.push(util.format(' ,"%s" ', params.status)); 
        query.push(' ,utc_timestamp() ');
        query.push(' )');           
      
        return query.join('');          
    },    
    updateUserSmsDuplicate : (params) => {
        let query = [];

        query.push(' INSERT INTO `tb_user_sms` ');
        query.push(' ( ');
        query.push(' `organization_id` ');
        query.push(' ,`user_id` ');
        query.push(' ,`sms_stop_yn` ');
        query.push(',`updated_dt`');
        if(params.end_dt) query.push(' ,`start_dt` ');
        if(params.end_dt) query.push(' ,`end_dt` ');
        query.push(' )');   
  
        query.push(' VALUES ');
        query.push(' (');
        query.push(util.format(' "%s" ', params.organization_id));
        query.push(util.format(' ,"%s" ', params.user_id));  
        query.push(util.format(' ,"%s" ', params.sms_stop_yn));
        query.push(',utc_timestamp()');
        if(params.end_dt) query.push(' ,utc_timestamp() ');
        if(params.end_dt) query.push(util.format(' ,convert_tz("%s", "+00:00", "-09:00") ', params.end_dt));
        query.push(' )');         

        query.push(' ON DUPLICATE KEY UPDATE ');
        query.push(' updated_dt=utc_timestamp() ');     
        query.push(util.format(' ,organization_id="%s" ', params.organization_id));
        query.push(util.format(' ,user_id="%s" ', params.user_id));
        query.push(util.format(' ,sms_stop_yn="%s" ', params.sms_stop_yn));
        if(params.end_dt) query.push(' ,start_dt=utc_timestamp() ');
        if(params.end_dt) query.push(util.format(' ,end_dt=convert_tz("%s", "+00:00", "-09:00") ', params.end_dt));
        return query.join('');    
    },      
    getUserSms: (params) =>  {
        let query = [];   
        let ymdhhmi='%Y-%m-%d %H:%i';
        query.push('  SELECT AA.organization_id ,AA.user_id,AA.sms_stop_yn');    
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.start_dt, "+00:00", "%s"), "%s") as start_dt_str ',params.z_value , ymdhhmi ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.end_dt, "+00:00", "%s"), "%s") as end_dt_str ',params.z_value , ymdhhmi ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.updated_dt, "+00:00", "%s"), "%s") as updated_dt_str ',params.z_value , ymdhhmi ));
        query.push('  from `tb_user_sms` AS AA ');

        query.push('  where 1=1 ');
        query.push(util.format(' and AA.user_id="%s" ', params.user_id));
        query.push(util.format(' and AA.organization_id="%s" ', params.organization_id));
        return query.join('');          
    },                                                        
    getUserSmsList: (params) =>  {
        let query = [];   
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';

        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push(' select AA.user_id,AA.group_nm ,CC.user_nm,CC.user_tel   ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(BB.start_dt, "+00:00", "%s"), "%s") as start_dt ',params.z_value , ymdhhmi ));
        query.push(util.format(' ,DATE_FORMAT(convert_tz(BB.end_dt, "+00:00", "%s"), "%s") as end_dt ',params.z_value , ymdhhmi ));
        query.push(util.format(' ,TIMESTAMPDIFF(MINUTE,convert_tz(utc_timestamp(), "+00:00", "%s"),convert_tz(BB.end_dt, "+00:00", "%s")) as time ', params.z_value, params.z_value));        
        query.push(' from ( ');
        query.push(' select AA.user_id,GROUP_CONCAT(BB.group_nm) AS group_nm  from tb_user_group_rels as AA  ');
        query.push(' LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id ');
        query.push(' LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id  ');
        query.push(' where  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N" ');
        query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
        // query.push(util.format(' AND AA.user_id="%s" ', params.user_id));
        query.push(' group by AA.user_id ');
        query.push(' ) as AA ');
    
        query.push(' left join tb_user_sms as BB on AA.user_id=BB.user_id ');
        query.push(' left join tb_user as CC on BB.user_id=CC.user_id ');
        query.push(' where 1=1  ');
        query.push(util.format(' AND BB.organization_id="%s" ', params.organization_id));
        query.push(' AND BB.sms_stop_yn="Y" ');
        query.push(' AND BB.end_dt>utc_timestamp() ');

        query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.updated_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.updated_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,'')));  

        if(params.search_name)  query.push(util.format(' AND (CC.user_nm  like "%s" OR CC.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));

        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));
        return query.join('');          
    },    
    getUserSmsListTotalCnt: (params) =>  {
        let query = [];  
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';

        query.push(' select count(*) as total_cnt   '); 
        query.push(' from ( ');
        query.push(' select AA.user_id,GROUP_CONCAT(BB.group_nm) AS group_nm  from tb_user_group_rels as AA  ');
        query.push(' LEFT JOIN tb_group as BB ON AA.group_id=BB.group_id ');
        query.push(' LEFT JOIN tb_organization as CC ON BB.organization_id=CC.organization_id  ');
        query.push(' where  AA.apply_cd="A1" and AA.expire_dt>now() and BB.del_yn="N" and CC.del_yn="N" ');
        query.push(util.format(' AND CC.organization_id="%s" ', params.organization_id));
        query.push(' group by AA.user_id ');
        query.push(' ) as AA ');
    

        query.push(' left join tb_user_sms as BB on AA.user_id=BB.user_id ');
        query.push(' left join tb_user as CC on BB.user_id=CC.user_id ');
        query.push(' where 1=1  ');
        query.push(util.format(' AND BB.organization_id="%s" ', params.organization_id));
        query.push(' AND BB.sms_stop_yn="Y" ');
        query.push(' and BB.end_dt>utc_timestamp() ');

        query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.updated_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        query.push(util.format(' AND DATE_FORMAT(convert_tz(BB.updated_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,'')));  

        if(params.search_name)  query.push(util.format(' AND (CC.user_nm  like "%s" OR CC.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));

        return query.join('');          
    }, 
    getUserSmsLogList: (params) =>  {
        let query = [];   
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';

        query.push('  SELECT * FROM  ( ');
        query.push('  SELECT @RNUM:=@RNUM+1 AS ROWNUM  ,A.*  ');
        query.push('  FROM (SELECT @RNUM:=0) R, ( ');

        query.push(' SELECT AA.*,BB.organization_nm,CC.admin_nm,DD.group_nm,EE.user_nm,EE.user_tel    ');
        query.push(util.format(' ,DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s") as created_dt_str ',params.z_value , ymdhhmi ));      
        query.push(' FROM tb_user_sms_log as AA ');
        query.push(' left join tb_organization as BB on AA.organization_id=BB.organization_id  ');
        query.push(' left join tb_organization_admin as CC on AA.organization_admin_id=CC.organization_admin_id ');
        query.push(' left join tb_group as DD on AA.group_id=DD.group_id ');
        query.push(' left join tb_user as EE on AA.user_id=EE.user_id ');
        query.push(' where  1=1 ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));

        query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,'')));  

        if(params.search_name)  query.push(util.format(' AND (EE.user_nm  like "%s" OR EE.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));

        query.push('  ) A  ');
        query.push('  ) Z ');
        query.push(util.format('  limit %s , %s  ', params.start_num , params.count_num));
        return query.join('');          
    },    
    getUserSmsLogListTotalCnt: (params) =>  {
        let query = [];  
        let ymd='%Y%m%d';
        let ymdhhmi='%Y-%m-%d %H:%i';

        query.push(' select count(*) as total_cnt   ');      
        query.push(' FROM tb_user_sms_log as AA ');
        query.push(' left join tb_organization as BB on AA.organization_id=BB.organization_id  ');
        query.push(' left join tb_organization_admin as CC on AA.organization_admin_id=CC.organization_admin_id ');
        query.push(' left join tb_group as DD on AA.group_id=DD.group_id ');
        query.push(' left join tb_user as EE on AA.group_id=EE.user_id ');
        query.push(' where  1=1 ');
        query.push(util.format(' AND AA.organization_id="%s" ', params.organization_id));

        query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")>="%s"  ',params.z_value ,ymd ,params.start_date.replace(/\-/g,'')));        
        query.push(util.format(' AND DATE_FORMAT(convert_tz(AA.created_dt, "+00:00", "%s"), "%s")<="%s" ',params.z_value ,ymd,params.end_date.replace(/\-/g,'')));  

        if(params.search_name)  query.push(util.format(' AND (EE.user_nm  like "%s" OR EE.user_tel like "%s") ' ,'%'+params.search_name+'%' ,'%'+params.search_name+'%'));

        return query.join('');          
    }, 

    deleteUserSms: (params,id) =>  {
        let query = [];
        query.push(' DELETE FROM `tb_user_sms` '); 
        query.push(' where 1=1 '); 
        query.push(util.format('  and organization_id="%s" ', params.organization_id));
        query.push(util.format('  and user_id="%s" ', id));
                    
        return query.join('');      
    },                                        
};         


