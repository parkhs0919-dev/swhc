const moment = require('moment-timezone');

module.exports = async (app) => {  
    app.use(async (req, res, next) =>{
        let timezone = (req.headers['TimeZone'])? req.headers['TimeZone'] : "Asia/Seoul";
        if(req.method=="GET"){
            req.query.timezone = timezone;    
            req.query.z_value = getTimeZoneValue(timezone);       
        }else if(req.method=="POST" || req.method=="PUT"){          
            req.body.timezone = timezone; 
            req.body.z_value = getTimeZoneValue(timezone); 
        }else {        
        }
        await next();      
    })               
};          
  
function getTimeZoneValue(timezone){ 
   return moment().tz(timezone).format('Z');
}
