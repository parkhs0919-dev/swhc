
module.exports = async (app) => {  
    app.use(async (req, res, next) => {
        if(req.method=="GET"){
            let lang = req.cookies['lang']==undefined?'ko':req.cookies['lang'];
            console.log('lang====',lang)  
            let metadata ={
                codes:{},  
                langs:{}
            }
            if(lang && lang=="en"){
                metadata.codes=Metadata.en.codes;
                metadata.langs=Metadata.en.langs;
                res.cookie('lang', 'en');
            }else{
                metadata.codes=Metadata.ko.codes;
                metadata.langs=Metadata.ko.langs;  
                res.cookie('lang', 'ko');
            }
            res.locals.metadata = metadata;
        }
        await next();      
    })               
};          
  