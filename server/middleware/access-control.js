
const accessControl = CONFIG.accessControl[SERVER_ENV];
module.exports = (app) => {
    app.use(function (req, res, next) {
    var origin = req.headers.origin;
    var allowOrigin = accessControl.allowOrigin;
    // if (allowOrigin.indexOf(origin) > -1) {    
    //     res.setHeader('Access-Control-Allow-Origin', origin);
    // }
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.setHeader('Access-Control-Allow-Methods', accessControl.allowMethods);
    res.setHeader('Access-Control-Allow-Headers', accessControl.allowHeaders);
    next();   
    })  
};          
  