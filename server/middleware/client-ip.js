const getIP = require('ipware')().get_ip;

module.exports = (app) => {
    app.use(function(req, res, next) {
        let clientIp=getCallerIP(req)
        console.log('clientIp : '+clientIp);
        if(req.method=="GET"){
            req.query.clientIp = clientIp+'';         
        }else if(req.method=="POST" || req.method=="PUT"){          
            req.body.clientIp = clientIp+''; 
        }
        next();
    });
};        
                                    
function getCallerIP(request) {
    let ip = request.headers['x-forwarded-for'] ||
        request.connection.remoteAddress ||
        request.socket.remoteAddress ||
        request.connection.socket.remoteAddress;
    ip = ip.split(',')[0];
    ip = ip.split(':').slice(-1); //in case the ip returned in a format: "::ffff:146.xxx.xxx.xxx"
    return ip;
}