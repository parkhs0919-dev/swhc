const path = require('path');
const auth = require('../utils/auth');  
const apiPath = path.join(__dirname, '../../routes/api');
const adminPath = path.join(__dirname, '../../routes/api/admin');
const sysPath = path.join(__dirname, '../../routes/api/sys');
const defaultPath = path.join(__dirname, '../../routes');

module.exports = (app) => {
    
    //앱 api
    app.use('/api/login' ,require(apiPath+'/v1/login')); 
    app.use('/api/register' ,require(apiPath+'/v1/register')); 
    app.use('/api/health' ,require(apiPath+'/v1/health'));  

    app.use('/api/main' ,auth.apiAuthorizedUser ,require(apiPath+'/v1/main')); 
    app.use('/api/user' ,auth.apiAuthorizedUser ,require(apiPath+'/v1/user')); 
    app.use('/api/paper',auth.apiAuthorizedUser  ,require(apiPath+'/v1/paper'));  
    app.use('/api/user-device' ,auth.apiAuthorizedUser ,require(apiPath+'/v1/user-device'));
    app.use('/api/time' ,auth.apiAuthorizedUser ,require(apiPath+'/v1/time'));
    app.use('/api/notice' ,auth.apiAuthorizedUser ,require(apiPath+'/v1/notice'));
    app.use('/api/message' ,auth.apiAuthorizedUser ,require(apiPath+'/v1/message')); 

    
    app.use('/api/v2/login' ,require(apiPath+'/v2/login')); 
    app.use('/api/v2/register' ,require(apiPath+'/v2/register')); 

    app.use('/api/v2/health' ,auth.apiAuthorizedUserV2 ,require(apiPath+'/v2/health'));  
    app.use('/api/v2/user' ,auth.apiAuthorizedUserV2 ,require(apiPath+'/v2/user')); 
    app.use('/api/v2/user-device' ,auth.apiAuthorizedUserV2 ,require(apiPath+'/v2/user-device'));
    app.use('/api/v2/main' ,auth.apiAuthorizedUserV2 ,require(apiPath+'/v2/main')); 
    app.use('/api/v2/paper',auth.apiAuthorizedUserV2  ,require(apiPath+'/v2/paper'));     
    app.use('/api/v2/time' ,auth.apiAuthorizedUserV2 ,require(apiPath+'/v2/time'));
    app.use('/api/v2/notice' ,auth.apiAuthorizedUserV2 ,require(apiPath+'/v2/notice'));
    app.use('/api/v2/message' ,auth.apiAuthorizedUserV2 ,require(apiPath+'/v2/message')); 

    //admin api
    app.use('/api/admin/login' ,require(adminPath+'/login'));
    app.use('/api/admin/organization' ,require(adminPath+'/organization'));

    app.use('/api/admin/organization-admin',auth.adminAuthorizedByCookie ,require(adminPath+'/organization-admin'));
    app.use('/api/admin/notice',auth.adminAuthorizedByCookie ,require(adminPath+'/notice'));
    app.use('/api/admin/user' ,auth.adminAuthorizedByCookie ,require(adminPath+'/user'));   
    app.use('/api/admin/group' ,auth.adminAuthorizedByCookie ,require(adminPath+'/group')); 
    app.use('/api/admin/message' ,auth.adminAuthorizedByCookie ,require(adminPath+'/message')); 
    app.use('/api/admin/schedule' ,auth.adminAuthorizedByCookie ,require(adminPath+'/schedule')); 
    app.use('/api/admin/share' ,auth.adminAuthorizedByCookie ,require(adminPath+'/share')); 
    app.use('/api/admin/main' ,auth.adminAuthorizedByCookie ,require(adminPath+'/main')); 

    app.use('/api/sys/login' ,require(sysPath+'/login'));
    app.use('/api/sys/paper'  ,require(sysPath+'/paper')); 
    app.use('/api/sys/organization'  ,require(sysPath+'/organization')); 
    app.use('/api/sys/user'  ,require(sysPath+'/user')); 
    app.use('/api/sys/main'  ,require(sysPath+'/main')); 
    app.use('/api/sys/notice'  ,require(sysPath+'/notice')); 
    app.use('/api/sys/user-device'  ,require(sysPath+'/user-device')); 

    app.use('/health-check' ,require(defaultPath+'/health-check'));

    app.use('/patch/login',require(defaultPath+'/patchlogin'));
    app.use('/patch/',require(defaultPath+'/patchindex'));
    app.use('/directlogin' ,require(defaultPath+'/directlogin'));
    app.use('/sys/login' ,require(defaultPath+'/syslogin'));
    app.use('/sys/',auth.SysAuthorizedByCookie,require(defaultPath+'/sysindex'));
    // app.use('/ko',auth.adminAuthorizedByCookie ,require(defaultPath+'/ko')); 
    // app.use('/en',auth.adminAuthorizedByCookie ,require(defaultPath+'/en')); 
    app.use('/*/login' ,require(defaultPath+'/login'));
    app.use('/',auth.adminAuthorizedByCookie,require(defaultPath+'/index'))

    //app.use('/semple/test' ,require(defaultPath+'/semple/test'));
};        
                                     
  
   