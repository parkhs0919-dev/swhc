
const CODE = require('../metadata/codes');

module.exports = async (app) =>{  
    app.use(async (req, res, next) => {
        if(req.path.indexOf("/api/admin/")!=-1){
            req.json = async (data, status = 200,m) => {
                res.status = status;
                let code = CODE.ko.m_code;
                let body ={
                    code : data.code,
                    data : null,
                    msg : null
                }
                if(m){
                    data.msg = m;
                }else{
                    if(data.code && data.code!="0000"){
                        if(code[data.code]) body.msg = code[data.code];
                    }
                } 
                if(data.data) body.data = data.data;
                if(!data.code) body.code="9900";
                res.send(body);
            };
            req.error = async (code, status = 400) => {
                res.status = status;
                let m_code = CODE.ko.m_code;
                if(m_code[code]) data.msg = m_code[code];
                if(!code) data.code="9900";
                res.send(data);
            };
        }else{
            req.json = async (data, status = 200,m) => {
                res.status = status;
                let code = CODE.ko.m_code;
                if(m){
                    data.msg = m;
                }else{
                    if(data.code && data.code!="0000"){
                        if(code[data.code]) data.msg = code[data.code];
                    }
                } 
                if(!data.code) data.code="9900";
                res.send(data);
            };
            req.error = async (code, status = 400) => {
                res.status = status;
                let m_code = CODE.ko.m_code;
                if(m_code[code]) data.msg = m_code[code];
                if(!code) data.code="9900";
                res.send(data);
            };
        } 
        await next();     
    })  
}          
