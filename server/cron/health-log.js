const fs = require("fs");
const path = require("path");
const moment = require('moment');
 
module.exports = async () => {
}

module.exports.createTableHealthLog = () => {
    return new Promise(async (resolve, reject) => {
        try {
            let currentMonth =moment().add(0, 'M').format("YYYYMM");
            let nextMonth =moment().add(1, 'M').format("YYYYMM");
            await Api.V2HealthLog.createTableHealthLog({yyyymm: currentMonth});
            await Api.V2HealthLog.createTableHealthLog({yyyymm: nextMonth});
            resolve(true);  
        } catch (e) {
            console.error('fail', e);
            reject(e);
        }
    });
};
