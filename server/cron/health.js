
module.exports = async () => {
}

module.exports.cronInitHealthCheck = () => {
    return new Promise(async (resolve, reject) => {
        try {
            await Api.AdHealth.cronInitHealthCheck();
            resolve(true);  
        } catch (e) {
            console.error('fail', e);
            reject(e);
        }
    });
};

module.exports.cronInitHealthGradeCheck = () => {
    return new Promise(async (resolve, reject) => {
        try {
            await Api.AdHealth.cronInitHealthGradeCheck();
            resolve(true);  
        } catch (e) {
            console.error('fail', e);
            reject(e);
        }
    });
};
