
module.exports = async () => {
}

module.exports.cronUserTimeCheck = () => {
    return new Promise(async (resolve, reject) => {
        try {
           
            resolve(true);  
        } catch (e) {
            console.error('fail', e);
            reject(e);
        }
    });
};

module.exports.cronInitUserTime = () => {
    return new Promise(async (resolve, reject) => {
        try {
             
            await Api.AdUserTime.updateInitUserTime();
            resolve(true);  
        } catch (e) {
            console.error('fail', e);
            reject(e);
        }
    });
};
module.exports.cronUserTimePush = () => {
    return new Promise(async (resolve, reject) => {
        try {
             
            await Api.AdUserTime.getUserTimePush();
            resolve(true);  
        } catch (e) {
            console.error('fail', e);
            reject(e);
        }
    });
};
