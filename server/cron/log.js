const fs = require("fs");   
const path = require("path");  
const pm2LogDir = path.join(__dirname, '../../../pm2/');  

if (!fs.existsSync(pm2LogDir)) {
	fs.mkdirSync(pm2LogDir) 
}    

module.exports = async () => {     
}       

module.exports.pm2LogBackup = () => {  
    return new Promise(async (resolve, reject) => {
        try {
            const cp  = await Api.childprocess.cmd(" \\cp -rf /root/.pm2/logs /home/pm2/ ");
            const pm2 = await Api.childprocess.cmd(" pm2 flush ");     
            resolve(true);      
        } catch (e) {     
            console.error('pm2LogBackup fail', e);
            reject(e);
        }   
    });
};     