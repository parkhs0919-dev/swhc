const cron = require('node-cron');
const log = require('./log');
const healthLog = require('./health-log');
const health = require('./health');
const userTime = require('./user-time');

const rand_schedule =Math.floor(Math.random() * 59)+" "+Math.floor(Math.random() * 4)+" * * *";

module.exports = async () => { 
    healthLog.createTableHealthLog();
    //health 테이블 생성  
    cron.schedule('30 2 * * 0', () => {//30 2 * * 0 매일 일요일 2시30분
        healthLog.createTableHealthLog();
    }); 
    //health 업데이트 시간 체크 
    cron.schedule('*/30 * * * *', () => {
        console.log('-health check 30m-')
        health.cronInitHealthCheck();
    }); 
    //health grade 체크 초기화 
    cron.schedule('*/15 * * * *', () => {
        console.log('-grade check 10m -')
        health.cronInitHealthGradeCheck();
    });    
}