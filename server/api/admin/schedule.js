
const url = require('url');
const convertTime = require('convert-time');

module.exports={  
  createSchedule: (params) => {
    return new Promise(async (resolve, reject) => {
      try { 
          let result ={code :"8300",data:{}}
          params.visit_time_str  = convertTime(params.visit_time,"hhMM");  
          let query = Queries.AdSchedule.createSchedule(params);
          Mysql.execQueryPromise(query)
          .then(
            (rows) => {
              let affectedRows = rows.affectedRows;
              if (affectedRows >= 1) {
                result.code="0000";
                result.data.schedule_id=rows.insertId;
                resolve(result);
              }else{
                resolve({ code: '8100' });
              } 
            }
          )
          .fail(
            (err) => {
              console.log(err)
              resolve({ code: '8200' });
            }
          );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  updateSchedule: (params, id) => {
    return new Promise(async (resolve, reject) => {
      try {    
        params.visit_time_str  = convertTime(params.visit_time,"hhMM");  
        let query = Queries.AdSchedule.updateSchedule(params, id);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            let affectedRows = rows.affectedRows;
            if (affectedRows >= 1) resolve({ code: '0000' });
            else resolve({ code: '8100' });
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: '8200' });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  getSchedule: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let result ={code :"8300",data:{}}
        let query = Queries.AdSchedule.getSchedule(params);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            result.code="0000";
            result.data.rows= rows[0];
            resolve(result);
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  getScheduleList: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}}
            let query = Queries.AdSchedule.getScheduleList(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                  result.code="0000";
                  result.data.rows= rows;
                  resolve(result);
                }
            )               
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },  
  getScheduleDetail: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}}
            let query = Queries.AdSchedule.getScheduleDetail(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                  result.code="0000";
                  result.data.rows= rows[0];
                  resolve(result);
                }
            )
            .fail(  
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },
  deleteSchedule: (id) => {
    return new Promise(async (resolve, reject) => {
        try {                
            let query=Queries.AdSchedule.deleteSchedule(id);
            Mysql.execQueryPromise(query)  
            .then(             
                (rows) => {                 
                    let affectedRows=rows.affectedRows;      
                    if(affectedRows>=1) resolve({code: '0000'});
                    else     resolve({code: '8100'});    
                }
              )                    
            .fail(   
              (err) => {
                resolve({code: '8200'});
              }   
            );                          
        } catch (e) {  
          resolve({code: '8300'});
        }
    }); 
},                      
};                       
  