
const url = require('url');
const moment = require('moment');

module.exports={  
    createUserInfoProcess: (params) => {
        return new Promise(async (resolve, reject) => {
            try {  
                let query=Queries.AdUser.createUserInfo(params);
                Mysql.execQueryPromise(query)            
                .then(
                    (rows) => {
                        let affectedRows = (rows)? rows.affectedRows : 0;
                        if (affectedRows >= 1) {
                          params.user_id = rows.insertId;
                          query=Queries.AdHealth.createHealth(params);
                          //console.log(query);
                          return  Mysql.execQueryPromise(query);
                        } else {
                          return null
                        }
                    }
                )
                .then(
                    (rows) => {
                        let affectedRows = (rows)? rows.affectedRows : 0;
                        if (affectedRows >= 1) {
                          query=Queries.AdUserDevice.createUserDevice(params);
                          //console.log(query);
                          return  Mysql.execQueryPromise(query);
                        } else {
                          return null
                        }
                    }
                ) 
                .then(
                    (rows) => {
                        let affectedRows = (rows)? rows.affectedRows : 0;
                        if (affectedRows >= 1) {
                            query=Queries.AdUserTime.createUserTime(params);
                          //console.log(query);
                          return  Mysql.execQueryPromise(query);
                        } else {
                          return null
                        }
                    }
                )                 
                .then(
                    (rows) => {
                        let affectedRows = rows.affectedRows;
                        if (affectedRows >= 1) resolve({ code: '0000' });
                        else resolve({ code: '8100' });
                    }
                )                                                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },    
    getUserListAll: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdUser.getUserListAll(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        resolve(result);
                    }
                )              
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    }, 
    getUserList: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdUser.getUserList(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        query = Queries.AdUser.getUserListTotalCnt(params);
                        return Mysql.execQueryPromise(query);
                    }
                )
                .then(
                    (rows) => {
                        result.data.total_cnt=rows[0].total_cnt;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },     
    getUserInfoList: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdUser.getUserInfoList(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        query = Queries.AdUser.getUserInfoListTotalCnt(params);
                        return Mysql.execQueryPromise(query);
                    }
                )
                .then(
                    (rows) => {
                        result.data.total_cnt=rows[0].total_cnt;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },     
    getUserConfirmedList: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdUser.getUserConfirmedList(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        resolve(result);
                    }
                )
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },     
    getUserDetail: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdUser.getUserDetail(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        if(rows && rows.length >= 1){
                          result.code="0000";
                          result.data.detail= rows[0];  
                          resolve(result);
                        }else{
                            resolve({ code: "8100" });
                        }
                    }
                )              
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },
    getUserDetailTempLog: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdUser.getUserDetailTempLog(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        query = Queries.AdUser.getUserDetailTempLogTotalCnt(params);
                        return Mysql.execQueryPromise(query);
                    }
                )
                .then(
                    (rows) => {
                        result.data.total_cnt=rows[0].total_cnt;
                        resolve(result);
                    }
                )   
                .then(
                    (rows) => {
                        result.temp_log= rows;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },   
    getUserDetailTempLogExcel: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                query = Queries.AdUser.getUserDetailTempLogExcel(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        resolve(result);
                    }
                )               
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },        
    getUserInfoDetail: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdUser.getUserDetail(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {  
                        if(rows && rows.length >= 1){
                            result.code="0000";
                            result.data.detail=rows[0];
                            resolve(result);
                        }else{
                            resolve({code: '8100'});
                        }     
                    }  
                )                    
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },    
    getUserConfirmedDetail: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let currentMonth =moment().add(0, 'M').format("YYYYMM");
                let preMonth =moment().add(-1, 'M').format("YYYYMM");
                console.log(preMonth)
                let dateArray =[preMonth,currentMonth]
                let query = Queries.AdUser.getUserConfirmedDetail(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        if(rows && rows.length >= 1){
                          result.code="0000";
                          result.data.detail= rows[0];  
                          query = Queries.AdUser.getUserConfirmedDetailTempLog(params,dateArray);
                          return Mysql.execQueryPromise(query);
                        }else{
                          result.code="8100";
                          return null;
                        }
                    }
                )
                .then(
                    (rows) => {
                        result.data.temp_log= rows;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },  
    getUserConfirmedDetailExcel: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let currentMonth =moment().add(0, 'M').format("YYYYMM");
                let preMonth =moment().add(-1, 'M').format("YYYYMM");
                let dateArray =[preMonth,currentMonth]
                let query = Queries.AdUser.getUserConfirmedDetailTempLog(params,dateArray);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows= rows;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },       
    getUserSearchSelectBox: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdUser.getUserSearchSelectBox(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        result.code="0000";
                        result.data.rows= rows;
                        resolve(result);
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },      
    updateUserConfirmed: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {              
                let query=Queries.AdUser.updateUserConfirmed(params,id);       
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1) resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                ) 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    },
    updateUserInfo: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {              
                let query=Queries.AdUser.updateUserInfo(params,id);       
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1) resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                ) 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    }, 
    updateUserPasswordInit: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {              
                let query=Queries.AdUser.updateUserPasswordInit(params,id);      
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1) resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                ) 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    },       
    getUserDuplicateCheck: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let query = Queries.AdUser.getUserDuplicateCheck(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows[0].cnt >= 1) {
                            resolve({ code: '1400' });
                        } else {
                            resolve({ code: '0000' });
                        }
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    }, 
    getUserAuthIdDuplicateCheck: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let user_auth_id="";
                let query = Queries.AdUser.getUserAuthIdDuplicateCheck(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {
                            rows.forEach(row => {
                                if(!row.user_id){
                                    user_auth_id=row.user_auth_id;
                                }    
                            })
                            result.code="0000";
                            result.data.user_auth_id=user_auth_id;
                            resolve(result);
                        } else {
                            return null;
                        }  
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },
    getUserGroupRelsList: (params) => {
        return new Promise(async (resolve, reject) => {
            try {       
                let result ={code :"8300",data:{}} 
                let query=Queries.AdUser.getUserGroupRelsList(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        query = Queries.AdUser.getUserGroupRelsListTotalCnt(params);
                        return Mysql.execQueryPromise(query);
                    }
                )
                .then(
                    (rows) => {
                        result.data.total_cnt=rows[0].total_cnt;
                        resolve(result);
                    }
                )  
                .fail(   
                  (err) => {
                    console.log(err)
                    resolve({code: 8200 });
                  }   
                );                        
            } catch (e) {
                
               resolve({code: '8300'}); 
            }
        });
    },    
    deleteUser: (id) => {
        return new Promise(async (resolve, reject) => {
            try {                
                let query=Queries.AdUser.deleteUser(id);
                Mysql.execQueryPromise(query)  
                .then(             
                    (rows) => {                 
                        let affectedRows=rows.affectedRows;      
                        if(affectedRows>=1) resolve({code: '0000'});
                        else     resolve({code: '8100'});    
                    }
                  )                    
                .fail(   
                  (err) => {
                    resolve({code: '8200'});
                  }   
                );                          
            } catch (e) {  
              resolve({code: '8300'});
            }
        }); 
    }, 
    deleteUserOut: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {            
                let query=Queries.AdUser.deleteUserOut(params,id);
                Mysql.execQueryPromise(query)  
                .then(                  
                  (rows) => {              
                    resolve({code: '0000' });   
                  }      
                )
                .fail(   
                  (err) => {
                    resolve({code: 8200 });
                  }   
                );                        
            } catch (e) {
               resolve({code: '8300'}); 
            }
        });
      },                                             
};                       
  