
const url = require('url');

module.exports={  
    createGroupProcess: (params) => {
        return new Promise(async (resolve, reject) => {
            try {  
                let result ={code :"8300",data:{}};   
                let organizationAdminIdArray = (params.organization_admin_id_array)? params.organization_admin_id_array.split(",") : [];
                let query=Queries.AdGroup.createGroup(params);                
                Mysql.execQueryPromise(query)            
                .then(
                  (rows) => {
                    let affectedRows = (rows)? rows.affectedRows : 0;
                    if (affectedRows >= 1) {
                      result.code="0000";
                      params.group_id = rows.insertId;
                      let sqls=[];
                      organizationAdminIdArray.forEach(item => {
                          params.organization_admin_id = item;
                          if(item) sqls.push(Queries.AdGroupAdminRels.createGroupAdminRels(params))
                      });  
                      return  Mysql.beginTransaction(sqls);
                    } else {
                      result.code="8100";
                      return null;
                    }
                  }
                )
                .then(
                  (rows) => {
                     resolve(result);
                  }
                )                
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({ code: '8200' });
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },
    createGroup: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.AdGroup.createGroup(params);
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000' });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },
    updateGroupProcess: (params,id) => {
      return new Promise(async (resolve, reject) => {
          try {   
              let query=Queries.AdGroup.updateGroup(params,id);
              Mysql.execQueryPromise(query)            
              .then(
                (rows) => {
                  let affectedRows = (rows)? rows.affectedRows : 0;
                  if (affectedRows >= 1) {
                    resolve({ code: '0000' });
                  } else {
                    resolve({ code: '8100' });
                  }
                }
              )            
              .fail(
                (err) => {
                  console.log(err)
                  resolve({ code: '8200' });
                }
              );                       
          } catch (e) {console.log(e)
            resolve({code: '8300'});  
          }
      });
  },    
  getGroupDetail: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={code :"8300",data:{}};
              let query = Queries.AdGroup.getGroupDetail(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {   
                    if(rows.length>=1){
                      result.code="0000";
                      result.data.detail=rows[0];
                      resolve(result); 
                    }else{
                      resolve({code: '8100'}); 
                    } 
                  }
              )             
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {s
              console.log(e)
              resolve({ code: '8300' });
          }
      });
  }, 
  getGroupOptionList: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}};
            let query = Queries.AdGroup.getGroupOptionList(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {   
                  result.code="0000";
                  result.data.rows=rows;
                  resolve(result); 
                }  
            )             
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  }, 
  getGroupList: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}}
            let query = Queries.AdGroup.getGroupList(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                    result.code="0000";
                    result.data.rows=rows;
                    query = Queries.AdGroup.getGroupListTotalCnt(params);
                    return Mysql.execQueryPromise(query);
                }
            )
            .then(
                (rows) => {
                    result.data.total_cnt=rows[0].total_cnt;
                    resolve(result);
                }
            )                
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },  
  getGroupSearchSelectBox: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}}
            let query = Queries.AdGroup.getGroupSearchSelectBox(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {   
                  result.code="0000";
                  result.data.rows=rows;
                  resolve(result);
                }  
            )             
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  }, 
  getGroupSearchLeft: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}}
            let query = Queries.AdGroup.getGroupSearchLeft(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {   
                  result.code="0000";
                  result.data.rows=rows;
                  resolve(result);
                }  
            )             
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },  
  getGroupSearchRight: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}}
            let query = Queries.AdGroup.getGroupSearchRight(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {   
                  result.code="0000";
                  result.data.rows=rows;
                  resolve(result);
                }  
            )             
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },  
  deleteGroup: (id) => {
    return new Promise(async (resolve, reject) => {
        try {                
            let query=Queries.AdGroup.deleteGroup(id);
            //console.log(query)
            Mysql.execQueryPromise(query)  
            .then(             
                (rows) => {                 
                    let affectedRows=rows.affectedRows;      
                    if(affectedRows>=1) resolve({code: '0000'});
                    else     resolve({code: '8100'});    
                }
              )                    
            .fail(   
              (err) => {
                resolve({code: '8200'});
              }   
            );                          
        } catch (e) {  
          resolve({code: '8300'});
        }
    }); 
  },                       
};                       
  