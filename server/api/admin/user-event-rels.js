
const url = require('url');

module.exports={  
  updateUserEventRels: (params,id) => {
    return new Promise(async (resolve, reject) => {
      try {
        let groupIdArray= params.group_id.split(",");
        let query = Queries.AdUserEventRels.deleteUserEventRels(params,id);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
              let sqls=[];
              groupIdArray.forEach((data) => {
                let sub_params={
                  organization_id:params.organization_id,
                  user_event_id:id,
                  group_id :data
                }
                if(data) sqls.push(Queries.AdUserEventRels.createUserEventRels(sub_params)); 
              })            
              return Mysql.beginTransaction(sqls);
          }
        )
        .then(
          (rows) => {
            resolve({ code: '0000' });
          }
        )        
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: '8200' });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  getUserEventRelsListLeft: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let query = Queries.AdUserEventRels.getUserEventRelsListLeft(params);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            resolve({ code: '0000', rows: rows });
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  getUserEventRelsList: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let query = Queries.AdUserEventRels.getUserEventRelsList(params);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            resolve({ code: '0000', rows: rows });
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },                    
};                       
  