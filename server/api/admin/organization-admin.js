
const jwt = require('jwt-simple');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();
const sms = CONFIG.sms[SERVER_ENV];

module.exports={  
    createOrganizationAdminProcess: (params) => {
        return new Promise(async (resolve, reject) => {
            try {  
                let result ={code :"8300",data:{}}; 
                let groupIdArray = (params.group_id_array)? params.group_id_array : [];
                let query=Queries.AdOrganizationAdmin.createOrganizationAdmin(params);
                Mysql.execQueryPromise(query)            
                .then(
                  (rows) => {
                    let affectedRows = (rows)? rows.affectedRows : 0;
                    if (affectedRows >= 1) {
                      let sqls = [];  
                      result.code="0000";
                      params.organization_admin_id = rows.insertId;
                      if(params.admin_grade!="M"){
                        groupIdArray.forEach(item => {
                          params.group_id = item;
                          if (item) sqls.push(Queries.AdGroupAdminRels.createGroupAdminRels(params))
                        });
                        return Mysql.beginTransaction(sqls);
                      }else{
                        return null;
                      }

                    } else {
                      result.code="8100";     
                      return null;
                    }   
                  }
                )
                .then(
                  (rows) => {
                     resolve(result);
                  }
                )                
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({ code: '8200' });
                  }
                );                     
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },
    createGroupAdmin: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.AdOrganizationAdmin.createOrganizationAdmin(params);
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000' });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },    
    getOrganizationAdminLoginCheck: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={};               
              let query=Queries.AdOrganizationAdmin.getOrganizationAdminLoginCheck(params);
              Mysql.execQueryPromise(query)  
              .then(                    
                (rows) => {    
                  if(rows.length>=1){
                    result.code = "0000";
                    result.token = createToken(rows[0]);   
                    resolve(result);
                  }else{
                    resolve({code: '8100'});
                  }     
                }      
              )
              .fail(   
                (err) => {console.log(err)
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {console.log(e)
             resolve({code: '8300'}); 
          }
      });
    },
    getOrganizationAdminCheck: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let query = Queries.AdOrganizationAdmin.getOrganizationAdminCheck(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                      if (rows && rows.length >= 1) {
                          resolve({ code: '1210' });
                      } else {
                          resolve({ code: '0000' });
                      }
                  }
              )             
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
    },  
    getOrganizationAdminSearchList: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={code :"8300",data:{}};
              let query = Queries.AdOrganizationAdmin.getOrganizationAdminSearchList(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                    result.code="0000";
                    result.data.rows=rows;
                    resolve(result);
                  }
              )
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
  }, 
  getOrganizationAdminLoginNewWindow: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={code :"8300",data:{}};
              let query = Queries.AdOrganizationAdmin.getOrganizationAdminLoginNewWindow(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                    //console.log(rows)
                    if(rows.length>0){
                      result.code="0000";
                      result.data=rows[0];
                      result.token = createToken(rows[0]);  
                      resolve(result);
                    }else{
                      resolve({code:"8100"});
                    }

                  }
              )
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
  }, 
  getOrganizationAdminSearchAllList: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={code :"8300",data:{}};
              let query = Queries.AdOrganizationAdmin.getOrganizationAdminSearchAllList(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                    result.code="0000";
                    result.data.rows=rows;
                    resolve(result);
                  }
              )
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
  },
  getOrganizationAdminSearchLeftList: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={code :"8300",data:{}};
              let query = Queries.AdOrganizationAdmin.getOrganizationAdminSearchLeftList(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                    result.code="0000";
                    result.data.rows=rows;
                    resolve(result);
                  }
              )
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
  },
  getOrganizationAdminSearchRightList: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}};
            let query = Queries.AdOrganizationAdmin.getOrganizationAdminSearchRightList(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                  result.code="0000";
                  result.data.rows=rows;
                  resolve(result);
                }
            )
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },  
  getOrganizationAdminDuplicateCheck: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = Queries.AdOrganizationAdmin.getOrganizationAdminDuplicateCheck(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {   
                    if (rows && rows[0].cnt >= 1) {
                        resolve({ code: '1210' });
                    } else {
                        resolve({ code: '0000' });
                    }
                }  
            )             
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },
  getOrganizationAdminInfoList: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}};
            let query = Queries.AdOrganizationAdmin.getOrganizationAdminInfoList(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                    result.code="0000";
                    result.data.rows=rows;
                    query = Queries.AdOrganizationAdmin.getOrganizationAdminInfoListTotalCnt(params);
                    return Mysql.execQueryPromise(query);
                }
            )
            .then(
                (rows) => {
                    result.data.total_cnt=rows[0].total_cnt;
                    resolve(result);
                }
            )                
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },
  getOrganizationAdminDetail: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}}; 
            let query = Queries.AdOrganizationAdmin.getOrganizationAdminDetail(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                    result.code="0000";
                    result.data.detail=rows[0];
                    query = Queries.AdGroupAdminRels.getGroupAdminRelsDetail(params);
                    return Mysql.execQueryPromise(query);
                }
            )  
            .then(
                (rows) => {
                    result.code="0000";
                    result.data.group_admin_rels=rows;
                    resolve(result);
                }
            )                         
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {s
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },
  updateOrganizationAdmin: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {              
            let query=Queries.AdOrganizationAdmin.updateOrganizationAdmin(params,id);       
            Mysql.execQueryPromise(query)    
            .then(      
              (rows) => {   
                let affectedRows=rows.affectedRows;            
                if(affectedRows>=1) resolve({code: '0000' });
                else    resolve({code: '8100'}); 
              }
            ) 
            .fail(
              (err) => {
                console.log(err)
                resolve({code: '8200'}); 
              }
            );                     
        } catch (e) {
            console.log(e)
          resolve({code: '8300'});   
        }
    });   
  },  
  updateOrganizationAdminNewWindow: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {              
            let query=Queries.AdOrganizationAdmin.updateOrganizationAdminNewWindow(params,id);       
            Mysql.execQueryPromise(query)    
            .then(      
              (rows) => {   
                let affectedRows=rows.affectedRows;            
                if(affectedRows>=1) resolve({code: '0000' });
                else    resolve({code: '8100'}); 
              }
            ) 
            .fail(
              (err) => {
                console.log(err)
                resolve({code: '8200'}); 
              }
            );                     
        } catch (e) {
            console.log(e)
          resolve({code: '8300'});   
        }
    });   
  },  
  getOrganizationAdminFindId: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}}; 
            let query = Queries.AdOrganizationAdmin.getOrganizationAdminFindId(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                  if(rows && rows.length >= 1){
                    result.code="0000";
                    result.data.admin_auth_id=rows[0].admin_auth_id;
                    resolve(result);
                  }else{
                    resolve({ code: '8100' });
                  }
                        
                }
            )             
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  }, 
  getOrganizationAdminFindPw: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data:{}};  
            let organization_admin_id;
            let query = Queries.AdOrganizationAdmin.getOrganizationAdminFindPw(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                  //console.log(rows)
                  if(rows && rows.length == 1){
                    result.code="0000";
                    params.admin_auth_pw=randomString();
                    result.data.admin_nm=rows[0].admin_nm;
                    result.data.admin_tel=rows[0].admin_tel;
                    organization_admin_id=rows[0].organization_admin_id;
                    let msg_str ="임시 비밀번호 : ["+params.admin_auth_pw+"]";
                    let form={
                      clientIp:params.clientIp,
                      z_value:params.z_value,
                      id:sms.id,
                      pw:sms.pw,
                      kind:'SMS',
                      rcvNum:rows[0].admin_tel,
                      sendNum:'16611944',
                      subject:'[COLDefend] 비밀번호 찾기',   
                      deliveryTime:'',  
                      msg:msg_str  

                    };
                    return Api.AdSend.sendSmsUser(form);
                  }else{
                    result.code="8100";
                    return null;
                  }
                        
                }
            ) 
            .then(
              (rows) => {
                if(rows && rows.code=="0000"){
                  query = Queries.AdOrganizationAdmin.updateOrganizationAdminPassword(params,organization_admin_id);
                  return Mysql.execQueryPromise(query);
                }else{
                  if(rows)result.code=rows.code;
                  else result.code="8100";
                  return null;
                }
              }
            )
            .then(
              (rows) => {
                resolve(result);  
              }
            )                                       
            .fail(
                (err) => {
                    console.log(err)
                    resolve(result);
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },             
};                       
  
function createToken(data){
  let result = data;
  result.key = auth.key;
  result.date = date.yyyymmddhhmiss;
  return jwt.encode(result, auth.secret);
}

function randomString() {
  let chars = "abcdefghiklmnopqrstuvwxyz0123456789";
  let string_length = 8;
  let randomstring = '';
  for (let i = 0; i < string_length; i++) {
      let rnum = Math.floor(Math.random() * chars.length);
      randomstring += chars.substring(rnum, rnum + 1);
  }
  return randomstring.toLowerCase();
}
