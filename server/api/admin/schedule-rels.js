
const url = require('url');

module.exports={  
  updateScheduleRels: (params,id) => {
    return new Promise(async (resolve, reject) => {
      try {
        let groupIdArray= (params.group_id)? params.group_id.split(",") : [];
        let query = Queries.AdScheduleRels.deleteScheduleRels(params,id);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
              let sqls=[];
              if(groupIdArray){
                groupIdArray.forEach((data) => {
                  let sub_params={
                    organization_id:params.organization_id,
                    schedule_id:id,
                    group_id :data
                  }
                  if(data) sqls.push(Queries.AdScheduleRels.createScheduleRels(sub_params)); 
                })            
                return Mysql.beginTransaction(sqls);
              }else{
                return null;
              }
          }
        )
        .then(
          (rows) => {
            resolve({ code: '0000' });
          }
        )        
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: '8200' });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  getScheduleRelsLeft: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let result ={code :"8300",data:{}}
        let query = Queries.AdScheduleRels.getScheduleRelsLeft(params);
        //console.log(query)
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            result.code="0000";
            result.data.rows=rows;
            resolve(result);
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  getScheduleRelsRight: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let result ={code :"8300",data:{}}
        let query = Queries.AdScheduleRels.getScheduleRelsRight(params);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            result.code="0000";
            result.data.rows=rows;
            resolve(result);
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },  
  getScheduleRelsList: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let query = Queries.AdScheduleRels.getScheduleRelsList(params);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            resolve({ code: '0000', rows: rows });
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },                    
};                       
  