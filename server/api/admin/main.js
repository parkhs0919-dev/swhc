
const url = require('url');

module.exports={  
    getMainUserCount: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdMain.getMainUserCount(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows= rows;
                        resolve(result);
                    }
                )
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    }, 
    getMainUserCountGroup: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdMain.getMainUserCountGroup(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows= rows;
                        resolve(result);
                    }
                )
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    }, 
    getMainUserApplyCdCount: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                let query = Queries.AdMain.getMainUserApplyCdCount(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows= rows;
                        resolve(result);
                    }
                )
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    }, 
    // getMainUser: (params) => {
    //     return new Promise(async (resolve, reject) => {
    //         try {
    //             let query = Queries.AdMain.getMainUser2(params);
    //             Mysql.execQueryPromise(query)
    //             .then(
    //                 (rows) => {
    //                     resolve({ code: '0000', rows: rows });
    //                 }
    //             )
    //             .fail(
    //                 (err) => {
    //                     console.log(err)
    //                     resolve({ code: "8200" });
    //                 }
    //             );
    //         } catch (e) {
    //             console.log(e)
    //             resolve({ code: '8300' });
    //         }
    //     });
    // }                               
};                       
  