
module.exports={  
    createMessageArray: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.AdMessage.createMessageArray(params);
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000' });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },  
    createMessageArrayGroup: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.AdMessage.createMessageArrayGroup(params);
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000' });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },  
    createMessageArrayAll: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.AdMessage.createMessageArrayAll(params);
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000' });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },  
    createMessageMainArray: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300"}     
                let query=Queries.AdUser.getUserMainFcmToken(params);         
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {        
                      if(rows && rows.length >= 1){              
                        rows.forEach((row) => {
                          if(row.fcm_token) Utils.fcm.pushMessageToken(row.fcm_token, 'admin' ,'', params.contents);
                        })
                        params.message_gb ="admin";
                        params.message_cd ="00";
                        result.code="0000";
                        let query=Queries.AdMessage.createMessageMainArray(params);
                        return Mysql.execQueryPromise(query);
                      }else {
                        return null;
                      }    
                    }         
                )                  
                .then(              
                    (rows) => {    
                        resolve(result);                    
                    }         
                )                                           
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },       
    getMessageList: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data :{}}
                let query = Queries.AdMessage.getMessageList(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        query = Queries.AdMessage.getMessageListTotalCnt(params);
                        return Mysql.execQueryPromise(query);
                    }
                )
                .then(
                    (rows) => {
                        result.data.total_cnt=rows[0].total_cnt;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },  
    updateMessageRead: (params,id) => {
      return new Promise(async (resolve, reject) => {
          try {              
              let query=Queries.AdMessage.updateMessageRead(params,id);          
              Mysql.execQueryPromise(query)    
              .then(      
                (rows) => {   
                  let affectedRows=rows.affectedRows;            
                  if(affectedRows>=1) resolve({code: '0000' });
                  else    resolve({code: '8100'}); 
                }
              ) 
              .fail(
                (err) => {
                  console.log(err)
                  resolve({code: '8200'}); 
                }
              );                     
          } catch (e) {
              console.log(e)
            resolve({code: '8300'});   
          }
      });   
    },     
//     getUserMessageList: (params) => {
//       return new Promise(async (resolve, reject) => {
//           try {
//               let result ={code :"8300"}
//               let query = Queries.AdMessage.getUserMessageList(params);
//               Mysql.execQueryPromise(query)
//               .then(
//                   (rows) => {
//                       result.code="0000";
//                       result.rows=rows;
//                       // query = Queries.AdMessage.getMessageListTotalCnt(params);
//                       // return Mysql.execQueryPromise(query);
//                       resolve(result);
//                   }
//               )             
//               .fail(
//                   (err) => {
//                       console.log(err)
//                       resolve({ code: "8200" });
//                   }
//               );
//           } catch (e) {
//               console.log(e)
//               resolve({ code: '8300' });
//           }
//       });
//   },                         
};                       
  