
const url = require('url');
const moment = require('moment');
const jwt = require('jwt-simple');
const date = Utils.date.getDate();
const auth = CONFIG.common.auth;
const sms = CONFIG.sms[SERVER_ENV];

module.exports={    
    sendUserToToken: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let query = Queries.AdUserDevice.getUserDevice(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {
                            rows.forEach((row) => {
                              if(row.fcm_token) Utils.fcm.pushMessageToken(row.fcm_token, 'admin' ,'', params.contents);
                            })
                        }
                        resolve(true);
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },   
    sendGroupToTopic: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let query = Queries.AdGroup.getGroupArray(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {
                            rows.forEach((row) => {
                              let topic = params.organization_id+'_'+row.group_id;
                              //console.log(topic)
                              if(row.group_id) Utils.fcm.pushMessageTopic(topic, 'admin' ,'', params.contents);
                            })
                        }
                        resolve(true);
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },   
    sendGroupToTopicAll: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let query = Queries.AdGroup.getGroupArrayAll(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {
                            rows.forEach((row) => {
                              let topic = params.organization_id+'_'+row.group_id;
                              //console.log(topic)
                              if(row.group_id) Utils.fcm.pushMessageTopic(topic, 'admin' ,'', params.contents);
                            })
                        }
                        resolve(true);
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },   
    sendScheduleGroupToTopic: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let query = Queries.AdScheduleRels.getScheduleListMessage(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {                       
                             rows.forEach((row) => {                             
                                let topic = params.organization_id+'_'+row.group_id;
                                //console.log(topic)
                                if(row.group_id) Utils.fcm.pushMessageTopic(topic, 'user_event' ,'', '일정이 변경 되었습니다. 확인해 주세요.');
                            })
                        }
                        resolve(true);
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },     
    sendUserLogToToken: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let query = Queries.AdUserDevice.getUserLogDevice(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {
                            //console.log(rows)
                            let fcm_token= rows[0].fcm_token;
                            //console.log(fcm_token)
                            if(fcm_token) Utils.fcm.pushMessageToken(fcm_token, 'user_log' ,'','data');
                        }
                        resolve(true);
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },
    sendSmsUser: (params) => {
        return new Promise(async (resolve, reject) => {
            try {           
                let result ={code :"8300"}
                let query = Queries.AdSmsLog.getSmsLogCheckCnt(params)
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if(rows && rows[0].cnt>5){
                            result.code="2100";
                            return null;
                        }else{
                            result.code="0000";
                            return Utils.sms.internalSendSms(params);
                        }
                    }  
                ) 
                .then(
                    (rows) => {
                        if(rows && result.code=="0000"){
                            let sub_params ={
                                ip:params.clientIp,
                                rcv_num:params.rcvNum,
                                send_num:params.sendNum,
                                contents:params.msg,
                                status:'',
                            }
                            if(rows.indexOf("OK")>-1) result.code="0000";
                            else result.code="2200";
                            sub_params.status=result.code;
                            query = Queries.AdSmsLog.createSmsLog(sub_params);
                            return Mysql.execQueryPromise(query);                          
                        }else{
                            resolve(result);
                        }   
                    }  
                ) 
                .then(
                    (rows) => {   
                        //console.log(rows)
                        resolve(result);
                    }  
                )                                               
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },
    sendSmsAdminTemp: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let key = createToken({id:sms.id,pw:sms.pw});
                let query = Queries.AdUser.getUserAdminList(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {                       
                            rows.forEach((row,index) => {
                                let user_nm=row.user_nm; 
                                let temperature_str=getTempNm(params.temperature); 
                                let temperature=params.temperature; 
                                let msg_text = '[COVIDefend] '+user_nm+'님의 체온이 '+temperature_str+'('+parseFloat(temperature).toFixed(2)+'도) 단계입니다. 시스템을 확인해 주세요.';
                                let form={
                                    organization_id:row.organization_id,
                                    group_id:row.group_id,
                                    organization_admin_id:row.organization_admin_id,
                                    user_id:row.user_id,
                                    clientIp:params.clientIp,
                                    z_value:params.z_value,
                                    key:key,
                                    kind:'SMS',
                                    rcvNum:row.admin_tel,
                                    sendNum:sms.sendNum,
                                    subject:'[COLDefend]',   
                                    deliveryTime:'',  
                                    msg: msg_text               
                                };   
                                if(index==0){
                                    let fl_form={
                                        organization_id:row.organization_id,
                                        group_id:row.group_id,
                                        organization_admin_id:row.organization_admin_id,
                                        clientIp:params.clientIp,
                                        z_value:params.z_value,
                                        key:key,
                                        kind:'SMS',
                                        rcvNum:'01084231163',
                                        sendNum:sms.sendNum,
                                        subject:'[COLDefend]',   
                                        deliveryTime:'',  
                                        msg: msg_text               
                                    }; 
                                    //Utils.sms.internalSendSms(fl_form); 
                                }
                                console.log(form)   
                                Api.AdUserSms.sendUserSmsLog(form)                
                                //Utils.sms.internalSendSms(form);
                                
                                                       
                            })                          
                        }
                        resolve(true);
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },  
    
};                       
  

function createToken(data){
    let result = data;
    result.date = date.yyyymmddhhmiss;
    return jwt.encode(result, auth.secret);
}

function getTempNm(val){
    let result = '' ;
    var temp = parseFloat(val).toFixed(2);
  
    if(temp>=34.0&&temp<37.0){
      result = '' ;
    }else if(temp>=37.0&&temp<37.3){
      result = '주의' ;
    }else if(temp>=37.3&&temp<37.5){
      result = '경계' ;    
    }else if(temp>=37.5&&temp<100){
      result = '심각' ;
    }
    return result; 
}