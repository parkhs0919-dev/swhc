
module.exports={  
    createSmsLog: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.AdSmsLog.createSmsLog(params);
                Mysql.execQueryPromise(query)            
                  .then(              
                    (rows) => {   
                     if(rows && rows.length>=1){                       
                        resolve({code: '0000' });
                     }else{
                        resolve({code: '8100'});   
                     }                                    
                    }         
                  )                                           
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },    
    getSmsLogCheckCnt: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300"}
                let query = Queries.AdSmsLog.getSmsLogCheckCnt(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.cnt=rows[0].cnt;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },                                
};                       
  