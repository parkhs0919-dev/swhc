const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
  updateUserSmsDuplicate: (params) => {
    return new Promise(async (resolve, reject) => {
        try {              
            let query=Queries.AdUserSms.updateUserSmsDuplicate(params);     
            Mysql.execQueryPromise(query)    
            .then(      
              (rows) => {   
                let affectedRows=rows.affectedRows;            
                if(affectedRows>=1) resolve({code: '0000' });
                else    resolve({code: '8100'}); 
              }
            ) 
            .fail(
              (err) => {
                console.log(err)
                resolve({code: '8200'}); 
              }
            );                     
        } catch (e) {
            console.log(e)
          resolve({code: '8300'});   
        }
    });   
  },    
  getUserSms: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let result ={code :"8300",data :{}}
              let query=Queries.AdUserSms.getUserSms(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {   
                  result.code="0000";
                  result.data.rows=rows;           
                  resolve(result);   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    }, 
    getUserSmsList: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={code :"8300",data:{}}
              let query = Queries.AdUserSms.getUserSmsList(params);
              //console.log(query)
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                      result.code="0000";
                      result.data.rows=rows;
                      query = Queries.AdUserSms.getUserSmsListTotalCnt(params);
                      return Mysql.execQueryPromise(query);
                  }
              )
              .then(
                  (rows) => {
                      result.data.total_cnt=rows[0].total_cnt;
                      resolve(result);
                  }
              )                
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
  }, 
  getUserSmsLogList: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={code :"8300",data:{}}
              let query = Queries.AdUserSms.getUserSmsLogList(params);
              //console.log(query)
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                      result.code="0000";
                      result.data.rows=rows;
                      query = Queries.AdUserSms.getUserSmsLogListTotalCnt(params);
                      return Mysql.execQueryPromise(query);
                  }
              )
              .then(
                  (rows) => {
                      result.data.total_cnt=rows[0].total_cnt;
                      resolve(result);
                  }
              )                
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
  }, 
  deleteUserSms: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {            
            let query=Queries.AdUserSms.deleteUserSms(params,id);
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {              
                resolve({code: '0000' });   
              }      
            )
            .fail(   
              (err) => {
                resolve({code: 8200 });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  },  
  sendUserSmsLog: (params) => {
    return new Promise(async (resolve, reject) => {
        try {           
            let result ={code :"8300"} 
            const sms = await Utils.sms.internalSendSms(params);
            let sub_params ={
                ip:params.clientIp,
                rcv_num:params.rcvNum,
                send_num:params.sendNum,
                contents:params.msg,
                status:'',
                organization_id:params.organization_id,
                group_id:params.group_id,
                organization_admin_id:params.organization_admin_id,
                user_id:params.user_id,
            }
            if(sms.indexOf("OK")>-1) result.code="0000";
            else result.code="2200";
            sub_params.status=result.code;
            
            let query = Queries.AdUserSms.createUserSmsLog(sub_params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {   
                    console.log(rows)
                    resolve(true);
                }  
            )                                               
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
},                                                         
};           

