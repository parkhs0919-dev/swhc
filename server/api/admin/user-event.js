
const url = require('url');
const convertTime = require('convert-time');

module.exports={  
  createUserEvent: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        params.start_time_str  = convertTime(params.start_time,"hhMM");  
        params.end_time_str  = convertTime(params.end_time,"hhMM");  
        params.visit_time_str  = convertTime(params.visit_time,"hhMM");  
        let query = Queries.AdUserEvent.createUserEvent(params);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            let affectedRows = rows.affectedRows;
            if (affectedRows >= 1) resolve({ code: '0000' });
            else resolve({ code: '8100' });
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: '8200' });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  updateUserEvent: (params, id) => {
    return new Promise(async (resolve, reject) => {
      try {    
        params.start_time_str  = convertTime(params.start_time,"hhMM");  
        params.end_time_str  = convertTime(params.end_time,"hhMM");  
        params.visit_time_str  = convertTime(params.visit_time,"hhMM");  
        let query = Queries.AdUserEvent.updateUserEvent(params, id);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            let affectedRows = rows.affectedRows;
            if (affectedRows >= 1) resolve({ code: '0000' });
            else resolve({ code: '8100' });
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: '8200' });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  getUserEvent: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let query = Queries.AdUserEvent.getUserEvent(params);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            resolve({ code: '0000', rows: rows[0] });
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  getUserEventList: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300"}
            let query = Queries.AdUserEvent.getUserEventList(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                    result.code="0000";
                    result.rows=rows;
                    //query = Queries.AdUserEvent.getUserEventListTotalCnt(params);
                    return null;//Mysql.execQueryPromise(query);
                }
            )
            .then(
                (rows) => {
                    //result.total_cnt=rows[0].total_cnt;
                    resolve(result);
                }
            )                
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },  
  getUserEventDetail: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = Queries.AdUserEvent.getUserEventDetail(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                    resolve({ code: '0000', rows: rows[0] });
                }
            )
            .fail(  
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },
  deleteUserEvent: (id) => {
    return new Promise(async (resolve, reject) => {
        try {                
            let query=Queries.AdUserEvent.deleteUserEvent(id);
            Mysql.execQueryPromise(query)  
            .then(             
                (rows) => {                 
                    let affectedRows=rows.affectedRows;      
                    if(affectedRows>=1) resolve({code: '0000'});
                    else     resolve({code: '8100'});    
                }
              )                    
            .fail(   
              (err) => {
                resolve({code: '8200'});
              }   
            );                          
        } catch (e) {  
          resolve({code: '8300'});
        }
    }); 
},                      
};                       
  