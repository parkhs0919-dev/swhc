
const url = require('url');

module.exports={  
    createOrganizationProcess: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.AdOrganization.createOrganization(params);
                Mysql.execQueryPromise(query)            
                .then(
                  (rows) => {
                    let affectedRows = (rows)? rows.affectedRows : 0;
                    if (affectedRows >= 1) {
                      params.organization_id = rows.insertId;
                      query=Queries.AdOrganizationAdmin.createOrganizationAdmin(params);
                      return  Mysql.execQueryPromise(query);
                    } else {
                      return null
                    }
                  }
                )
                .then(
                  (rows) => {
                    let affectedRows=(rows)? rows.affectedRows : 0 ;      
                    if(affectedRows>=1) resolve({code: '0000' ,organization_id :params.organization_id });
                    else     resolve({code: '8100'});  
                  }
                )                
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({ code: '8200' });
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },
    createOrganization: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.AdOrganization.createOrganization(params);
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000' });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },
    getOrganizationCheck: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let query = Queries.AdOrganization.getOrganizationCheck(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                      if (rows && rows.length >= 1) {
                          resolve({ code: '1200' });
                      } else {
                          resolve({ code: '0000' });
                      }
                  }
              )             
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
  }, 
  getOrganizationInfo: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data :{}}
            let query = Queries.AdOrganization.getOrganizationInfo(params);
            //console.log(query)
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                  result.code="0000";
                  result.data.detail=rows[0];
                  resolve(result);
                }
            )             
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },
  updateOrganization: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {              
            let query=Queries.AdOrganization.updateOrganization(params,id);       
            Mysql.execQueryPromise(query)    
            .then(      
              (rows) => {   
                let affectedRows=rows.affectedRows;            
                if(affectedRows>=1) resolve({code: '0000' });
                else    resolve({code: '8100'}); 
              }
            ) 
            .fail(
              (err) => {
                console.log(err)
                resolve({code: '8200'}); 
              }
            );                     
        } catch (e) {
            console.log(e)
          resolve({code: '8300'});   
        }
    });   
  },  
  updateOrganizationAdmin: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {              
            let query=Queries.AdOrganization.updateOrganizationAdmin(params,id);       
            Mysql.execQueryPromise(query)    
            .then(      
              (rows) => {   
                let affectedRows=rows.affectedRows;            
                if(affectedRows>=1) resolve({code: '0000' });
                else    resolve({code: '8100'}); 
              }
            ) 
            .fail(
              (err) => {
                console.log(err)
                resolve({code: '8200'}); 
              }
            );                     
        } catch (e) {
            console.log(e)
          resolve({code: '8300'});   
        }
    });   
  },                    
};                       
  