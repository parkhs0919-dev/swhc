
const url = require('url');

module.exports={  
    updateGroupAdminRels: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.AdGroupAdminRels.deleteGroupAdminRelsId(params.group_admin_rels_id);
                Mysql.execQueryPromise(query)            
                .then(
                  (rows) => {
                    params.group_id= id;
                    query = Queries.AdGroupAdminRels.createGroupAdminRels(params);
                    return Mysql.execQueryPromise(query);
                  }
                )
                .then(
                  (rows) => {
                    let affectedRows=(rows)? rows.affectedRows : 0 ;      
                    if(affectedRows>=1) resolve({code: '0000' });
                    else     resolve({code: '8100'});  
                  }
                )                
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({ code: '8200' });
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },
    updateGroupAdminRelsGroupId: (params,id) => {
      return new Promise(async (resolve, reject) => {
          try {  
              let result={code:"8300",data:{}}   
              let organizationAdminIdArray = (params.organization_admin_id_array)? params.organization_admin_id_array.split(",") : [];
              let query=Queries.AdGroupAdminRels.deleteGroupAdminRelsGroupId(id);
              Mysql.execQueryPromise(query)            
              .then(
                (rows) => {
                  result.code="0000";
                  params.group_id = id;
                  let sqls=[];
                  organizationAdminIdArray.forEach(item => {
                      params.organization_admin_id = item;
                      if(item) sqls.push(Queries.AdGroupAdminRels.createGroupAdminRels(params))
                  });  
                  return  Mysql.beginTransaction(sqls);
                }
              )
              .then(
                (rows) => {
                  resolve(result);
                }
              )                
              .fail(
                (err) => {
                  console.log(err)
                  resolve({ code: '8200' });
                }
              );                       
          } catch (e) {console.log(e)
            resolve({code: '8300'});  
          }
      });
    },    
    updateGroupAdminRelsOrganizationAdminId: (params,id) => {
      return new Promise(async (resolve, reject) => {
          try {   
              let result = {code : "8300"}
              let groupIdArray = (params.group_id_array)? params.group_id_array.split(",") : [];
              let query=Queries.AdGroupAdminRels.deleteGroupAdminRelsOrganizationAdminId(id);
              Mysql.execQueryPromise(query)            
              .then(
                (rows) => {
                  let sqls = [];  
                  result.code="0000";
                  params.organization_admin_id =id;
                  if(params.admin_grade!="M"){
                    groupIdArray.forEach(item => {
                        params.group_id = item;
                        if(item) sqls.push(Queries.AdGroupAdminRels.createGroupAdminRels(params))
                    }); 
                    return  Mysql.beginTransaction(sqls);
                  }else{
                    return null;
                  } 
                }
              )
              .then(
                (rows) => {
                   resolve(result);
                }
              )                  
              .fail(
                (err) => {
                  console.log(err)
                  resolve({ code: '8200' });
                }
              );                       
          } catch (e) {console.log(e)
            resolve({code: '8300'});  
          }
      });
  },                  
};                       
  