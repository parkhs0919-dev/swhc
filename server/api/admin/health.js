
const jwt = require('jwt-simple');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();

module.exports={  
    cronInitHealthCheck: () => {
    return new Promise(async (resolve, reject) => {
        try {              
            let query=Queries.AdHealth.cronInitHealthCheck();       
            Mysql.execQueryPromise(query)    
            .then(      
              (rows) => {   
                let affectedRows=rows.affectedRows;            
                if(affectedRows>=1) resolve({code: '0000' });
                else    resolve({code: '8100'}); 
              }
            ) 
            .fail(
              (err) => {
                console.log(err)
                resolve({code: '8200'}); 
              }
            );                     
        } catch (e) {
            console.log(e)
          resolve({code: '8300'});   
        }
    });   
  },  
    cronInitHealthGradeCheck: () => {
    return new Promise(async (resolve, reject) => {
        try {              
            let query=Queries.AdHealth.cronInitHealthGradeCheck();       
            Mysql.execQueryPromise(query)    
            .then(      
              (rows) => {   
                let affectedRows=rows.affectedRows;            
                if(affectedRows>=1) resolve({code: '0000' });
                else    resolve({code: '8100'}); 
              }
            ) 
            .fail(
              (err) => {
                console.log(err)
                resolve({code: '8200'}); 
              }
            );                     
        } catch (e) {
            console.log(e)
          resolve({code: '8300'});   
        }
    });   
  },  
       
};                       



