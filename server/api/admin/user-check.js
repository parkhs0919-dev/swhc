const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
  getUserCheck: (params) => {
    return new Promise(async (resolve, reject) => {
        try {            
            let query=Queries.AdUserCheck.getUserCheckUnionAll(params);
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {              
                resolve({code: '0000' ,rows : rows});   
              }      
            )
            .fail(   
              (err) => {
                resolve({code: 8200 });
              }   
            );                        
        } catch (e) {
            resolve({code: '8300'}); 
        }
    });
  },    
  getUserCheckUnionAll: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data :{}}
            let query = Queries.AdUserCheck.getUserCheckUnionAll(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                    result.code="0000";
                    result.data.rows=rows;
                    query = Queries.AdUserCheck.getUserCheckUnionAllTotalCnt(params);
                    return Mysql.execQueryPromise(query);
                }
            )
            .then(
                (rows) => {
                    result.data.total_cnt=rows[0].total_cnt;
                    resolve(result);
                }
            )                
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
},                                                          
};           