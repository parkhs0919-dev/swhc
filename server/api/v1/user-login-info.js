
const jwt = require('jwt-simple');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();

module.exports = { 
  createUserLoginInfo: (params) => {
    return new Promise(async (resolve, reject) => {
        try {     
            let query=Queries.UserLoginInfo.createUserLoginInfo(params);           
            Mysql.execQueryPromise(query)            
            .then(              
                (rows) => {    
                 let affectedRows=rows.affectedRows;      
                 if(affectedRows>=1) resolve({code: '0000' });
                 else     resolve({code: '8100'});                    
                }         
              )                        
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
  },    
  getUserLoginInfoCheck: (params) => {
    return new Promise(async (resolve, reject) => {
        try {        
            let query=Queries.UserLoginInfo.getUserLoginInfoCheck(params);
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {       
                let check_cnt =0; 
                if(rows && rows.length >= 1){
                  rows.forEach((row) => {
                    if(row.uuid==params.uuid ) check_cnt+=0;
                    else check_cnt+=1;
                  })
                  if(check_cnt>=1) resolve({code: '1500'});  
                  else  resolve({code: '0000' });    
                }else{
                  resolve({code: '0000' });  
                }                   
              }      
            )
            .fail(   
              (err) => {
                resolve({code: 8200 });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  }, 
  
  updateUserLoginInfoCheck: (params) => {
    return new Promise(async (resolve, reject) => {
        try {        
            let data ={}
            let query=Queries.UserLoginInfo.getUserLoginInfoCheck(params);
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {         
                if(rows && rows.length>=1){
                  let sqls=[];
                  rows.forEach((row) => {            
                    if(!(row.uuid==params.uuid)){
                      Utils.fcm.pushMessageToken(row.fcm_token, 'logout' ,'', '로그아웃');
                    } 
                  })  
                }
                data.user_id =params.user_id;
                data.login_yn = "N";
                query=Queries.UserLoginInfo.updateUserLoginInfo(data);
                return Mysql.execQueryPromise(query);               
              }      
            ) 
            .then(                  
              (rows) => {  
                data.user_id =params.user_id;
                data.uuid =params.uuid;
                data.platform =params.platform;
                data.fcm_token =params.fcm_token;
                data.login_yn = "Y";
                query=Queries.UserLoginInfo.updateUserLoginInfoDuplicate(data);
                return Mysql.execQueryPromise(query);
              }      
            ) 
            .then(                  
              (rows) => {    
               // console.log(rows)
                resolve({ code: "0000" });
              }      
            )                               
            .fail(   
              (err) => {
                resolve({code: "8200" });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  },                           
};
