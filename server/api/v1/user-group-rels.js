
const jwt = require('jwt-simple');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();

module.exports = { 
   createUserGroupRels: (params) => {
    return new Promise(async (resolve, reject) => {
        try {   
            let query=Queries.UserGroupRels.createUserGroupRels(params);
            Mysql.execQueryPromise(query)            
            .then(
              (rows) => {
                let affectedRows=rows.affectedRows;      
                if(affectedRows>=1) resolve({code: '0000' });
                else     resolve({code: '8100'});  
              }
            )                                      
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
  },   
   updateUserGroupRels: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {            
            let query=Queries.UserGroupRels.updateUserGroupRels(params,id);
            Mysql.execQueryPromise(query)            
            .then(              
                (rows) => {    
                 let affectedRows=rows.affectedRows;      
                 if(affectedRows>=1) resolve({code: '0000' });
                 else     resolve({code: '8100'});                    
                }         
              )                        
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
  }, 
  getUserGroupRels: (params) => {
    return new Promise(async (resolve, reject) => {
        try {            
            let query=Queries.UserGroupRels.getUserGroupRels(params);
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {              
                resolve({code: '0000' ,rows : rows});   
              }      
            )
            .fail(   
              (err) => {
                resolve({code: 8200 });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  },
  getUserGroupRelsCheck: (params) => {
    return new Promise(async (resolve, reject) => {
        try {               
            let query=Queries.UserGroupRels.getUserGroupRelsCheck(params); 
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {            
                resolve({code: '0000' ,rows : rows});   
              }      
            )
            .fail(   
              (err) => {
                resolve({code: 8200 });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  },
  getUserGroupRelsOrganizationCheck: (params) => {
    return new Promise(async (resolve, reject) => {
        try {               
            let query=Queries.UserGroupRels.getUserGroupRelsOrganizationCheck(params); 
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {
                if(rows && rows[0] && rows[0].cnt>0){
                  resolve({code: '3100' });   
                }else{
                  resolve({code: '0000' });   
                }            
                
              }      
            )
            .fail(   
              (err) => {
                resolve({code: 8200 });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  },
  getUserGroupRelsMessage: (params) => {
    return new Promise(async (resolve, reject) => {
        try {            
            let query=Queries.UserGroupRels.getUserGroupRelsMessage(params);
            console.log(query)
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {              
                resolve({code: '0000' ,rows : rows});   
              }      
            )
            .fail(   
              (err) => {
                resolve({code: 8200 });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  },                            
};
