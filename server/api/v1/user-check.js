const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserCheck: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.UserCheck.createUserCheck(params);           
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000' , user_check_id :rows.insertId });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },  
    getUserCheck: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.UserCheck.getUserCheck(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },
    getUserCheckUnionAll: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.UserCheck.getUserCheckUnionAll(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },    
    getUserCheckId: (id) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.UserCheck.getUserCheckId(id);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },
    deleteUserCheck: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.UserCheck.deleteUserCheck(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' });   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },                                                         
};           
