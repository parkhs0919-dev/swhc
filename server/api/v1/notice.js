module.exports = {
    getNotice: (params) => {
        return new Promise(async (resolve, reject) => {
            try {               
                let query=Queries.Notice.getNotice(params);
                Mysql.execQueryPromise(query)  
                .then(                  
                  (rows) => {         
                    if(rows && rows.length >= 1 && !rows[0].notice_id) rows=[];       
                    resolve({code: '0000' ,rows : rows});   
                  }      
                )
                .fail(   
                  (err) => {
                    resolve({code: 8200 });
                  }   
                );                        
            } catch (e) {
               resolve({code: '8300'}); 
            }
        });
    }, 
    getNoticeLast: (params) => {
      return new Promise(async (resolve, reject) => {
          try {               
              let query=Queries.Notice.getNoticeLast(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {          
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
  },               
};
