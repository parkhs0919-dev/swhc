
const jwt = require('jwt-simple');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();

module.exports = { 
    createHealthCheck: (params) => {
      return new Promise(async (resolve, reject) => {
          try {   
              let query=Queries.Health.getHealthCheck(params);
              Mysql.execQueryPromise(query)            
              .then(
                (rows) => {
                    if(rows && rows.length){
                        return null;
                    }else{
                        params.temperature = 0;
                        params.heart_rat = 0;
                        query=Queries.Health.createHealth(params);
                        return Mysql.execQueryPromise(query);
                    }
                }
              )
              .then(
                (rows) => {
                    resolve({ code: '0000' });
                }
              )                                         
              .fail(      
                (err) => {console.log(err)
                  resolve({code: '8200'});
                }
              );                       
          } catch (e) {console.log(e)
            resolve({code: '8300'});  
          }
      });
   },
   createHealth: (params) => {
    return new Promise(async (resolve, reject) => {
        try {   
            let query=Queries.Health.createHealth(params);
            Mysql.execQueryPromise(query)            
            .then(
              (rows) => {
                  if(rows && rows.length){
                    resolve({ code: '0000' });
                  }else{
                    resolve({ code: '8100' });
                  }
              }
            )                                      
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
 },   
  updateHealth: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {     
            params.network_connect_st =1;         
            let query=Queries.Health.updateHealth(params,id);
            Mysql.execQueryPromise(query)            
            .then(              
                (rows) => {    
                 let affectedRows=rows.affectedRows;      
                 if(affectedRows>=1) resolve({code: '0000' });
                 else     resolve({code: '8100'});                    
                }         
              )                        
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
 },
 updateHealthStatus: (params,id) => {
  return new Promise(async (resolve, reject) => {
      try {             
          let query=Queries.Health.updateHealthStatus(params,id);
          Mysql.execQueryPromise(query)            
          .then(              
              (rows) => {    
               let affectedRows=rows.affectedRows;      
               if(affectedRows>=1) resolve({code: '0000' });
               else     resolve({code: '8100'});                    
              }         
            )                        
          .fail(      
            (err) => {console.log(err)
              resolve({code: '8200'});
            }
          );                       
      } catch (e) {console.log(e)
        resolve({code: '8300'});  
      }
  });
 },  
 updateHealthGrade: (params,id) => {
  return new Promise(async (resolve, reject) => {
      try {             
          let query=Queries.Health.updateHealthGrade(params,id);
          Mysql.execQueryPromise(query)            
          .then(              
              (rows) => {    
               let affectedRows=rows.affectedRows;      
               if(affectedRows>=1) resolve({code: '0000' });
               else     resolve({code: '8100'});                    
              }         
            )                        
          .fail(      
            (err) => {console.log(err)
              resolve({code: '8200'});
            }
          );                       
      } catch (e) {console.log(e)
        resolve({code: '8300'});  
      }
  });
 },  
 getHealthGradeStatus: (params) => {
  return new Promise(async (resolve, reject) => {
      try {            
          let query=Queries.Health.getHealthGradeStatus(params);
          Mysql.execQueryPromise(query)  
          .then(                  
            (rows) => {
              let row = rows[0];
              resolve({code: '0000' ,rows : row});   
            }      
          )
          .fail(   
            (err) => {
              resolve({code: 8200 });
            }   
          );                        
      } catch (e) {
         resolve({code: '8300'}); 
      }
   });
 },
 getHealthCheck: (params) => {
  return new Promise(async (resolve, reject) => {
      try {            
          let query=Queries.Health.getHealthCheck(params);
          Mysql.execQueryPromise(query)  
          .then(                  
            (rows) => {
              if(rows && rows.length >= 1){
                resolve({code: '0000' ,rows : rows[0]});  
              }else{
                resolve({code: '8100' });  
              }                  
            }      
          )
          .fail(   
            (err) => {
              console.log(err)
              resolve({code: 8200 });
            }   
          );                        
      } catch (e) {
         resolve({code: '8300'}); 
      }
   });
 },
 getHealthGradeStatusMain: (params) => {
  return new Promise(async (resolve, reject) => {
      try {            
          let query=Queries.Health.getHealthGradeStatus(params);
          Mysql.execQueryPromise(query)  
          .then(                  
            (rows) => {
              let row = rows[0];
              let result={code:"8100"};
              if(row){
                if(row.grade_cd=="G0")  row.grade_cd ="G6"; 
                result=row;
                result.code="0000";
                resolve(result); 
              }else{
                resolve(result); 
              }     
            }      
          )
          .fail(   
            (err) => {
              resolve({code: 8200 });
            }   
          );                        
      } catch (e) {
         resolve({code: '8300'}); 
      }
   });
 },                           
};
