const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserVaccineFile: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.UserVaccineFile.createUserVaccineFile(params);           
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000'  });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },
    getUserVaccineFile: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.UserVaccineFile.getUserVaccineFile(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },   
    deleteUserVaccineFile: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.UserVaccineFile.deleteUserVaccineFile(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' });   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },                                                 
};           
