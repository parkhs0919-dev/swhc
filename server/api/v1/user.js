
// @ts-nocheck

const jwt = require('jwt-simple');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();
const sms = CONFIG.sms[SERVER_ENV];
const email = CONFIG.common.email;


module.exports = {
    createUser: (params) => {
      return new Promise(async (resolve, reject) => {
          try { 
              let result = {code:8300} 
              let query=Queries.User.createUser(params);
              Mysql.execQueryPromise(query)            
              .then(
                  (rows) => {
                      let affectedRows = (rows)? rows.affectedRows : 0;
                      if (affectedRows >= 1) {
                        let data = rows;
                        result.code="0000";
                        result.user_id=data.insertId;
                        result.user_auth_id=params.user_auth_id;
                        resolve(result);
                      } else {   
                        resolve({code: '8100'});
                      }
                  }
              )                                                     
              .fail(      
                (err) => {console.log(err)
                  resolve({code: '8200'});
                }
              );                       
          } catch (e) {console.log(e)
            resolve({code: '8300'});  
          }
      });
    },       
    getUserDuplicateCheck: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let query = Queries.User.getUserDuplicateCheck(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {   
                      if (rows && rows[0].cnt >= 1) {
                          resolve({ code: '1100' });
                      } else {
                          resolve({ code: '0000' });
                      }
                  }  
              )             
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
    },  
    getUserDuplicateCheckEmail: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let query = Queries.User.getUserDuplicateCheckEmail(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {   
                      if (rows && rows[0].cnt >= 1) {
                          resolve({ code: '1300' });
                      } else {
                          resolve({ code: '0000' });
                      }
                  }  
              )             
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
    },     
    getUserLoginCheck: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code : "8300"};               
                let query = Queries.User.getUserLoginCheck(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        if (rows && rows.length >= 1) {
                            params.user_id = rows[0].user_id;
                            result.code = "0000";
                            result.token = createToken(rows[0]);
                            result.first_login = rows[0].login_first_yn;
                            result.user_nm = rows[0].user_nm;
                            result.user_id = rows[0].user_id;
                            result.user_auth_id = rows[0].user_auth_id;
                            result.all_topic_key = 'hpitech';
                            query =  Queries.User.getUserGroupRels(params);
                            return Mysql.execQueryPromise(query);
                        } else {
                            resolve({ code: '8100' });
                        }  
                    }
                )  
                .then(
                  (rows) => {
                    if(result.code=="0000"){
                      let group_topic_key =[];
                      if(rows){
                        rows.forEach(function (row,index) {
                          group_topic_key.push(row.organization_id+'_'+row.group_id)
                        });                      
                      }
                      result.group_topic_key=group_topic_key;
                      resolve(result);
                    }else{
                      resolve({ code: '8100' });  
                    }
                  }
              )                                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },
    getUserLoginCheckToken: (params) => {
      return new Promise(async (resolve, reject) => {
        try {
          let result = {
            code: "8300"
          };
          let query = Queries.User.getUserLoginCheckToken(params);
          Mysql.execQueryPromise(query)
            .then(
              (rows) => {
                if (rows && rows.length >= 1) {
                  result.code = "0000";
                  result.token = createToken(rows[0]);
                  result.first_login = rows[0].login_first_yn;
                  result.user_nm = rows[0].user_nm;
                  result.user_id = rows[0].user_id;
                  result.user_auth_id = rows[0].user_auth_id;
                  result.all_topic_key = 'hpitech';
                  resolve(result);
                } else {
                  resolve({
                    code: '8100'
                  });
                }
              }
            )
            .fail(
              (err) => {
                console.log(err)
                resolve({
                  code: "8200"
                });
              }
            );
        } catch (e) {
          console.log(e)
          resolve({
            code: '8300'
          });
        }
      });
    },
    getUser: (params) => {
      return new Promise(async (resolve, reject) => {
          try {               
              let query=Queries.User.getUser(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  if(rows.length>=1) resolve({code: '0000' ,rows : rows[0]});
                  else   resolve({code: '8100'});    
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },
    getUserGroupRels: (params) => {
      return new Promise(async (resolve, reject) => {
          try {               
              let query=Queries.User.getUserGroupRels(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },        
    getUserGroupRelsList: (params) => {
      return new Promise(async (resolve, reject) => {
          try {        
              let query=Queries.User.getUserGroupRelsList(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },      
    getUserGroupSearch: (params) => {
      return new Promise(async (resolve, reject) => {
          try {               
              let query=Queries.User.getUserGroupSearch(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },   
    getUserHealth: (params) => {
      return new Promise(async (resolve, reject) => {
          try {               
              let query=Queries.User.getUserHealth(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },      
    getUserPhone: (params) => {
      return new Promise(async (resolve, reject) => {
          try {               
              let query=Queries.User.getUserPhone(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {
                  if(rows && rows[0].cnt>0){
                    resolve({code: '1500' });
                  }else{
                    resolve({code: '0000' });
                  }                                   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },      
    updateUserPassword: (params,id) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result = {code: '8300'}         
              params.user_id = id;     
              let query=Queries.User.getUserPasswordCheck(params);      
              Mysql.execQueryPromise(query)    
              .then(      
                (rows) => {
                  if(rows && rows[0].cnt==1){
                    result.code = "0000";
                    query=Queries.User.updateUserPassword(params,id);
                    return Mysql.execQueryPromise(query);
                  }else{
                    result.code = "1600";
                  }
                }
              ) 
              .then(      
                (rows) => {
                  if(result.code=="0000"){
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1) resolve(result);
                    else    resolve({code: '8200'});
                  }else{
                    resolve(result);
                  }
                }
              )                               
              .fail(
                (err) => {
                  console.log(err)
                  resolve({code: '8200'}); 
                }
              );                     
          } catch (e) {
              console.log(e)
            resolve({code: '8300'});   
          }
      });   
    }, 
    getUserFindId: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let query = Queries.User.getUserFindId(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                      if(rows && rows.length>=1) resolve({ code: '0000' ,user_auth_id : rows[0].user_auth_id });
                      else resolve({ code: '8100' });
                  }
              )
              .fail(   
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
    }, 
    getUserFindIdV2: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let query = Queries.User.getUserFindIdV2(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                      if(rows && rows.length>=1) resolve({ code: '0000' ,user_auth_id : rows[0].user_auth_id });
                      else resolve({ code: '8100' });
                  }
              )
              .fail(   
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
    }, 
    getUserFindPw: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let key = createToken({id:sms.id,pw:sms.pw});
                let result ={code:"8300"}
                let query = Queries.User.getUserFindPw(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                      //console.log(rows)
                        if(rows && rows.length>=1) {
                          result.code="0000";
                          result.user_auth_id=rows[0].user_auth_id;
                          result.user_email=rows[0].user_email;
                          result.user_tel=rows[0].user_tel;
                          result.user_nm=rows[0].user_nm;
                          //params.new_user_auth_pw=Math.random().toString(36).slice(2);
                          params.new_user_auth_pw = randomString(8);
                          query = Queries.User.updateUserPassword(params,rows[0].user_id);
                          return Mysql.execQueryPromise(query);
                        }else {
                          return null;   
                        }      
                    }
                )
                .then(
                  (rows) => {
                    let affectedRows=(rows)? rows.affectedRows : 0 ;            
                    if(affectedRows>=1){
                      let msg_str ="[COLDefend] " + result.user_nm + "님 임시 비밀번호입니다. ["+params.new_user_auth_pw+"]";
                      let form={
                        clientIp:params.clientIp,
                        z_value:params.z_value,
                        key:key,
                        kind:'SMS',
                        rcvNum:result.user_tel,
                        sendNum:'16611944',
                        subject:'[COLDefend] 비밀번호 찾기',   
                        deliveryTime:'',  
                        msg:msg_str   
  
                      };
                      Api.AdSend.sendSmsUser(form);
                      // let sub_params ={
                      //   from:email.auth_user,
                      //   to:result.user_email,
                      //   subject:"[codefend] 임시비밀번호",
                      //   num:params.new_user_auth_pw
                      // }        
                      // Utils.email.sendToUserAuth(sub_params);
                      resolve({code: '0000' });
                    }else{
                      resolve({code: '8100'}); 
                    }    
                  }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },                
    getUserFindPwV2: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let key = createToken({id:sms.id,pw:sms.pw});
                let result ={code:"8300"}
                let query = Queries.User.getUserFindPwV2(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                      //console.log(rows)
                        if(rows && rows.length>=1) {
                          result.code="0000";
                          result.user_auth_id=rows[0].user_auth_id;
                          result.user_email=rows[0].user_email;
                          result.user_tel=rows[0].user_tel;
                          result.user_nm=rows[0].user_nm;
                          //params.new_user_auth_pw=Math.random().toString(36).slice(2);
                          params.new_user_auth_pw = randomString(8);
                          query = Queries.User.updateUserPassword(params,rows[0].user_id);
                          return Mysql.execQueryPromise(query);
                        }else {
                          return null;   
                        }      
                    }
                )
                .then(
                  (rows) => {
                    let affectedRows=(rows)? rows.affectedRows : 0 ;            
                    if(affectedRows>=1){
                      let msg_str ="[COLDefend] " + result.user_nm + "님 임시 비밀번호입니다. ["+params.new_user_auth_pw+"]";
                      let form={
                        clientIp:params.clientIp,
                        z_value:params.z_value,
                        key:key,
                        kind:'SMS',
                        rcvNum:result.user_tel,
                        sendNum:'16611944',
                        subject:'[COLDefend] 비밀번호 찾기',   
                        deliveryTime:'',  
                        msg:msg_str   
  
                      };
                      Api.AdSend.sendSmsUser(form);
                      // let sub_params ={
                      //   from:email.auth_user,
                      //   to:result.user_email,
                      //   subject:"[codefend] 임시비밀번호",
                      //   num:params.new_user_auth_pw
                      // }        
                      // Utils.email.sendToUserAuth(sub_params);
                      resolve({code: '0000' });
                    }else{
                      resolve({code: '8100'}); 
                    }    
                  }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },                
    updateUserFirstPassword: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {              
                let query=Queries.User.updateUserFirstPassword(params,id);     
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1) resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                ) 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    },  
      updateUserFirstInfo: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {              
                let query = Queries.User.getUserDuplicateCheck(params);   
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {  
                    if (rows && rows[0].cnt >= 1) {
                      resolve({ code: '1400' });
                    } else {
                      query=Queries.User.updateUserFirstInfo(params,id);  
                      return  Mysql.execQueryPromise(query);    
                    } 
                  }
                ) 
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1) resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                )                 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
      },             
      updateUserLoginTime: (params,id) => {
          return new Promise(async (resolve, reject) => {
              try {              
                  let query=Queries.User.updateUserLoginTime(params,id);       
                  Mysql.execQueryPromise(query)    
                  .then(      
                    (rows) => {   
                      let affectedRows=rows.affectedRows;            
                      if(affectedRows>=1) resolve({code: '0000' });
                      else    resolve({code: '8100'}); 
                    }
                  ) 
                  .fail(
                    (err) => {
                      console.log(err)
                      resolve({code: '8200'}); 
                    }
                  );                     
              } catch (e) {
                  console.log(e)
                resolve({code: '8300'});   
              }
          });   
      },
      updateUser: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {              
                let query=Queries.User.updateUser(params,id);          
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1) resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                ) 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    },   
    updateUserLoginInfoDuplicate: (params,id) => {
      return new Promise(async (resolve, reject) => {
          try {              
              let query=Queries.UserLoginInfo.updateUserLoginInfoDuplicate(params,id);     
              Mysql.execQueryPromise(query)    
              .then(      
                (rows) => {   
                  let affectedRows=rows.affectedRows;            
                  if(affectedRows>=1) resolve({code: '0000' });
                  else    resolve({code: '8100'}); 
                }
              ) 
              .fail(
                (err) => {
                  console.log(err)
                  resolve({code: '8200'}); 
                }
              );                     
          } catch (e) {
              console.log(e)
            resolve({code: '8300'});   
          }
      });   
    },                      
};

function createToken(data){
    let result = data;
    result.key = auth.key;
    result.date = date.yyyymmddhhmiss;
    return jwt.encode(result, auth.secret);
}
function randomString(len, charSet) {
  charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let randomString = '';
  for (var i = 0; i < len; i++) {
      var randomPoz = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz,randomPoz+1);
  }
  return randomString.toLowerCase() ;
}