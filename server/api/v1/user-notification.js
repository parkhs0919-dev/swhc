const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
  sendUserNotification : (params) => {
        return new Promise(async (resolve, reject) => {
            try {   
                let query = Queries.UserDevice.getUserDeviceCheck(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {
                            let fcm_token = rows[0].fcm_token;
                            if(fcm_token){
                                let sub_params ={
                                    type : 'grade_change',
                                    title : '',
                                    body : '등급이 변경 되었습니다.',
                                    grade :params.grade,
                                    sound: 'default'
                                }
                                Utils.fcm.pushMessageDefaultToken(fcm_token,sub_params);
                                sub_params.type='grade_refresh';
                                sub_params.body='공유내역을 확인하세요.';
                                Utils.fcm.pushMessageDefaultToken(fcm_token,sub_params);
                            } 
                        }
                        resolve(true);
                    }  
                )                             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },                                                
    sendUserNotificationUpdate : (params) => {
        return new Promise(async (resolve, reject) => {
            try {    
                let query = Queries.UserDevice.getUserDeviceCheck(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {
                            let fcm_token = rows[0].fcm_token;
                            let grade = rows[0].grade;
                            if(fcm_token && grade=="Y"){
                                //Utils.fcm.pushMessageToken(fcm_token, 'grade_change' ,'', '등급이 변경 되었습니다.');
                                //Utils.fcm.pushMessageToken(fcm_token, 'grade_refresh' ,'', '등급이 변경 되었습니다 공유내역을 확인하세요.');
                                let sub_params ={
                                    type : 'grade_change',
                                    title : '',
                                    body : '등급이 변경 되었습니다.',
                                    grade :params.grade,
                                    sound: 'default'
                                }
                                Utils.fcm.pushMessageDefaultToken(fcm_token,sub_params);
                                sub_params.type='grade_refresh';
                                sub_params.body='등급이 변경 되었습니다 공유내역을 확인하세요.';
                                Utils.fcm.pushMessageDefaultToken(fcm_token,sub_params);

                                params.grade='N';
                                query = Queries.UserNotification.updateUserNotificationDuplicate(params);
                                return Mysql.execQueryPromise(query);
                            }else{
                                return null;
                            } 
                        }else{
                            return null;
                        }    
                    }  
                )                             
                .then(
                    (rows) => {   
                        console.log(rows)
                        resolve(true);
                    }  
                )                             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },  
    sendUserPaperNotification : (params) => {
        return new Promise(async (resolve, reject) => {
            try {   
                let query = Queries.UserDevice.getUserDeviceCheck(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {
                            let fcm_token = rows[0].fcm_token;
                            if(fcm_token){
                                Utils.fcm.pushMessageToken(fcm_token, 'paper' ,'', params.reason);
                            } 
                        }
                        resolve(true);
                    }  
                )                             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },                                                   
};           

