const moment = require('moment');
const convertTime = require('convert-time');

module.exports = {
  getScheduleList: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let date = moment().tz(params.timezone);
        let today = date.format("YYYY-MM-DD");
        let result = {code: "8300"}
        let current_time = parseInt(date.format("HHmm"));
        params.today = today;
        params.week = convertWeek(date.day());
        let query = Queries.Schedule.getScheduleList(params);
        //console.log(query)
        Mysql.execQueryPromise(query)
          .then(
            (rows) => {
              let data = [];
              result.code="0000";
              let oId=0;
              if (rows && rows.length >= 1) {
                rows.forEach((row) => {
                  let s_data = row;
                  //if((oId!=s_data.organization_id) && sId){
                    //console.log(111111111111)
                    //console.log('oId==' ,oId)
                    //console.log('organization_id==' ,s_data.organization_id)
                    oId=s_data.organization_id;

                    let visit_status = "V0";    
                    let wear_status = "W0";
                    let v_time = parseInt(row.visit_time_str);
                    // console.log(v_time)
                    // console.log(current_time)
                    let isData = false;
                    if (v_time >= current_time) isData = true;
                    else isData = false;

                    if (row.grade_cd == "G1" && params.grade_cd == "G1") visit_status = "V2";
                    else if (row.grade_cd == "G2" && (params.grade_cd == "G1" || params.grade_cd == "G2")) visit_status = "V2";
                    else if (row.grade_cd == "G3" && (params.grade_cd == "G1" || params.grade_cd == "G2" || params.grade_cd == "G3")) visit_status = "V2";
                    else visit_status = "V3";

                    let sub_data = {};
                    sub_data = row;
                    sub_data.current_date = today;
                    sub_data.visit_status = visit_status;
                    sub_data.wear_status = wear_status;
                    sub_data.visit_time_str = v_time;
                    if (isData) data.push(sub_data);
                  //}
                })
              }
              
              result.rows = data;
              resolve(result);
            }
          )
          .fail(
            (err) => {
              console.log(err)
              resolve({
                code: "8200"
              });
            }
          );
      } catch (e) {
        console.log(e)
        resolve({
          code: '8300'
        });
      }
    });
  },
};

function convertWeek(num) {
  let result = "";
  if (num == 0) result = "sun";
  else if (num == 1) result = "mon";
  else if (num == 2) result = "tue";
  else if (num == 3) result = "wed";
  else if (num == 4) result = "thu";
  else if (num == 5) result = "fri";
  else if (num == 6) result = "sat";

  return result;
}