module.exports = {
    createMessage: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.Message.createMessage(params);
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000' });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },   
    createMessageReply: (params) => {
      return new Promise(async (resolve, reject) => {
          try {     
              let query=Queries.Message.createMessageReply(params);
              Mysql.execQueryPromise(query)            
              .then(              
                  (rows) => {    
                   let affectedRows=rows.affectedRows;      
                   if(affectedRows>=1) resolve({code: '0000' });
                   else     resolve({code: '8100'});                    
                  }         
                )                        
              .fail(      
                (err) => {console.log(err)
                  resolve({code: '8200'});
                }
              );                       
          } catch (e) {console.log(e)
            resolve({code: '8300'});  
          }
      });
  },       
    getMessageSend: (params) => {
        return new Promise(async (resolve, reject) => {
            try {               
                let query=Queries.Message.getMessageSend(params);
                Mysql.execQueryPromise(query)  
                .then(                  
                  (rows) => {              
                    resolve({code: '0000' ,rows : rows}); 
                  }      
                )
                .fail(   
                  (err) => {
                    resolve({code: 8200 });
                  }   
                );                        
            } catch (e) {
               resolve({code: '8300'}); 
            }
        });
    },
    getMessageReceive: (params) => {
        return new Promise(async (resolve, reject) => {
            try {               
                let query=Queries.Message.getMessageReceive(params);
                Mysql.execQueryPromise(query)  
                .then(                  
                  (rows) => {              
                    resolve({code: '0000' ,rows : rows});   
                  }      
                )
                .fail(   
                  (err) => {
                    resolve({code: 8200 });
                  }   
                );                        
            } catch (e) {
               resolve({code: '8300'}); 
            }
        });
    }, 
    updateMessageReadSt: (params,id) => {
      return new Promise(async (resolve, reject) => {
          try {              
              let query=Queries.Message.updateMessageReadSt(params,id);          
              Mysql.execQueryPromise(query)    
              .then(      
                (rows) => {   
                  let affectedRows=rows.affectedRows;            
                  if(affectedRows>=1) resolve({code: '0000' });
                  else    resolve({code: '8100'}); 
                }
              ) 
              .fail(
                (err) => {
                  console.log(err)
                  resolve({code: '8200'}); 
                }
              );                     
          } catch (e) {
              console.log(e)
            resolve({code: '8300'});   
          }
      });   
    },                
};
