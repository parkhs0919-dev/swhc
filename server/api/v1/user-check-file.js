const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserCheckFile: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.UserCheckFile.createUserCheckFile(params);           
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000'  });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    }, 
    getUserCheckFile: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.UserCheckFile.getUserCheckFile(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },  
    deleteUserCheckFile: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.UserCheckFile.deleteUserCheckFile(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' });   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },                                                    
};           
