const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserAuth: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.V2UserAuth.createUserAuth(params);           
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000' });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },  
                                        
};           

