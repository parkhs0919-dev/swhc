
const jwt = require('jwt-simple');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();

module.exports = { 
    createHealthCheck: (params) => {
      return new Promise(async (resolve, reject) => {
          try {   
              let query=Queries.V2Health.getHealthCheck(params);
              Mysql.execQueryPromise(query)            
              .then(
                (rows) => {
                    if(rows && rows.length){
                        return null;
                    }else{
                        params.temperature = 0;
                        params.heart_rat = 0;
                        query=Queries.V2Health.createHealth(params);
                        return Mysql.execQueryPromise(query);
                    }
                }
              )
              .then(
                (rows) => {
                    resolve({ code: '0000' });
                }
              )                                         
              .fail(      
                (err) => {console.log(err)
                  resolve({code: '8200'});
                }
              );                       
          } catch (e) {console.log(e)
            resolve({code: '8300'});  
          }
      });
   },
   createHealth: (params) => {
    return new Promise(async (resolve, reject) => {
        try {   
            let query=Queries.V2Health.createHealth(params);
            Mysql.execQueryPromise(query)            
            .then(
              (rows) => {
                  if(rows && rows.length){
                    resolve({ code: '0000' });
                  }else{
                    resolve({ code: '8100' });
                  }
              }
            )                                      
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
 },   
  updateHealth: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {     
            params.network_connect_st =1;         
            let query=Queries.V2Health.updateHealth(params,id);
            Mysql.execQueryPromise(query)            
            .then(              
                (rows) => {    
                 let affectedRows=rows.affectedRows;      
                 if(affectedRows>=1) resolve({code: '0000' });
                 else     resolve({code: '8100'});                    
                }         
              )                        
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
 },
 updateHealthStatus: (params,id) => {
  return new Promise(async (resolve, reject) => {
      try {        
        console.log('---온도 상태 업데이트 s--- ')   
        console.log('user_id = ',params.user_id)   
        console.log('uuid = ',params.uuid)   
        console.log('platform = ',params.platform)   
        console.log('status_cd = ',params.status_cd)   
        console.log('---온도 상태 업데이트 e--')      
          let query=Queries.V2Health.updateHealthStatus(params,id);
          Mysql.execQueryPromise(query)            
          .then(              
              (rows) => {    
               let affectedRows=rows.affectedRows;      
               if(affectedRows>=1) resolve({code: '0000' });
               else     resolve({code: '8100'});                    
              }         
            )                        
          .fail(      
            (err) => {console.log(err)
              resolve({code: '8200'});
            }
          );                       
      } catch (e) {console.log(e)
        resolve({code: '8300'});  
      }
  });
 },  
 updateHealthGrade: (params,id) => {
  return new Promise(async (resolve, reject) => {
      try {             
          let query=Queries.V2Health.updateHealthGrade(params,id);
          Mysql.execQueryPromise(query)            
          .then(              
              (rows) => {    
               let affectedRows=rows.affectedRows;      
               if(affectedRows>=1) resolve({code: '0000' });
               else     resolve({code: '8100'});                    
              }         
            )                        
          .fail(      
            (err) => {console.log(err)
              resolve({code: '8200'});
            }
          );                       
      } catch (e) {console.log(e)
        resolve({code: '8300'});  
      }
  });
 },  
 getHealthGradeStatus: (params) => {
  return new Promise(async (resolve, reject) => {
      try {            
          let query=Queries.V2Health.getHealthGradeStatus(params);
          Mysql.execQueryPromise(query)  
          .then(                  
            (rows) => {
              let row = rows[0];
              resolve({code: '0000' ,rows : row});   
            }      
          )
          .fail(   
            (err) => {
              resolve({code: 8200 });
            }   
          );                        
      } catch (e) {
         resolve({code: '8300'}); 
      }
   });
 },
 getHealthCheck: (params) => {
  return new Promise(async (resolve, reject) => {
      try {            
          let query=Queries.V2Health.getHealthCheck(params);
          Mysql.execQueryPromise(query)  
          .then(                  
            (rows) => {
              if(rows && rows.length >= 1){
                resolve({code: '0000' ,rows : rows[0]});  
              }else{
                resolve({code: '8100' });  
              }                  
            }      
          )
          .fail(   
            (err) => {
              console.log(err)
              resolve({code: 8200 });
            }   
          );                        
      } catch (e) {
         resolve({code: '8300'}); 
      }
   });
 },
 getHealthGradeStatusMain: (params) => {
  return new Promise(async (resolve, reject) => {
      try {            
          let query=Queries.V2Health.getHealthGradeStatus(params);
          Mysql.execQueryPromise(query)  
          .then(                  
            (rows) => {
              let row = rows[0];
              let result={code:"8100"};
              if(row){
                if(row.grade_cd=="G0")  row.grade_cd ="G6"; 
                result=row;
                result.code="0000";
                resolve(result); 
              }else{
                resolve(result); 
              }     
            }      
          )
          .fail(   
            (err) => {
              resolve({code: 8200 });
            }   
          );                        
      } catch (e) {
         resolve({code: '8300'}); 
      }
   });
 },                           
};
