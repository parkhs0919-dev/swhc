
const jwt = require('jwt-simple');
const moment = require('moment');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();

module.exports = { 
  createUserLoginInfo: (params) => {
    return new Promise(async (resolve, reject) => {
        try {     
            let query=Queries.V2UserLoginInfo.createUserLoginInfo(params);           
            Mysql.execQueryPromise(query)            
            .then(              
                (rows) => {    
                 let affectedRows=rows.affectedRows;      
                 if(affectedRows>=1) resolve({code: '0000' });
                 else     resolve({code: '8100'});                    
                }         
              )                        
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
  },    
  getUserLoginInfoCheck: (params) => {
    return new Promise(async (resolve, reject) => {
        try {        
            let query=Queries.V2UserLoginInfo.getUserLoginInfoCheck(params);
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {       
                let check_cnt =0; 
                if(rows && rows.length >= 1){
                  rows.forEach((row) => {
                    if(row.uuid==params.uuid ) check_cnt+=0;
                    else check_cnt+=1;
                  })
                  if(check_cnt>=1) resolve({code: '1500'});  
                  else  resolve({code: '0000' });    
                }else{
                  resolve({code: '0000' });  
                }                   
              }      
            )
            .fail(   
              (err) => {
                resolve({code: 8200 });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  }, 
  getUserLoginInfoDeviceCheck: (params) => {
    return new Promise(async (resolve, reject) => {
        try {       
            let query=Queries.V2UserLoginInfo.getUserLoginInfoDeviceCheck(params);
           // console.log(query)
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {       
                if(rows && rows[0].cnt==1){
                  resolve({code: '0000'});  
                }else{
                  resolve({code: '9900' });  
                }                   
              }      
            )
            .fail(   
              (err) => {
                resolve({code: 8200 });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  },  
  setRedisLoginDevice: (params) => {//redis 업데이트 
    return new Promise(async (resolve, reject) => {
        try {   
          //console.log(params)
          let sub={
            uuid :params.uuid,
            platform :params.platform,
            unix:moment().tz(params.timezone).unix()
          }
          const data = await Utils.redis.set(params.user_id,sub);
          console.log(data)
          if(data && data.indexOf('OK')>-1){
            resolve({code:"0000"});
          }else{
            resolve({code:"8100"}); 
          }                                 
        } catch (e) {
           console.log(e)
           resolve({code: '8300'}); 
        }
    });
  },  
  getRedisLoginDevice: (params) => {//redis 조회
    return new Promise(async (resolve, reject) => {
        try { 
          const data = await Utils.redis.get(params.user_id);
          console.log(params.user_id)
          console.log(data)
          if(data && data.uuid==params.uuid && data.platform==params.platform){
            let result ={
              code: '0000',
              unix:(data.unix)?data.unix:moment().tz(params.timezone).unix()
            }
            resolve(result);     
          }else{
            resolve({code: '9900'}); 
          }
        } catch (e) {
           console.log(e)
           resolve({code: '9900'}); 
        }
    });
  },    
  updateUserLoginInfoCheck: (params) => {
    return new Promise(async (resolve, reject) => {
        try {        
            let data ={}
            data.user_id =params.user_id;
            data.login_yn = "N";
            let query=Queries.V2UserLoginInfo.updateUserLoginInfo(data);
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {  
                data.user_id =params.user_id;
                data.uuid =params.uuid;
                data.platform =params.platform;
                data.fcm_token =params.fcm_token;
                data.login_yn = "Y";
                query=Queries.V2UserLoginInfo.updateUserLoginInfoDuplicate(data);
                return Mysql.execQueryPromise(query);
              }      
            ) 
            .then(                  
              (rows) => {    
               // console.log(rows)
                resolve({ code: "0000" });
              }      
            )                               
            .fail(   
              (err) => {
                resolve({code: "8200" });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  },                           
};
