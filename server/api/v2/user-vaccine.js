const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    createUserVaccine: (params) => {
        return new Promise(async (resolve, reject) => {
            try {     
                let query=Queries.V2UserVaccine.createUserVaccine(params);           
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=rows.affectedRows;      
                     if(affectedRows>=1) resolve({code: '0000' , user_vaccine_id :rows.insertId });
                     else     resolve({code: '8100'});                    
                    }         
                  )                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },  
    getUserVaccine: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.V2UserVaccine.getUserVaccine(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    }, 
    getUserVaccineId: (id) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.V2UserVaccine.getUserVaccineId(id);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },
    deleteUserVaccine: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.V2UserVaccine.deleteUserVaccine(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' });   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    },                                                    
};           

