const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {
    getGroup: (params) => {
      return new Promise(async (resolve, reject) => {
          try {            
              let query=Queries.V2Group.getGroup(params);
              Mysql.execQueryPromise(query)  
              .then(                  
                (rows) => {              
                  resolve({code: '0000' ,rows : rows});   
                }      
              )
              .fail(   
                (err) => {
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {
             resolve({code: '8300'}); 
          }
      });
    }, 

                                                    
};           

