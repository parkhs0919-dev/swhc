
const jwt = require('jwt-simple');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();

module.exports = { 
    createUserDeviceCheck: (params) => {
      return new Promise(async (resolve, reject) => {
          try {   
              let query=Queries.V2UserDevice.getUserDeviceCheck(params);
              Mysql.execQueryPromise(query)            
              .then(
                (rows) => {
                    if(rows && rows.length){
                        return null;
                    }else{
                        query=Queries.V2UserDevice.createUserDevice(params);
                        return Mysql.execQueryPromise(query);
                    }
                }  
              )
              .then(
                (rows) => {
                    resolve({ code: '0000' });
                }
              )                                         
              .fail(      
                (err) => {console.log(err)
                  resolve({code: '8200'});
                }
              );                       
          } catch (e) {console.log(e)
            resolve({code: '8300'});  
          }
      });
   },
   updateUserDevice: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {    
            let query=Queries.V2UserDevice.updateUserDevice(params,id);
            Mysql.execQueryPromise(query)            
            .then(                
                (rows) => {    
                 let affectedRows=rows.affectedRows;      
                 if(affectedRows>=1) resolve({code: '0000' });
                 else     resolve({code: '8100'});                    
                }         
              )                        
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
  },  
  updateUserDeviceInit: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {    
            let query=Queries.V2UserDevice.updateUserDeviceInit(params,id);
            Mysql.execQueryPromise(query)            
            .then(                
                (rows) => {    
                 let affectedRows=rows.affectedRows;      
                 if(affectedRows>=1) resolve({code: '0000' });
                 else     resolve({code: '8100'});                    
                }         
              )                        
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
  },  
  getUserDeviceSnListCheck: (params) => {
    return new Promise(async (resolve, reject) => {
        try {            
            let query=Queries.V2UserDevice.getUserDeviceSnListCheck(params);
            Mysql.execQueryPromise(query)  
            .then(                  
              (rows) => {              
                resolve({code: '0000' ,rows : rows});   
              }      
            )
            .fail(   
              (err) => {
                resolve({code: 8200 });
              }   
            );                        
        } catch (e) {
           resolve({code: '8300'}); 
        }
    });
  }, 
  updateUserDeviceSnInit: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try {    
            let query=Queries.V2UserDevice.updateUserDeviceSnInit(params,id);
            Mysql.execQueryPromise(query)            
            .then(                
                (rows) => {    
                let affectedRows=rows.affectedRows;      
                if(affectedRows>=1) resolve({code: '0000' });
                else     resolve({code: '8110'});                    
                }         
              )                        
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
  }, 
  updateUserDeviceSnInitMac: (params) => {
    return new Promise(async (resolve, reject) => {
        try {    
            let query=Queries.V2UserDevice.updateUserDeviceSnInitMac(params);
            Mysql.execQueryPromise(query)            
            .then(                
                (rows) => {    
                let affectedRows=rows.affectedRows;      
                if(affectedRows>=1) resolve({code: '0000' });
                else     resolve({code: '8110'});                    
                }         
              )                        
            .fail(      
              (err) => {console.log(err)
                resolve({code: '8200'});
              }
            );                       
        } catch (e) {console.log(e)
          resolve({code: '8300'});  
        }
    });
  },                        
};
