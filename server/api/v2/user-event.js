
const moment = require('moment');
const convertTime = require('convert-time');
const momentTimeZone = require('moment-timezone');
const { ConsoleTransportOptions } = require('winston/lib/winston/transports');

module.exports={  
  getUserEventToday: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let result={code:"8300",rows:[]};
        let date = moment().tz(params.timezone);
        let today =date.format("YYYY-MM-DD");
        let yyyymm =date.format("YYYYMM");
        params.today=today;
        params.week =convertWeek(date.day());
        let query = Queries.V2UserEvent.getUserEventToday(params);
        //console.log(query)
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            if(rows && rows.length>=1) {
              result.code="0000";
              let sqls =[];
              let rows_cnt =rows.length;
              rows.forEach((row,index) => {
                let t1_str =today+' '+((convertTime(row.start_time,"hh:MM").length==4)? "0"+convertTime(row.start_time,"hh:MM") : convertTime(row.start_time,"hh:MM"));
                let t2_str =today+' '+((convertTime(row.end_time,"hh:MM").length==4)? "0"+convertTime(row.end_time,"hh:MM") : convertTime(row.end_time,"hh:MM"));
                let t1=moment(t1_str).tz(params.timezone);
                let t2=moment(t2_str).tz(params.timezone);  
                let diff = t2.diff(t1, 'minutes');
                let sub_params ={
                  z_value: params.z_value,
                  title: row.title,
                  start_date: row.start_date,
                  end_date: row.end_date,
                  week:row.week,
                  start_time:row.start_time,
                  end_time: row.end_time,
                  visit_time:row.visit_time,
                  today :today,
                  yyyymm :yyyymm,
                  user_id:params.user_id,
                  user_event_id :row.user_event_id,
                  s_time :convertTime(row.start_time,"hhMM"),
                  e_time :convertTime(row.end_time,"hhMM"),
                  diff:diff
                }
                sqls.push(Queries.V2UserEvent.getUserEventHealthLog(sub_params));
                if(index == (rows_cnt-1))sqls.push(' '); 
                else sqls.push(' union '); 
              })  
              //console.log(sqls.join(''))
              return Mysql.execQueryPromise(sqls.join(''));
            }else{
              resolve({code:"8100"});
            }
          }
        )
        .then(
          (rows) => {       
           //console.log(rows) 
           let data =[];
           let current =date.format("YYYYMMDDHHmm");
           let current_time =parseInt(date.format("HHmm"));
            if (rows && rows.length >= 1) {
              rows.forEach((row, index) => {
                let visit_status = "V0";
                let wear_status = "W0";
                let v_time = parseInt(convertTime(row.visit_time,"hhMM")); 
                let e_time = parseInt(convertTime(row.end_time,"hhMM")); 
                if (row.s_time <= current && row.e_time > current) {//진행중
                  visit_status = "V1";
                  wear_status = "W1";
                } else if (row.s_time > current) {//대기중
                  visit_status = "V0";
                  wear_status = "W0";
                } else if (row.e_time < current) {//완료
                  let wear_per = (((row.cnt) ? row.cnt : 1) / Math.abs(row.diff)) * 100;
                  let temp_per = (((row.temp_cnt) ? row.temp_cnt : 1) / Math.abs(row.diff)) * 100;
                    //방문시간 완료시간 비교 
                      // console.log(v_time);
                      // console.log(e_time);
                      // console.log(current_time);
                    // if(v_time>=current_time){
                    //   visit_status = "V1";
                    //   wear_status = "W2";
                    // }else{
                    //   if (wear_per >= 20 && temp_per >= 20) {    
                    //     visit_status = "V2";
                    //     wear_status = "W2";
                    //   } else {
                    //     visit_status = "V3";
                    //     wear_status = "W2";
                    //   } 
                    // }

                    if (wear_per >= 20 && temp_per >= 20) {    
                      visit_status = "V2";
                      wear_status = "W2";
                    } else {
                      visit_status = "V3";
                      wear_status = "W2";
                    } 
                    
                }

                let sub_data ={};
                sub_data=row;
                sub_data.visit_status=visit_status;
                sub_data.wear_status=wear_status;
                sub_data.start_time_str=convertTime(row.start_time,"hh:MM");
                sub_data.end_time_str=convertTime(row.end_time,"hh:MM");
                sub_data.visit_time_str=convertTime(row.visit_time,"hh:MM");

                data.push(sub_data)
              })
           }
           //console.log(data)
            result.rows=data;  
            resolve(result);
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  getUserEventTomorrow: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let result={code:"8300",rows:[]};
        let date = moment().tz(params.timezone).add(1, 'days');
        let today =date.format("YYYY-MM-DD");
        let yyyymm =date.format("YYYYMM");
        params.today=today;
        params.week =convertWeek(date.day());
        let query = Queries.V2UserEvent.getUserEventToday(params);
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            if(rows && rows.length>=1) {
              result.code="0000";
              let sqls =[];
              let rows_cnt =rows.length;
              rows.forEach((row,index) => {
                let t1_str =today+' '+((convertTime(row.start_time,"hh:MM").length==4)? "0"+convertTime(row.start_time,"hh:MM") : convertTime(row.start_time,"hh:MM"));
                let t2_str =today+' '+((convertTime(row.end_time,"hh:MM").length==4)? "0"+convertTime(row.end_time,"hh:MM") : convertTime(row.end_time,"hh:MM"));
                let t1=moment(t1_str).tz(params.timezone);
                let t2=moment(t2_str).tz(params.timezone);  
                let diff = t2.diff(t1, 'minutes');
                let sub_params ={
                  z_value: params.z_value,
                  title: row.title,
                  start_date: row.start_date,
                  end_date: row.end_date,
                  week:row.week,
                  start_time:row.start_time,
                  end_time: row.end_time,
                  visit_time:row.visit_time,
                  today :today,
                  yyyymm :yyyymm,
                  user_id:params.user_id,
                  user_event_id :row.user_event_id,
                  s_time :convertTime(row.start_time,"hhMM"),
                  e_time :convertTime(row.end_time,"hhMM"),
                  diff:diff
                }
                sqls.push(Queries.V2UserEvent.getUserEventHealthLog(sub_params));
                if(index == (rows_cnt-1))sqls.push(' '); 
                else sqls.push(' union '); 
              })  
              //console.log(sqls.join(''))
              return Mysql.execQueryPromise(sqls.join(''));
            }else{
              resolve({code:"8100"});
            }
          }
        )
        .then(
          (rows) => {       
           // console.log(rows) 
           let data =[];
           let current =date.format("YYYYMMDDHHmm");
           if(rows && rows.length>=1) {
            rows.forEach((row,index) => {              
              let visit_status = "V0";
              let wear_status = "W0";
              // if (row.s_time <= current && row.e_time > current) {//진행중
              //   visit_status = "V1";
              //   wear_status = "W1";
              // } else if (row.s_time > current) {//대기중
              //   visit_status = "V0";
              //   wear_status = "W0";
              // } else if (row.e_time < current) {//완료
              //   let wear_per = (((row.cnt) ? row.cnt : 1) / Math.abs(row.diff)) * 100;
              //   let temp_per = (((row.temp_cnt) ? row.temp_cnt : 1) / Math.abs(row.diff)) * 100;
              //   if (wear_per >= 50 && temp_per >= 50) {
              //     visit_status = "V2";
              //     wear_status = "W2";
              //   } else {
              //     visit_status = "V3";
              //     wear_status = "W2";
              //   }
              // }
              let sub_data ={};
              sub_data=row;
              sub_data.visit_status=visit_status;
              sub_data.wear_status=wear_status;
              sub_data.start_time_str=convertTime(row.start_time,"hh:MM");
              sub_data.end_time_str=convertTime(row.end_time,"hh:MM");
              sub_data.visit_time_str=convertTime(row.visit_time,"hh:MM");
              data.push(sub_data)
            })
           }
           //console.log(data)
            result.rows=data;  
            resolve(result);
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },
  getUserEventNumToday: (params) => {
    return new Promise(async (resolve, reject) => {
      try {
        let result={code:"8300",rows:[]};
        let date = moment().tz(params.timezone);
        let today =date.format("YYYY-MM-DD");
        let yyyymm =date.format("YYYYMM");
        params.today=today;
        params.week =convertWeek(date.day());
        let query = Queries.V2UserEvent.getUserEventToday(params);
        console.log(query)
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            if(rows && rows.length>=1) {
              result.code="0000";
              let sqls =[];
              let rows_cnt =rows.length;
              rows.forEach((row,index) => {
                let t1_str =today+' '+((convertTime(row.start_time,"hh:MM").length==4)? "0"+convertTime(row.start_time,"hh:MM") : convertTime(row.start_time,"hh:MM"));
                let t2_str =today+' '+((convertTime(row.end_time,"hh:MM").length==4)? "0"+convertTime(row.end_time,"hh:MM") : convertTime(row.end_time,"hh:MM"));
                let t1=moment(t1_str).tz(params.timezone);
                let t2=moment(t2_str).tz(params.timezone);  
                let diff = t2.diff(t1, 'minutes');
                let sub_params ={
                  z_value: params.z_value,
                  title: row.title,
                  start_date: row.start_date,
                  end_date: row.end_date,
                  week:row.week,
                  start_time:row.start_time,
                  end_time: row.end_time,
                  visit_time:row.visit_time,
                  today :today,
                  yyyymm :yyyymm,
                  user_id:params.user_id,
                  user_event_id :row.user_event_id,
                  s_time :convertTime(row.start_time,"hhMM"),
                  e_time :convertTime(row.end_time,"hhMM"),
                  diff:diff
                }
                sqls.push(Queries.V2UserEvent.getUserEventHealthLog(sub_params));
                if(index == (rows_cnt-1))sqls.push(' '); 
                else sqls.push(' union '); 
              })  
              //console.log(sqls.join(''))
              return Mysql.execQueryPromise(sqls.join(''));
            }else{
              resolve({code:"8100"});
            }
          }
        )
        .then(
          (rows) => {       
           //console.log(rows) 
           let data =[];
           let current =date.format("YYYYMMDDHHmm");
           let current_time =parseInt(date.format("HHmm"));
            if (rows && rows.length >= 1) {
              rows.forEach((row, index) => {
                let visit_status = "V0";
                let wear_status = "W0";
                let v_time = parseInt(convertTime(row.visit_time,"hhMM")); 
                let e_time = parseInt(convertTime(row.end_time,"hhMM")); 
                if (row.s_time <= current && row.e_time > current) {//진행중
                  visit_status = "V1";
                  wear_status = "W1";
                } else if (row.s_time > current) {//대기중
                  visit_status = "V0";
                  wear_status = "W0";
                } else if (row.e_time < current) {//완료
                  let wear_per = (((row.cnt) ? row.cnt : 1) / Math.abs(row.diff)) * 100;
                  let temp_per = (((row.temp_cnt) ? row.temp_cnt : 1) / Math.abs(row.diff)) * 100;
                    if (wear_per >= 20 && temp_per >= 20) {    
                      visit_status = "V2";
                      wear_status = "W2";
                    } else {
                      visit_status = "V3";
                      wear_status = "W2";
                    } 
                    
                }

                let sub_data ={};
                sub_data=row;
                sub_data.current_date=today;
                sub_data.visit_status=visit_status;
                sub_data.wear_status=wear_status;
                sub_data.start_time_str=convertTime(row.start_time,"hh:MM");
                sub_data.end_time_str=convertTime(row.end_time,"hh:MM");
                sub_data.visit_time_str=convertTime(row.visit_time,"hh:MM");

                data.push(sub_data)
              })
           }
           //console.log(data)
            result.rows=data;  
            resolve(result);
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },        
  getUserEventNum: (params,num) => {
    return new Promise(async (resolve, reject) => {
      try {
        let result={code:"8300",rows:[]};
        let date = moment().tz(params.timezone).add(num, 'days');
        let today =date.format("YYYY-MM-DD");
        let yyyymm =date.format("YYYYMM");
        params.today=today;
        params.week =convertWeek(date.day());
        let query = Queries.V2UserEvent.getUserEventToday(params);
        //console.log(query)
        Mysql.execQueryPromise(query)
        .then(
          (rows) => {
            if(rows && rows.length>=1) {
              result.code="0000";
              let sqls =[];
              let rows_cnt =rows.length;
              rows.forEach((row,index) => {
                let t1_str =today+' '+((convertTime(row.start_time,"hh:MM").length==4)? "0"+convertTime(row.start_time,"hh:MM") : convertTime(row.start_time,"hh:MM"));
                let t2_str =today+' '+((convertTime(row.end_time,"hh:MM").length==4)? "0"+convertTime(row.end_time,"hh:MM") : convertTime(row.end_time,"hh:MM"));
                let t1=moment(t1_str).tz(params.timezone);
                let t2=moment(t2_str).tz(params.timezone);  
                let diff = t2.diff(t1, 'minutes');
                let sub_params ={
                  z_value: params.z_value,
                  title: row.title,
                  start_date: row.start_date,
                  end_date: row.end_date,
                  week:row.week,
                  start_time:row.start_time,
                  end_time: row.end_time,
                  visit_time:row.visit_time,
                  today :today,
                  yyyymm :yyyymm,
                  user_id:params.user_id,
                  user_event_id :row.user_event_id,
                  s_time :convertTime(row.start_time,"hhMM"),
                  e_time :convertTime(row.end_time,"hhMM"),
                  diff:diff
                }
                sqls.push(Queries.V2UserEvent.getUserEventHealthLog(sub_params));
                if(index == (rows_cnt-1))sqls.push(' '); 
                else sqls.push(' union '); 
              })  
              //console.log(sqls.join(''))
              return Mysql.execQueryPromise(sqls.join(''));
            }else{
              resolve({code:"8100"});
            }
          }
        )
        .then(
          (rows) => {       
           // console.log(rows) 
           let data =[];
           let current =date.format("YYYYMMDDHHmm");
           if(rows && rows.length>=1) {
            rows.forEach((row,index) => {              
              let visit_status = "V0";
              let wear_status = "W0";
              let sub_data ={};
              sub_data=row;
              sub_data.current_date=today;
              sub_data.visit_status=visit_status;
              sub_data.wear_status=wear_status;
              sub_data.start_time_str=convertTime(row.start_time,"hh:MM");
              sub_data.end_time_str=convertTime(row.end_time,"hh:MM");
              sub_data.visit_time_str=convertTime(row.visit_time,"hh:MM");
              console.log(sub_data)
              data.push(sub_data)
            })
           }
           //console.log(data)
            result.rows=data;  
            resolve(result);
          }
        )
        .fail(
          (err) => {
            console.log(err)
            resolve({ code: "8200" });
          }
        );
      } catch (e) {
        console.log(e)
        resolve({ code: '8300' });
      }
    });
  },                    
};                       
  
  
// function convertTime(str){
//   return (convertTime(str,"hhMM").length==3)? "0"+convertTime(str,"hhMM"):convertTime(str,"hhMM");
// }

function convertWeek(num){
  let result ="";
  if(num==0 ) result ="sun";
  else if(num==1) result ="mon";
  else if(num==2) result ="tue";
  else if(num==3) result ="wed";
  else if(num==4 ) result ="thu";
  else if(num==5) result ="fri";
  else if(num==6) result ="sat";
   
  return result;
}