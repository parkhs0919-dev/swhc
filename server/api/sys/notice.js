
module.exports={  
    createNotice: (params) => {
        return new Promise(async (resolve, reject) => {
            try {   
                let result ={code :"8300",data:{}}; 
                let query=Queries.SysNotice.createNotice(params);
                Mysql.execQueryPromise(query)            
                .then(              
                    (rows) => {    
                     let affectedRows=(rows)? rows.affectedRows:0;      
                     if(affectedRows>=1){
                        result.code="0000";
                        let topic="hpitech";
                        //Utils.fcm.pushMessageTopic(topic, 'notice', '', params.contents);
                       return null;
                     }else{
                        result.code="8200";   
                     }                                    
                    }         
                  ) 
                  .then(              
                    (rows) => {   
                        resolve(result);                               
                    }         
                  )                                           
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },    
    getNoticeList: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}};
                let query = Queries.SysNotice.getNoticeList(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        query = Queries.SysNotice.getNoticeListTotalCnt(params);
                        return Mysql.execQueryPromise(query);
                    }
                )
                .then(
                    (rows) => {
                        result.data.total_cnt=rows[0].total_cnt;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },
    getNoticeDetail: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}};
                let query = Queries.SysNotice.getNoticeDetail(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                      if(rows.length>=1){
                        result.code="0000";
                        result.data=rows[0];
                        resolve(result);
                      } else{
                        resolve({code: '8100'});
                      }
                    }
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {s
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },  
    updateNotice: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try { 
                let result ={code :"8300",data:{}};             
                let query=Queries.SysNotice.updateNotice(params,id);       
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1)resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                ) 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    },                                
};                       
  