
const url = require('url');
const moment = require('moment');

module.exports={  
    createUserInfoProcess: (params) => {
        return new Promise(async (resolve, reject) => {
            try {  
                let query=Queries.SysUser.createUserInfo(params);
                Mysql.execQueryPromise(query)            
                .then(
                    (rows) => {
                        let affectedRows = (rows)? rows.affectedRows : 0;
                        if (affectedRows >= 1) {
                          params.user_id = rows.insertId;
                          query=Queries.AdHealth.createHealth(params);
                          //console.log(query);
                          return  Mysql.execQueryPromise(query);
                        } else {
                          return null
                        }
                    }
                )
                .then(
                    (rows) => {
                        let affectedRows = (rows)? rows.affectedRows : 0;
                        if (affectedRows >= 1) {
                          query=Queries.SysUserDevice.createUserDevice(params);
                          //console.log(query);
                          return  Mysql.execQueryPromise(query);
                        } else {
                          return null
                        }
                    }
                ) 
                .then(
                    (rows) => {
                        let affectedRows = (rows)? rows.affectedRows : 0;
                        if (affectedRows >= 1) {
                            query=Queries.SysUserTime.createUserTime(params);
                          //console.log(query);
                          return  Mysql.execQueryPromise(query);
                        } else {
                          return null
                        }
                    }
                )                 
                .then(
                    (rows) => {
                        let affectedRows = rows.affectedRows;
                        if (affectedRows >= 1) resolve({ code: '0000' });
                        else resolve({ code: '8100' });
                    }
                )                                                        
                .fail(      
                  (err) => {console.log(err)
                    resolve({code: '8200'});
                  }
                );                       
            } catch (e) {console.log(e)
              resolve({code: '8300'});  
            }
        });
    },    
    getUserList: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300"}
                let query = Queries.SysUser.getUserList(params);  
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.rows=rows;
                        query = Queries.SysUser.getUserListTotalCnt(params);
                        return Mysql.execQueryPromise(query);
                    }
                )
                .then(
                    (rows) => {
                        result.total_cnt=rows[0].total_cnt;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    }, 
    getUserInfoList: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300"}
                let query = Queries.SysUser.getUserInfoList(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.rows=rows;
                        query = Queries.SysUser.getUserInfoListTotalCnt(params);
                        return Mysql.execQueryPromise(query);
                    }
                )
                .then(
                    (rows) => {
                        result.total_cnt=rows[0].total_cnt;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },     
    getUserConfirmedList: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let query = Queries.SysUser.getUserConfirmedList(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        resolve({ code: '0000', rows: rows });
                    }
                )
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },     
    getUserDetail: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300"};
                let query = Queries.SysUser.getUserDetail(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        if(rows && rows.length >= 1){
                          result.code="0000";
                          result.detail= rows[0];  
                          resolve(result);
                        }else{
                            resolve({ code: "8100" });
                        }
                    }
                )
                .then(
                    (rows) => {
                        result.temp_log= rows;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },
    getUserDetailTempLog: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300"};
                let query = Queries.SysUser.getUserDetailTempLog(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.rows=rows;
                        query = Queries.SysUser.getUserDetailTempLogTotalCnt(params);
                        return Mysql.execQueryPromise(query);
                    }
                )
                .then(
                    (rows) => {
                        result.total_cnt=rows[0].total_cnt;
                        resolve(result);
                    }
                )   
                .then(
                    (rows) => {
                        result.temp_log= rows;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },   
    getUserDetailTempLogExcel: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}}
                query = Queries.SysUser.getUserDetailTempLogExcel(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        resolve(result);
                    }
                )               
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },        
    getUserInfoDetail: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300"};
                let query = Queries.SysUser.getUserDetail(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {  
                        if(rows && rows.length >= 1){
                            resolve({code: '0000' ,rows: rows[0] });
                        }else{
                            resolve({code: '8100'});
                        }     
                    }  
                )                    
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },    
    getUserConfirmedDetail: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300"};
                let currentMonth =moment().add(0, 'M').format("YYYYMM");
                let preMonth =moment().add(-1, 'M').format("YYYYMM");
                let dateArray =[preMonth,currentMonth]
                let query = Queries.SysUser.getUserConfirmedDetail(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        if(rows && rows.length >= 1){
                          result.code="0000";
                          result.detail= rows[0];  
                          query = Queries.SysUser.getUserConfirmedDetailTempLog(params,dateArray);
                          return Mysql.execQueryPromise(query);
                        }else{
                          result.code="8100";
                          return null;
                        }
                    }
                )
                .then(
                    (rows) => {
                        result.temp_log= rows;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },  
    getUserConfirmedDetailExcel: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300"};
                let currentMonth =moment().add(0, 'M').format("YYYYMM");
                let preMonth =moment().add(-1, 'M').format("YYYYMM");
                let dateArray =[preMonth,currentMonth]
                let query = Queries.SysUser.getUserConfirmedDetailTempLog(params,dateArray);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.rows= rows;
                        resolve(result);
                    }
                )                
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },       
    getUserSearchSelectBox: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let query = Queries.SysUser.getUserSearchSelectBox(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                      resolve({code: '0000' ,rows: rows });
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },      
    updateUserConfirmed: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {              
                let query=Queries.SysUser.updateUserConfirmed(params,id);       
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1) resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                ) 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    },
    updateUserInfo: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {              
                let query=Queries.SysUser.updateUserInfo(params,id);       
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1) resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                ) 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    }, 
    updateUserPasswordInit: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {              
                let query=Queries.SysUser.updateUserPasswordInit(params,id);     
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1) resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                ) 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    },       
    getUserDuplicateCheck: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let query = Queries.SysUser.getUserDuplicateCheck(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows[0].cnt >= 1) {
                            resolve({ code: '1400' });
                        } else {
                            resolve({ code: '0000' });
                        }
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    }, 
    getUserAuthIdDuplicateCheck: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let user_auth_id="";
                let query = Queries.SysUser.getUserAuthIdDuplicateCheck(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {   
                        if (rows && rows.length >= 1) {
                            rows.forEach(row => {
                                if(!row.user_id){
                                    user_auth_id=row.user_auth_id;
                                }    
                            })
                            resolve({ code: "0000", user_auth_id : user_auth_id});
                        } else {
                            return null;
                        }  
                    }  
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    },
    updateUserDeviceInit: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try {   
                params.user_id= id;           
                let query=Queries.SysUser.updateUserDeviceInit(params,id);      
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {  
                    console.log(rows) 
                    let affectedRows = (rows)? rows.affectedRows :"";            
                    if(affectedRows && affectedRows>=1){
                        let data ={}
                        data.user_id =id;
                        data.login_yn = "N";
                        query=Queries.UserLoginInfo.updateUserLoginInfo(data);
                        return Mysql.execQueryPromise(query);  
                    }else{
                        return null;
                    }                      
                  }
                ) 
                .then(      
                    (rows) => {  
                      console.log(rows) 
                      let affectedRows = (rows)? rows.affectedRows :"";            
                      if(affectedRows && affectedRows>=1) resolve({code: '0000' });
                      else    resolve({code: '8100'});           
                    }
                  )                 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    },      
    deleteUser: (id) => {
        return new Promise(async (resolve, reject) => {
            try {                
                let query=Queries.SysUser.deleteUser(id);
                Mysql.execQueryPromise(query)  
                .then(             
                    (rows) => {                 
                        let affectedRows=rows.affectedRows;      
                        if(affectedRows>=1) resolve({code: '0000'});
                        else     resolve({code: '8100'});    
                    }
                  )                    
                .fail(   
                  (err) => {
                    resolve({code: '8200'});
                  }   
                );                          
            } catch (e) {  
              resolve({code: '8300'});
            }
        }); 
    },                                         
};                       
  