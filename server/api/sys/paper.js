const util = require('util');
const auth = CONFIG.common.auth;

/* register mysql queries */
module.exports = {  
  getUserCheckUnionAll: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data :{}}
            let query = Queries.SysPaper.getUserCheckUnionAll(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {
                    result.code="0000";
                    result.data.rows=rows;
                    query = Queries.SysPaper.getUserCheckUnionAllTotalCnt(params);
                    return Mysql.execQueryPromise(query);
                }
            )
            .then(
                (rows) => {
                    result.data.total_cnt=rows[0].total_cnt;
                    resolve(result);
                }
            )                
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },  
  getPaperDetail: (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result ={code :"8300",data :{}}
            let query = Queries.SysPaper.getPaperDetail(params);
            Mysql.execQueryPromise(query)
            .then(
                (rows) => {   
                  if(rows.length>=1){
                    result.code="0000";
                    result.data=rows[0];
                    resolve(result);
                  } else{
                    resolve({code: '8100'});
                  }
                }
            )                
            .fail(
                (err) => {
                    console.log(err)
                    resolve({ code: "8200" });
                }
            );
        } catch (e) {
            console.log(e)
            resolve({ code: '8300' });
        }
    });
  },
  updatePaper: (params,id) => {
    return new Promise(async (resolve, reject) => {
        try { 
            let result ={code :"8300",data:{}};             
            let query=Queries.SysPaper.updatePaper(params,id);       
            Mysql.execQueryPromise(query)    
            .then(      
              (rows) => {   
                let affectedRows=rows.affectedRows;            
                if(affectedRows>=1)resolve({code: '0000' });
                else    resolve({code: '8100'}); 
              }
            ) 
            .fail(
              (err) => {
                console.log(err)
                resolve({code: '8200'}); 
              }
            );                     
        } catch (e) {
            console.log(e)
          resolve({code: '8300'});   
        }
    });   
},                                                              
};           