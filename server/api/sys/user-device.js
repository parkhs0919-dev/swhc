
module.exports={   
    getUserDeviceSearch: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result ={code :"8300",data:{}};
                let query = Queries.SysUserDevice.getUserDeviceSearch(params);
                Mysql.execQueryPromise(query)
                .then(
                    (rows) => {
                        result.code="0000";
                        result.data.rows=rows;
                        resolve(result);
                    }
                )             
                .fail(
                    (err) => {
                        console.log(err)
                        resolve({ code: "8200" });
                    }
                );
            } catch (e) {
                console.log(e)
                resolve({ code: '8300' });
            }
        });
    }, 
    deleteUserDevice: (params,id) => {
        return new Promise(async (resolve, reject) => {
            try { 
                let result ={code :"8300",data:{}};             
                let query=Queries.SysUserDevice.deleteUserDevice(params,id);       
                Mysql.execQueryPromise(query)    
                .then(      
                  (rows) => {   
                    let affectedRows=rows.affectedRows;            
                    if(affectedRows>=1)resolve({code: '0000' });
                    else    resolve({code: '8100'}); 
                  }
                ) 
                .fail(
                  (err) => {
                    console.log(err)
                    resolve({code: '8200'}); 
                  }
                );                     
            } catch (e) {
                console.log(e)
              resolve({code: '8300'});   
            }
        });   
    },                                
};                       
  