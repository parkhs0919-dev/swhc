
const jwt = require('jwt-simple');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();


module.exports={     
  getOrganizationAll: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={code :"8300",data :{}}              
              let query=Queries.SysOrganization.getOrganizationAll(params);
              Mysql.execQueryPromise(query)  
              .then(                    
                (rows) => {    
                    result.code="0000";
                    result.data.rows=rows;
                    resolve(result);   
                }      
              )
              .fail(   
                (err) => {console.log(err)
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {console.log(e)
             resolve({code: '8300'}); 
          }
      });
    },
    getOrganizationList: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={code :"8300"}
              let query = Queries.SysOrganization.getOrganizationList(params);
              Mysql.execQueryPromise(query)
              .then(
                  (rows) => {
                      result.code="0000";
                      result.rows=rows;
                      query = Queries.SysOrganization.getOrganizationListTotalCnt(params);
                      return Mysql.execQueryPromise(query);
                  }
              )
              .then(
                  (rows) => {
                      result.total_cnt=rows[0].total_cnt;
                      resolve(result);
                  }
              )                
              .fail(
                  (err) => {
                      console.log(err)
                      resolve({ code: "8200" });
                  }
              );
          } catch (e) {
              console.log(e)
              resolve({ code: '8300' });
          }
      });
  },
  deleteOrganization: (id) => {
    return new Promise(async (resolve, reject) => {
        try {                
            let query=Queries.SysOrganization.deleteOrganization(id);   
            Mysql.execQueryPromise(query)  
            .then(             
                (rows) => {                 
                    let affectedRows=rows.affectedRows;      
                    if(affectedRows>=1) resolve({code: '0000'});
                    else     resolve({code: '8100'});    
                }
              )                    
            .fail(   
              (err) => {
                resolve({code: '8200'});
              }   
            );                          
        } catch (e) {  
          resolve({code: '8300'});
        }
    }); 
  },            
};                       
  