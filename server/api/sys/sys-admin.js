
const jwt = require('jwt-simple');
const auth = CONFIG.common.auth;
const date = Utils.date.getDate();
const sms = CONFIG.sms[SERVER_ENV];

module.exports={     
  getSysAdminLoginCheck: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={};               
              let query=Queries.SysAdmin.getSysAdminLoginCheck(params);
              Mysql.execQueryPromise(query)  
              .then(                    
                (rows) => {    
                  if(rows.length>=1){
                    let data =rows[0];
                    data.type = "SYS";
                    result.code = "0000";
                    result.token = createToken(data);   
                    resolve(result);
                  }else{
                    resolve({code: '8100'});
                  }     
                }      
              )
              .fail(   
                (err) => {console.log(err)
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {console.log(e)
             resolve({code: '8300'}); 
          }
      });
    },
    getSysAdminLoginNewWindow: (params) => {
      return new Promise(async (resolve, reject) => {
          try {
              let result ={};               
              let query=Queries.SysAdmin.getSysAdminLoginNewWindow(params);
              Mysql.execQueryPromise(query)  
              .then(                    
                (rows) => { 
                  if(rows.length>=1){
                    let data =rows[0];
                    result.code = "0000";
                    result.type = data.type;
                    result.link_login_key = randomString(20);
                    result.organization_admin_id=data.organization_admin_id;
                    // routerresult.token = createTokenNew(data);   
                    resolve(result);
                  }else{
                    resolve({code: '8100'});
                  }     
                }      
              )
              .fail(   
                (err) => {console.log(err)
                  resolve({code: 8200 });
                }   
              );                        
          } catch (e) {console.log(e)
             resolve({code: '8300'}); 
          }
      });
    },    
    updateSysOrganizationAdmin: (params,id) => {
      return new Promise(async (resolve, reject) => {
          try {              
              let query=Queries.SysAdmin.updateSysOrganizationAdmin(params,id);       
              Mysql.execQueryPromise(query)    
              .then(      
                (rows) => {   
                  let affectedRows=rows.affectedRows;            
                  if(affectedRows>=1) resolve({code: '0000' });
                  else    resolve({code: '8100'}); 
                }
              ) 
              .fail(
                (err) => {
                  console.log(err)
                  resolve({code: '8200'}); 
                }
              );                     
          } catch (e) {
              console.log(e)
            resolve({code: '8300'});   
          }
      });   
    },         
};                       
  
function createToken(data){
  let result = data;
  result.key = auth.key;
  result.date = date.yyyymmddhhmiss;
  return jwt.encode(result, auth.secret);
}
function createTokenNew(data){
  let result = data;
  result.key = auth.key;
  result.date = date.yyyymmddhhmiss;
  return jwt.encode(result, auth.secret);
}

function randomString(num) {
  let chars = "abcdefghiklmnopqrstuvwxyz0123456789";
  let string_length = num;
  let randomstring = '';
  for (let i = 0; i < string_length; i++) {
      let rnum = Math.floor(Math.random() * chars.length);
      randomstring += chars.substring(rnum, rnum + 1);
  }
  return randomstring;
}
