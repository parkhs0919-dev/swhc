const fs = require('fs');  
const path = require("path");           
  
const createGroupTime = async () => {
    return new Promise(async (resolve, reject) => {
        try{
            let query=[];
            query.push(" CREATE TABLE IF NOT EXISTS `tb_group_time` ( ");
            query.push(" `user_time_id` INT NOT NULL AUTO_INCREMENT, ");
            query.push(" `group_id` INT NULL, ");
            query.push(" `start_date` VARCHAR(10) NULL, ");
            query.push(" `end_date` VARCHAR(10) NULL, ");
            query.push(" `week` VARCHAR(40) NULL, ");
            query.push(" `start_time` VARCHAR(10) NULL, ");
            query.push(" `end_time` VARCHAR(10) NULL, ");
            query.push(" `holiday_yn` ENUM('Y', 'N') NULL DEFAULT 'N', ");
            query.push(" `updated_dt` DATETIME NOT NULL DEFAULT current_timestamp, ");
            query.push(" PRIMARY KEY (`user_time_id`)) ");
            query.push(" ENGINE = InnoDB ");

            Mysql.execQueryPromise(query.join(""))      
            .then(  
                function(rows){          
                    resolve(true);
                }
            )                 
            .fail(    
                function(err){
                    console.log(err);
                    resolve(false);  
                }    
            ); 
        } catch (e) {
            reject(e);   
        }                           
    }); 
};
const uploadsPath = path.join(__dirname, '../../uploads');


module.exports.initDirectories = async () => {
    if (!fs.existsSync(uploadsPath)) fs.mkdirSync(uploadsPath);
    return true;
};

module.exports.initCreateTable = async () => {
    if (createGroupTime()) {  
        console.log('initialize createGroupTime  success');
    } else {
        console.log('initialize createGroupTime  fail');
    }                   
    return true;      
};  