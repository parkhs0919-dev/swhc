const { logger } = CONFIG.log;

const onStart = () => {
  logger.info("server start")
}

const onSendingMsgError = (error, msg) => {
  error.msg = msg
  logger.error(`Error on sending message: msg=${msg.data}`)
  logger.error(error)  
}  

const onSendingMsg = (msg) => {
    logger.info(`${msg.data}`)
}  
         
module.exports = {
  onSendingMsgError: onSendingMsgError,
  onSendingMsg:onSendingMsg, 
  onStart: onStart
}  