const DB_OPTIONS = CONFIG['DB_OPTIONS'];
const mysqlDump = require('node-mysql-dump');
const fs = require('fs');
const path = require('path');
const request = require('request');
  
// const tables=[
 
// ]  
const dumpPath = path.join(__dirname, '../../dump');    
if (!fs.existsSync(dumpPath)) fs.mkdirSync(dumpPath);

module.exports = {
    sqlDataSave : () => {
        return new Promise(async (resolve, reject) => {
            try {                     
                let query=' SHOW TABLE STATUS ';
                Mysql.execQueryPromise(query)            
                .then(    
                    function(rows){
                        let tables=[];
                        rows.forEach((data) => {                 
                        tables.push(data.Name);
                        });    
                                                         
                        if(rows.length>0){
                            mysqlDump({          
                                host: DB_OPTIONS.host,
                                user: DB_OPTIONS.user,
                                password: DB_OPTIONS.password,
                                database: DB_OPTIONS.database,
                                tables: tables, // only these tables
                                //@todo where: {'players': 'id < 1000'}, // Only test players with id < 1000
                                //@todo ifNotExist:true, // Create table if not exist
                                extendedInsert: true, // use one insert for many rows
                                addDropTable: true,// add "DROP TABLE IF EXISTS" before "CREATE TABLE"
                                addLocks: true,// add lock before inserting data
                                disableKeys: true,//adds /*!40000 ALTER TABLE table DISABLE KEYS */; before insert
                                dest: dumpPath+'/data.sql' // destination file
                            }, function (err) {  
                                if (err) resolve({is_success: 0});     
                                else  resolve({is_success: 1});  
                                // data.sql file created;  
                            }) 
                        }else{            
                            resolve({is_success: 0}); 
                        }                       
                    }         
                )                      
                .fail( 
                    function(err){
                    //console.log(err);
                    resolve({is_success: 0});
                    }
                );                                                                
                            
            } catch (e) {             
                reject(e);       
            }     
        });  
    },
    sqlDataRestore : () => {
        return new Promise(async (resolve, reject) => {
            try {                  
                          
            } catch (e) {  
                reject(e);       
            }    
        });    
    },
    sqlDataUpload : () => {
        return new Promise(async (resolve, reject) => {
            try {                  
                request.post({
                    url: 'https://slack.com/api/files.upload',
                    formData: {
                        file: fs.createReadStream('sample.zip'),
                        token: '### access token ###',
                        filetype: 'zip',
                        filename: 'samplefilename',
                        channels: 'sample',
                        title: 'sampletitle',
                    }, 
                }, function(error, response, body) {
                    resolve({is_success: 1});
                    console.log(body);
                });                      
            } catch (e) {  
                reject(e);       
            }    
        });  
    }                   
}

