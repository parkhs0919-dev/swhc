const request = require('request');
const sms = CONFIG.sms[SERVER_ENV];

module.exports = {
    sendSms: function (params) {
        return new Promise(async (resolve, reject) => {
            try {
                let option = {
                    url: sms.sms_url,
                    method: 'post',   
                    form: params,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }                      
                } 
                console.log(option);
                request(option, (err, res, body) => {
                    if (err) {
                        console.log(err);
                        resolve(err);
                    } else {
                        console.log('getRequestWorker===' + JSON.stringify(body));
                        resolve(body);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }, 
    internalSendSms: function (params) {
        return new Promise(async (resolve, reject) => {
            try {
                let option = {
                    url: sms.internal_url,
                    method: 'post',   
                    form: params,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }                      
                } 
                console.log(option);
                request(option, (err, res, body) => {
                    if (err) {
                        console.log(err);
                        resolve(err);
                    } else {
                        console.log('getRequestWorker===' + JSON.stringify(body));
                        resolve(body);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    },                  
}   
          