const nodemailer = require('nodemailer');
const email = CONFIG.common.email;
const smtpTransport = nodemailer.createTransport({
    service: email.service,
    auth: {
        user: email.auth_user,
        pass: email.auth_pass,
    },
    tls: {
        rejectUnauthorized: false
    }
});

const sendToAdminAuth = (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            const mailOptions = {
                from: params.from,
                to: params.to,
                subject: params.subject,
                html: getAdminLoginHtml(params.num)
            };         
            smtpTransport.sendMail(mailOptions, (error, responses) =>{
                console.log(responses)
                if(error){
                    console.log('email err')
                    resolve('err');                
                }else{
                    console.log('email success')
                    resolve('success');
                }
                smtpTransport.close();
            });

            
        } catch (e) {
            reject(e);
        }
    });
};
const sendToAdmin = (params) => {
    return new Promise(async (resolve, reject) => {
        try {

            // const mailOptions = {
            //     from: "pv@gmail.com",
            //     to: "p@gmail.com",
            //     subject: "테스트",
            //     text: "테스트 내용ㅇㅇ"
            //     html: "<p>테스트</p>"
            // };         
            smtpTransport.sendMail(params, (error, responses) =>{
                console.log(responses)
                if(error){
                    console.log('email err')
                    resolve('err');                
                }else{
                    console.log('email success')
                    resolve('success');
                }
                smtpTransport.close();
            });

            
        } catch (e) {
            reject(e);
        }
    });
};
const sendToUserAuth = (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            const mailOptions = {
                from: params.from,
                to: params.to,
                subject: params.subject,
                html: getUserLoginHtml(params)
            };         
            smtpTransport.sendMail(mailOptions, (error, responses) =>{
                console.log(responses)
                if(error){
                    console.log('email err')
                    resolve('err');                
                }else{
                    console.log('email success')
                    resolve('success');
                }
                smtpTransport.close();
            });

            
        } catch (e) {
            reject(e);
        }
    });
};
module.exports = {
    sendToAdminAuth,
    sendToAdmin,
    sendToUserAuth
};
//

function getAdminLoginHtml(params) {
    let html =[];
    html.push('<div class="">');
    html.push('<div class="aHl"></div>');
    html.push('<div id=":1hn" tabindex="-1"></div>');
    html.push('<div id=":1id" class="ii gt">');
    html.push('<div id=":1ic" class="a3s aiL "><u></u>');

    html.push('<div style="margin:0;background-color:#f7f7f7">');
    html.push('<table style="table-layout:fixed;border-collapse:collapse;width:100%;background-color:#f7f7f7">');
    html.push('<tbody>');
    html.push('<tr><td style="padding:0;height:50px"></td></tr>');
    html.push('<tr><td style="padding:0 14px" align="center">');

    html.push('<div style="margin:0 auto;max-width:520px">');
    html.push('<table style="margin:0 auto;table-layout:fixed;border-collapse:collapse;max-width:520px;width:100%;background-color:#fff">');
    html.push(' <tbody><tr><td style="padding:50px 20px 0;text-align:center" align="center">');

    html.push(' <img src="https://smarthealthcare.hpitech.com/dist/img/logo.png" alt="Logo" style="display:block;margin:0 auto;width:200px;height:30px" width="200" height="30" class="CToWUd"></img>');

    html.push('<p style="margin:46px auto 0;max-width:420px;font-family:-apple-system,BlinkMacSystemFont,sans-serf;font-size:24px;font-weight:700;line-height:1.42;letter-spacing:-0.1px;color:#222">');
    html.push('이메일 인증코드를<br>확인하세요.</p>');
    html.push('<p style="margin:20px auto 0;max-width:420px;font-family:-apple-system,BlinkMacSystemFont,sans-serf;font-size:14px;line-height:1.71;color:#666;word-break:keep-all;letter-spacing:-0.1px">');
    html.push('로그인을 위한 이메일 인증코드입니다. 아래의 인증코드를 진행 중인 로그인 화면에 입력하시면 인증이 완료됩니다.</p>');
    html.push('<p style="margin:32px auto 0;padding-top:32px;max-width:420px;border-top:1px solid #e5e5e5;font-family:-apple-system,BlinkMacSystemFont,sans-serf;font-size:14px;font-weight:700;line-height:1;color:#222;text-align:center">');
    html.push('인증코드</p>');
    html.push('</td></tr>');

    html.push('<tr><td style="padding:16px 20px 50px" align="center">');
    html.push('<table style="table-layout:fixed;border-collapse:collapse;width:260px;height:66px;text-align:center">');
    html.push('<tbody><tr><td style="margin:0 auto;padding:0 16px;height:66px;border-radius:4px;background-color:#f6f8fe;font-family:-apple-system,BlinkMacSystemFont,sans-serf;font-size:28px;font-weight:700;letter-spacing:4px;color:#194bdc" align="center">');
    html.push(params.num);
    html.push(' </td></tr>');
    html.push('</tbody></table>');
    html.push('</td></tr></tbody>');
    html.push('</table></div></td></tr>');

    html.push('<tr><td style="padding:0;height:50px"></td></tr>');
    html.push('</tbody></table></div>');
    html.push(' </div></div>');
    html.push(' <div class="hi"></div></div>');

    return html.join('');
}

function getUserLoginHtml(params) {
    let html =[];
    html.push('<div class="">');
    html.push('<div class="aHl"></div>');
    html.push('<div id=":1hn" tabindex="-1"></div>');
    html.push('<div id=":1id" class="ii gt">');
    html.push('<div id=":1ic" class="a3s aiL "><u></u>');

    html.push('<div style="margin:0;background-color:#f7f7f7">');
    html.push('<table style="table-layout:fixed;border-collapse:collapse;width:100%;background-color:#f7f7f7">');
    html.push('<tbody>');
    html.push('<tr><td style="padding:0;height:50px"></td></tr>');
    html.push('<tr><td style="padding:0 14px" align="center">');

    html.push('<div style="margin:0 auto;max-width:520px">');
    html.push('<table style="margin:0 auto;table-layout:fixed;border-collapse:collapse;max-width:520px;width:100%;background-color:#fff">');
    html.push(' <tbody><tr><td style="padding:50px 20px 0;text-align:center" align="center">');

    html.push(' <img src="https://smarthealthcare.hpitech.com/dist/img/logo.png" alt="Logo" style="display:block;margin:0 auto;width:200px;height:30px" width="200" height="30" class="CToWUd"></img>');

    html.push('<p style="margin:46px auto 0;max-width:420px;font-family:-apple-system,BlinkMacSystemFont,sans-serf;font-size:24px;font-weight:700;line-height:1.42;letter-spacing:-0.1px;color:#222">');
    html.push('임시 비밀번호입니다.</p>');
    html.push('<p style="margin:20px auto 0;max-width:420px;font-family:-apple-system,BlinkMacSystemFont,sans-serf;font-size:14px;line-height:1.71;color:#666;word-break:keep-all;letter-spacing:-0.1px">');
    html.push('로그인을 위한 임시 비밀번호입니다. 아래의 임시 비밀번호를  로그인시 화면에 입력하세요.</p>');
    html.push('<p style="margin:32px auto 0;padding-top:32px;max-width:420px;border-top:1px solid #e5e5e5;font-family:-apple-system,BlinkMacSystemFont,sans-serf;font-size:14px;font-weight:700;line-height:1;color:#222;text-align:center">');
    html.push('임시 비밀번호</p>');
    html.push('</td></tr>');

    html.push('<tr><td style="padding:16px 20px 50px" align="center">');
    html.push('<table style="table-layout:fixed;border-collapse:collapse;width:260px;height:66px;text-align:center">');
    html.push('<tbody><tr><td style="margin:0 auto;padding:0 16px;height:66px;border-radius:4px;background-color:#f6f8fe;font-family:-apple-system,BlinkMacSystemFont,sans-serf;font-size:28px;font-weight:700;letter-spacing:4px;color:#194bdc" align="center">');
    html.push(params.num);
    html.push(' </td></tr>');
    html.push('</tbody></table>');
    html.push('</td></tr></tbody>');
    html.push('</table></div></td></tr>');

    html.push('<tr><td style="padding:0;height:50px"></td></tr>');
    html.push('</tbody></table></div>');
    html.push(' </div></div>');
    html.push(' <div class="hi"></div></div>');

    return html.join('');
}
