const Iamport= require('iamport');
const iamport = new Iamport({
  impKey: CONFIG.iamport[SERVER_ENV].apiKey,
  impSecret: CONFIG.iamport[SERVER_ENV].apiSecret
});

const getByImpUid = (imp_uid) => {
    return new Promise((resolve, reject) => {
        try {
            let result={code:"7300",data:{}};
            iamport.payment.getByImpUid({
                imp_uid: imp_uid
            }).then(function (data) {
                if(data.code==0){
                    result.code="0000";
                    result.data=data.response;
                    resolve(result);
                }else{
                    resolve({code:"7200"});
                }             
            }).catch(function (error) {
                resolve(result);
            });
        } catch (e) {
            resolve(result);
        }   
    });
};
const getCertificationPhone = (imp_uid) => {
    return new Promise((resolve, reject) => {
        try {
            let result={code:"7300",data:{}};
            iamport.certification.get({
                imp_uid: imp_uid
            }).then(function (data) {
                result.code="0000";
                result.data=data;
                resolve(result);     
            }).catch(function (error) {
                console.log(error)
                resolve(result);
            });
        } catch (e) {
            resolve(result);
        }   
    });
};

module.exports = {
    getByImpUid,
    getCertificationPhone
};

//certification.get
//certification.delete


// // 아임포트 고유 아이디로 결제 정보를 조회
// iamport.payment.getByImpUid({
//     imp_uid: 'your imp_uid'  
//   }).then(function(result){
//     // To do
//   }).catch(function(error){
//     // handle error
//   });