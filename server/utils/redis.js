
//onst Redis = require('redis');
const Redis = require('ioredis');
const OPTIONS = CONFIG.redis[SERVER_ENV];
// const redisClient = REDIS.createClient({
//     host : "127.0.0.1",
//     port : 6379,
//     db : 0,
//     //password:"1q2w3e4r!@"
// });
function writeConnection() {
    console.log(OPTIONS)
    if (RedisClientWrite && RedisClientWrite.status){
        return RedisClientWrite;
    }else{
        if(RedisClientRead) RedisClientWrite.quit();
        RedisClientWrite = Redis.createClient(OPTIONS.write,{
          retry_strategy: function(options) {
            if (options.error && options.error.code === "ECONNREFUSED") {
              // End reconnecting on a specific error
              return new Error("The server refused the connection");
            }
            if (options.total_retry_time > 1000 * 60 * 60) {
              // End reconnecting after a specific timeout
              return new Error("Retry time exhausted");
            }
            if (options.attempt > 10) {
              // End reconnecting with built in error
              return undefined;
            }     
            // reconnect after
            return Math.min(options.attempt * 100, 3000);
          },
        });
        return RedisClientWrite;
    }    
}
function readConnection() {
    console.log(OPTIONS)
    if (RedisClientRead && RedisClientRead.status){
        return RedisClientRead;
    }else{
        if(RedisClientRead) RedisClientRead.quit();
        RedisClientRead = Redis.createClient(OPTIONS.read,{
        retry_strategy: function(options) {
            if (options.error && options.error.code === "ECONNREFUSED") {
                // End reconnecting on a specific error
                return new Error("The server refused the connection");
            }
            if (options.total_retry_time > 1000 * 60 * 60) {
                // End reconnecting after a specific timeout
                return new Error("Retry time exhausted");
            }
            if (options.attempt > 10) {
                // End reconnecting with built in error
                return undefined;
            }
        
            // reconnect after
            return Math.min(options.attempt * 100, 3000);
            },    
        });
        return RedisClientRead;        
    }
}

// function openRedisConnection() {
//     if (RedisClient && RedisClient.connected)
//         return RedisClient;
//         //console.log(RedisClient);
//     if (RedisClient)
//         RedisClient.quit(); // End and open once more    
//     RedisClient = new Ioredis.Cluster(OPTIONS ,
//         {
//             scaleReads: 'all',
//             slotsRefreshTimeout: 2000
//         }

//     );
//     return RedisClient;
// }

const set = (key ,value) => {
    return new Promise(async (resolve, reject) => {
        try {
            writeConnection().set(key, JSON.stringify(value), function(error, result) {
                //RedisClient.quit();
                if (error) reject(error);
                else resolve(result);
            });
        } catch (e) {
            reject(e);
        }
    });
};
const get = (key ,value) => {
    return new Promise(async (resolve, reject) => {
        try {
            readConnection().get(key, async(error, result) => {
                //RedisClient.quit();
                if (error) reject(error);
                else resolve(JSON.parse(result));
            });
        } catch (e) {
            reject(e);
        }
    });
};

module.exports = {
    set,
    get
};