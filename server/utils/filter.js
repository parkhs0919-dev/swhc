
module.exports = {
    leadingZeros :  (n, digits) =>  {
        let zero = '';
        n = n.toString();
    
        if (n.length < digits) {
            for (let i = 0; i < digits - n.length; i++)
                zero += '0';
        }
        return zero + n;
    },
    setFloatNum :  (obj) => {
        let num;
        let num_check = /^([0-9]*)[\.]?([0-9])?$/;
        if(obj){
            let reNum=Number(obj);
            if (!num_check.test(reNum)) {
                num=reNum.toFixed(1);
            }else{
                num=reNum;
            }
          }
        return num;
    }, 
    /**
     * 문자열이 빈 문자열인지 체크하여 결과값을 리턴한다.
     * @param str       : 체크할 문자열
     */
    isEmpty : (str) => {        
        if(typeof str == "undefined" || str == null || str == "")
            return true;
        else
            return false ;
    },   
    /**
     * 문자열이 빈 문자열인지 체크하여 기본 문자열로 리턴한다.
     * @param str           : 체크할 문자열
     * @param defaultStr    : 문자열이 비어있을경우 리턴할 기본 문자열
     */
    nvl : (str, defaultStr) => {        
        if(typeof str == "undefined" || str == null || str == ""){
            if(defaultStr) str = defaultStr ;
            else str = "" ;
        }  
        return str ;
    }            
}
        