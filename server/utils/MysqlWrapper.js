const MysqlConnect     = require('mysql')
  ,Q          = require('q')
  ,util       = require('util');   
const OPTIONS = CONFIG.database[SERVER_ENV].OPTIONS;
          
console.log('\n/*************** database options ******************/');
console.log(OPTIONS);       
console.log('/*************** database options ******************/\n'); 

let pool  = MysqlConnect.createPool(OPTIONS);
module.exports = {        
  getConnection : () => {
    let defer = Q.defer();
    pool.getConnection((err, conn) => {
      if(err){
        conn.release();  
        defer.reject(err);
      }else{  
        defer.resolve(conn);
      }
    });
    return defer.promise;
  },
  execQueryPromise : ( sql, params ) => {
    let defer = Q.defer();
    Mysql.getConnection().then(
      ( conn )=>{
        conn.query({sql:sql, timeout : 30000}, params, (err, rows)=>{
          conn.release();
          if(err){
            defer.reject(err);
          }else{
            defer.resolve(rows);
          } 
        });
      },
      ( err )=>{
        defer.reject(err);   
      }  
    );
    return defer.promise;
  },  
  beginTransaction : function(sqlList){
    let defer = Q.defer();
    Mysql.getConnection().then(
      function(conn){
        conn.beginTransaction({sql:'START TRANSACTION',timeout:3000},function(err){
          if(err){
            conn.release();
            defer.reject(err);
          }else{

            let promiseGroup = [];

            for(let i=0; i<sqlList.length; i++){
              let sql = sqlList[i];
              let promise = queryPromise(conn, sql);
              promiseGroup.push(promise);
            }

            Q.all(promiseGroup).then(
              function( result ){
                Mysql.endTransaction(conn, null, result)
                .then(
                  function( result ){
                    defer.resolve(result);
                  }
                  ,function( err1 ){
                    defer.reject(err1);
                  }
                )
              }
              ,function( err0 ){
                Mysql.endTransaction(conn, err0)
                .then(
                  function( result ){
                    defer.resolve(result);
                  }
                  ,function( err1 ){
                    defer.reject(err1);
                  }
                );
              }
            );
          }
        });
      },
      function(err){
        defer.reject(err);
      }
    );

    return defer.promise;
  },
  endTransaction : function(conn, tranErr, result){
    let defer = Q.defer();
    if(tranErr){
      conn.query({sql:'ROLLBACK', timeout:1000}, null, function(err){
        conn.release();
        if(err){
          defer.reject(err);
        }else{
          defer.reject(tranErr);
        }
      });
    }else{
      conn.query({sql:'COMMIT', timeout:1000}, null, function(err){
        conn.release();
        if(err){
          defer.reject(err);
        }else{
          defer.resolve(result);
        }
      });
    }
    return defer.promise;
  }
  ,beginTransaction2 : function(sqlList){
    let defer = Q.defer();
    Mysql.getConnection().then(
      function(conn){
        conn.beginTransaction({sql:'START TRANSACTION',timeout:3000},function(err){
          if(err){
            conn.release();
            defer.reject(err);
          }else{

            let promiseGroup = [];
            let DateCheck= /[0-9]{2}-[0-9]{2}-[0-9]{2}/;
            for(let i=0; i<sqlList.length; i++){
              let sql = sqlList[i];
              if(sql.indexOf('"null"')){
                sql = sql.replace(/\"null"/gi, "null");
              } 
              if(DateCheck.test(sql)){               
                sql = sql.replace(/\.000Z/gi, "");                
              }
              
              let promise = queryPromise(conn, sql);
              promiseGroup.push(promise);
            }

            Q.all(promiseGroup).then(
              function( result ){
                self.endTransaction(conn, null, result)
                .then(
                  function( result ){
                    defer.resolve(result);
                  }
                  ,function( err1 ){
                    defer.reject(err1);
                  }
                )
              }
              ,function( err0 ){
                self.endTransaction(conn, err0)
                .then(
                  function( result ){
                    defer.resolve(result);
                  }
                  ,function( err1 ){
                    defer.reject(err1);
                  }
                );
              }
            );
          }
        });
      },
      function(err){
        defer.reject(err);
      }
    );

    return defer.promise;
  }
}

function queryPromise(conn, sql){
  let defer = Q.defer();
  conn.query({sql:sql,timeout:30000}, null,function(err, result){
    if(err){
      defer.reject(err);
    }else{
      defer.resolve(result);
    }
  });
  return defer.promise;
}
