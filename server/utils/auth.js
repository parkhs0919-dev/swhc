const Jwt = require('jwt-simple');
//const redis = require('./redis');
const auth = CONFIG.common.auth;

module.exports = {
  //헤더에서 토큰값 파싱하여 req객체에 user정보 세팅
  authorizedByHeader:  (req, res, next) => {
    let access_token = req.headers['access_token'];
    let accept = req.headers['accept'];
    if (access_token) {
      try {
        let user = Jwt.decode(access_token, auth.secret);
        req.access_user = user;
        //res.locals.session = user;
        next();
      } catch (e) {
        let err = new Error();
        err.status = 401;
        res.status(err.status).send(err);
      }
    } else { 
      let err = new Error();
      err.status = 401;

      if (accept.indexOf("application/json") > -1) {
        res.status(err.status).send(err);
      } else {
        res.render('error', { status: err.status, message: err.message });
      }
    }

  }
  , apiAuthorizedUserV2: async (req, res, next) => {
    let access_token = (req.cookies['access_token'])?req.cookies['access_token']:req.body.token;
    if (access_token) {
      try {
        let user = Jwt.decode(access_token, auth.secret); 
        let uuid = (req.query.uuid)?req.query.uuid:req.body.uuid;
        let platform = (req.query.platform)?req.query.platform:req.body.platform;
        let checkData ={};
        checkData.uuid=uuid;
        checkData.platform=platform;
        checkData.user_id=user.user_id;
        console.log('--------------start-----------------');
        console.log(user);
        console.log(checkData);
        console.log('--------------end-----------------');
        req.access_user = user;   
        if(user.key==auth.key && uuid && platform){       
          const check = await Api.V2UserLoginInfo.getUserLoginInfoDeviceCheck(checkData);
          console.log(check)
          if(check.code=="0000"){
            res.cookie('access_token', access_token, { maxAge: auth.maxAge, httpOnly: true });
            next();
          }else{
            res.status(401).send(check);
          }
        }else{
          res.status(401).send({code:"9900"});
        }  
      } catch (e) {
        console.log(e);
        res.status(401).send({code:"9900"});
      }
    } else {
      res.status(401).send({code:"9900"});
    }
  } 
  // , apiAuthorizedUserV2: async (req, res, next) => {
  //   let access_token = (req.cookies['access_token'])?req.cookies['access_token']:req.body.token;

  //   if (access_token) {
  //     try {
  //       let user = Jwt.decode(access_token, auth.secret); 
  //       let uuid = (req.query.uuid)?req.query.uuid:req.body.uuid;
  //       let platform = (req.query.platform)?req.query.platform:req.body.platform;
  //       let checkData ={};
  //       checkData.uuid=uuid;
  //       checkData.platform=platform;
  //       checkData.user_id=user.user_id;
  //       console.log('--------------start-----------------');
  //       console.log(user);
  //       console.log(checkData);
  //       console.log('--------------end-----------------');
  //       req.access_user = user;   
  //       if(user.key==auth.key && uuid && platform){
  //         if(SERVER_ENV=='local'){  
  //           const check = await Api.V2UserLoginInfo.getUserLoginInfoDeviceCheck(checkData);
  //           if(check.code=="0000"){
  //             res.cookie('access_token', access_token, { maxAge: auth.maxAge, httpOnly: true });
  //             next();
  //           }else{
  //             res.status(401).send(check);
  //           }
  //         }else{
  //           const redisDataCheck =await Api.V2UserLoginInfo.getRedisLoginDevice(checkData);
  //           console.log(redisDataCheck);         
  //           //const check = await Api.V2UserLoginInfo.getUserLoginInfoDeviceCheck(checkData);
  //           if(redisDataCheck.code=="0000"){
  //             res.cookie('access_token', access_token, { maxAge: auth.maxAge, httpOnly: true });
  //             next();
  //           }else{
  //             res.status(401).send(redisDataCheck);
  //           }
  //         }
  //       }else{
  //         res.status(401).send({code:"9900"});
  //       }  
  //     } catch (e) {
  //       console.log(e);
  //       res.status(401).send({code:"9900"});
  //     }
  //   } else {
  //     res.status(401).send({code:"9900"});
  //   }
  // }    
  , apiAuthorizedUser: (req, res, next) => {
    let access_token = req.cookies['access_token'];
    let accept = req.headers['accept'];
    let timezone = req.headers['timezone'];
    //console.log(req.headers);
    //console.log(req.cookies);
    //console.log(access_token);
    if (access_token) {
      try {
        let user = Jwt.decode(access_token, auth.secret);
        req.access_user = user;
        if(user.key==auth.key){
          res.cookie('access_token', access_token, { maxAge: auth.maxAge, httpOnly: true });
          next();
        }else{
          let err = new Error();
          err.status = 401;
          err.message='인증 실패';
          console.log(err);
          res.status(err.status).send(err);
        }  
      } catch (e) {
        console.log(e);
        let err = new Error();
        err.status = 401;
        res.status(err.status).send(err);
      }
    } else {
      let err = new Error();
      err.status = 401;
      res.status(err.status).send(err);
    }
  }  
  //쿠키에서 토큰값 파싱하여 req객체에 user정보 세팅
  , adminAuthorizedByCookie: (req, res, next) => {
    let access_token = req.cookies['admin_access_token'];
    let lang = req.cookies['lang']==undefined?'ko':req.cookies['lang'];
    let accept = req.headers['accept'];
    if (access_token) {
      try {      
        let user = Jwt.decode(access_token, auth.secret);
        //console.log(user);
        //console.log('----accept----'+accept);
          req.access_admin = user; 
          res.locals.session = user; 
          if(user && user.organization_admin_id){
            res.cookie('admin_access_token', access_token, { maxAge: auth.admin_maxAge, httpOnly: true });
            next();      
          }else{
            let err = new Error();
            err.status = 401;
            if (accept && accept.indexOf("application/json") > -1) {
              res.status(err.status).send(err);
            } else {
              res.redirect(`/${lang}/login`);
            } 
          } 
      } catch (e) {     
        console.log(e);
        let err = new Error();
        err.status = 401;
        if (accept && accept.indexOf("application/json") > -1) {
          res.status(err.status).send(err);
        } else {
          res.redirect(`/${lang}/login`);
        } 
      }    
    } else {
      let err = new Error();
      err.status = 401; 
      if (accept && accept.indexOf("application/json") > -1) {
        res.status(err.status).send(err);
      } else {
          res.redirect(`/${lang}/login`);
      }
    }
  }
  , SysAuthorizedByCookie: (req, res, next) => {
    let access_token = req.cookies['sys_access_token'];
    let lang = req.cookies['lang']==undefined?'ko':req.cookies['lang'];
    let accept = req.headers['accept'];
    if (access_token) {
      try {      
        let user = Jwt.decode(access_token, auth.secret);
        //console.log(user);
        //console.log('----accept----'+accept);
          req.access_admin = user; 
          res.locals.session = user; 
          if(user){
            res.cookie('sys_access_token', access_token, { maxAge: auth.sys_maxAge, httpOnly: true });
            next();      
          }else{
            let err = new Error();
            err.status = 401;
            if (accept && accept.indexOf("application/json") > -1) {
              res.status(err.status).send(err);
            } else {
              res.redirect(`/sys/login`);
            } 
          } 
      } catch (e) {     
        console.log(e);
        let err = new Error();
        err.status = 401;
        if (accept && accept.indexOf("application/json") > -1) {
          res.status(err.status).send(err);
        } else {
          res.redirect(`/sys/login`);
        } 
      }    
    } else {
      let err = new Error();
      err.status = 401; 
      if (accept && accept.indexOf("application/json") > -1) {
        res.status(err.status).send(err);
      } else {
          res.redirect(`/sys/login`);
      }
    }

  }

  , patchAuthorizedByCookie: (req, res, next) => {
    let access_token = req.cookies['patch_access_token'];
    let lang = req.cookies['lang']==undefined?'ko':req.cookies['lang'];
    let accept = req.headers['accept'];
    if (access_token) {
      try {      
        let user = Jwt.decode(access_token, auth.secret);
        //console.log(user);
        //console.log('----accept----'+accept);
          req.access_admin = user; 
          res.locals.session = user; 
          if(user){
            res.cookie('patch_access_token', access_token, { maxAge: auth.sys_maxAge, httpOnly: true });
            next();      
          }else{
            let err = new Error();
            err.status = 401;
            if (accept && accept.indexOf("application/json") > -1) {
              res.status(err.status).send(err);
            } else {
              res.redirect(`/patch/login`);
            } 
          } 
      } catch (e) {     
        console.log(e);
        let err = new Error();
        err.status = 401;
        if (accept && accept.indexOf("application/json") > -1) {
          res.status(err.status).send(err);
        } else {
          res.redirect(`/patch/login`);
        } 
      }    
    } else {
      let err = new Error();
      err.status = 401; 
      if (accept && accept.indexOf("application/json") > -1) {
        res.status(err.status).send(err);
      } else {
          res.redirect(`/patch/login`);
      }
    }

  }

  /**
   * setSessionValueFromCookie - 쿠키로부터 세션정보 세팅 및 쿠키갱신
   *
   * @param  {object} req  request
   * @param  {object} res  response
   * @param  {function} next next
   */
  , setSessionValueFromCookie:  (req, res, next) => {

    console.log(req.cookies);
    console.log(req.headers);

    if (req.cookies && req.cookies['access_token']) {
      let access_token = req.cookies['access_token'];
      let sessionValues = Jwt.decode(access_token, auth.secret);
      console.log(sessionValues);
      req.session = sessionValues;
      // res.locals.session = sessionValues;
      res.cookie('access_token', access_token, { maxAge: ((24 * 60 * 60 * 1000) * 30), httpOnly: true });
    } else {
      res.locals.session = null;
    }

    console.log('\n****************** SESSION *******************');
    console.log(req.session);
    console.log('****************** SESSION *******************\n');

    next();
  }

  ,encode:  (payload) => {
    return Jwt.encode(payload, SECRET);
  },
  decode:  (token) => {
    return Jwt.decode(token, SECRET);
  },
  encodeWithSecret:  (payload, secret) => {
    return Jwt.encode(payload, secret);
  },
  decodeWithSecret:  (token, secret) => {
    try {
      return Jwt.decode(token, secret);
    } catch (e) {
      return null;
    }
  },

};
