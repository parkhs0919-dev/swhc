
const momentTimeZone = require('moment-timezone');

module.exports = {
    getDate :  ()  => {
        const filter=Utils.filter;
        let date = new Date();  
        let year  = date.getFullYear();
        let month = filter.leadingZeros((date.getMonth() + 1),2);
        let day   = filter.leadingZeros(date.getDate(),2);
        let hh = filter.leadingZeros(date.getHours(), 2);
        let mi = filter.leadingZeros(date.getMinutes(), 2);
        let ss = filter.leadingZeros(date.getSeconds(), 2);
        let week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        let dayOfWeek = week[date.getDay()];
            
        return {     
            year:year,
            month:month,
            day:day,
            hh:hh,
            mi:mi,
            ss:ss,
            dayOfWeek:dayOfWeek,
            yyyymmddhhmiss:year+month+day+hh+mi+ss,      
        };
    }, 
    getTimeZoneValue :  (time_zone) =>  {
        let timeZone=(time_zone)? time_zone : "Asia/Seoul";
        return momentTimeZone().tz(timeZone).format('z');
    },                   
}  