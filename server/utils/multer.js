const multer = require('multer');
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');
const config_s3 = CONFIG.aws[SERVER_ENV].s3;
aws.config.loadFromPath(__dirname + '/../config/s3.json');
const s3 = new aws.S3();

const admin = multerS3({
    s3: s3,
    bucket: config_s3.bucket_name+"/admin",
    acl: 'public-read-write',
    key: function (req, file, cb) {
        let filename = req.params.imageName;
        let ext = file.mimetype.split('/')[1]; 
        if(!['png', 'jpg', 'jpeg', 'gif', 'bmp'].includes(ext)) {
            return cb(new Error('Only images are allowed'));
        }
        cb(null, Date.now()+'_'+randomString(5)+ '.' + file.originalname.split('.').pop()); // 이름 설정
    }
});
const user = multerS3({
    s3: s3,
    bucket: config_s3.bucket_name+"/user",
    acl: 'public-read-write',
    key: function (req, file, cb) {
        let filename = req.params.imageName;
        let ext = file.mimetype.split('/')[1]; 
        if(!['png', 'jpg', 'jpeg'].includes(ext)) {
            return cb(new Error('Only images are allowed'));
        }
        cb(null, Date.now()+'_'+randomString(5)+ '.' + 'png'); // 이름 설정
    }
});

const upload = multer({
    //errorHandling: 'manual',
    storage: multerS3({
        s3: s3,
        bucket: config_s3.bucket_name,
        acl: 'public-read-write',
        key: function (req, file, cb) {
            let filename = req.params.imageName;
            let ext = file.mimetype.split('/')[1]; 
            if(!['png', 'jpg', 'jpeg', 'gif', 'bmp'].includes(ext)) {
                return cb(new Error('Only images are allowed'));
            }
            cb(null, Date.now() + '.' + file.originalname.split('.').pop()); // 이름 설정
        }
    }),
    limits: { files: 1, fileSize: 5 * 1024 * 1024, }

});

// s3.deleteObject({
//     Bucket: MY_BUCKET,
//     Key: myKey
//   },function (err,data){})
const del = (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            s3.deleteObject(params, function(err, data) {
                if (err) {
                  console.log('aws  delete error')
                  resolve({code:"8600"})
                } else {
                  console.log('aws  delete success' + data)
                  resolve({code:"0000"})
                }
            })          
        } catch (e) {
            reject(e);
        }
    });
};

module.exports = {
    del,
    upload,
    user,
    admin
};

function randomString(num) {
    let chars = "abcdefghiklmnopqrstuvwxyz0123456789";
    let string_length = num;
    let randomstring = '';
    for (let i = 0; i < string_length; i++) {
        let rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
  }
  





