const admin = require('firebase-admin');
const request = require('request');
//const serviceAccount = require("../config/mytest-83d5a-firebase-adminsdk-6h48z-654d32c3c9");

// admin.initializeApp({
//     credential: admin.credential.cert(serviceAccount),
//     databaseURL: "https://smartfarm-ffe34.firebaseio.com"
// });

// const sendToToken = (token, title, body, data, channel, platform = 'android') => {
//     return new Promise(async (resolve, reject) => {
//         try {
//             let message = {
//                 token,
//             };
//             if (platform === 'android') {
//                 message.android = {
//                     priority: 'high',
//                     notification: {
//                         title,
//                         body,
//                     },
//                     data: data
//                 };
//                 if (channel) {
//                     //message.android.notification.sound = STD.fcm.sound.android[channel];
//                     message.android.notification.channel_id = channel;
//                     message.android.data.channel_id = channel;
//                 }
//             } else {
//                 message.notification = {
//                     title,
//                     body,
//                 };
//                 message.data = data;
//                 message.apns = {
//                     headers: {
//                         'apns-priority': '10'
//                     },
//                 };
//                 if (channel) {
//                     message.apns.payload = {
//                         aps: {
//                            // sound: STD.fcm.sound.ios[channel]
//                         }
//                     };
//                 }
//             }
//             const response = await admin.messaging().send(message);
//             resolve(true);
//         } catch (e) {
//             reject(e);
//         }
//     });
// };

const pushMessageToken = (token, type ,title, body) => {
    return new Promise((resolve, reject) => {
        try {
            let form = {
                notification :{
                    type : type,
                    title : title,
                    body : body,
                    sound: 'default'
                 },
                data : {
                    type : type,
                    title : title,
                    body : body,
                },
                apns: {
                    headers: {
                        'apns-priority': '10',
                    },
                    payload: {
                        aps: {
                            sound: 'default',
                        }
                    },
                },
                android: {
                  priority: 'high',
                  notification: {
                      sound: 'default',
                      click_action:'SplashActivity'
                  }
                },                
                to : token
            };
            request({
                uri: `https://fcm.googleapis.com/fcm/send`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization' : 'key='+CONFIG.common.fcm.server_key,
                },
                body:form,
                json:true
            }, (error, response, body) => {
                console.log(body);  
                if (error) { 
                    //console.log(body)  
                    console.log(error)
                    reject(error);
                } else {
                    try {
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                }
            });
        } catch (e) {
            reject(e);
        }   
    });
};

const pushMessageDefaultToken = (token, params) => {
    return new Promise((resolve, reject) => {
        try {
            let form = {
                notification :params,
                data : params,
                apns: {
                    headers: {
                        'apns-priority': '10',
                    },
                    payload: {
                        aps: {
                            sound: 'default',
                        }
                    },
                },
                android: {
                  priority: 'high',
                  notification: {
                      sound: 'default',
                      click_action:'SplashActivity'
                  }
                },                
                to : token
            };
            request({
                uri: `https://fcm.googleapis.com/fcm/send`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization' : 'key='+CONFIG.common.fcm.server_key,
                },
                body:form,
                json:true
            }, (error, response, body) => {
                console.log(body);  
                if (error) { 
                    //console.log(body)  
                    console.log(error)
                    reject(error);
                } else {
                    try {
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                }
            });
        } catch (e) {
            reject(e);
        }   
    });
};

const pushMessageTopic = (topic, type ,title, body) => {
    return new Promise((resolve, reject) => {
        try {
            let form = {
                notification :{
                    type : type,
                    title : title,
                     body : body,
                     sound: 'default',
                 },
                data : {
                    type : type,
                    title : title,
                    body : body,
                },
                apns: {
                    headers: {
                        'apns-priority': '10',
                    },
                    payload: {
                        aps: {
                            sound: 'default',
                        }
                    },
                },
                android: {
                  priority: 'high',
                  notification: {
                      sound: 'default',
                  }
                },
                to : '/topics/'+topic
            };
            
            request({
                uri: `https://fcm.googleapis.com/fcm/send`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization' : 'key='+CONFIG.common.fcm.server_key,
                },
                body:form,
                json:true
            }, (error, response, body) => {
                console.log(body);  
                if (error) {
                    //console.log(body)  
                    console.log(error) 
                    reject(error);
                } else {
                    try {
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                }
            });
        } catch (e) {
            reject(e);
        }   
    });
};

module.exports = {
    pushMessageToken,
    pushMessageTopic,
    pushMessageDefaultToken
};
