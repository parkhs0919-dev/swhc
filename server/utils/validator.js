const intExp = new RegExp('^[-]?[0-9]*$');
const floatExp = new RegExp('^[-]?[0-9]*[\.]?[0-9]+$');
const emailExp = new RegExp('^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$');

module.exports = {  
    isInt: (req,key) => {
        console.log(key)
        const param = getParams(req, key);
        if (param && !intExp.test(param)) {
            response(400, `:${key}:901`);
        }
    },    
    isFloat: (req,key) => {
        const param = getParams(req, key);
        if (param && !floatExp.test(param)) {
            response(400, `:${key}:902`);
        }
    },
    isEmail: (req,key) => {
        const param = getParams(req, key);
        if (param && !emailExp.test(param)) {
            response(400, `:${key}:903`);
        }
    },
    isBoolean: (req,key) => {
        const param = getParams(req, key);
        if (param && param !== 'true' && param !== 'false' && param !== true && param !== false && param !== '1' && param !== 1 && param !== '0' && param !== 0) {
            response(400, `:${key}:904`);
        }
    },
    isDate: (req,key) => {
        const param = getParams(req, key);
        if (param) {
            const date = new Date(param);
            if (isNaN(date.getTime())) {
                response(400, `:${key}:905`);
            }
        }
    },
    len: (req,key, min = null, max = null) => {
        const param = getParams(req, key);
        if (typeof min === 'number' && typeof max === 'number') {
            if (param && (param.length < min || param.length > max)) {
                response(400, `:${key}:906`);
            }
        } else {
             response(400, `:${key}:906`);
        }
    },
    isRegExp: (req,key, RegExp) => {
        const param = getParams(req, key);
        if (param && !RegExp.test(param)) {
            response(400, `:${key}:907`);
        }
    },
    isEnum: (req,key, enumList) => {
        const param = getParams(req, key);
        if (param && enumList.indexOf(param) === -1) {
            response(400, `:${key}:908`);
        }
    }  

   
};  
const response = (status, data) => {
    throw new Error(status);   
};           
function getParams(req, key) {
    if (req.params && req.params[key] !== undefined && req.params[key] !== undefined) {
        return req.params[key] + '';
    } else if (req.query && req.query[key] !== undefined && req.query[key] !== null) {
        return req.query[key] + '';
    } else if (req.request && req.request.body && req.request.body[key] !== undefined && req.request.body[key] !== null) {
        return req.request.body[key] + '';
    } else {
        return null;
    }
}
