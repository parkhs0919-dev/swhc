const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');

router.get('/', (req, res, next) => {  
    res.render('patch-render',{langs:res.locals.langs,session:res.locals.session});
    
}); 

router.get('/*', (req, res, next) => {   
      res.render('patch-render',{langs:res.locals.langs,session:res.locals.session});
}); 


module.exports = router;   
         