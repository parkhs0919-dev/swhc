const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');


router.get('/', function(req, res, next) {  
  let lang = req.cookies['lang'];
  if(lang && lang=="en"){
    res.cookie('lang', 'en');
    res.locals.langs = 'en';
  }else{
    res.cookie('lang', 'ko');
    res.locals.langs = 'ko';      
  }    
  res.redirect('./ko');
});

router.get('/*', function(req, res, next) {
  let lang = req.cookies['lang'];
    if(lang && lang=="en"){
      res.cookie('lang', 'en');
      res.locals.langs = 'en';
    }else{
      res.cookie('lang', 'ko');
      res.locals.langs = 'ko';      
    }    
      res.render('admin-render',{langs:res.locals.langs,session:res.locals.session,mode:process.env.NODE_ENV});
});
                 

module.exports = router;   
         