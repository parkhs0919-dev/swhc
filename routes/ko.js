const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
let locales_ko={
  codes: Metadata.ko.codes,
  langs: Metadata.ko.langs,   
}
router.get('/', (req, res, next) => {
  res.cookie('lang', 'ko');
  res.locals.metadata = locales_ko;
  res.render('index', {
    view_path: 'main',
    result: {}
  });
});
router.get('/out', function (req, res, next) {
  res.clearCookie('admin_access_token');
  res.status(200).send({code:'0000'}); 
}); 
router.get('/error', function(req, res, next) {
  res.render('error', {
    view_path: 'error',
    result: {}
  });
});     

router.get('/user-list', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/user/user-list',
    result: {}
  });    
});  
router.get('/user-confirmed-list', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/user/user-confirmed-list',
    result: {}
  });    
});  
router.get('/user-info-list', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/user/user-info-list',
    result: {}
  });    
});  
router.get('/time-setting', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/message/time-setting',
    result: {}
  });         
});  
router.get('/message-list', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/message/message-list',
    result: {}
  });         
});  
router.get('/group-list', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/group/group-list',
    result: {}
  });         
});  
router.get('/notice-list', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/message/notice-list',
    result: {}
  });         
});  
router.get('/organization-list', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/organization/organization-list',
    result: {}
  });         
});  
router.get('/visit-time-list', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/visit-time/visit-time-list',
    result: {}
  });         
});  
router.get('/wear-time-list', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/wear-time/wear-time-list',
    result: {}
  });         
});  
router.get('/user-event-list', (req, res, next) => {
  res.locals.metadata = locales_ko;
  res.render('index', {  
    view_path: './pages/user-event/user-event-list',
    result: {}
  });         
});  

module.exports = router;   
         