
const checkUserDuplicate = (params) => {
    return new Promise(async(resolve, reject) => {
        try {           
            const check = await Api.User.getUserDuplicateCheck(params);
            resolve(check);        
        } catch (e) {
            //reject(e);
            resolve({code:"9100"});
        }   
    });
};
const checkUserDuplicateEmail = (params) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const check = await Api.User.getUserDuplicateCheckEmail(params);   
            resolve(check);  
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    checkUserDuplicate,
    checkUserDuplicateEmail
};

