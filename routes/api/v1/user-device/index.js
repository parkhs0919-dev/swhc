// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const put = require('./put');

router.post('/check', [
  check('device_list').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.getUserDeviceSnListCheck(req);  
    req.json(data);
  }   
}))

router.put('/init', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserDeviceSnInit(req);  
    req.json(data);
  }
}))


module.exports = router;

