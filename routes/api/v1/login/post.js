// @ts-nocheck
const Jwt = require('jwt-simple');
const auth = CONFIG.common.auth;

const checkUser = (params) => {
    return new Promise(async(resolve, reject) => {
        try {           
            const user = await Api.User.getUserLoginCheck(params);
            params.user_id = user.user_id;
            const check = await Api.UserLoginInfo.getUserLoginInfoCheck(params);
            if (user.code == '0000' && check.code == '0000') {
                delete user.user_id;
                await Api.UserDevice.updateUserDevice(params, params.user_id);
                await Api.User.updateUserLoginTime(params, params.user_id)
                await Api.UserLoginInfo.updateUserLoginInfoCheck(params);
                resolve(user);
            } else if (user.code == '0000' && check.code == '1500') {
                if (params.login_yn && params.login_yn == "Y") {
                    const update = await Api.UserLoginInfo.updateUserLoginInfoCheck(params);
                    if (update.code == '0000') {
                        await Api.UserDevice.updateUserDevice(params, params.user_id);
                        await Api.User.updateUserLoginTime(params, params.user_id);
                        resolve(user);
                    } else {
                        resolve({code: "9100"});
                    }
                } else {
                    resolve({code: "1500"});
                }
            } else {
                resolve(user);
            }
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const checkUserToken = (req) => {
    return new Promise(async(resolve, reject) => {
        try {      
            let params =req.body;  
            let access_token = getUserToken(req);
            if(access_token){
                params.user_id=access_token.user_id;   
                const user = await Api.User.getUserLoginCheckToken(params);
                params.user_id = user.user_id;
                const check = await Api.UserLoginInfo.getUserLoginInfoCheck(params);
                if (user.code == '0000' && check.code == '0000') {
                    delete user.user_id;
                    await Api.UserDevice.updateUserDevice(params, params.user_id);
                    await Api.User.updateUserLoginTime(params, params.user_id)
                    await Api.UserLoginInfo.updateUserLoginInfoCheck(params);
                    resolve(user);
                } else if (user.code == '0000' && check.code == '1500') {
                    if (params.login_yn && params.login_yn == "Y") {
                        const update = await Api.UserLoginInfo.updateUserLoginInfoCheck(params);
                        if (update.code == '0000') {
                            await Api.UserDevice.updateUserDevice(params, params.user_id);
                            await Api.User.updateUserLoginTime(params, params.user_id);
                            resolve(user);
                        } else {
                            resolve({code: "9100"});
                        }
                    } else {
                        resolve({code: "1500"});
                    }
                } else {
                    resolve(user);
                }
            }else{
                resolve({code: "9200"});
            }

        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    checkUser,
    checkUserToken
};

function getUserToken(req) {
    let result;
    let access_token = req.cookies['access_token'];
    console.log(req.cookies)
    if (access_token) {
        let user = Jwt.decode(access_token, auth.secret);
        console.log(user)
        if (user.key == auth.key) {
            result = user;
        } else {
            result = null;
        }
    } else {
        result = null;
    }
    return result;
}
  
