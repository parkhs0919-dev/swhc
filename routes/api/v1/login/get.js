
const checkUserDuplicate = (params) => {
    return new Promise(async(resolve, reject) => {
        try {           
            const check = await Api.User.getUserDuplicateCheck(params);
            resolve(check);        
        } catch (e) {
            //reject(e);
            resolve({code:"9100"});
        }   
    });
};
const checkUserDuplicateEmail = (params) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const check = await Api.User.getUserDuplicateCheckEmail(params);   
            resolve(check);  
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};
const findIdUser = (params) => {
    return new Promise(async(resolve, reject) => {
        try {           
            const check = await Api.User.getUserFindId(params);
            resolve(check);        
        } catch (e) {
            //reject(e);
            resolve({code:"9100"});
        }   
    });
};
const findPwUser = (params) => {
    return new Promise(async(resolve, reject) => {
        try {           
            const check = await Api.User.getUserFindPw(params);
            resolve(check);        
        } catch (e) {
            //reject(e);
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    checkUserDuplicate,
    checkUserDuplicateEmail,
    findIdUser,
    findPwUser
};

// router.get('/find/id', [
//     check('user_nm').not().isEmpty(),
//     check('user_tel').not().isEmpty(),
// ], asyncHandler(async (req, res, next) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     res.status(400).json({ code: '9000' });
//   } else {
//     const data = await Api.User.getUserFindId(req.query);    
//     res.status(200).send(data);
//   }
// }))

// router.get('/find/pw', [
//     check('user_nm').not().isEmpty(),
//     check('user_auth_id').not().isEmpty(),
//     check('user_tel').not().isEmpty(),
// ], asyncHandler(async (req, res, next) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     res.status(400).json({ code: '9000' });
//   } else {
//     const data = await Api.User.getUserFindPw(req.query);    
//     res.status(200).send(data);
//   }
// }))  
