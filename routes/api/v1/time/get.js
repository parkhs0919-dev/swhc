

const getUserEventList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {  
            let result ={code:"0000" ,type:"E0"};    
            let arrData =[];    
            req.query.user_id = req.access_user.user_id;
            const today = await Api.UserEvent.getUserEventNumToday(req.query);
            const tomorrow1= await Api.UserEvent.getUserEventNum(req.query,1);
            const tomorrow2 = await Api.UserEvent.getUserEventNum(req.query,2);
            const tomorrow3 = await Api.UserEvent.getUserEventNum(req.query,3);
      
            if (today.code == "0000") arrData = arrData.concat(...today.rows);
            if (tomorrow1.code == "0000") arrData = arrData.concat(...tomorrow1.rows);
            if (tomorrow2.code == "0000") arrData = arrData.concat(...tomorrow2.rows);
            if (tomorrow3.code == "0000") arrData = arrData.concat(...tomorrow3.rows);
            //arrData = arrData.concat(...today.rows,tomorrow1.rows,tomorrow2.rows,tomorrow3.rows);
            if(arrData.length>4) result.rows = arrData.slice(0, 4);
            else result.rows = arrData;
            resolve(result);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getScheduleList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {     
            req.query.user_id = req.access_user.user_id;
            const health = await Api.Health.getHealthCheck(req.query);
            if(health.code=="0000"){
                req.query.grade_cd=health.rows.grade_cd;
                const data = await Api.Schedule.getScheduleList(req.query);
                resolve(data);
            }else{
                resolve(health);
            }
            //const today = await Api.UserEvent.getUserEventNumToday(req.query);

            
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    getUserEventList,
    getScheduleList
};
