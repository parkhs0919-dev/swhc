
const updateUserLoginInfoDuplicate = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {           
            req.body.user_id = req.access_user.user_id
            req.body.login_yn = "N";
            const data = await Api.User.updateUserLoginInfoDuplicate(req.body);
            resolve(data);
        } catch (e) {
            resolve(result);
        }   
    });
};
const createUserGroup = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {          
            req.body.user_id = req.access_user.user_id; 
            const data = await Api.UserGroupRels.getUserGroupRelsCheck(req.body);          
            const data2 = await Api.UserGroupRels.getUserGroupRelsOrganizationCheck(req.body);          
            if (data.code == "0000" && data2.code == "0000") {
                const health_data = await Api.User.getUserHealth(req.body);
                let health = health_data.rows[0]; 
                if (data.rows && data.rows.length == 1) {
                    let row = data.rows[0];
                    if(!isGradeCheck(row,row.type,health.grade_cd)){
                        resolve({code:"3100",grade:row.grade});
                    }else{
                        if(row.apply_cd =="A1"){
                            resolve({code:"0000"});
                        }else{
                            req.body.apply_cd ="A1";
                            req.body.expire_dt=(row.type=="LT")? "180" : "30";
                            const update = await Api.UserGroupRels.updateUserGroupRels(req.body,row.user_group_rels_id);
                            resolve(update);                        
                        }
                    }
                }else if(data.rows && data.rows.length == 0){                 
                    const group = await Api.Group.getGroup(req.body);
                    if(group.code="0000" && group.rows && group.rows.length == 1){
                        let row = group.rows[0];
                        if(!isGradeCheck(row,row.type,health.grade_cd)){
                            resolve({code:"3100",grade:row.grade});
                        }else{
                            req.body.apply_cd ="A1";
                            req.body.expire_dt=(row.type=="LT")? "180" : "30";
                            const create = await Api.UserGroupRels.createUserGroupRels(req.body);
                            resolve(create);
                        }
                    }else{
                        resolve(group);
                    }
                }else {
                    resolve(result);
                }
            } else {
                if(data2!="0000") resolve({code:"3100"}); 
                else resolve(result);           
            }
        } catch (e) {
            console.log(e)
            resolve(result);
        }   
    });
};
module.exports={
    updateUserLoginInfoDuplicate,
    createUserGroup
};

function isGradeCheck(user,type,val){
    let result = false;
    let data = (user)? user : "";
    if(type=="ST"){
        if(data.grade=="G1" && val=="G1") result = true;
        else if(data.grade=="G2" && (val=="G1" || val=="G2" )) result = true;
        else if(data.grade=="G3" && (val=="G1" || val=="G2" || val=="G3" )) result = true;
    }else if(type=="LT"){
        result = true;
    }
    console.log(data.grade,type,val);
    console.log('result==',result);
    return result;
}

