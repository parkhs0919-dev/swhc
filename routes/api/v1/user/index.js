// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const get = require('./get');
const put = require('./put');

router.get('/info', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUser(req);    
    req.json(data);
  }
}))

router.get('/group-search', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserGroupSearch(req);    
    req.json(data);
  }
}))
router.get('/group-list', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserGroupRelsList(req);    
    req.json(data);
  }
}))

router.put('/first-pw', [
  check('user_auth_pw').not().isEmpty().isLength({ min: 6 }),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserFirstPassword(req);
    req.json(data);
  }
}))
router.put('/password', [
  check('old_user_auth_pw').not().isEmpty(),
  check('new_user_auth_pw').not().isEmpty().isLength({ min: 6 }),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserPassword(req);
    req.json(data);
  }
}))
router.put('/info', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUser(req);
    req.json(data);
  }
}))
router.put('/group-remove/:id', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateGroupRemove(req);
    req.json(data);
  }
}))

router.post('/logout', [
  check('uuid').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.updateUserLoginInfoDuplicate(req);
    if(data.code=="0000") res.clearCookie('access_token');
    req.json(data);
  }
}))
router.post('/group-add', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.createUserGroup(req);
    req.json(data);
  }
}))
module.exports = router;

