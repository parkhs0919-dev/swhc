// @ts-nocheck
const Jwt = require('jwt-simple');
const moment = require('moment');
const auth = CONFIG.common.auth;
const temperatureCodeArray = ( Metadata.ko.codes)? Metadata.ko.codes.temperature : [];
const setting = CONFIG.setting;

const createHealth = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
          console.log('----api close----')
            let user = Jwt.decode(req.body.token, auth.secret);
            req.body.user_id = user.user_id;
            //req.body.user_device_sn =req.body.device_num;
            req.body.device_num =req.body.device_sn;
            //req.body.temperature_cd = checkTempCd(temperatureCodeArray ,req.body.temperature);
            // req.body.temperature_cd = getHealthTempCd(req.body.temperature);
            // if (user.user_id) {
            //   req.body.currentMonth = moment().add(0, 'M').format("YYYYMM");
            //   const data = await Api.Health.updateHealth(req.body, user.user_id);
            //   const data2 = await Api.HealthLog.createHealthLog(req.body);
            //   await Api.UserDevice.updateUserDevice(req.body, user.user_id);
            //   if (data.code == "0000" && data2.code == "0000") {

            //     if(isSendSms(req.body.temperature)){
            //       console.log('==고온 발송==')
            //       await Api.AdSend.sendSmsAdminTemp(req.body);
            //     }      
            //     resolve({ code: "0000" })
            //   } else {
            //     resolve({ code: "8100" })
            //   }
            // } else {
            //   resolve({ code: "9100" })
            // }
            resolve({ code: "0000"})
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    createHealth
};

function checkTempCd(array ,val){
  let result="T0" ;
  array.forEach(function(element){
      let data =element;
      if(Number(data.min)<=Number(val) && Number(data.max)>=Number(val)) result=data.temperature_cd;
      else if(Number(data.min)<=Number(val) && Number(data.min)>=Number(val)) result=data.temperature_cd;
      else if(Number(data.min)<=Number(val) && Number(data.min)>=Number(val)) result=data.temperature_cd;
      else if(Number(data.min)<=Number(val) && Number(data.min)>=Number(val)) result=data.temperature_cd;
      else if( Number(data.min)<=Number(val) && Number(data.min)>=Number(val)) result=data.temperature_cd;

   });  
  return result; 
}
  
function getHealthTempCd(val){
  let result= "T0";
  setting.temperature.forEach(function(element){
      let data =element;
      if(parseFloat(data.min)<=parseFloat(val) && parseFloat(data.max)>=parseFloat(val) )  result=data.temperature_cd;
   }); 
   console.log('health temp = ',val ,' result  = ',result)
  return result; 
}

function isSendSms(val){
  let result = false ;
  let temp = (val)? parseFloat(val).toFixed(2) : 0;

  if(temp>=parseFloat(34.0)&&temp<parseFloat(37.0)){
    result = false ;
  }else if(temp>=parseFloat(37.0)&&temp<parseFloat(37.3)){
    result = false ;
  }else if(temp>=parseFloat(37.3)&&temp<parseFloat(37.5)){
    result = false ;    
  }else if(temp>=parseFloat(37.5)&&temp<parseFloat(100)){
    result = true ;
  }
  return result; 
}
