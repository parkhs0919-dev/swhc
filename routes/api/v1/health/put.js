// @ts-nocheck
const Jwt = require('jwt-simple');
const moment = require('moment');
const auth = CONFIG.common.auth;

const updateHealthStatus = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            let user = Jwt.decode(req.body.token, auth.secret);
            req.body.user_id = user.user_id;
            //const data = await Api.Health.updateHealthStatus(req.body,req.body.user_id);
            //const status = await Api.Health.getHealthGradeStatus(req.body);      
            // if(status.code="0000"){
            //   let sub={}; 
            //   let grade_cd=status.rows.grade_cd;
            //   let ch_grade_cd= await getHealthGradeCode(status.rows);
            //   sub.grade_cd=ch_grade_cd;
            //   const health = await Api.Health.updateHealthGrade(sub,req.body.user_id);

            //   if(grade_cd!=ch_grade_cd){
            //     console.log(' ===grade_change=== ');
            //     req.body.grade=ch_grade_cd;
            //     await Api.UserNotification.sendUserNotification(req.body);
            //   } 
              
            //   resolve(health);
            // }else{
            //   resolve(data);
            // }
            resolve({code:"0000"});
            
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    updateHealthStatus
};

async function getHealthGradeCode(data){
  let result ="G0";
  if(data.status_cd=="S2")  result ="G4";
  else if(data.check_status=="C2" || data.self_quarantine_yn=="Y")  result ="G5";
  else if(data.status_cd=="S1" && data.vaccine_status=="C1" )  result ="G1";
  else if(data.status_cd=="S1" && data.check_status=="C1" )  result ="G2";
  else if(data.vaccine_status=="C0" && data.check_status=="C0" && data.status_cd=="S1")  result ="G3";
  else if(data.vaccine_status=="C0" && data.check_status=="C0" && data.status_cd=="S0")  result ="G0";
  //console.log(result)
  return result;
}