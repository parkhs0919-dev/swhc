
const createUserCheck = (req) => {
    return new Promise(async(resolve, reject) => {
        try {       
            if(req.file.size && req.file.key){
                req.body.user_id = req.access_user.user_id;
                req.body.file_url =req.file.location;
                req.body.file_name =req.file.key;
                req.body.bucket_name = req.file.bucket;
                const userCheck = await Api.UserCheck.createUserCheck(req.body);
                if(userCheck.code=="0000"){
                    req.body.user_check_id=userCheck.user_check_id;
                    const data = await Api.UserCheckFile.createUserCheckFile(req.body);         
                    resolve(data);
                }else{
                    resolve(userCheck);
                }
            }        
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};

const createUserVaccine = (req) => {
    return new Promise(async(resolve, reject) => {
        try {       
            if(req.file.size && req.file.key){
                req.body.user_id = req.access_user.user_id;
                req.body.file_url =req.file.location;
                req.body.file_name =req.file.key;
                req.body.bucket_name = req.file.bucket;
                const userVaccine = await Api.UserVaccine.createUserVaccine(req.body);
                if(userVaccine.code=="0000"){
                    req.body.user_vaccine_id=userVaccine.user_vaccine_id;
                    const data = await Api.UserVaccineFile.createUserVaccineFile(req.body);         
                    resolve(data);
                }else{
                    resolve(userVaccine);
                }
            }        
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    createUserCheck,
    createUserVaccine
};

