
const getUserPaper = (req) => {
    return new Promise(async(resolve, reject) => {
        try {       
            let arrayData = [];
            req.query.user_id = req.access_user.user_id
            // const userCheck = await Api.UserCheck.getUserCheck(req.query);
            // console.log(userCheck)
            // const userVaccine = await Api.UserVaccine.getUserVaccine(req.query);
            // console.log(userVaccine)
            const data = await Api.UserCheck.getUserCheckUnionAll(req.query);       
            resolve(data)
           
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    getUserPaper
};