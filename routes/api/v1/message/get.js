
const getMessageGroup = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.user_id = req.access_user.user_id; 
            const data = await Api.UserGroupRels.getUserGroupRelsMessage(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getMessageReceive = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.user_id = req.access_user.user_id; 
            const data = await Api.Message.getMessageReceive(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getMessageSend = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.user_id = req.access_user.user_id; 
            const data = await Api.Message.getMessageSend(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    getMessageGroup,
    getMessageReceive,
    getMessageSend

};
