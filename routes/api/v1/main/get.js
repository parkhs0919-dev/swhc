
const getHealthGradeStatus = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.user_id = req.access_user.user_id; 
            const data = await Api.Health.getHealthGradeStatusMain(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    getHealthGradeStatus
};
