// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const get = require('./get');
const auth = CONFIG.common.auth;

// router.get('/view/v4', [
// ], asyncHandler(async (req, res, next) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) { 
//     res.status(400).json({ code: '9000' });
//   } else {
//     const data = await get.getUserEventList(req);
//     res.status(200).json(data);
//   }   
// }))
router.get('/view', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getScheduleList(req);
    res.status(200).json(data);
  }   
}))


module.exports = router;
