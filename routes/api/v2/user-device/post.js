
const getUserDeviceSnListCheck = (req) => {
    return new Promise(async(resolve, reject) => {
        try {           
            req.body.user_id = req.access_user.user_id
            req.body.organization_id = req.access_user.organization_id
            const data = await Api.V2UserDevice.getUserDeviceSnListCheck(req.body);    
            resolve(data)
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    getUserDeviceSnListCheck
};

