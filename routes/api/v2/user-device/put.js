
const updateUserDeviceSnInit = (req) => {
    return new Promise(async(resolve, reject) => {
        try {           
            if (req.body.mac && req.body.mac.length > 5) {
                const data = await Api.V2UserDevice.updateUserDeviceSnInitMac(req.body);
                resolve(data)
            } else {
                const data = await Api.V2UserDevice.updateUserDeviceSnInit(req.body, req.access_user.user_id);
                resolve(data)
            }
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    updateUserDeviceSnInit
};

