
const getUserPaper = (req) => {
    return new Promise(async(resolve, reject) => {
        try {       
            let arrayData = [];
            req.query.user_id = req.access_user.user_id
            // const userCheck = await Api.V2UserCheck.getUserCheck(req.query);
            // console.log(userCheck)
            // const userVaccine = await Api.V2UserVaccine.getUserVaccine(req.query);
            // console.log(userVaccine)
            const data = await Api.V2UserCheck.getUserCheckUnionAll(req.query);       
            resolve(data)
           
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    getUserPaper
};