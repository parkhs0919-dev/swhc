const s3 = CONFIG.common.s3;
const multer = Utils.multer;

const deleteUserCheck = (req) => {
    return new Promise(async(resolve, reject) => {
        try {            
            const userCheckFile = await Api.V2UserCheck.getUserCheckId(req.params.id);
            console.log(userCheckFile)
            if(req.params.id && userCheckFile.code=="0000" && userCheckFile.rows.length>0){
                let data = userCheckFile.rows[0];
                const delData = await multer.del({
                    Bucket: data.bucket_name,
                    Key: data.file_name
                })
                if(delData.code=="0000"){
                    await Api.V2UserCheckFile.deleteUserCheckFile(data.user_check_file_id);
                    await Api.V2UserCheck.deleteUserCheck(data.user_check_id);           
                    resolve({code:"0000"})
                }else{
                    resolve(delData)
                }                  
            }else{
                resolve(userCheckFile)     
            }                 
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};
const deleteUserVaccine = (req) => {
    return new Promise(async(resolve, reject) => {
        try {            
            const userVaccineFile = await Api.V2UserVaccine.getUserVaccineId(req.params.id);
            console.log(userVaccineFile)
            if(req.params.id && userVaccineFile.code=="0000" && userVaccineFile.rows.length>0){
                let data = userVaccineFile.rows[0];
                const delData = await multer.del({
                    Bucket: data.bucket_name,
                    Key: data.file_name
                })
                if(delData.code=="0000"){
                    await Api.V2UserVaccineFile.deleteUserVaccineFile(data.user_vaccine_file_id);
                    await Api.V2UserVaccine.deleteUserVaccine(data.user_vaccine_id);           
                    resolve({code:"0000"})
                }else{
                    resolve(delData)
                }                  
            }else{
                resolve(userCheckFile)     
            }                 
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    deleteUserCheck,
    deleteUserVaccine
};

