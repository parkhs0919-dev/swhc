// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const del = require('./del');
const get = require('./get');

const multer = require('multer');
const upload =  multer({storage:Utils.multer.user,limits: { files: 1, fileSize: 5 * 1024 * 1024, }}).single('file');


router.get('/', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserPaper(req);    
    req.json(data);
  }
}))

router.post('/check', [
],asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    upload( req, res, async ( err ) => {
      if ( err ){
        console.log(err);
        res.status(400).json({ code: '9100' });
      }else{
        console.log(req.file)
        console.log(req.body)
        const data = await post.createUserCheck(req);
        req.json(data);
      }
    });
  }  
}))

router.post('/vaccine', [
],asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    upload( req, res, async ( err ) => {
      if ( err ){
        console.log(err);
        res.status(400).json({ code: '9100' });
      }else{
        console.log(req.file)
        console.log(req.body)
        const data = await post.createUserVaccine(req);
        req.json(data);
      }
    });
  }  
}))

router.delete('/check/:id', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await del.deleteUserCheck(req);    
    req.json(data);
  }
}))
router.delete('/vaccine/:id', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await del.deleteUserVaccine(req);    
    req.json(data);
  }
}))

module.exports = router;
