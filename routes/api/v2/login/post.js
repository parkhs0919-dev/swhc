// @ts-nocheck
const Jwt = require('jwt-simple');
const moment = require('moment');
const auth = CONFIG.common.auth;

const checkUser = (params) => {
    return new Promise(async(resolve, reject) => {
        try {  
            const user = await Api.V2User.getUserLoginCheck(params);
            params.user_id = user.user_id;
            if(user.code == '0000'){
                const check = await Api.V2UserLoginInfo.getUserLoginInfoCheck(params);
                //const timeCheck = await isUserLoginTimeCheck(check,params);
                if (check.code == '0000') {
                    delete user.user_id;
                    params.device_sn='null';
                    await Api.V2UserDevice.updateUserDevice(params, params.user_id);
                    await Api.V2User.updateUserLoginTime(params, params.user_id)
                    await Api.V2UserLoginInfo.updateUserLoginInfoCheck(params);               
                    resolve(user);
                } else if (check.code == '1500') {
                    if (params.login_yn && params.login_yn == "Y") {
                        const update = await Api.V2UserLoginInfo.updateUserLoginInfoCheck(params);
                        if (update.code == '0000') {
                            params.device_sn='null';
                            await Api.V2UserDevice.updateUserDevice(params, params.user_id);
                            await Api.V2User.updateUserLoginTime(params, params.user_id);
                            resolve(user);
                        } else {
                            resolve({code:"9100"});
                        }  
                    } else {
                        resolve({code: "1500"});
                    }
                } else {
                    resolve(check);
                }
            }else{
                resolve(user);
            }

        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const checkUserToken = (req) => {
    return new Promise(async(resolve, reject) => {
        try {      
            let params =req.body;  
            let access_token = getUserToken(req);
            if(access_token){
                params.user_id=access_token.user_id;   
                const user = await Api.V2User.getUserLoginCheckToken(params);
                if(user.code == '0000'){
                    params.user_id = user.user_id;
                    const check = await Api.V2UserLoginInfo.getUserLoginInfoCheck(params);
                    //const timeCheck = await isUserLoginTimeCheck(check,params);
                    if (check.code == '0000') {
                        delete user.user_id;
                        params.device_sn='null';
                        await Api.V2UserDevice.updateUserDevice(params, params.user_id);
                        await Api.V2User.updateUserLoginTime(params, params.user_id)
                        await Api.V2UserLoginInfo.updateUserLoginInfoCheck(params);
                        resolve(user);
                    } else if (check.code == '1500') {
                        if (params.login_yn && params.login_yn == "Y") {
                            const update = await Api.V2UserLoginInfo.updateUserLoginInfoCheck(params);
                            if (update.code == '0000') {
                                params.device_sn='null';
                                await Api.V2UserDevice.updateUserDevice(params, params.user_id);
                                await Api.V2User.updateUserLoginTime(params, params.user_id);
                                resolve(user);
                            } else {
                                resolve({code: "9100"});
                            }
                        } else {
                            resolve({code: "1500"});
                        }
                    } else {
                        resolve(check);
                    }
                }else{
                    resolve(user);
                }
                
            }else{
                resolve({code: "9200"});
            }

        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
// const checkUser = (params) => {
//     return new Promise(async(resolve, reject) => {
//         try {  
//             const user = await Api.V2User.getUserLoginCheck(params);
//             params.user_id = user.user_id;
//             if(user.code == '0000'){
//                 const check = await Api.V2UserLoginInfo.getUserLoginInfoCheck(params);
//                 const timeCheck = await isUserLoginTimeCheck(check,params);
//                 if (timeCheck.code == '0000') {
//                     delete user.user_id;
//                     params.device_sn='null';
//                     await Api.V2UserDevice.updateUserDevice(params, params.user_id);
//                     await Api.V2User.updateUserLoginTime(params, params.user_id)
//                     await Api.V2UserLoginInfo.updateUserLoginInfoCheck(params);
//                     if(SERVER_ENV!='local') await Api.V2UserLoginInfo.setRedisLoginDevice(params);
//                     resolve(user);
//                 } else if (timeCheck.code == '1500') {
//                     if (params.login_yn && params.login_yn == "Y") {
//                         const update = await Api.V2UserLoginInfo.updateUserLoginInfoCheck(params);
//                         if (update.code == '0000') {
//                             params.device_sn='null';
//                             await Api.V2UserDevice.updateUserDevice(params, params.user_id);
//                             await Api.V2User.updateUserLoginTime(params, params.user_id);
//                             if(SERVER_ENV!='local') await Api.V2UserLoginInfo.setRedisLoginDevice(params);
//                             resolve(user);
//                         } else {
//                             resolve({code:"9100"});
//                         }  
//                     } else {
//                         resolve({code: "1500"});
//                     }
//                 } else {
//                     resolve(timeCheck);
//                 }
//             }else{
//                 resolve(user);
//             }

//         } catch (e) {
//             console.log(e)
//             resolve({code:"9100"});
//         }   
//     });
// };
// const checkUserToken = (req) => {
//     return new Promise(async(resolve, reject) => {
//         try {      
//             let params =req.body;  
//             let access_token = getUserToken(req);
//             if(access_token){
//                 params.user_id=access_token.user_id;   
//                 const user = await Api.V2User.getUserLoginCheckToken(params);
//                 if(user.code == '0000'){
//                     params.user_id = user.user_id;
//                     const check = await Api.V2UserLoginInfo.getUserLoginInfoCheck(params);
//                     const timeCheck = await isUserLoginTimeCheck(check,params);
//                     if (timeCheck.code == '0000') {
//                         delete user.user_id;
//                         params.device_sn='null';
//                         await Api.V2UserDevice.updateUserDevice(params, params.user_id);
//                         await Api.V2User.updateUserLoginTime(params, params.user_id)
//                         await Api.V2UserLoginInfo.updateUserLoginInfoCheck(params);
//                         if(SERVER_ENV!='local') await Api.V2UserLoginInfo.setRedisLoginDevice(params);
//                         resolve(user);
//                     } else if (timeCheck.code == '1500') {
//                         if (params.login_yn && params.login_yn == "Y") {
//                             const update = await Api.V2UserLoginInfo.updateUserLoginInfoCheck(params);                      
//                             if (update.code == '0000') {                         
//                                 params.device_sn='null';
//                                 await Api.V2UserDevice.updateUserDevice(params, params.user_id);
//                                 await Api.V2User.updateUserLoginTime(params, params.user_id);
//                                 if(SERVER_ENV!='local') await Api.V2UserLoginInfo.setRedisLoginDevice(params);
//                                 resolve(user);
//                             } else {
//                                 resolve({code: "9100"});
//                             }
//                         } else {
//                             resolve({code: "1500"});
//                         }
//                     } else {
//                         resolve(timeCheck);
//                     }
//                 }else{
//                     resolve(user);
//                 }
                
//             }else{
//                 resolve({code: "9200"});
//             }

//         } catch (e) {
//             console.log(e)
//             resolve({code:"9100"});
//         }   
//     });
// };

module.exports={
    checkUser,
    checkUserToken
};

function getUserToken(req) {
    let result;
    let access_token = req.cookies['access_token'];
    console.log(req.cookies)
    if (access_token) {
        let user = Jwt.decode(access_token, auth.secret);
        console.log(user)
        if (user.key == auth.key) {
            result = user;
        } else {
            result = null;
        }
    } else {
        result = null;
    }
    return result;
}
  
async function isUserLoginTimeCheck (check,params){
    let result ={code:"0000"}
    let time = moment().tz(params.timezone).unix();
    if(check.code=="1500"){
        const data = await Utils.redis.get(params.user_id);
        if(data && data.unix){
           let num = parseInt(time) - parseInt((data.unix)?data.unix:0);
           console.log('unix time==',num)
           if(num>86400) result.code="0000";
           else result.code="1500";
        }else{
            result.code="1500";
        }
    }
    //console.log(result)
    return result;
}