// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const get = require('./get');
const auth = CONFIG.common.auth;


router.post('/', [
  check('user_auth_id').not().isEmpty(),
  check('user_auth_pw').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.checkUser(req.body);
    if(data.code=="0000") {
      res.cookie('access_token', data.token, {
        maxAge: auth.maxAge,
        httpOnly: true
      });
    }
    res.status(200).json(data);
  }   
}))
router.post('/auto', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.checkUserToken(req);
    if(data.code=="0000") {
      res.cookie('access_token', data.token, {
        maxAge: auth.maxAge,
        httpOnly: true
      });
    }
    res.status(200).json(data);
  }   
}))

router.get('/find-id', [
    check('user_nm').not().isEmpty(),
    check('user_tel').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.findIdUser(req.query);    
    res.status(200).send(data);
  }
}))

router.get('/find-pw', [
    check('user_tel').not().isEmpty(),
    check('user_auth_id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.findPwUser(req.query);    
    res.status(200).send(data);
  }
}))  

router.get('/user-auth', [
  check('user_auth_id').not().isEmpty(),
  check('user_nm').not().isEmpty(),
  check('user_tel').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserAuthCheck(req.query);    
    res.status(200).send(data);
  }
}))

module.exports = router;
