// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const get = require('./get');

router.get('/check-id', [
  check('user_auth_id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.checkUserDuplicate(req);
    console.log(data)
    req.json(data);
  }   
}))
router.get('/check-email', [
  check('user_email').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {   
    const data = await get.checkUserDuplicateEmail(req);
    req.json(data);
  }   
}))
router.get('/phone-auth', [
  check('imp_uid').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {   
    const data = await get.getUserPhoneAuth(req);
    console.log(data)
    req.json(data);
  }   
}))

router.post('/', [
  check('user_auth_id').not().isEmpty(),
  check('user_auth_pw').not().isEmpty().isLength({ min: 6 }),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.registerUser(req);
    req.json(data);
  }   
}))
router.post('/auth', [
  check('user_auth_id').not().isEmpty(),
  check('user_auth_pw').not().isEmpty().isLength({ min: 6 }),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.registerUserAuth(req);
    req.json(data);
  }   
}))

module.exports = router;

