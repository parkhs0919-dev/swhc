
const registerUserAuth = (req) => {
    return new Promise(async(resolve, reject) => {
        try {  
            console.log(req.body)         
            const check = await Api.V2User.getUserDuplicateCheck(req.body);        
            if(check.code=="0000" ){
                const auth = await Utils.iamport.getCertificationPhone(req.body.imp_uid);
                if(auth.code=="0000" && auth.data){         
                    const phone = await Api.V2User.getUserPhone(req.body);   
                    //console.log(phone)
                    if(req.body.child_yn=="N" && phone.code!="0000"){
                        resolve({code:"1500"  });
                    }else{
                        let sub_params={};
                        sub_params.name = auth.data.name;
                        sub_params.birth = auth.data.birthday.replace(/\-/g,'');
                        sub_params.phone = auth.data.phone;
                        sub_params.imp_uid = auth.data.imp_uid;
                        const user = await Api.V2User.createUser(req.body);
                        if(user.user_id){
                            sub_params.user_id = user.user_id;
                            req.body.user_id = user.user_id;
                            await Api.V2UserAuth.createUserAuth(sub_params);
                            await Api.V2Health.createHealth(req.body);
                            await Api.V2UserDevice.createUserDeviceCheck(req.body);
                        }           
                        resolve(user);
                    }    
                }else{
                    resolve(auth);
                }

            }else{
                if(check.code!="0000") resolve(check);
                else if(phone.code!="0000") resolve(phone);              
            }       
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};

const registerUser = (req) => {
    return new Promise(async(resolve, reject) => {
        try {           
            const check = await Api.V2User.getUserDuplicateCheck(req.body);
            //const phnoe = await Api.V2User.getUserPhone(params);
            if(check.code=="0000" ){          
                if(true){             
                    let sub_params={};
                    sub_params.name = req.body.user_nm;
                    sub_params.birth = "19990101";
                    sub_params.phone = "01011112222";
                    sub_params.imp_uid = "13414134";

                    const user = await Api.V2User.createUser(req.body);
                    if(user.user_id){
                        sub_params.user_id = user.user_id;
                        req.body.user_id = user.user_id;
                        await Api.V2UserAuth.createUserAuth(sub_params);
                        await Api.V2Health.createHealth(req.body);
                        await Api.V2UserDevice.createUserDeviceCheck(req.body);
                    }           
                    resolve(user);
                }else{
                    resolve({code:"9300"});
                }

            }else{
                resolve(check);
            }       
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    registerUser,
    registerUserAuth
};
