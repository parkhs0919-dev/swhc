
const checkUserDuplicate = (req) => {
    return new Promise(async(resolve, reject) => {
        try {           
            const check = await Api.V2User.getUserDuplicateCheck(req.query);
            resolve(check);        
        } catch (e) {
            //reject(e);
            resolve({code:"9100"});
        }   
    });
};
const checkUserDuplicateEmail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const check = await Api.V2User.getUserDuplicateCheckEmail(req.query);   
            resolve(check);  
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};
const getUserPhoneAuth = (req) => {
    return new Promise(async(resolve, reject) => {
        try {
            let result ={}                   
            const check = await Utils.iamport.getCertificationPhone(req.query.imp_uid);  
            if(check.code=="0000"){
                req.body.user_tel=check.data.phone;
                const phone = await Api.V2User.getUserPhone(req.body); 
                if(req.query.child_yn=="N" && phone.code!="0000"){
                    resolve({code:"1500"  });
                }else{
                    result.foreigner_yn =(check.data.foreigner)? "Y":"N";
                    result.gender =(check.data.gender=="male")? "M":"F";
                    result.birth =check.data.birthday;
                    result.phone =check.data.phone;
                    result.name =check.data.name;
    
                    resolve({code:"0000" ,rows :result });
                }

            }else{
                resolve(check); 
            } 
        } catch (e) {
            console.log(e);
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    checkUserDuplicate,
    checkUserDuplicateEmail,
    getUserPhoneAuth
};

