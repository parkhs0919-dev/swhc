// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const get = require('./get');
const post = require('./post');
const put = require('./put');

router.get('/send', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getMessageSend(req);    
    res.status(200).send(data);
  }  
}))

router.get('/receive', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getMessageReceive(req);    
    res.status(200).send(data);
  }  
}))

router.get('/group', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getMessageGroup(req);    
    res.status(200).send(data);
  }  
}))

router.post('/', [
  check('group_id').not().isEmpty(),
  check('contents').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.createMessage(req);    
    res.status(200).send(data);
  }  
}))

router.post('/reply', [
  check('message_id').not().isEmpty(),
  check('contents').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.createMessageReply(req);    
    res.status(200).send(data);
  }  
}))
router.put('/read', [
  check('message_id').not().isEmpty().isNumeric(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateMessageReadSt(req);
    res.status(200).send(data);
  }
}))

// router.put('/read/:id', [
// ], asyncHandler(async (req, res, next) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     res.status(400).json({ code: '9000' });
//   } else {
//     const data = await put.updateMessageRead(req);
//     req.json(data);
//   }
// }))

module.exports = router;

