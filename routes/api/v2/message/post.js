
const createMessage = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.body.user_id = req.access_user.user_id; 
            req.body.message_gb = 'user'; 
            const data = await Api.V2Message.createMessage(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const createMessageReply = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.body.user_id = req.access_user.user_id; 
            req.body.message_gb = 'user'; 
            const data = await Api.V2Message.createMessageReply(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    createMessage,
    createMessageReply
};
