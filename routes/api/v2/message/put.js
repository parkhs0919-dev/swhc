
const updateMessageReadSt = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.body.user_id = req.access_user.user_id
            req.body.read_st = "1";
            const data = await Api.V2Message.updateMessageReadSt(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    updateMessageReadSt
};
