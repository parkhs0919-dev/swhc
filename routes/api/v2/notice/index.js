// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const get = require('./get');

router.get('/', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    req.query.user_id = req.access_user.user_id
    const data = await get.getNotice(req);    
    res.status(200).send(data);
  }  
}))

router.get('/last', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    req.query.user_id = req.access_user.user_id
    const data = await get.getNoticeLast(req);    
    res.status(200).send(data);
  }  
}))


module.exports = router;
