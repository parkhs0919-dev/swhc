
const getNotice = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.user_id = req.access_user.user_id; 
            const data = await Api.V2Notice.getNotice(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const getNoticeLast = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.user_id = req.access_user.user_id; 
            const data = await Api.V2Notice.getNoticeLast(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    getNotice,
    getNoticeLast

};
