
const getUser = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {           
            let group_nm="";
            req.query.user_id = req.access_user.user_id
            const data1 = await Api.V2User.getUser(req.query);
            const data2 = await Api.V2User.getUserGroupRels(req.query);
            if(data1.code=="0000" && data2.code=="0000"){
                if(data2.rows && data2.rows.length>0){
                    if(data2.rows.length==1) group_nm =data2.rows[0].organization_nm ;
                    else group_nm =data2.rows[0].organization_nm +"등 "+ (data2.rows.length-1)+"개";
                }
                resolve({
                    code:"0000",
                    user:data1.rows,
                    group_nm : group_nm
                });
            }else{
                resolve({code:"8100"});
            }   
        } catch (e) {
            //reject(e);
            resolve(result);
        }   
    });
};

const getUserGroupSearch = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {           
            req.query.user_id = req.access_user.user_id
            const data = await Api.V2User.getUserGroupSearch(req.query);
            resolve(data);
        } catch (e) {
            //reject(e);
            resolve(result);
        }   
    });
};
const getUserGroupRelsList = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {           
            req.query.user_id = req.access_user.user_id
            const data = await Api.V2User.getUserGroupRelsList(req.query);
            resolve(data);
        } catch (e) {
            //reject(e);
            resolve(result);
        }   
    });
};

module.exports={
    getUser,
    getUserGroupSearch,
    getUserGroupRelsList
};

