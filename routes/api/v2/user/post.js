
const updateUserLoginInfoDuplicate = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {           
            req.body.user_id = req.access_user.user_id
            req.body.login_yn = "N";
            const data = await Api.V2User.updateUserLoginInfoDuplicate(req.body);
            if(data.code=="0000"){
                await Api.V2UserDevice.updateUserDeviceInit(req.body,req.body.user_id);        
                //if(SERVER_ENV!='local') await Api.V2UserLoginInfo.setRedisLoginDevice(req.body);
            } 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve(result);
        }   
    });
};
const createUserGroup = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {          
            req.body.user_id = req.access_user.user_id; 
            const data = await Api.V2UserGroupRels.getUserGroupRelsCheck(req.body);          
            const data2 = await Api.V2UserGroupRels.getUserGroupRelsOrganizationCheck(req.body);          
            const group = await Api.V2Group.getGroup(req.body);
            let group_data = (group.rows[0])? group.rows[0]: null; 
            if(group_data.auth_code == req.body.auth_code ){
                if (data.code == "0000" && data2.code == "0000" ) {
                    const health_data = await Api.V2User.getUserHealth(req.body);
                    let health = health_data.rows[0]; 
                    if (data.rows && data.rows.length == 1) {
                        let row = data.rows[0];
                        if(!isGradeCheck(row,row.type,health.grade_cd)){
                            resolve({code:"3100",grade:row.grade});
                        }else{
                            if(row.apply_cd =="A1"){
                                resolve({code:"0000"});
                            }else{
                                req.body.apply_cd ="A1";
                                req.body.expire_dt=(row.type=="LT")? "180" : "30";
                                const update = await Api.V2UserGroupRels.updateUserGroupRels(req.body,row.user_group_rels_id);
                                resolve(update);                        
                            }
                        }
                    }else if(data.rows && data.rows.length == 0){                               
                        if(group.code="0000" && group.rows && group.rows.length == 1){
                            let row = group.rows[0];
                            if(!isGradeCheck(row,row.type,health.grade_cd)){
                                resolve({code:"3100",grade:row.grade});
                            }else{
                                req.body.apply_cd ="A1";
                                req.body.expire_dt=(row.type=="LT")? "180" : "30";
                                const create = await Api.V2UserGroupRels.createUserGroupRels(req.body);
                                resolve(create);
                            }
                        }else{
                            resolve(group);
                        }
                    }else {
                        resolve(result);
                    }
                } else {
                    if(data2!="0000") resolve({code:"3100"}); 
                    else resolve(result);           
                }
            }else{
                resolve({code:"3200"});
            }
        } catch (e) {
            console.log(e)
            resolve(result);
        }   
    });
};
module.exports={
    updateUserLoginInfoDuplicate,
    createUserGroup
};

function isGradeCheck(user,type,val){
    let result = false;
    let data = (user)? user : "";
    if(type=="ST"){
        if(data.grade=="G1" && val=="G1") result = true;
        else if(data.grade=="G2" && (val=="G1" || val=="G2" )) result = true;
        else if(data.grade=="G3" && (val=="G1" || val=="G2" || val=="G3" )) result = true;
    }else if(type=="LT"){
        result = true;
    }
    console.log(data.grade,type,val);
    console.log('result==',result);
    return result;
}

function isAuthCode(code,val){
    return (code==val)? true:false;
}