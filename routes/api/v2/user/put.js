
const updateUserFirstPassword = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {           
            if(req.access_user.user_id){
                const data = await Api.V2User.updateUserFirstPassword(req.body, req.access_user.user_id);   
                resolve(data); 
            }else{ 
                resolve(result); 
            }       
        } catch (e) {
            //reject(e);
            resolve(result);
        }   
    });
};
const updateUserPassword = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {           
            if(req.access_user.user_id){
                const data = await Api.V2User.updateUserPassword(req.body, req.access_user.user_id);   
                resolve(data); 
            }else{ 
                resolve(result); 
            }       
        } catch (e) {
            //reject(e);
            resolve(result);
        }   
    });
};
const updateUser = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {           
            req.query.user_id = req.access_user.user_id
            const data = await Api.V2User.updateUser(req.query);
            resolve(data);
        } catch (e) {
            //reject(e);
            resolve(result);
        }   
    });
};
const updateGroupRemove = (req) => {
    return new Promise(async(resolve, reject) => {
        let result={code:"8300"}
        try {           
            req.body.apply_cd ="A4";
            const update = await Api.V2UserGroupRels.updateUserGroupRels(req.body,req.params.id);
            resolve(update);  
        } catch (e) {
            //reject(e);
            resolve(result);
        }   
    });
};
module.exports={
    updateUserFirstPassword,
    updateUserPassword,
    updateUser,
    updateGroupRemove
};

