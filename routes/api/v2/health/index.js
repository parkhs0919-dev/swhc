// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const put = require('./put');
const auth = CONFIG.common.auth;

router.post('/', [
    check('token').not().isEmpty(),
    check('uuid').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.createHealth(req);
    res.status(200).json(data);
  }   
}))

router.put('/status', [
  check('token').not().isEmpty(),
  check('status_cd').not().isEmpty()
], asyncHandler(async (req, res, next) => {
const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateHealthStatus(req);
    res.status(200).json(data);
  }   
}))


module.exports = router;
