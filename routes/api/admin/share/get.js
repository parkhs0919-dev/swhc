
const getUserCheck = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id = req.access_admin.organization_id;
            req.query.organization_admin_id = req.access_admin.organization_admin_id;
            const data = await Api.AdUserCheck.getUserCheckUnionAll(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    getUserCheck,
};
