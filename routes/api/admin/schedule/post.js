const createSchedule = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id = req.access_admin.organization_id;
            const data = await Api.AdSchedule.createSchedule(req.body);
            if(data.code=="0000"){
                await Api.AdScheduleRels.updateScheduleRels(req.body,data.data.schedule_id); 
              //await Api.AdSend.sendScheduleGroupToTopic(req.body);
            }
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    createSchedule
};
