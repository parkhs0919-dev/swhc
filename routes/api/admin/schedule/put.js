
const updateScheduleRels = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id = req.access_admin.organization_id;
            const data = await Api.AdScheduleRels.updateScheduleRels(req.body, req.params.id);
            if(data.code=="0000"){
              //await Api.AdSend.sendScheduleGroupToTopic(req.body);
            }
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const updateSchedule = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id = req.access_admin.organization_id;
            const data = await Api.AdSchedule.updateSchedule(req.body, req.params.id);
            if(data.code=="0000"){
              //await Api.AdSend.sendScheduleGroupToTopic(req.body);
            }
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    updateSchedule,
    updateScheduleRels
};
