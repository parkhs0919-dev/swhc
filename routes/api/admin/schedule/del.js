
const deleteSchedule = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.AdSchedule.deleteSchedule(req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};



module.exports={
    deleteSchedule
};
