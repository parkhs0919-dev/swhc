const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const setting = CONFIG.setting;
const post = require('./post');
const get = require('./get');
const put = require('./put');
const del = require('./del');

router.get('/', [
  check('schedule_st').not().isEmpty(),
  check('group_id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getScheduleList(req);    
    res.json(data);
  }      
}))

// router.get('/detail', [
// ], asyncHandler(async (req, res, next) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     res.status(400).json({ code: '9000' });
//   } else {
//     const data = await get.getScheduleDetail(req);    
//     res.json(data);
//   }      
// }))

router.get('/left', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getScheduleRelsLeft(req);    
    res.json(data);
  }      
}))
router.get('/right', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getScheduleRelsRight(req);    
    res.json(data);
  }      
}))

router.post('/', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      res.status(400).json({ code: '9000' });
  } else {
      const data = await post.createSchedule(req);
      res.json(data);
  }      
}))
router.put('/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });  
  } else {
    const data = await put.updateSchedule(req);
    res.json(data);
  }
}))
router.put('/rels/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });  
  } else {
    const data = await put.updateScheduleRels(req);
    res.json(data);
  }
}))

router.delete('/:id', [
  check('id').not().isEmpty().isNumeric()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await del.deleteSchedule(req);
    res.json(data);
  }
}))

module.exports = router;   

