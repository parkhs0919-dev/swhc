
const getScheduleList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id = req.access_admin.organization_id;
            req.query.organization_admin_id = req.access_admin.organization_admin_id;
            const data = await Api.AdSchedule.getScheduleList(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getScheduleRelsRight = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id = req.access_admin.organization_id;
            const data = await Api.AdScheduleRels.getScheduleRelsRight(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getScheduleRelsLeft = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id = req.access_admin.organization_id;
            const data = await Api.AdScheduleRels.getScheduleRelsLeft(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};




module.exports={
    getScheduleList,
    getScheduleRelsLeft,
    getScheduleRelsRight
};
