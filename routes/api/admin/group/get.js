
const getGroupList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.organization_id=req.access_admin.organization_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdGroup.getGroupList(req.query);    
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getGroupDetail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.AdGroup.getGroupDetail(req.query);    
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getGroupOptionList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdGroup.getGroupOptionList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getGroupSearchSelectBox = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdGroup.getGroupSearchSelectBox(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getGroupSearchLeft = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdGroup.getGroupSearchLeft(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getGroupSearchRight = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdGroup.getGroupSearchRight(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    getGroupList,
    getGroupDetail,
    getGroupOptionList,
    getGroupSearchSelectBox,
    getGroupSearchLeft,
    getGroupSearchRight

};
