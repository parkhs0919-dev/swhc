const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const get = require('./get');
const put = require('./put');
const del = require('./del');

//그룹 조회 
router.get('/', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getGroupList(req);    
    res.json(data);
  }  
}))
//그룹 상세 조회
router.get('/detail', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getGroupDetail(req);    
    res.json(data);
  }  
}))
//그룹 selectbox 조회
router.get('/option', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getGroupOptionList(req);    
    res.json(data);
  }  
}))
//그룹 selectbox 조회
router.get('/search', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getGroupSearchSelectBox(req);    
    res.json(data);
  }  
}))   
router.get('/left', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getGroupSearchLeft(req);    
    res.json(data);
  }  
}))   
router.get('/right', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getGroupSearchRight(req);    
    res.json(data);
  }  
}))   

router.post('/', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.createGroupProcess(req);
    res.json(data);
  }
}));
  
router.put('/detail/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateGroupProcess(req);
    res.json(data);
  }
}))   

router.delete('/:id', [
  check('id').not().isEmpty().isNumeric()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await del.deleteGroup(req);
    res.json(data);
  }
}))


module.exports = router;

