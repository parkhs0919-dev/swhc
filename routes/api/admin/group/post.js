
const createGroupProcess = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.body.organization_id=req.access_admin.organization_id;
            req.body.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdGroup.createGroupProcess(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    createGroupProcess
};
