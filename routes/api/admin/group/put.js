
const updateGroupProcess = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.AdGroup.updateGroupProcess(req.body, req.params.id);
            if(data.code=="0000"){
              const admin = await Api.AdGroupAdminRels.updateGroupAdminRelsGroupId(req.body, req.params.id);
            }  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    updateGroupProcess
};
