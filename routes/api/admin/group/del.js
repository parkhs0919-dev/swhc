
const deleteGroup = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.AdGroup.deleteGroup(req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    deleteGroup
};
