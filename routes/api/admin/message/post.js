const createMessageArray = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id = req.access_admin.organization_id;
            req.body.organization_admin_id = req.access_admin.organization_admin_id;
            req.body.admin_grade = req.access_admin.admin_grade;
            req.body.message_gb ="admin";      
            const data = await Api.AdMessage.createMessageArray(req.body);
            if(data.code=="0000"){
                await Api.AdSend.sendUserToToken(req.body);
            }
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const createMessageArrayGroup = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id = req.access_admin.organization_id;
            req.body.organization_admin_id = req.access_admin.organization_admin_id;
            req.body.admin_grade = req.access_admin.admin_grade;
            req.body.message_gb ="admin";
            const data = await Api.AdMessage.createMessageArrayGroup(req.body);
            if(data.code=="0000"){
                await Api.AdSend.sendGroupToTopic(req.body);
            }
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const createMessageArrayAll = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id = req.access_admin.organization_id;
            req.body.organization_admin_id = req.access_admin.organization_admin_id;
            req.body.admin_grade = req.access_admin.admin_grade;
            req.body.message_gb ="admin";
            const data = await Api.AdMessage.createMessageArrayAll(req.body);
            if(data.code=="0000"){
                await Api.AdSend.sendGroupToTopicAll(req.body);
            }
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const createMessageMainArray = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id = req.access_admin.organization_id;
            req.body.organization_admin_id = req.access_admin.organization_admin_id;
            req.body.admin_grade = req.access_admin.admin_grade;
            const data = await Api.AdMessage.createMessageMainArray(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    createMessageMainArray,
    createMessageArray,
    createMessageArrayGroup,
    createMessageArrayAll

};
