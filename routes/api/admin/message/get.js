
const getMessageList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id = req.access_admin.organization_id;
            req.query.organization_admin_id = req.access_admin.organization_admin_id;
            req.query.admin_grade = req.access_admin.admin_grade;
            const data = await Api.AdMessage.getMessageList(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const getUserMessageList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id = req.access_admin.organization_id;
            req.query.organization_admin_id = req.access_admin.organization_admin_id;
            req.query.admin_grade = req.access_admin.admin_grade;      
            const data = await Api.AdMessage.getUserMessageList(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    getMessageList,
    getUserMessageList
};
