
const updateMessageRead = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.body.read_st= "1";
            const data = await Api.AdMessage.updateMessageRead(req.body, req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    updateMessageRead
};
