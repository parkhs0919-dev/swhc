const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const get = require('./get');
const post = require('./post');
const put = require('./put');

router.get('/', [
  check('group_id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getMessageList(req);    
     req.json(data);
  }      
}))   
// router.get('/user', [
//   check('user_id').not().isEmpty(),
// ], asyncHandler(async (req, res, next) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     res.status(400).json({ code: '9000' });
//   } else {
//     const data = await get.getUserMessageList(req);    
//      req.json(data);
//   }      
// }))   

router.post('/', [
  check('user_id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.createMessageArray(req);
     req.json(data);
  }
}));  
router.post('/group', [
  check('group_id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.createMessageArrayGroup(req);
     req.json(data);
  }
}));  
router.post('/all', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.createMessageArrayAll(req);
     req.json(data);
  }
}));  

router.post('/main', [
  check('user_id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.createMessageMainArray(req);
     req.json(data);
  }
}));  

router.put('/read/:id', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateMessageRead(req);
    req.json(data);
  }
}))
module.exports = router;   

