
const createOrganization = (req) => {
    return new Promise(async(resolve, reject) => {
        try {     
            const organizationCheck = await Api.AdOrganization.getOrganizationCheck(req.body); 
            const adminCheck = await Api.AdOrganizationAdmin.getOrganizationAdminCheck(req.body); 
            if(organizationCheck.code=="0000" && adminCheck.code=="0000"){
              const data = await Api.AdOrganization.createOrganizationProcess(req.body);
              if(data.code=="0000" && data.organization_id && req.body.type=="ST" ){
                  req.body.organization_id = data.organization_id;
                  req.body.group_nm = "";
                 await Api.AdGroup.createGroup(req.body);
              }
              resolve(data);
            }else{          
              if(organizationCheck.code!="0000") resolve(organizationCheck);
              else if(adminCheck.code!="0000") resolve(adminCheck);      
            }  
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    createOrganization
};
