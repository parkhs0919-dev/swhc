// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const get = require('./get');
const auth = CONFIG.common.auth;

router.get('/check', [
    check('organization_auth_id').not().isEmpty()
  ], asyncHandler(async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(400).json({ code: '9000' });
    } else {  
      const data = await get.getOrganizationCheck(req);  
      console.log(data)  
      res.send(data);
    }   
  }))

router.post('/', [
    check('organization_auth_id').not().isEmpty(),
    check('organization_nm').not().isEmpty(),
    check('admin_auth_id').not().isEmpty(),
    check('admin_auth_pw').not().isEmpty(),
    check('admin_nm').not().isEmpty(),
    check('admin_tel').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { 
    res.status(400).json({ code: '9000' });
  } else {
    const data = await post.createOrganization(req);
    req.json(data);
  }   
}))


module.exports = router;
