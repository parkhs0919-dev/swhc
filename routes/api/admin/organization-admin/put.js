const updateOrganizationAdmin = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.AdOrganizationAdmin.updateOrganizationAdmin(req.body, req.params.id);

            if(data.code=="0000" ){
              await Api.AdGroupAdminRels.updateGroupAdminRelsOrganizationAdminId(req.body,  req.params.id);
            }
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const updateOrganizationInfo = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.AdOrganization.updateOrganization(req.body, req.params.id);
            if(data.code=="0000" ){
              await Api.AdOrganization.updateOrganizationAdmin(req.body, req.params.id);
            }
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    updateOrganizationAdmin,
    updateOrganizationInfo
};
