
const createOrganizationAdminProcess = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            req.body.organization_id=req.access_admin.organization_id;
            const data = await Api.AdOrganizationAdmin.createOrganizationAdminProcess(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const updateOrganizationAdmin = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.AdOrganizationAdmin.updateOrganizationAdmin(req.body, req.params.id);
            if(data.code=="0000" ){
              await Api.AdOrganizationAdmin.updateGroupAdminRelsOrganizationAdminId(req.body,  req.params.id);
            }
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    createOrganizationAdminProcess,
    updateOrganizationAdmin
};
