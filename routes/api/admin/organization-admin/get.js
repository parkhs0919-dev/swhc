
const getOrganizationAdminSearchList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {       
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;   
            const data = await Api.AdOrganization.getOrganizationAdminSearchList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getOrganizationAdminSearchAllList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {       
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;   
            const data = await Api.AdOrganizationAdmin.getOrganizationAdminSearchAllList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getOrganizationAdminSearchLeftList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {       
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;   
            const data = await Api.AdOrganizationAdmin.getOrganizationAdminSearchLeftList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getOrganizationAdminSearchRightList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {       
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;   
            const data = await Api.AdOrganizationAdmin.getOrganizationAdminSearchRightList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const getOrganizationAdminInfoList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {       
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;   
            const data = await Api.AdOrganizationAdmin.getOrganizationAdminInfoList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getOrganizationAdminDuplicateCheck = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.AdOrganizationAdmin.getOrganizationAdminDuplicateCheck(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getOrganizationAdminDetail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id = req.access_admin.organization_id;       
            const data = await  Api.AdOrganizationAdmin.getOrganizationAdminDetail(req.query);   
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getOrganizationInfo = (req) => {
    return new Promise(async(resolve, reject) => {
        try {  
            req.query.organization_id = req.access_admin.organization_id;          
            const data = await Api.AdOrganization.getOrganizationInfo(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    getOrganizationAdminSearchList,
    getOrganizationAdminInfoList,
    getOrganizationAdminDuplicateCheck,
    getOrganizationAdminDetail,
    getOrganizationAdminSearchLeftList,
    getOrganizationAdminSearchRightList,
    getOrganizationAdminSearchAllList,
    getOrganizationInfo
};
