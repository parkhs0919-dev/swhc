// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const get = require('./get');
const put = require('./put');
const auth = CONFIG.common.auth;

router.get('/', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {  
    const data = await get.getOrganizationAdminSearchList(req);    
    req.json(data);
  }   
}))
router.get('/all', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {  
    const data = await get.getOrganizationAdminSearchAllList(req);    
    req.json(data);
  }   
}))
router.get('/left', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {  
    const data = await get.getOrganizationAdminSearchLeftList(req);    
    req.json(data);
  }   
}))
router.get('/right', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {  
    const data = await get.getOrganizationAdminSearchRightList(req);    
    req.json(data);
  }   
}))

router.get('/info', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {  
    const data = await get.getOrganizationAdminInfoList(req);    
    req.json(data);
  }   
}))
router.get('/check-id', [
  check('admin_auth_id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getOrganizationAdminDuplicateCheck(req);    
    req.json(data);
  }      
}))
router.get('/info/detail', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getOrganizationAdminDetail(req);    
    req.json(data);
  }          
}))
router.get('/oz-info', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {  
    const data = await get.getOrganizationInfo(req);    
    req.json(data);
  }   
}))
router.put('/oz-info/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateOrganizationInfo(req);
    req.json(data);
  }
}))  

router.post('/info', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    req.body.organization_id=req.access_admin.organization_id;
    const data = await post.createOrganizationAdminProcess(req);
    req.json(data);
  }
}));

router.put('/info/detail/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateOrganizationAdmin(req);
    req.json(data);
  }
}))  


module.exports = router;
