
const getOrganizationAdminFindId = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.AdOrganizationAdmin.getOrganizationAdminFindId(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const getOrganizationAdminFindPw = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.AdOrganizationAdmin.getOrganizationAdminFindPw(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    getOrganizationAdminFindId,
    getOrganizationAdminFindPw
};
