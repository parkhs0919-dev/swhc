
const getOrganizationAdminLoginCheck = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.AdOrganizationAdmin.getOrganizationAdminLoginCheck(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getOrganizationAdminLoginNewWindow = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.AdOrganizationAdmin.getOrganizationAdminLoginNewWindow(req.body);
            if(data.code=="0000" && data.data && data.data.organization_admin_id ){
                req.body.link_login_key="";
                await Api.AdOrganizationAdmin.updateOrganizationAdminNewWindow(req.body,data.data.organization_admin_id);
                resolve(data);
            }else{
                resolve({code:"8100"});
            }
           
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    getOrganizationAdminLoginCheck,
    getOrganizationAdminLoginNewWindow
};
