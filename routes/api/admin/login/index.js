// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const get = require('./get');
const auth = CONFIG.common.auth;

router.get('/login', function (req, res, next) {
    res.render('login', {
        view_path: 'login',
        result: {}
    });
});

router.get('/find-id', [
    check('admin_nm').not().isEmpty(),
    check('admin_tel').not().isEmpty()
], asyncHandler(async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            code: '9000'
        });
    } else {
        const data = await get.getOrganizationAdminFindId(req);
        req.json(data);
    }
}))

router.get('/find-pw', [
    check('admin_nm').not().isEmpty(),
    check('admin_tel').not().isEmpty()
], asyncHandler(async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            code: '9000'
        });
    } else {
        const data = await get.getOrganizationAdminFindPw(req);
        req.json(data);
    }
}))  
router.post('/', [
    check('organization_auth_id').not().isEmpty(),
    check('admin_auth_id').not().isEmpty(),
    check('admin_auth_pw').not().isEmpty()
], asyncHandler(async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            code: '9000'
        });
    } else {
        const data = await post.getOrganizationAdminLoginCheck(req);
        if (data.code == "0000") {
            res.cookie('admin_access_token', data.token, {
                maxAge: auth.admin_maxAge,
                httpOnly: true
            });
        }
        req.json(data);
    }
}));
router.post('/link', [
    check('link_login_key').not().isEmpty()
], asyncHandler(async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            code: '9000'
        });
    } else {
        const data = await post.getOrganizationAdminLoginNewWindow(req);
        if (data.code == "0000") {
            res.cookie('admin_access_token', data.token, {
                maxAge: auth.admin_maxAge,
                httpOnly: true
            });
        }
        req.json(data);
    }
}));
  
router.get('/out', function (req, res, next) {
    res.clearCookie('admin_access_token');
    res.status(200).send({
        code: '0000'
    });
});
  
  

module.exports = router;
