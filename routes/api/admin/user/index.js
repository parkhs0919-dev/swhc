const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const Excel = require('exceljs');
const setting = CONFIG.setting;
const del = require('./del');
const get = require('./get');
const put = require('./put');

router.get('/', [
  check('group_id').not().isEmpty(),
  check('grade_cd').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserList(req);    
    req.json(data);
  }      
}))
router.get('/info', [
  check('group_id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserInfoList(req);
    req.json(data);
  }      
}))
router.get('/info/detail', [
  check('user_id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserInfoDetail(req);    
     req.json(data);
  }      
}))
router.get('/detail', [
  check('user_id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserDetail(req);    
    req.json(data);
  }      
}))
router.get('/detail/log', [
  check('user_id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserDetailTempLog(req);    
     req.json(data);
  }      
}))
router.get('/search', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserSearchSelectBox(req);    
     req.json(data);
  }  
}))
router.get('/check-id', [
  check('user_auth_id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserDuplicateCheck(req);    
    req.json(data);
  }      
}))
router.get('/share', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserGroupRelsList(req);    
    req.json(data);
  }      
}))
router.get('/excel', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    try {
      const data = await get.getUserDetailTempLogExcel(req);
      if (data.code == "0000" && data.data.rows && data.data.rows.length > 0) await createExcelData(data.data.rows, res)
      else {
        res.status(400).json({code: '8300'});
      }
    } catch (e) {
      console.log(e)
      res.status(400).json({ code: '8300'});
    }
  }      
}))
router.get('/excel/all', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else { 
    try {
      const data = await get.getUserListAll(req);
      //console.log(data.data.rows)
      if (data.code == "0000" && data.data.rows && data.data.rows.length > 0) await createUserAllExcelData(data.data.rows, res)
      else {
        res.status(400).json({code: '8300'});
      }
    } catch (e) {
      console.log(e)
      res.status(400).json({ code: '8300'});
    }      
  }      
}))
router.get('/sms', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserSms(req);    
    req.json(data);
  }      
}))
router.get('/sms-list', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserSmsList(req);    
    req.json(data);
  }      
}))
router.get('/sms-log-list', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserSmsLogList(req);    
    req.json(data);
  }      
}))

router.put('/detail/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserConfirmed(req);
    req.json(data);
  }
}))  
router.put('/info/detail/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserInfo(req);
    req.json(data);
  }
}))
router.put('/init/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserPasswordInit(req);
    req.json(data);
  }
}))    
router.put('/sms/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserSmsDuplicate(req);
    req.json(data);
  }
}))    

router.delete('/info/:id', [
  check('id').not().isEmpty().isNumeric()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await del.deleteUser(req);
     req.json(data);
  }
}))
router.delete('/sms/:id', [
  check('id').not().isEmpty().isNumeric()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await del.deleteUserSms(req);
     req.json(data);
  }
}))
router.delete('/out/:id', [
  check('id').not().isEmpty().isNumeric()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await del.deleteUserOut(req);
     req.json(data);
  }
}))

module.exports = router;   

async function createExcelData(data,res){
  let fileName = "user_data_list.xlsx"  
  const workbook = new Excel.Workbook();
  const worksheet = workbook.addWorksheet('ExampleSheet');
  worksheet.columns = [
    { header: '시간', key: 'time' },
    { header: '등급', key: 'status' },
    { header: '체온', key: 'temp' }
  ];
  data.forEach((row) => { 
    worksheet.addRow({ time:row.created_dt_str, status: row.grade , temp: row.temperature });
  })
  res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
  workbook.xlsx.write(res)
  .then(() => {
        res.end();
  });
}
async function createUserAllExcelData(data,res){
  let fileName = "user_all_list.xlsx"  
  const workbook = new Excel.Workbook();
  const worksheet = workbook.addWorksheet('ExampleSheet');
  worksheet.columns = [
    { header: '소속', key: 'group_nm' },
    { header: '이름', key: 'user_nm' },
    { header: 'S/N', key: 'user_device_sn' },
    { header: '등급', key: 'grade_cd' },
    { header: '상태', key: 'temperature_cd' },
    { header: '체온', key: 'temperature' },
    { header: '기기상태', key: 'device_connect_st' }
  ];
  data.forEach((row) => { 
    worksheet.addRow({ group_nm:row.group_nm, user_nm: row.user_nm ,
       user_device_sn: row.user_device_sn,
       grade_cd: getGradeName(row.grade_cd), 
       temperature_cd: getTemperatureName(row.temperature_cd), 
       temperature: row.temperature, 
       device_connect_st: getDeviceConnectStName(row.device_connect_st) });
  })
  res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
  workbook.xlsx.write(res)
  .then(() => {
        res.end();
  });
}

function getGradeName(val){
  let result ="보류";
  if(val=="G0") result="보류";
  else if(val=="G1") result="1등급";
  else if(val=="G2") result="2등급";
  else if(val=="G3") result="3등급";
  else if(val=="G4") result="4등급";
  else if(val=="G5") result="5등급";
  else if(val=="G6") result="보류";
  return result;
}
function getTemperatureName(val){
  let result ="미착용";
  if(val=="T0") result="미착용";
  else if(val=="T1") result="정상";
  else if(val=="T2") result="주의";
  else if(val=="T3") result="경계";
  else if(val=="T4") result="심각";
  else if(val=="T5") result="미착용";
  return result;
}
function getDeviceConnectStName(val){
  let result ="비활성";
  if(val=="1") result="정상";
  return result;
}