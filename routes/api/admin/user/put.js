const updateUserConfirmed = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.AdUser.updateUserConfirmed(req.body, req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const updateUserInfo = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.AdUser.updateUserInfo(req.body, req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const updateUserPasswordInit = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.user_auth_pw="000000";
            const data = await Api.AdUser.updateUserPasswordInit(req.body, req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const updateUserSmsDuplicate = (req) => {
    return new Promise(async(resolve, reject) => {
        try {       
            req.body.organization_id = req.access_admin.organization_id;  
            req.body.user_id=req.params.id;
            const data = await Api.AdUserSms.updateUserSmsDuplicate(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    updateUserConfirmed,
    updateUserInfo,
    updateUserPasswordInit,
    updateUserSmsDuplicate
};
