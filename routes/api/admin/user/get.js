
const getUserListAll = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id=req.access_admin.organization_id;
            req.query.group_id="ALL";
            const data = await Api.AdUser.getUserListAll(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id=req.access_admin.organization_id;
            const data = await Api.AdUser.getUserList(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserInfoList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdUser.getUserInfoList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserGroupRelsList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdUser.getUserGroupRelsList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserInfoDetail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.AdUser.getUserInfoDetail(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserDetail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdUser.getUserDetail(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const getUserDetailTempLog = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            const data = await Api.AdUser.getUserDetailTempLog(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserConfirmedList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdUser.getUserConfirmedList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserConfirmedDetail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdUser.getUserConfirmedDetail(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserConfirmedDetailExcel = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdUser.getUserConfirmedDetailExcel(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getHealthLogGps = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            const data = await Api.AdHealthLog.getHealthLogGps(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserSearchSelectBox = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.AdUser.getUserSearchSelectBox(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserDuplicateCheck = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            const data = await Api.AdUser.getUserDuplicateCheck(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const getUserDetailTempLogExcel = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            const data = await Api.AdUser.getUserDetailTempLogExcel(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserSms = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=(req.query.organization_id)?req.query.organization_id:req.access_admin.organization_id;
            const data = await Api.AdUserSms.getUserSms(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserSmsList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=(req.query.organization_id)?req.query.organization_id:req.access_admin.organization_id;
            const data = await Api.AdUserSms.getUserSmsList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserSmsLogList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=(req.query.organization_id)?req.query.organization_id:req.access_admin.organization_id;
            const data = await Api.AdUserSms.getUserSmsLogList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    getUserList,
    getUserInfoList,
    getUserInfoDetail,
    getUserDetail,
    getUserDetailTempLog,
    getUserConfirmedList,
    getUserConfirmedDetail,
    getUserConfirmedDetailExcel,
    getHealthLogGps,
    getUserSearchSelectBox,
    getUserDuplicateCheck,
    getUserDetailTempLogExcel,
    getUserGroupRelsList,
    getUserListAll,
    getUserSms,
    getUserSmsList,
    getUserSmsLogList
};
