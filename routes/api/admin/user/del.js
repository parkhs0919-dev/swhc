const deleteUser = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.AdUser.deleteUser(req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const deleteUserSms = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id=(req.body.organization_id)?req.body.organization_id:req.access_admin.organization_id;
            const data = await Api.AdUserSms.deleteUserSms(req.body,req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const deleteUserOut = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id=(req.body.organization_id)?req.body.organization_id:req.access_admin.organization_id;
            const data = await Api.AdUser.deleteUserOut(req.body,req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    deleteUser,
    deleteUserSms,
    deleteUserOut
};
