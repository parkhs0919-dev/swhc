
const updateNotice = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.organization_id = req.access_admin.organization_id;
            req.query.organization_admin_id = req.access_admin.organization_admin_id;
            req.query.admin_grade = req.access_admin.admin_grade;
            const data = await Api.AdNotice.updateNotice(req.body, req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    updateNotice
};
