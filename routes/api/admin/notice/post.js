const createNotice = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id = req.access_admin.organization_id;
            req.body.organization_admin_id = req.access_admin.organization_admin_id;
            req.body.admin_grade = req.access_admin.admin_grade;
            req.body.notice_gb=(req.body.notice_gb)? req.body.notice_gb :"admin";
            const data = await Api.AdNotice.createNotice(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    createNotice
};
