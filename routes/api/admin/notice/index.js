const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const setting = CONFIG.setting;
const post = require('./post');
const get = require('./get');
const put = require('./put');

router.get('/', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getNoticeList(req);    
     req.json(data);
  }          
}))
router.get('/detail', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getNoticeDetail(req);    
     req.json(data);
  }          
}))

router.post('/', [
    check('title').not().isEmpty(),
    check('contents').not().isEmpty()
  ], asyncHandler(async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ code: '9000' });
    } else {
        const data = await post.createNotice(req);
        req.json(data);
    }      
}))
router.put('/detail/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateNotice(req);
     req.json(data);
  }
}))  
module.exports = router;   

