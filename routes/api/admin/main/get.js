const moment = require('moment');

const getMainUserCount = (req) => {
    return new Promise(async(resolve, reject) => {
        try { 
            req.query.organization_id=req.access_admin.organization_id;  
            let currentDate = moment().tz(req.query.timezone).format("YYYY.MM.DD HH:mm");                   
            const data = await Api.AdMain.getMainUserCount(req.query); 
            data.data.currentDate =currentDate;
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getMainUserCountGroup = (req) => {
    return new Promise(async(resolve, reject) => {
        try { 
            req.query.organization_id=req.access_admin.organization_id;  
            let currentDate = moment().tz(req.query.timezone).format("YYYY.MM.DD HH:mm");                   
            const data = await Api.AdMain.getMainUserCountGroup(req.query);   
            data.data.currentDate =currentDate;
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getMainUserApplyCdCount = (req) => {
    return new Promise(async(resolve, reject) => {
        try { 
            req.query.organization_id=req.access_admin.organization_id;  
            let currentDate = moment().tz(req.query.timezone).format("YYYY.MM.DD HH:mm");                   
            const data = await Api.AdMain.getMainUserApplyCdCount(req.query);   
            data.data.currentDate =currentDate;
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    getMainUserCount,
    getMainUserCountGroup,
    getMainUserApplyCdCount
};
