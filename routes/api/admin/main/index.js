const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const get = require('./get');

router.get('/', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getMainUserCount(req);   
    res.status(200).send(data);
  }      
}))
router.get('/group', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getMainUserCountGroup(req);   
    res.status(200).send(data);
  }      
}))
router.get('/apply', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getMainUserApplyCdCount(req);   
    res.status(200).send(data);
  }      
}))
// router.get('/user', [
// ], asyncHandler(async (req, res, next) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     res.status(400).json({ code: '9000' });
//   } else {
//     req.query.organization_id=req.access_admin.organization_id;
//     if(req.query.type!="ALL"){
//       if(req.query.type=="T1") req.query.temperature_cd=req.query.type;
//       else if(req.query.type=="T2") req.query.temperature_cd=req.query.type;
//       else if(req.query.type=="T3") req.query.temperature_cd=req.query.type;
//       else if(req.query.type=="T4") req.query.temperature_cd=req.query.type;
//       else if(req.query.type=="T5") req.query.temperature_cd=req.query.type;
//       else if(req.query.type=="T0") req.query.temperature_cd=req.query.type;
//       else if(req.query.type=="device") req.query.device_connect_st="0";

//     }
//     const data = await Api.AdMain.getMainUser(req.query);   
//     res.status(200).send(data);
//   }      
// }))
module.exports = router;

