// @ts-nocheck
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const post = require('./post');
const auth = CONFIG.common.auth;

//로그인 
router.post('/', [
    check('admin_auth_id').not().isEmpty(),
    check('admin_auth_pw').not().isEmpty()
], asyncHandler(async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            code: '9000'
        });
    } else {
        const data = await post.getSysAdminLoginCheck(req);
        if (data.code == "0000") {
            res.cookie('sys_access_token', data.token, {
                maxAge: auth.sys_maxAge,
                httpOnly: true
            });
        }
        req.json(data);
    }
}));
// 다이렉트 로그인 
router.post('/link', [
    check('admin_auth_id').not().isEmpty(),
    check('admin_auth_pw').not().isEmpty()
], asyncHandler(async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            code: '9000'
        });
    } else {
        const data = await post.getSysAdminLoginNewWindow(req);
        req.json(data);
    }
}));
  
router.get('/out', function (req, res, next) {
    res.clearCookie('sys_access_token');
    res.status(200).send({
        code: '0000'
    });
});
  
  

module.exports = router;
