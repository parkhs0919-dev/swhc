
const getSysAdminLoginCheck = (req) => {
    return new Promise(async(resolve, reject) => {
        try {          
            const data = await Api.SysAdmin.getSysAdminLoginCheck(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getSysAdminLoginNewWindow = (req) => {
    return new Promise(async(resolve, reject) => {
        try { 
            let result={code :"9100"}         
            const data = await Api.SysAdmin.getSysAdminLoginNewWindow(req.body);
            if(data.code=="0000"){
                req.body.link_login_key =data.link_login_key ;
                await Api.SysAdmin.updateSysOrganizationAdmin(req.body,data.organization_admin_id);
                resolve(data);
            }else{
                resolve(result);
            }   
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    getSysAdminLoginCheck,
    getSysAdminLoginNewWindow
};

// function createToken(data){
//     let result = data;
//     result.key = auth.key;
//     result.date = date.yyyymmddhhmiss;
//     return jwt.encode(result, auth.secret);
//   }
  