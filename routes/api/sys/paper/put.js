
const updatePaper = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysPaper.updatePaper(req.body,req.params.id);
            const status = await Api.Health.getHealthGradeStatus(req.body);     
            if(status.code="0000"){
              let sub={}; 
              let grade_cd=status.rows.grade_cd;
              let ch_grade_cd= await getHealthGradeCode(status.rows);
              sub.grade_cd=ch_grade_cd;
              const health = await Api.Health.updateHealthGrade(sub,req.body.user_id);
              
              if(grade_cd!=ch_grade_cd){
                console.log(' ===sys_grade_change=== ');
                req.body.grade=ch_grade_cd;
                await Api.UserNotification.sendUserNotification(req.body);
              } 
              if(req.body.approval_cd=="02"){
                console.log(' ===approval_cd 02=== ')
                await Api.UserNotification.sendUserPaperNotification(req.body);
              }
              resolve(health);
            }else{
              resolve(data);
            }
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};  



module.exports={
    updatePaper
};

async function getHealthGradeCode(data){
    let result ="G0";
    if(data.status_cd=="S2")  result ="G4";
    else if(data.check_status=="C2" || data.self_quarantine_yn=="Y")  result ="G5";
    else if(data.status_cd=="S1" && data.vaccine_status=="C1" )  result ="G1";
    else if(data.status_cd=="S1" && data.check_status=="C1" )  result ="G2";
    else if(data.vaccine_status=="C0" && data.check_status=="C0" && data.status_cd=="S1")  result ="G3";
    else if(data.vaccine_status=="C0" && data.check_status=="C0" && data.status_cd=="S0")  result ="G0";
    //console.log(result)
    return result;
  }