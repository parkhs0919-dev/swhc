
const getPaperList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysPaper.getUserCheckUnionAll(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};  
const getPaperDetail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysPaper.getPaperDetail(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};  


module.exports={
    getPaperList,
    getPaperDetail
};
