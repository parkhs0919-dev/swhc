const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const get = require('./get');
const put = require('./put');

router.get('/', [
  check('organization_id').not().isEmpty(),
  check('approval_cd').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getPaperList(req);    
    res.json(data);
  }      
}))
router.get('/detail', [
  check('paper_id').not().isEmpty(),
  check('type').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getPaperDetail(req);    
    res.json(data);
  }      
}))
router.put('/detail/:id', [
  check('id').not().isEmpty(),
  check('type').not().isEmpty(),
  check('approval_cd').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updatePaper(req);    
    res.json(data);
  }      
}))


module.exports = router;   

