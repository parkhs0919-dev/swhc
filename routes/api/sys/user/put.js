const updateUserConfirmed = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.AdUser.updateUserConfirmed(req.body, req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const updateUserInfo = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.AdUser.updateUserInfo(req.body, req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const updateUserPasswordInit = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.user_auth_pw="000000";
            const data = await Api.AdUser.updateUserPasswordInit(req.body, req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const updateUserDeviceInit = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysUser.updateUserDeviceInit(req.body, req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    updateUserConfirmed,
    updateUserInfo,
    updateUserPasswordInit,
    updateUserDeviceInit
};
