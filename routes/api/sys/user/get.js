
const getUserList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysUser.getUserList(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserInfoList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysUser.getUserInfoList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserInfoDetail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysUser.getUserInfoDetail(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserDetail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysUser.getUserDetail(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const getUserDetailTempLog = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            const data = await Api.SysUser.getUserDetailTempLog(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserConfirmedList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.SysUser.getUserConfirmedList(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserConfirmedDetail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.SysUser.getUserConfirmedDetail(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserConfirmedDetailExcel = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.SysUser.getUserConfirmedDetailExcel(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getHealthLogGps = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            const data = await Api.AdHealthLog.getHealthLogGps(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserSearchSelectBox = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            req.query.organization_id=req.access_admin.organization_id;
            req.query.organization_admin_id=req.access_admin.organization_admin_id;
            req.query.admin_grade=req.access_admin.admin_grade;
            const data = await Api.SysUser.getUserSearchSelectBox(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getUserDuplicateCheck = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            const data = await Api.SysUser.getUserDuplicateCheck(req.query); 
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const getUserDetailTempLogExcel = (req) => {
    return new Promise(async(resolve, reject) => {
        try {    
            const data = await Api.SysUser.getUserDetailTempLogExcel(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    getUserList,
    getUserInfoList,
    getUserInfoDetail,
    getUserDetail,
    getUserDetailTempLog,
    getUserConfirmedList,
    getUserConfirmedDetail,
    getUserConfirmedDetailExcel,
    getHealthLogGps,
    getUserSearchSelectBox,
    getUserDuplicateCheck,
    getUserDetailTempLogExcel
};
