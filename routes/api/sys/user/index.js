const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const Excel = require('exceljs');
const setting = CONFIG.setting;
const del = require('./del');
const get = require('./get');
const put = require('./put');

//사용자 조회
router.get('/', [
  check('organization_id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserList(req);    
    req.json(data);
  }      
}))
//사용자 상세 조회
router.get('/detail', [
  check('user_id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserDetail(req);    
    req.json(data);
  }      
}))
//사용자 상세 시간별 온도 조회
router.get('/detail/log', [
  check('user_id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserDetailTempLog(req);    
     req.json(data);
  }      
}))
router.put('/detail/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserConfirmed(req);
    req.json(data);
  }
}))  

router.get('/info', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserInfoList(req);
    req.json(data);
  }      
}))
router.get('/info/detail', [
  check('user_id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserInfoDetail(req);    
     req.json(data);
  }      
}))


router.get('/search', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserSearchSelectBox(req);    
     req.json(data);
  }  
}))
router.get('/check-id', [
  check('user_auth_id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserDuplicateCheck(req);    
    req.json(data);
  }      
}))
router.get('/excel', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    try {
      const data = await get.getUserDetailTempLogExcel(req);
      if (data.code == "0000" && data.data.rows && data.data.rows.length > 0) await createExcelData(data.data.rows, res)
      else {
        res.status(400).json({code: '8300'});
      }
    } catch (e) {
      console.log(e)
      res.status(400).json({ code: '8300'});
    }
  }      
}))


router.put('/info/detail/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserInfo(req);
    req.json(data);
  }
}))
router.put('/init/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserPasswordInit(req);
    req.json(data);
  }
}))    
router.put('/device-init/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await put.updateUserDeviceInit(req);
    req.json(data);
  }
}))    

router.delete('/info/:id', [
  check('id').not().isEmpty().isNumeric()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await del.deleteUser(req);
     req.json(data);
  }
}))

module.exports = router;   

async function createExcelData(data,res){
  let fileName = "user_data_list.xlsx"  
  const workbook = new Excel.Workbook();
  const worksheet = workbook.addWorksheet('ExampleSheet');
  worksheet.columns = [
    { header: '시간', key: 'time' },
    { header: '등급', key: 'status' },
    { header: '체온', key: 'temp' }
  ];
  data.forEach((row) => { 
    worksheet.addRow({ time:row.created_dt_str, status: row.grade , temp: row.temperature });
  })
  res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
  workbook.xlsx.write(res)
  .then(() => {
        res.end();
  });
}
