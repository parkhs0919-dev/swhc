const moment = require('moment');

const getMainUserCount = (req) => {
    return new Promise(async(resolve, reject) => {
        try { 
            let currentDate = moment().tz(req.query.timezone).format("YYYY.MM.DD");                   
            const data = await Api.SysMain.getMainUserCount(req.query); 
            data.data.currentDate =currentDate;
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};
const getMainUserCountGroup = (req) => {
    return new Promise(async(resolve, reject) => {
        try {  
            let currentDate = moment().tz(req.query.timezone).format("YYYY.MM.DD");                   
            const data = await Api.SysMain.getMainUserCountGroup(req.query);   
            data.data.currentDate =currentDate;
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    getMainUserCount,
    getMainUserCountGroup
};
