const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const Excel = require('exceljs');
const setting = CONFIG.setting;
const get = require('./get');
const del = require('./del');


router.get('/search', [
  check('search_name').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getUserDeviceSearch(req);    
     req.json(data);
  }  
}))
 
router.delete('/:id', [
  check('id').not().isEmpty(),
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' }); 
  } else {
    const data = await del.deleteUserDevice(req);
    req.json(data);
  }
}))    

module.exports = router;   
