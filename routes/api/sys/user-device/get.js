
const getUserDeviceSearch = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysUserDevice.getUserDeviceSearch(req.query);  
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    getUserDeviceSearch
};
