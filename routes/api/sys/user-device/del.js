const deleteUserDevice = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.user_device_sn ='';
            const data = await Api.SysUserDevice.deleteUserDevice(req.body, req.params.id);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    deleteUserDevice,
};
