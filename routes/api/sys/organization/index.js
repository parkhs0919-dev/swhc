const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { check, validationResult } = require('express-validator');
const get = require('./get');
const del = require('./del');

// 단체 조회
router.get('/', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getOrganizationList(req);    
    res.json(data);
  }      
}))
// 단체 selectbox 조회
router.get('/option', [
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await get.getOrganizationAll(req);    
    res.json(data);
  }      
}))
// 단체 삭제
router.delete('/:id', [
  check('id').not().isEmpty()
], asyncHandler(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ code: '9000' });
  } else {
    const data = await del.deleteOrganization(req);    
    res.json(data);
  }      
}))

module.exports = router;   

