
const getOrganizationAll = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysOrganization.getOrganizationAll(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};  

const getOrganizationList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            const data = await Api.SysOrganization.getOrganizationList(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};  


module.exports={
    getOrganizationAll,
    getOrganizationList
};
