const createNotice = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.body.organization_id = "0";
            req.body.notice_gb=(req.body.notice_gb)? req.body.notice_gb :"sys";
            const data = await Api.SysNotice.createNotice(req.body);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

module.exports={
    createNotice
};
