
const getNoticeList = (req) => {
    return new Promise(async(resolve, reject) => {
        try {         
            req.query.notice_gb=(req.query.notice_gb)? req.query.notice_gb :"sys";
            const data = await Api.SysNotice.getNoticeList(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};

const getNoticeDetail = (req) => {
    return new Promise(async(resolve, reject) => {
        try {      
            const data = await Api.SysNotice.getNoticeDetail(req.query);
            resolve(data);
        } catch (e) {
            console.log(e)
            resolve({code:"9100"});
        }   
    });
};


module.exports={
    getNoticeList,
    getNoticeDetail
};
