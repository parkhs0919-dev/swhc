const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');

router.get('/', (req, res, next) => {  
    res.render('sys-render',{langs:res.locals.langs,session:res.locals.session,mode:process.env.NODE_ENV});
    
}); 


router.get('/*', (req, res, next) => {   
      res.render('sys-render',{langs:res.locals.langs,session:res.locals.session,mode:process.env.NODE_ENV});
}); 


module.exports = router;   
         