const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
// let locales_ko={
//     codes: Metadata.ko.codes,
//     langs: Metadata.ko.langs,   
// }
// let locales_en={
//     codes: Metadata.en.codes,
//     langs: Metadata.en.langs, 
// }
// router.get('/', (req, res, next) => {   
//     let lang = req.cookies['lang'];
//     if (lang && lang == "en") {
//         res.locals.metadata = locales_en;
//         res.redirect('./login/en');
//     } else {
//         res.locals.metadata = locales_ko;
//         res.cookie('lang', 'ko');   
//         res.redirect('./login/ko');
//     }  
// }); 

// router.get('/ko', function (req, res, next) {
//     res.locals.metadata = locales_ko;
//     res.render('login', {
//         view_path: 'login',
//         result: {}
//     });
// });
// router.get('/en', function (req, res, next) {
//     res.locals.metadata = locales_en;
//     res.render('login', {
//         view_path: 'login',
//         result: {}
//     });
// });
// router.get('/out', function (req, res, next) {
//     res.clearCookie('admin_access_token');
//     res.status(200).send({code:'0000'}); 
// }); 

// router.get('/ko/organization-create', function (req, res, next) {
//     res.locals.metadata = locales_ko;
//     res.render('organization-create', {
//         view_path: 'organization-create',
//         result: {}
//     });
// });  
// router.get('/ko/login-find-id', function (req, res, next) {
//     res.locals.metadata = locales_ko;
//     res.render('login-find-id', {
//         view_path: 'login-find-id',
//         result: {}
//     });
// });    
// router.get('/ko/login-find-pw', function (req, res, next) {
//     res.locals.metadata = locales_ko;
//     res.render('login-find-pw', {
//         view_path: 'login-find-pw',
//         result: {}
//     });
// });  
// router.get('/ko/organization-complete', function(req, res, next) {
//     res.locals.metadata = locales_ko ;
//     res.render('organization-complete', {
//         view_path: 'organization-complete',
//         result:{
//         organization_nm : (req.query.organization_nm)? req.query.organization_nm : "",
//         organization_auth_id : (req.query.organization_auth_id)? req.query.organization_auth_id : "",
//         admin_auth_id : (req.query.admin_auth_id)? req.query.admin_auth_id : "" 
//         },
//     });
// }); 

// router.get('/en/organization-create', function (req, res, next) {
//     res.locals.metadata = locales_en;
//     res.render('organization-create', {
//         view_path: 'organization-create',
//         result: {}
//     });
// });  
 
// router.get('/en/login-find-id', function (req, res, next) {
//     res.locals.metadata = locales_ko;
//     res.render('login-find-id', {
//         view_path: 'login-find-id',
//         result: {}
//     });
// });    
// router.get('/en/login-find-pw', function (req, res, next) {
//     res.locals.metadata = locales_ko;
//     res.render('login-find-pw', {
//         view_path: 'login-find-pw',
//         result: {}
//     });
// });  
// router.get('/en/organization-complete', function(req, res, next) {
//     res.locals.metadata = locales_en;
//     res.render('organization-complete', {
//         view_path: 'organization-complete',
//         result:{
//         organization_nm : (req.query.organization_nm)? req.query.organization_nm : "",
//         organization_auth_id : (req.query.organization_auth_id)? req.query.organization_auth_id : "",
//         admin_auth_id : (req.query.admin_auth_id)? req.query.admin_auth_id : "" 
//         },
//     });
// }); 


router.get('/', (req, res, next) => {  
    let lang = req.cookies['lang'];
    if (lang && lang == "en") {
        res.cookie('lang', 'en');  
        res.locals.langs = 'en';
    } else {
        res.cookie('lang', 'ko');  
        res.locals.langs = 'ko';  
    }      
    res.render('admin-render',{langs:res.locals.langs,session:res.locals.session,mode:process.env.NODE_ENV});    
}); 


router.get('/*', (req, res, next) => {   
    let lang = req.cookies['lang'];
    if(lang && lang=="en"){
      res.cookie('lang', 'en');
      res.locals.langs = 'en';
    }else{
      res.cookie('lang', 'ko');   
      res.locals.langs = 'ko';      
    }
      res.render('admin-render',{langs:res.locals.langs,session:res.locals.session,mode:process.env.NODE_ENV});
}); 


module.exports = router;   
         