const fs = require('fs');
const path = require('path');
const gulp = require('gulp');
const { series, src, dest} = require('gulp');
const inject = require('gulp-inject');
const rename = require('gulp-rename');
const webpackStream = require('webpack-stream');
const injectString = require('gulp-inject-string');
const args = require('get-gulp-args')();

const ENV_DEVELOPMENT = 'development';
const now = new Date().getTime();

if (!args.env) {
    args.env = process.env.NODE_ENV = ENV_DEVELOPMENT;
} else {
    process.env.NODE_ENV = args.env;
}
const distPath = './dist';
if (!fs.existsSync(distPath)) fs.mkdirSync(distPath);

const modulePages = fs.readdirSync(path.join(__dirname, './client/pages/'));

modulePages.forEach((page) => {
    if (!fs.existsSync(`${distPath}/${page}.js`)) fs.writeFileSync(`${distPath}/${page}.js`, '');
    if (!fs.existsSync(`${distPath}/${page}.css`)) fs.writeFileSync(`${distPath}/${page}.css`, '');
});

console.log("NODE_ENV=", process.env.NODE_ENV);
console.log("v=", now);

// gulp 4.0.2
let fn_inject={};        
let fn_inject_string={};  
let arr_inject=[]; 
let arr_inject_string=[];  

modulePages.forEach((page, index) => {
    fn_inject[page] =function(){
        console.log(`inject-${page}`);
        const path = './server/views/';
        const array = [`./dist/${page}.js`, `./dist/${page}.css`];
        return src(path + `${page}-origin.ejs`,{allowEmpty: true})
            .pipe(inject(gulp.src(array, {read: false})))
            .pipe(rename(`${page}-render.ejs`))
            .pipe(gulp.dest(path));     
    }     
    arr_inject[index]=fn_inject[page];
});    

modulePages.forEach((page, index) => {
    fn_inject_string[page] =function(){
        console.log(`inject-string-${page}`);
        const path = `./server/views/${page}-render.ejs`;
        return src(path, {base: './',allowEmpty: true})     
            .pipe(injectString.replace(`./dist/${page}.js`, `"/dist/${page}.js?v=${now}`))
            .pipe(injectString.replace(`./dist/${page}.css`, `"/dist/${page}.css?v=${now}`))
            .pipe(dest('./'));        
    }   
    arr_inject_string[index]=fn_inject_string[page];
});        

function webpack_watch() {
    console.log('webpack-watch');
    let webpackConfig = require('./webpack.config');

    if (process.env.NODE_ENV === ENV_DEVELOPMENT) {
        webpackConfig.watch = true;
    }

    webpackConfig.entry = {};
  
    modulePages.forEach(page => {
        webpackConfig.entry[page] = './client/pages/' + page + '/index.js';
    });

    return src('.', {allowEmpty: true})
        .pipe(webpackStream(webpackConfig))
        .pipe(dest('dist'));
}   

exports.build = series(arr_inject,arr_inject_string,webpack_watch)            
       

   